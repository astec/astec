# Change Log

## current version

## version 2.2.4 - 2025-02-27

- Post-correction: property name uniformisation was not propagated properly.

## version 2.2.3 - 2025-02-23

- Enrich drift computation: score upper threshold; early stop at first iteration
- Correct I/O of tissue map fate
- Enrich documentation

## version 2.2.2 - 2025-02-19

- Correction of extrapolation of weight image for drift-based fusion.

## version 2.2.1 

- add a missing __init__.py

## version 2.2.0 - 2025-02-13

- introduce drift estimation within a fused time series, and between corresponding images
  of two fused time series
- fusion: introduce drift-compensated fusion
- fusion: generalize transformation saving for both drift-compensated fusion and
  hierarchical fusion

## version 2.1.11 - 2024-12-09

- I/O: correct conversion procedures (they did not work properly)
- I/O: correct tulip file writing (was not compatible with new properties definition)
- astec_embryoproperties: do not write tulip file anymore

## version 2.1.10

- I/O: conversion procedures, to avoir writing numpy types in output files
  for backward compatibility with older versions of numpy/python
- fusion: transformations are saved in a separate directory 
- fusion: corrections to fuse with a subset of views

## version 2.1.9

- manualcorrection: corrects the correction files; allows for restarting
- enrich morphonet file writing: enables more properties to be written out

## version 2.1.8

- manual correction: correct potential overflow problem.
  In python 3.9, we have (np.uint16) * (int) -> (np.int64) while in 
  python 3.11 (np.uint16) * (int) -> (np.uint16).

## version 2.1.7

- manual correction: correct the propagation of newly created divisions

## version 2.1.6

- inrimage IO: bytes/string management

## version 2.1.5

- fusion: if changing images to be fused, keep the same output geometry (ie from the same camera)

## version 2.1.4

- fusion: correct bad behavior for direct fusion when the 'first' image is not
  Left Camera from stack #0

## version 2.1.3

- allows to have new labels when dividing cell in astec_manualcorrection
- update documentation

## version 2.1.2

- improve name comparison behavior
- correct bad behavior of astec_astec when restarting the computation

## version 2.1.1

- documentation updating
- correct bad behavior of ReconstructionParameters.is_normalization_equal() when intensity_transformation is None

## version 2.1.0

- documentation updating
- pursued code splitting into astec/ascidian repositories
- documentation splitting into astec/ascidian

## version 2.0.0

- moved ascidian algorithms/binaries to a dedicated git repository
- replaced pkg_resources by importlib.metadata
- added test infrastructure
- refactor setup.py (adding setup.cfg)

## version 1.7.2

- review of conda env files (*.yaml) and build
- use bultin which command instead of external process to launch astec binaries
- removed obsolete python-3.7 builds, added python-3.10 and python-3.11

## version 1.7.1

- Write embryo as a graph
- Computation of symmetric cells
- Allows to extract any XZ-section(s) at the fusion stage.

## version 1.7.0

- Fusion of one single time point: allows longer suffixes than recognized image extensions.
- Pre-processed images
  are no more stored into ``.../SEG/SEG_XXX/RECONSTRUCTION/`` but in their
  own directories ``.../REC-MEMBRANE/REC_XXX/`` and/or ``.../REC-SEED/REC_XXX/``
  and/or ``.../REC-MORPHOSNAKE/REC_XXX/``. They can be transformed by the
  motion compensation stage.
- User-provided outer contour images (to be stored in ``.../CONTOUR_/CONTOUR_XXX/``)
  can be used in image pre-processing.
- Properties computation computes cell lineage
  when it is not available.

## version 1.6.2

- documentation update

## version 1.6.1

- fusion: morphological opening of the MIP image to crop the acquisition data

## version 1.6.0

- write/update documentation for astec_atlas_init_naming and astec_atlas_naming
- add assessment measure for initial naming

## version 1.5.3

- astec_atlas_init_naming: enrich outputs
- astec_fusion: allows to fuse with only a subset of acquisition images

## version 1.5.2

- add utility to name one time point of an unnamed embryo from
  a set of named atlases (astec_atlas_init_naming)

## version 1.5.1

- add missing package in pkg/recipe/meta.yaml

## version 1.5.0

- allows .h5.gz images as camera acquisition images (used in fusion step)
- uses threads to recompress the images after reading

## version 1.4.0

- get the package version number from conda, and write it in the history log

## version 1.3.0

- allow cropping along the Z direction for fusion

## version 1.2.0

- will try from now on to keep trace on the changes in this file and
to maintain a coherent version numbering
- enrich property diagnosis and comparison
