import unittest

import inspect
import os

import astec.io.image as as_img

class TestIOImage(unittest.TestCase):
    '''
    Test the io.image class
    '''

    # before the tests
    def setUp(self):
        pass

    # after the tests
    def tearDown(self):
        pass

    def test_01_io_image_imread(self):
        '''
        test imread()
        '''

        filename = os.path.join(
           os.path.dirname(
               os.path.abspath(
                   inspect.getfile(inspect.currentframe()))),
            "test_data",
        "img_01.inr")

        #
        # DOES NOT WORK !!!
        # at lines 51, 55 of astec/io/format/inrimage.pytest
        # open(filename, 'rb')
        # POSSIBLE FIX
        # line 67, header += str(f.read(256))
        #

        #image = as_img.imread(filename)

        #self.assertEqual(image.vdim, 1)

        pass


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
