
.. _cli-parameters-astec-postcorrection:

``astec_postcorrection`` parameters
-----------------------------------

These parameters are prefixed by ``postcorrection_``.

* ``volume_minimal_value``
  branch ending with leaf cell below this value are candidate
  for deletion. Expressed in voxel unit.
* ``lifespan_minimal_value``
* ``test_early_division``
* ``test_volume_correlation``
* ``correlation_threshold``
* ``test_postponing_division``
* ``postponing_correlation_threshold``
* ``postponing_minimal_length``
* ``postponing_window_length``
* ``lineage_diagnosis``
  performs a kind of diagnosis on the lineage before and after
  the post-correction.
