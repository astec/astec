
.. _cli-parameters-astec-fusion:

``astec_fusion`` parameters
---------------------------

* ``acquisition_orientation``: ``'left'`` or ``'right'``.
  Gives the rotation (with respect to the Y axis) of the left camera
  frame of stack #0 to be aligned with the the left camera
  frame of stack #1.

  * ``'right'``: +90 degrees
  * ``'left'``: -90 degrees

  See also ``'acquisition_mirrors'``.
  Since it should depend on the apparatus, this parameter should not
  change for all acquisitions performed by the same microscope.
  See section :ref:`cli-fusion-important-parameters`.
* ``acquisition_mirrors``: :python:`True` or :python:`False`.
  If :python:`False`, the right camera images are mirrored to make them similar
  to left camera images.
  To determine the configuration (``acquisition_orientation``, ``acquisition_mirrors``)
  (ie (``'left'``, :python:`False`), (``'left'``, :python:`True`), (``'right'``, :python:`False`), or (``'right'``, :python:`True`)),
  it is advised to perform the fusion for only one time point (by setting
  ``'begin'`` and ``'end'`` at the same value) with a large ``'target_resolution'``,
  for each of those 4 choices, ``xzsection_extraction`` being set to :python:`True` (or some adequate value).

  See also ``'acquisition_orientation'``.
  Since it should depend on the apparatus, this parameter should not
  change for all acquisitions performed by the same microscope.
  See section :ref:`cli-fusion-important-parameters`.
* ``acquisition_resolution``:
  voxel size of acquired images
  e.g.

  .. code-block:: python

    raw_resolution = (.195, .195, 1.)

  see section :ref:`cli-fusion-important-parameters`.

* ``acquisition_stack0_leftcamera_z_stacking``: ``'direct'`` or ``'inverse'``.
  Defines where are the high contrasted XZ-sections of the *left* camera
  image of stack0.

  * ``'direct'``: small z are well contrasted (close to the camera), while
    large z are fuzzy. It is useful for direction-dependent weighting
    schemes.
  * ``'inverse'``: the other way around.

  Changing 'direct' to 'inverse' (or the other way) implies to change
  ``acquisition_orientation`` as well.
  Setting ``xzsection_extraction`` to :python:`True`  (or some adequate value)
  allows to verify whether
  ``acquisition_stack0_leftcamera_z_stacking`` is set to the right value.

* ``acquisition_stack1_leftcamera_z_stacking``:
  same as ``acquisition_stack0_leftcamera_z_stacking`` for stack #1.

* ``acquisition_leftcamera_z_stacking``:
  allows to set both ``acquisition_stack0_leftcamera_z_stacking``
  and ``acquisition_stack1_leftcamera_z_stacking``.
  See section :ref:`cli-fusion-important-parameters`.

* ``acquisition_slit_line_correction``:
  :python:`True` or :python:`False`.
  Slit lines are Y lines that appear brighter or darker in the acquisition,
  which may cause artifacts in the reconstructed (ie fused) image, which, in
  turn, may impair further segmentation steps.
  See section :ref:`cli-fusion-overview`.
* ``target_resolution``:
  Voxel size of the reconstructed image after fusion of the four views.
  e.g.

  .. code-block:: python

    target_resolution = 0.3

  See section :ref:`cli-fusion-output-data`.

* ``fusion_strategy``:
  Possible values are 'direct-fusion' and 'hierarchical-fusion'.

  * ``'direct-fusion'``: each acquisition is linearly
    co-registered with the first acquisition (stack #0, left camera).
    The transformation is computed in two steps:

    * with parameters prefixed by ``fusion_preregistration_``
      (by default deactivated).
    * with parameters prefixed by ``fusion_registration_``
      (by default ``'affine'`` transformation).

    Then weights and images are transformed thanks to the
    computed transformations. Finally a weighted linear combination
    gives the result.

  * ``'hierarchical-fusion'``: from the couple
    (left camera, right camera), each stack is reconstructed
    following the same scheme than the direct fusion but with only 2 images.
    (thus with the registration parameters prefixed by
    ``fusion_preregistration_`` and ``fusion_registration_``).
    Then stack #1 is co-registered with stack #0 in two steps:

    * with parameters prefixed by ``fusion_stack_preregistration_``
      (by default ``'affine'`` transformation).
    * with parameters prefixed by ``fusion_stack_registration_``
      (by default ``'vectorfield'`` transformation).

    Images and weights associated with stack#1 are then transformed.
    Finally a weighted linear combination gives the result.

  See section :ref:`cli-fusion-image-registration`.

* ``acquisition_cropping``:
  :python:`True` or :python:`False`. If set to :python:`True`,
  the acquisitions stacks are cropped before fusion along the X and Y directions.
  Maximum Intensity Projection (MIP) images are automatically thresholded
  (Otsu algorithm) to determine the bounding box of the object of interest.
  Margins are then added to the bounding box.
  See section :ref:`cli-fusion-raw-data-cropping`.

* ``acquisition_z_cropping``:
  :python:`True` or :python:`False`. If set to :python:`True`,
  the acquisitions stacks are cropped before fusion along the Z direction.
  Margins are then added to the bounding box.

* ``acquisition_cropping_margin_x_0``:
  Added margin of the bounding box computed for the cropping of the raw
  acquisition image in 'left' X direction.
* ``acquisition_cropping_margin_x_1``:
  Added margin of the bounding box computed for the cropping of the raw
  acquisition image in 'right' X direction.
* ``acquisition_cropping_margin_y_0``:
  Added margin of the bounding box computed for the cropping of the raw
  acquisition image in 'left' Y direction.
* ``acquisition_cropping_margin_y_1``:
  Added margin of the bounding box computed for the cropping of the raw
  acquisition image in 'right' Y direction.
* ``acquisition_cropping_margin_z_0``:
  Added margin of the bounding box computed for the cropping of the raw
  acquisition image in 'left' Z direction.
* ``acquisition_cropping_margin_z_1``:
  Added margin of the bounding box computed for the cropping of the raw
  acquisition image in 'right' Z direction.

* ``acquisition_cropping_margin_x``:
  allows to set both ``acquisition_cropping_margin_x_0`` and
  ``acquisition_cropping_margin_x_1``
* ``acquisition_cropping_margin_y``:
  allows to set both ``acquisition_cropping_margin_y_0`` and
  ``acquisition_cropping_margin_y_1``
* ``acquisition_cropping_margin_z``:
  allows to set both ``acquisition_cropping_margin_z_0`` and
  ``acquisition_cropping_margin_z_1``
* ``acquisition_cropping_margin``:
  allows to set the six margin variables.

* ``acquisition_cropping_opening``:
  Active when ``acquisition_cropping`` is set to :python:`True`.
  Perform an opening (mathematical morphology) on the thresholded MIP image.
  It may be useful if some structures are connected to the desired one
  (0 means no opening, else specify the structuring element size).

* ``raw_crop`` same as ``acquisition_cropping``

* Registration parameters
  (see section :ref:`cli-parameters-registration`) prefixed
  by ``fusion_preregistration_``.
  First registration step of the co-registration of each of the four acquisitions
  for the ``'direct-fusion'`` scheme (see ``fusion_strategy``), or of the
  left and right camera acquisitions to reconstruct a stack for the
  ``'hierarchical-fusion'`` scheme.
  By default deactivated.
* Registration parameters
  (see section :ref:`cli-parameters-registration`) prefixed
  by ``fusion_registration_``.
  Second registration step of the co-registration of each of the four acquisitions
  for the ``'direct-fusion'`` scheme (see ``fusion_strategy``), or of the
  left and right camera acquisitions to reconstruct a stack for the
  ``'hierarchical-fusion'`` scheme.
  By default an affine transformation is sought.
* Registration parameters
  (see section :ref:`cli-parameters-registration`) prefixed
  by ``fusion_stack_preregistration_``.
  First registration step of the co-registration of the two stacks
  reconstructed with left and right cameras for the
  ``'hierarchical-fusion'`` scheme (see ``fusion_strategy``).
  By default an affine transformation is sought.
* Registration parameters
  (see section :ref:`cli-parameters-registration`) prefixed
  by ``fusion_stack_registration_``
  Second registration step of the co-registration of the two stacks
  reconstructed with left and right cameras for the
  ``'hierarchical-fusion'`` scheme (see ``fusion_strategy``).
  By default an non-linear transformation is sought.

* ``xzsection_extraction``:
  :python:`False`, :python:`True`, an integer or a list of integers.

    * :python:`True`: extracts the XZ-section at the middle of the volume
    * one integer: extracts the XZ-section for the given Y value
    * a list of integers: extracts the XZ-sections for all given Y values

    .. note:: Setting the Y value(s) of the extracted XY sections
       allows to choose a convenient section for the fusion assessment.

  If not :python:`False`, XZ-sections XZ-sections of the co-registered acquisitions,
  as well as the weighting function images, are stored in the directory
  ``<PATH_EMBRYO>/FUSE/FUSE_<EXP_FUSE>/XZSECTION_<xxxx>`` where ``<xxxx>`` is
  the time point index. It provides a direct and efficient means to check
  whether the parameters ``acquisition_orientation``, ``acquisition_mirrors``,
  or ``acquisition_stack[0,1]_leftcamera_z_stacking`` are correctly set.
  If the value is an integer or a list of integers, it indicates the indices
  of XZ-sections to be extracted
  See section :ref:`cli-fusion-nonlinear-registration`

* ``keep_transformation``:
  :python:`True` or :python:`False`.
  If :python:`True`, the computed transformations are kept in directories
  ``<PATH_EMBRYO>/FUSE/FUSE_<EXP_FUSE>/TRSF_<xxxx>`` where ``<xxxx>`` is
  the time point index.

* ``fusion_cropping``:
  :python:`True` or :python:`False`.
  If :python:`True`, the fusion image is cropped along the X and Y directions.
  Maximum Intensity Projection (MIP) images are automatically thresholded
  (Otsu algorithm) to determine the bounding box of the object of interest.
  Margins are then added to the bounding box.
  See section :ref:`cli-fusion-fused-data-cropping`
* ``fusion_z_cropping``:
  :python:`True` or :python:`False`.
  If :python:`True`, the fusion image is also cropped along the Z direction.
  Margins are then added to the bounding box.

* ``fusion_cropping_margin_x_0``:
  Added margin of the bounding box computed for the cropping of the fusion
  image in 'left' X direction.
* ``fusion_cropping_margin_x_1``
  Added margin of the bounding box computed for the cropping of the fusion
  image in 'right' X direction.
* ``fusion_cropping_margin_y_0``
  Added margin of the bounding box computed for the cropping of the fusion
  image in 'left' Y direction.
* ``fusion_cropping_margin_y_1``
  Added margin of the bounding box computed for the cropping of the fusion
  image in 'right' Y direction.
* ``fusion_cropping_margin_z_0``
  Added margin of the bounding box computed for the cropping of the fusion
  image in 'left' Z direction.
* ``fusion_cropping_margin_z_1``
  Added margin of the bounding box computed for the cropping of the fusion
  image in 'right' Z direction.

* ``fusion_cropping_margin_x``:
  allows to set both ``fusion_cropping_margin_x_0``
  and ``fusion_cropping_margin_x_1``
* ``fusion_cropping_margin_y``:
  allows to set both ``fusion_cropping_margin_y_0``
  and ``fusion_cropping_margin_y_1``
* ``fusion_cropping_margin_z``:
  allows to set both ``fusion_cropping_margin_z_0``
  and ``fusion_cropping_margin_z_1``
* ``fusion_cropping_margin``:
  allows to set the six margin variables.

* ``fusion_weighting``:
  Possible values are 'uniform', 'ramp', 'corner', or 'guignard'.
  Allows to set the weighting function used to compute
  the weighted linear combination of the 4 co-registered acquisitions
  (for all channels to be processed).

  * ``'uniform'``: uniform (or constant) weighting, it comes
    to the average of the resampled co-registered stacks
  * ``'ramp'``: the weights are linearly increasing or
    decreasing along the Z axis
  * ``'corner'``: the weights are constant in a corner portion
    of the stack, defined by two diagonals in the XZ-section
  * ``'guignard'``: original historical weighting function,
    described in Leo Guignard's Phd thesis :cite:p:`guignard:tel-01278725`,
    that puts more weight to sections close to the camera and take
    also account the traversed material.

  Setting the variable ``xzsection_extraction`` to :python:`True`
  (or some adequate value) allows to see
  the weights used for the extracted XZ sections.
  See section :ref:`cli-fusion-nonlinear-registration`.

* ``fusion_weighting_channel_1``:
  set the weighting function for the weighted sum of the registered
  acquisition stacks for the first channel (in case of multi-channel
  acquisition).
* ``fusion_weighting_channel_2``:
  set the weighting function for the weighted sum of the registered
  acquisition stacks for the second channel (in case of multi-channel
  acquisition).
* ``fusion_weighting_channel_3``:
  set the weighting function for the weighted sum of the registered
  acquisition stacks for the third channel (in case of multi-channel
  acquisition).


The following parameters are kept for backward compatibility:

* ``fusion_crop``
  same as ``fusion_cropping``
* ``fusion_margin_x_0``
  same as ``fusion_cropping_margin_x_0``
* ``fusion_margin_x_1``
  same as ``fusion_cropping_margin_x_1``
* ``fusion_margin_y_0``
  same as ``fusion_cropping_margin_y_0``
* ``fusion_margin_y_1``
  same as ``fusion_cropping_margin_y_1``
* ``fusion_xzsection_extraction``
  same as ``xzsection_extraction``
* ``raw_crop``
  same as ``acquisition_cropping``
* ``raw_margin_x_0``
  same as ``acquisition_cropping_margin_x_0``
* ``raw_margin_x_1``
  same as ``acquisition_cropping_margin_x_1``
* ``raw_margin_y_0``
  same as ``acquisition_cropping_margin_y_0``
* ``raw_margin_y_1``
  same as ``acquisition_cropping_margin_y_1``
* ``raw_mirrors``
  same as ``acquisition_mirrors``
* ``raw_ori``
  same as ``acquisition_orientation``
* ``raw_resolution``
  same as ``acquisition_resolution``

* ``begin``: first time point of the series to be processed.
  See section :ref:`cli-fusion-important-parameters`.
* ``delta``
* ``end``: last time point of the series to be processed.
  When testing or tuning parameters, it is advised not to
  processed the whole series, but only one or a few time
  points.
  See section :ref:`cli-fusion-important-parameters`.
* ``fusion_weighting``
* ``fusion_weighting_channel_1``
* ``fusion_weighting_channel_2``
* ``fusion_weighting_channel_3``
* ``raw_delay``
