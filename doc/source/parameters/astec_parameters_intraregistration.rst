
.. _cli-parameters-astec-intraregistration:

``astec_intraregistration`` parameters
--------------------------------------

These parameters are prefixed by ``intra_registration_``.

* Registration parameters
  (see section :ref:`cli-fusion-image-registration`)

* ``reference_index``:
  defines the still image after transformation compositions it will
  only translated, except if ``reference_transformation_file``
  or ``reference_transformation_angles`` are set.
  See section :ref:`cli-intraregistration-template`.
* ``reference_transformation_file``:
  resampling transformation to be applied to the reference image
  (and to the whole serie) after transformation compositions.
  See section :ref:`cli-intraregistration-template`.
* ``reference_transformation_angles``:
  list of rotations wrt the X, Y,or Z axis that defines the resampling
  transformation.

  .. code-block:: python

     reference_transformation_angles = 'X 30 Y 50'

  represents a rotation of 30 degree around the X axis followed by a
  rotation of 50 degrees around the Y axis.

  Beware: rotation composition depends on the order, so
  ``'X 30 Y 50'`` is not equivalent to ``'Y 50 X 30'``.

* ``template_type``
  Possible values are 'FUSION', 'SEGMENTATION', or 'POST-SEGMENTATION'
  The template is built so that the useful information of
  all resampled images fits into it. Useful information
  can be issued from either the fused sequence, the segmentation
  sequence or the post-segmentation sequence.
* ``template_threshold``
  Giving a threshold with the ``template_type``, only points
  above the threshold are considered to be included in the
  template after resampling, this allows to reduce the template.
  According the background value is either 0 or 1 in both the
  segmentation and the post-segmentation sequences, setting
  this threshold to 2 for these sequences allows to keep the
  entire embryo in the resampled/reconstructed sequence.
* ``margin``
  In addition, a margin can be given for a more comfortable
  visualization. By default, it is 0 when only fusion
  images are used, and 10 if either segmentation or
  post-segmentation images are also used.
* ``resolution``
  gives the resulting (isotropic) voxel size (as the
  'target_resolution' gives the voxel size of the fused images).
  However, for visualization purposes, it may be indicated to
  have a larger voxel size (hence the 0.6 instead of 0.3)
* ``rebuild_template``:
  :python:`True` or :python:`False`.
  If set to :python:`True`, force to recompute the template as well as the
  transformations from existing co-registrations (that are not
  re-computed). It is useful when a first intra-registration has been
  done with only the fusion images: a second intra-registration with
  the segmentation images as template can be done without recomputing
  the co-registrations.
* ``sigma_segmentation_images``
  Sigma to smooth (post-)segmentation images when resampling.
* ``resample_fusion_images``
  Possible values are :python:`True` or :python:`False`
* ``resample_reconstruction_images``
  Possible values are :python:`True` or :python:`False`
* ``resample_segmentation_images``
  Possible values are :python:`True` or :python:`False`
* ``resample_post_segmentation_images``
  Possible values are :python:`True` or :python:`False`
* ``movie_fusion_images``
  Possible values are :python:`True` or :python:`False`.
  To build 2D+t movies from the resampled fusion images.
* ``movie_segmentation_images``
  Possible values are :python:`True` or :python:`False`.
  To build 2D+t movies from the resampled segmentation images.
* ``movie_post_segmentation_images``
  Possible values are :python:`True` or :python:`False`.
  To build 2D+t movies from the resampled post-segmentation images.
* ``xy_movie_fusion_images``
  List of XY-sections used to build the 2D+t movies
  eg ``xy_movie_fusion_images = [100, 200]``
* ``xz_movie_fusion_images``
  List of XZ-sections used to build the 2D+t movies
  eg ``xz_movie_fusion_images = [100, 200]``
* ``yz_movie_fusion_images``
  List of YZ-sections used to build the 2D+t movies
  eg ``yz_movie_fusion_images = [100, 200]``
* ``xy_movie_segmentation_images``
* ``xz_movie_segmentation_images``
* ``yz_movie_segmentation_images``
* ``xy_movie_post_segmentation_images``
* ``xz_movie_post_segmentation_images``
* ``yz_movie_post_segmentation_images``
* ``maximum_fusion_images``
  Possible values are :python:`True` or :python:`False`.
  Build a maximum image from the resampled series.
  It may be useful to define a cropping valid area for the whole sequence.
* ``maximum_segmentation_images``
* ``maximum_post_segmentation_images``
