
.. _cli-parameters-astec-drift:

``astec_drift`` parameters
---------------------------

* Intra-registration parameters
  (see section :numref:`%s <cli-parameters-astec-intraregistration>`) 

* ``dilation_radius``:
  defines the radius of the structuring element for dilation
  Dilation will enlarge the membrane of the 'i+1' image, thus
  allow to compute the score wrt of the 'i' image defined as 
  the enlarged membrane minus the 'i' image.
  Default value is 5.

* ``intensity_lower_quantile``:
  defines the upper quantile of the fusion image to be used where computing
  the scores. It allows to select the hyper-intense areas (ie membranes).
  Default value is 0.1 (ie 10 %).

* ``intensity_upper_quantile``:
  defines the upper quantile of the fusion image to be used where *not* computing
  the scores. It allows to remove the hyper-hyper-intense areas (ie fragments)
  that are  not membranes.
  Default value is 0.002 (ie 0.2 %).

* ``only_initialisation``:
  If ``True``, do not compute the corrections after the first intra-series
  registration. It allows to check whether the drift computation is required,
  or whether the fusion parameters have to be adjusted, or whether the
  drift parameters (eg ``score_threshold``) have to be adjusted.

* ``score_threshold``:
  defines a threshold on the 'co-registration' score. It allows first to
  select couples of images to be co-registered, and second to stop
  the co-registration procedure. If None, the threshold is automatically
  computed as the Otsu threshold from the scores.

* ``figurefile_suffix``:
  suffix used to named the above python files as well as the generated figures.

* ``rotation_sphere_radius``:
  Sphere radius to build the support of the rotation vector distribution.
  Any rotation is represented by a point inside a sphere of radius :math:`\pi`.
  Discretizing the sphere allows then to discretize the rotation space.
  The higher the radius, the more points inside the sphere, ie the more rotations
  to be tested (at a higher computational cost). Below are correspondances between
  the value of a radius (used to discretize a sphere in :math:`\mathbb{Z}^3`) and
  the number of (rotation) vectors.

  * radius = 3:   123 vectors, angle increment is 51.43 degrees
  * radius = 3.2: 147 vectors, angle increment is 49.15 degrees
  * radius = 3.4: 171 vectors, angle increment is 47.16 degrees
  * radius = 3.5: 179 vectors, angle increment is 45.41 degrees
  * radius = 3.7: 203 vectors, angle increment is 43.84 degrees
  * radius = 4.0: 257 vectors, angle increment is 40.00 degrees
  * radius = 4.2: 305 vectors, angle increment is 38.93 degrees
  * radius = 4.3: 341 vectors, angle increment is 37.95 degrees
  * radius = 4.4: 365 vectors, angle increment is 37.05 degrees
  * radius = 4.5: 389 vectors, angle increment is 36.20 degrees
  * radius = 4.6: 437 vectors, angle increment is 35.42 degrees
  * radius = 4.7: 461 vectors, angle increment is 34.68 degrees
  * radius = 4.9: 485 vectors, angle increment is 33.34 degrees
  * radius = 5.0: 515 vectors, angle increment is 32.73 degrees
  * radius = 5.1: 587 vectors, angle increment is 32.15 degrees
  * radius = 5.2: 619 vectors, angle increment is 31.60 degrees
  * radius = 5.4: 691 vectors, angle increment is 30.59 degrees
  * radius = 5.5: 739 vectors, angle increment is 30.11 degrees
  * radius = 5.7: 751 vectors, angle increment is 29.24 degrees
  * radius = 5.8: 799 vectors, angle increment is 28.83 degrees
  * radius = 5.9: 847 vectors, angle increment is 28.43 degrees
  * radius = 6.0: 925 vectors, angle increment is 27.69 degrees

  Default value is 4.2.

* ``corrections_to_be_done``:
  acquisition time points to be corrected. Other time points,
  included those above the
  threshold, are ignored. For 'i' in the list, it means that the ith
  image is considered not being correctly registered with the
  (i+1)th one. However, setting the threshold for an earlier
  stop of computations is recommended.

* ``corrections_to_be_added``:
  acquisition time points to be corrected (even if the
  corresponding score is below the threshold), *in addition* to the
  time points above the computed or given threshold. For 'i' in
  the list, it means that the ith image is not correctly registered
  with the (i+1)th one.

* ``corrections_to_be_skipped``:
  acquisition time points *not* to be corrected (even if the
  corresponding score is above the threshold). For 'i' in the 
  list, it means that the ith image is correctly registered with 
  the (i+1)th one.
