
.. _cli-parameters-astec-mars:

``astec_mars`` parameters
-------------------------

These parameters are prefixed by ``mars_``.

* ``first_time_point``:
  first time point to be segmented by the mars method.
  Overrides the value of the ``begin`` variable.
* ``last_time_point``:
  last time point to be segmented by the mars method.
* Watershed parameters
  (see section :ref:`cli-parameters-watershed`)
* Seed edition parameters
  (see section :ref:`cli-parameters-seed-edition`)
* Preprocessing parameters
  (see section :ref:`cli-parameters-preprocessing`)
  prefixed by ``seed_``
* Preprocessing parameters
  (see section :ref:`cli-parameters-preprocessing`)
  prefixed by ``membrane_``
