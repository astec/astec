
.. _cli-parameters-astec-astec:

``astec_astec`` parameters
--------------------------

These parameters are prefixed by ``astec_``.

* Watershed parameters
  (see section :ref:`cli-parameters-watershed`)
* Preprocessing parameters
  (see section :ref:`cli-parameters-preprocessing`)
  prefixed by ``seed_``
* Preprocessing parameters
  (see section :ref:`cli-parameters-preprocessing`)
  prefixed by ``membrane_``
* Preprocessing parameters
  (see section :ref:`cli-parameters-preprocessing`)
  prefixed by ``morphosnake_``
* Morphosnake parameters
  (see section :ref:`cli-parameters-morphosnake`)
* ``propagation_strategy``:

  * ``'seeds_from_previous_segmentation'``
  * ``'seeds_selection_without_correction'``

* ``previous_seg_method``:
  how to build the seeds :math:`S^e_{t-1 \leftarrow t}`
  for the computation of :math:`\tilde{S}_{t}`

  * ``'deform_then_erode'``: :math:`S^{\star}_{t-1}` is transformed
    towards :math:`I_t` frame through :math:`\mathcal{T}_{t-1 \leftarrow t}`,
    and then the cells and the background  are eroded.
  * ``'erode_then_deform'``: historical method. The cells
    and the background of :math:`S^{\star}_{t-1}` are eroded, and
    then transformed
    towards :math:`I_t` frame through :math:`\mathcal{T}_{t-1 \leftarrow t}`.

* ``previous_seg_erosion_cell_iterations``:
  set the cell erosion size for :math:`S^e_{t-1 \leftarrow t}` computation.
* ``previous_seg_erosion_background_iterations``:
  set the background erosion size for :math:`S^e_{t-1 \leftarrow t}`
  computation.
* ``previous_seg_erosion_cell_min_size``:
  size threshold. Cells whose size is below this threshold will
  be discarded seeds in :math:`S^e_{t-1 \leftarrow t}`

* ``watershed_seed_hmin_min_value``:
  set the :math:`h_{min}` value of the :math:`[h_{min}, h_{max}]` interval.
* ``watershed_seed_hmin_max_value``:
  set the :math:`h_{max}` value of the :math:`[h_{min}, h_{max}]` interval.
* ``watershed_seed_hmin_delta_value``
  set the :math:`\delta h` to go from one :math:`h` to the next in the
  :math:`[h_{min}, h_{max}]` interval.

* ``background_seed_from_hmin``:
  :python:`True` or :python:`False`.
  Build the background seed at time point :math:`t` by cell propagation.
* ``background_seed_from_previous``:
  :python:`True` or :python:`False`.
  Build the background seed at time point :math:`t` by using the background
  seed from :math:`S^e_{t-1 \leftarrow t}`.
  Fragile.

* ``seed_selection_tau``:
  Set the :math:`\tau` value for division decision (seed selection step).

* ``minimum_volume_unseeded_cell``:
  Volume threshold for cells without found seeds in the seed
  selection step. Cells with volume (in :math:`\tilde{S}_t`) whose size is below
  this threshold and for which no seed was found are discarded.

* ``volume_ratio_tolerance``:
  Ratio threshold to decide whether there is a volume decrease
  (due to the background) for morphosnake correction.
* ``volume_ratio_threshold``:
  Ratio threshold to decide whether there is a large volume decrease
  for segmentation consistency checking.
* ``volume_minimal_value``:
  Size threshold for seed correction step. For a given cell at time
  point :math:`t-1`, if the corresponding cell(s) at time point :math:`t` has(ve)
  volume below this threshold, they are discarded (and the cell at time
  point :math:`t-1` has no lineage.

* ``morphosnake_correction``:
  :python:`True` or :python:`False`.
* ``outer_correction_radius_opening``
