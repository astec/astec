
.. _cli-parameters-astec-manualcorrection:

``astec_manualcorrection`` parameters
-------------------------------------

* Diagnosis parameters
  (see section :ref:`cli-parameters-diagnosis`)

* Astec parameters
  (see section :ref:`cli-parameters-astec-astec`)

* ``first_time_point``:
  first time point to be corrected.
  Overrides the value of the ``begin`` variable.
* ``last_time_point``:
  last time point to be corrected.
* ``input_image``:
  defines the input file names (to be used when correcting
  other files than the ``astec_mars`` output file.
* ``output_image``:
  defines the output file names (to be used when correcting
  other files than the ``astec_mars`` output file.
* ``manualcorrection_dir``:
  path to directory where to find the mapping file.
* ``manualcorrection_file``:
  path to mapping file for manual correction of a segmentation (ie label)
  image. See above the syntax of this file.

  * 1 line per label association
  * background label has value 1
  * the character ``#`` denotes commented lines

  Example of ``mapping_file``:

  .. code-block:: none

     # a line beginning by '#' is ignored (comment)

     # lines with only numbers concern changes for the first time point of the time series
     # or the only time point when correcting the segmentation of the first time point
     # - one single number: label of the cell to be divided at the first time point
     # - several numbers: labels of the cells to be fused
     # Hence

     8
     # means that cell of label 8 have to be splitted

     9 2 7
     # means that cells of label 9, 7, and 2 have to be fused

     30 1
     # means that cell of label 30 have to be fused with the background (of label 1)

     # lines beginning by 'timevalue:' concern changes for the given time point
     # - 'timevalue:' + one single number: label of the cell to be splitted
     # - 'timevalue:' + several numbers: labels of the cells to be fused
     # Note there is no space between the time point and ':'

     8: 7
     # means that cell of label 7 of time point 8 have to be splitted

     10: 14 12 6
     # means that cells of label 14, 12 and 6 of time point 10 have to be fused

     # lines beginning by 'timevalue-timevalue:' concern changes for the given time point range
     # - 'timevalue-timevalue:' + several numbers: labels of the cells to be fused

     10-12: 14 16
     # means that cells of label 14 and 16 of time point 10 have to be fused
     # their offspring will be fused until time point 12

* ``use_new_labels``:
  :python:`True` or :python:`False`. If set to :python:`True`, try to use new labels when
  encountering a division to be made. Divisions will be pursued with the
  same two (new) parameters whenever possible.
