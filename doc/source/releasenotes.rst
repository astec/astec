-------------
Release notes
-------------

Developer version
=================

Version 2.2.3
=============

* Post-correction: property name uniformisation was not propagated properly.

Version 2.2.3
=============

* Enrich drift computation: score upper threshold; early stop at first iteration
* Correct I/O of tissue map fate
* Enrich documentation

Version 2.2.2
=============

* Correction of extrapolation of weight image for drift-based fusion.

Version 2.2.1
=============

* add a missing __init__.py

Version 2.2.0
=============

* introduce drift estimation within a fused time series, and between corresponding images
  of two fused time series
* fusion: introduce drift-compensated fusion
* fusion: generalize transformation saving for both drift-compensated fusion and
  hierarchical fusion

Version 2.1.11
==============

* I/O: correct conversion procedures (they did not work properly)
* I/O: correct tulip file writing (was not compatible with new properties definition)
* astec_embryoproperties: do not write tulip file anymore

Version 2.1.10
==============

* I/O: conversion procedures, to avoir writing numpy types in output files
  for backward compatibility with older versions of numpy/python.
  It seems that some "equivalence" with raw python type are now lost
  (see `NEP 40 — Legacy datatype implementation in NumPy <https://numpy.org/neps/nep-0040-legacy-datatype-impl.html>`_
  and related NEPs)
* fusion: transformations are saved in a separate directory
* fusion: corrections to fuse with a subset of views

Version 2.1.9
=============

* manualcorrection: corrects the correction files; allows for restarting
* enrich morphonet file writing: enables more properties to be written out

Version 2.1.8
=============

* manual correction: correct potential overflow problem
  (see `NEP 50 — Promotion rules for Python scalars <https://numpy.org/neps/nep-0050-scalar-promotion.html>`_)

  * python 3.9: (np.uint16) * (int) -> (np.int64)
  * python 3.11: (np.uint16) * (int) -> (np.uint16)

  This may cause similar troubles in the future.

Version 2.1.7
=============

* manual correction: correct the propagation of newly created divisions

Version 2.1.6
=============

* correct inrimage IO

Version 2.1.5
=============

* allows to change for the images kept for fusion, while keeping the
  same point of view fusion (ie from the same camera)

Version 2.1.4
=============

* correct bad behavior for direct fusion when the
  left camera image from stack #0 is not present


Version 2.1.3
=============

* allows to have new labels when dividing cell in astec_manualcorrection,
  and mother cell label is not reused for a daughter cell

* update documentation


Version 2.1.2
=============

* improve name comparison behavior

* correct bad behavior of astec_astec when restarting the computation

Version 2.1.1
=============

* documentation updating

* correct bad behavior of ReconstructionParameters.is_normalization_equal()
  when intensity_transformation is None

Version 2.1.0
=============

* documentation updating

* pursued code splitting into astec/ascidian repositories

* documentation splitting into astec/ascidian

Version 2.0.0
=============

* moved ascidian algorithms/binaries to a dedicated git repository

* replaced pkg_resources by importlib.metadata

* added test infrastructure

* refactor setup.py (adding setup.cfg)

Version 1.7.2
=============

* Review of conda env files (\*.yaml) and build

* Use builtin which command instead of external process to launch astec binaries

* Removed obsolete python-3.7 builds, added python-3.10 and python-3.11

Version 1.7.1
=============

* Write embryo as a graph

* Computation of symmetric cells

* Allows to extract any XZ-section(s) at the fusion stage.

Version 1.7.0
=============

* Fusion of one single time point: allows longer suffixes than recognized image extensions.

* Pre-processed images (see :ref:`cli-input-image-preprocessing`)
  are no more stored into ``.../SEG/SEG_XXX/RECONSTRUCTION/`` but in their
  own directories ``.../REC-MEMBRANE/REC_XXX/`` and/or ``.../REC-SEED/REC_XXX/``
  and/or ``.../REC-MORPHOSNAKE/REC_XXX/``. They can be transformed by the
  motion compensation stage (see :ref:`cli-intraregistration`).

* User-provided outer contour images (to be stored in ``.../CONTOUR_/CONTOUR_XXX/``)
  can be used in image pre-processing (see :ref:`cli-input-image-preprocessing`)

* Properties computation (see :ref:`cli-astec-embryoproperties`) computes cell lineage
  when it is not available.

Version 1.6.2
=============

Release notes not available
