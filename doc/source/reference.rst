Reference API
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Fusion
------
.. automodule:: astec.algorithms.fusion
   :members:

Mars
----
.. automodule:: astec.algorithms.mars
   :members:

Manual correction
-----------------
.. automodule:: astec.algorithms.manualcorrection
   :members:

Astec
-----
.. automodule:: astec.algorithms.astec
   :members:

Post correction
---------------
.. automodule:: astec.algorithms.postcorrection
   :members:

Motion compensation aka intra-registration
-------------------------------------------
.. automodule:: astec.algorithms.intraregistration
   :members:



