
.. _cli-image-registration:

Image registration
==================

Image registration is a basic brick for the fusion of the four cameras.
There are two fusion strategies c.

* The *direct* strategy (see section :ref:`cli-fusion-direct-strategy`),
  where one image is chosen as the reference image,
  and the 3 others are co-registered with it ,
  these 3 co-registrations are *image-to-image* registrations.

* The *hierarchical* strategy (see section :ref:`cli-fusion-hierarchical-strategy`),
  where stacks are first reconstructed by co-registering pairs
  of images issued from opposite cameras (involving two *image-to-image* registrations, one per pair),
  and then co-registered together (involving one *stack-to-stack* registration).

Each registration, either *image-to-image* or *stack-to-stack*,
can be a sequence of two registrations. Usually, these two registrations differ by
the transformation type to be computed: a first transformation
with few degrees of freedom (more likely to bring the two images close to each other)
is first calculated, then, from this starting point (the first computed transformation),
a second transformation with more degrees of freedom (that will bring the images closer to each other)
is calculated. This two-step calculation is more likely to avoid local minima
while computing a transformation.

Therefore, there are 4 sets of registration parameters in fusion parameters
(see section :ref:`cli-parameters-astec-fusion`)

* For *image-to-image* registrations:

  * registration parameters prefixed by ``fusion_preregistration_``
  * registration parameters prefixed by ``fusion_registration_``

* For *stack-to-stack* registrations:

  * registration parameters prefixed by ``fusion_stack_preregistration_``
  * registration parameters prefixed by ``fusion_stack_registration_``

A full description of the registration parameters can be found in
section :ref:`cli-parameters-registration`.


Image-to-image registration
---------------------------

Default parameters include

.. code-block:: python

   fusion_preregistration_compute_registration = False

   fusion_registration_compute_registration = True
   fusion_registration_pyramid_highest_level = 6
   fusion_registration_pyramid_lowest_level = 3
   fusion_registration_transformation_type = 'affine'

Therefore this is only a one-step registration, which computes an affine transformation.
Registration is done with pyramids of image.
Images at the higher levels of the pyramid
are first co-registered, then the computed transformation served as initialization for
the next level, and so on and so forth until the lowest required level
(see section :ref:`cli-parameters-registration` for a more detailed description of pyramid levels).
Computing the transformation at a high level allows to capture large displacements.

Roughly, setting ``pyramid_highest_level`` to 6 and ``pyramid_lowest_level`` to 3 comes to
compute the transformation from an image each dimension is divided
by :math:`2^6 = 64` to an image where they are divided by :math:`2^3 = 8`. Decreasing
``pyramid_lowest_level`` may improve the calculation precision
but at a high computational cost.

Setting the transformation type to translation, ie

.. code-block:: python

   fusion_registration_transformation_type = 'translation'

is an efficient way to check whether the acquisition parameters are correctly set
(see section :ref:`cli-fusion-important-parameters` for fusion).

The two-steps registration is a means that may prevent from registration errors
(minimisation towards an undesired local minima)

The parameters

.. code-block:: python

   fusion_preregistration_compute_registration = True
   fusion_preregistration_pyramid_highest_level = 6
   fusion_preregistration_pyramid_lowest_level = 3
   fusion_preregistration_transformation_type = 'translation'

   fusion_registration_compute_registration = True
   fusion_registration_pyramid_highest_level = 4
   fusion_registration_pyramid_lowest_level = 3
   fusion_registration_transformation_type = 'affine'

first compute a translation to align the images, allowing for large displacements
(with ``pyramid_highest_level = 6``)
and then refine it by an affine translation with smaller allowed displacements
(with ``pyramid_highest_level = 4``).


Stack-to-stack registration
---------------------------

Default parameters include

.. code-block:: python

   fusion_stack_preregistration_compute_registration = True
   fusion_stack_preregistration_pyramid_highest_level = 6
   fusion_stack_preregistration_pyramid_lowest_level = 3
   fusion_stack_preregistration_transformation_type = 'affine'

   fusion_stack_registration_compute_registration = True
   fusion_stack_registration_pyramid_highest_level = 6
   fusion_stack_registration_pyramid_lowest_level = 3
   fusion_stack_registration_transformation_type = 'vectorfield'

Therefore this is a two-steps registration, which first computes an affine transformation,
and then a deformation field.



