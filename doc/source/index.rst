=================================
Astec package
=================================

.. image:: https://anaconda.org/morpheme/astec/badges/version.svg
   :target: https://anaconda.org/morpheme/astec

.. image:: https://anaconda.org/morpheme/astec/badges/latest_release_date.svg
   :target: https://anaconda.org/morpheme/astec

.. image:: https://anaconda.org/morpheme/astec/badges/platforms.svg
   :target: https://anaconda.org/morpheme/astec

.. image:: https://anaconda.org/morpheme/astec/badges/license.svg
   :target: https://anaconda.org/morpheme/astec

.. image:: https://anaconda.org/morpheme/astec/badges/downloads.svg
   :target: https://conda.anaconda.org/morpheme/astec



ASTEC, for **Adaptive Segmentation and Tracking of Embryonic Cells**,
was first developed during L. Guignard PhD thesis :cite:p:`guignard:tel-01278725`.

Its development has been pursued, and it was used to process 3D+t movies acquired by the MuViSPIM light-sheet
microscope :cite:p:`guignard:hal-02903409`. Please cite this last article if you use ASTEC.

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :numbered:

   installation.rst
   cli.rst
   data.rst
   tutorial.rst
   astec_fusion.rst
   astec_drift.rst
   astec_mars.rst
   astec_manualcorrection.rst
   astec_astec.rst
   astec_postcorrection.rst
   astec_intraregistration.rst
   astec_embryoproperties.rst
   astec_properties.rst
   astec_input_image.rst
   astec_registration.rst
   astec_parameters.rst
   notations.rst
   reference.rst
   releasenotes.rst
   publications.rst

