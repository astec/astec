# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'astec'
copyright = '2021-2025, Grégoire MALANDAIN'
author = 'Grégoire MALANDAIN'

# The full version, including alpha/beta/rc tags
release = '2.2.4'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinxcontrib.bibtex',
    'sphinx.ext.autosectionlabel'
]

bibtex_bibfiles = ['latex/bib-astec.bib']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    'tutorial/fusion.rst',
    'tutorial/fusion_drift.rst',
    'tutorial/fusion_settings.rst',
    'parameters/astec_parameters_astec.rst',
    'parameters/astec_parameters_drift.rst',
    'parameters/astec_parameters_fusion.rst',
    'parameters/astec_parameters_intraregistration.rst',
    'parameters/astec_parameters_manualcorrection.rst',
    'parameters/astec_parameters_mars.rst',
    'parameters/astec_parameters_postcorrection.rst'
]

root_doc = 'index'

numfig = True
numfig_format = {'figure': 'Fig. %s:', 'table': 'Tab. %s:', 'code-block': 'Code %s:'}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinxdoc'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
