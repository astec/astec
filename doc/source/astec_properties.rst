
.. _cli-properties:

Image sequence properties
=========================

Cell unique id
--------------

Properties are computed over a temporal series of 3D images. The time point
corresponding to an image is encoded into the image name by a string
``_t<ttt>`` where ``<ttt>`` are three digits. Time points may start
at 0 (ie ``_t000``), 1 (ie ``_t001``), or later, but are a continuous series
of integers.

In one 3D image, or snapshot, objects
of interest (cells) are individualized by a *label* (ie an integer number).
By convention, the background used the 1 label, and
the cell labels are strictly larger than 1, but they are not necessarily
a continuous series of integer numbers starting at 2.

A same *label* can be used in different snapshots. To differentiate cells
from the different snapshots, each cell is being given an unique id,
defined by :math:`T \times 10000 + label`, where *T* is the time point
of the image and *label* the cell identifier in the image.
Thus the id 100256 designates
the cell of label 256 in the snapshot of time point 10.

Properties file format
----------------------

The handled format are

* ``pkl``: a native python format. It can be used for its efficiency.
  Properties are stored as a python dictionary, each entry corresponding to a
  given property.
* ``xml``: as a text format, it can be easily parsed.
  Properties are enclosed in the element defined by the *data* tag as above.

.. code-block:: xml

  <data>
    ...
  </data>

Properties
----------

Each given property are identified by a *tag* in ``xml`` files, which is also
the *key* for the same properties in the python dictionary stored
in ``pkl`` files.

The command

  .. code-block:: bash

     $ astec_embryoproperties -i file.pkl --print-content

displays the properties stored in a property file. The same stands for ``xml`` files.

In ``pkl`` files, each property is also a dictionary whose keys are cell unique ids,
while in ``xml`` files, each property is an element tagged by *cell* with the
*cell-id* attribute.

Properties computed by ``astec_astec``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cf section :ref:`cli-astec-astec`.

* ``cell_volume``: cell volume (in voxel unit)

    .. code-block:: xml

      <data>
        ...
        <cell_volume>
          ...
          <cell cell-id="100256">1234000</cell>
          ...
        </cell_volume>
        ...
      </data>

  indicates that cell labeled 256 in image of time 10 is made of 1234000 voxels.
  For a volume in :math:`\mu m^3`, this measure has to be multiplied
  by the cube of the voxel size (assuming voxels are isotropic).


* ``cell_lineage``: relationships between cells of successive snapshots

    .. code-block:: xml

      <data>
        ...
        <cell_lineage>
          ...
          <cell cell-id="100256">[110257]</cell>
          <cell cell-id="100257">[110300, 110301]</cell>
          ...
        </cell_lineage>
        ...
      </data>

  indicates that

  * the cell labeled 256 in image of time 10 has been labeled 300 in image of time 11,
  * the cell labeled 257 in image of time 10 divided into the cells labeled
    300 and 301 in image of time 11.


Properties computed by ``astec_embryoproperties``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Additional properties can be computed by ``astec_embryoproperties``
(cf section :ref:`cli-astec-embryoproperties`).

* ``cell_surface``: cell surface (in pixel unit)

    .. code-block:: xml

      <data>
        ...
        <cell_surface>
          ...
          <cell cell-id="100256">42000.42</cell>
          ...
        </cell_surface>
        ...
      </data>

  indicates that cell labeled 256 in image of time 10 has a total surface of 42000.42 pixels.
  For a surface in :math:`\mu m^2`, this measure has to be multiplied
  by the square of the voxel size (assuming voxels are isotropic).

* ``cell_barycenter``: cell center of mass (in voxel unit)

    .. code-block:: xml

      <data>
        ...
        <cell_barycenter>
          ...
          <cell cell-id="100256">[311.75, 288.18, 148.37]</cell>
          ...
        </cell_barycenter>
        ...
      </data>

  indicates that the center of mass of cell labeled 256 in image of time 10
  has coordinates :math:`[311.75, 288.18, 148.37]` in voxel units.

* ``cell_principal_values``: principal value are issued from the diagonalization of the
  cell covariance matrix (in voxel unit). They are sorted in decreasing order.

    .. code-block:: xml

      <data>
        ...
        <cell_principal_values>
          ...
          <cell cell-id="100256">[2175.17, 936.26, 799.08]</cell>
          ...
        </cell_principal_values>
        ...
      </data>

* ``cell_principal_vectors``: principal vectors are issued from the diagonalization of
  the cell covariance matrix (in voxel unit). The vector ordering corresponds to the
  one of the principal values.

    .. code-block:: xml

      <data>
        ...
        <cell_principal_vectors>
          ...
          <cell cell-id="100256">[[-0.42, -0.74, -0.51], [-0.07, 0.59, -0.80], [0.90, -0.29, -0.30]]</cell>
          ...
        </cell_principal_vectors>
        ...
      </data>

* ``cell_contact_surface``: contact surfaces with the adjacent cells (in pixel unit).

    .. code-block:: xml

      <data>
        ...
        <cell_contact_surface>
          ...
          <cell cell-id="100256">
            <cell cell-id="100001">1234.00</cell>
            <cell cell-id="100257">4567.00</cell>
            <cell cell-id="100300">13579.00</cell>
            ...
          </cell>
          ...
        </cell_contact_surface>
        ...
      </data>

  indicates that the cell labeled 256 in image of time 10 is adjacent to

  * the background (cell id = :math:`100001 = 10 \times 10000 + 1`)
    with a contact surface of 1234.00
  * cell labeled 257 (in image of time 10)
    with a contact surface of 4567.00
  * cell labeled 300 (in image of time 10)
    with a contact surface of 13579.00
  * ...
