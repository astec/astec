Parameters
==========

The different command line interfaces, or CLIs, (``astec_fusion``, ``astec_mars``, etc.) requires a parameter file (which is nothing but a ``python`` file) that contains both information on the experiment (path to the experiment directory, on the sub-directory names -- see section :ref:`cli-parameters-data`) as well as specific parameters for the CLIs.


.. _cli-parameters-prefixed:

Prefixed parameters
-------------------

Some of the parameter sets are said to be *prefixed*, such as the two sets of pre-processing parameters for the ``astec_mars`` CLI (see section :ref:`cli-parameters-astec-mars`). Indeed, the pre-processing can be set differently for the seed input image and the membrane input image (eg see section :ref:`cli-mars`).

Prefixing parameters allows to either set *all* the parameters with the same name together or set them *independently*.

As exemplified in section :ref:`cli-mars-input-data`, 
the parameter file lines (where the variables are not prefixed)

.. code-block:: python
		
   intensity_transformation = 'normalization_to_u8'
   intensity_enhancement = None

will set the corresponding pre-processing parameters for both the seed and the membrane image pre-processing. However, using prefixes, as in the lines

.. code-block:: python
		
   seed_intensity_transformation = 'Identity'
   membrane_intensity_transformation = 'normalization_to_u8'
   intensity_enhancement = None

allows to set them independently.

This mechanism is designed to simplify the parameter file, but may have undesired consequences. Indeed, using the basic variable names of the registration parameters (see section :ref:`cli-parameters-registration`)  for the ``astec_astec`` CLI will change all registration parameters included in the pre-processing parameters.

To check whether the parameters have been set correctly, one can either use
the ``--print-param`` CLI option (see section :ref:`cli-common-options`) beforehand, and/or to a posteriori check the used parameters in the ``log`` file. 
  
  

Common parameters
-----------------

``first_time_point``
  first time point to be processed (``astec_fusion``, 
  ``astec_astec`` or ``astec_postcorrection``) 
  or single time point to be processed
  (``astec_mars`` or ``astec_manualcorrection``).

``last_time_point``
  last time point to be processed (``astec_fusion``, 
  ``astec_astec`` or ``astec_postcorrection``).

``delta_time_point``
  interval between two time points to be processed. Set to 1 by default.
  Fragile.
  
``delay_time_point``
  Delay to be added to the time points to build the file names. 
  Fragile.
  
``time_digits_for_filename``
  number of digits used to build the file names.
  
``time_digits_for_cell_id``
  number of digits used to define unique cellule id. in lineage file.
  The unique id of cell :math:`c` at time :math:`t` is
  :math:`t \times 10^d + c` where :math:`d` is set by
  ``time_digits_for_cell_id``.  

``default_image_suffix``:
  used for both the result and the temporary image data.

    * ``'inr'``: Inrimage format, kept for historical reasons.
    * ``'mha'``: MetaImage format, readable by Fiji.
    * ``'tif'``: not advised, since the tiff format does not allow
      to keep the voxel size along the z direction (aka spacing),
      at least in a standardized way. 
    * ``'nii'``: Nifti format, compatible with Omero.

  Gzipped image files (with the additional extension ``'.gz'`` are also readable.
  
``result_image_suffix``:
  used for the result image data.

``result_lineage_suffix``:

    * ``'pkl'``: pickle file
    * ``'xml'``: xml file

``begin``
  same as ``first_time_point``

``end``
  same as ``last_time_point``

``delta``
  same as ``delta_time_point``

``raw_delay``
  same as ``delay_time_point``

.. _cli-parameters-data:

Data organisation parameters
----------------------------

``DIR_LEFTCAM_STACKONE`` 
  see section :ref:`cli-fusion-input-data`, 
  see figures :numref:`fig-data-rawdata-1`, 
  :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.

``DIR_LEFTCAM_STACKONE_CHANNEL_2`` 
  see section :ref:`cli-fusion-input-data`

``DIR_LEFTCAM_STACKONE_CHANNEL_3`` 
  see section :ref:`cli-fusion-input-data`

``DIR_LEFTCAM_STACKZERO`` 
  see section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.

``DIR_LEFTCAM_STACKZERO_CHANNEL_2`` 
  see section :ref:`cli-fusion-input-data`

``DIR_LEFTCAM_STACKZERO_CHANNEL_3`` 
  see section :ref:`cli-fusion-input-data`

``DIR_RAWDATA`` 
  see section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.

``DIR_RAWDATA_CHANNEL_2`` 
  see section :ref:`cli-fusion-input-data`

``DIR_RAWDATA_CHANNEL_3`` 
  see section :ref:`cli-fusion-input-data`

``DIR_RIGHTCAM_STACKONE`` 
  see section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.

``DIR_RIGHTCAM_STACKONE_CHANNEL_2`` 
  see section :ref:`cli-fusion-input-data`

``DIR_RIGHTCAM_STACKONE_CHANNEL_3`` 
  see section :ref:`cli-fusion-input-data`

``DIR_RIGHTCAM_STACKZERO`` 
  see section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.

``DIR_RIGHTCAM_STACKZERO_CHANNEL_2`` 
  see section :ref:`cli-fusion-input-data`

``DIR_RIGHTCAM_STACKZERO_CHANNEL_3`` 
  see section :ref:`cli-fusion-input-data`

``EN``: 
  the so-called *embryo* name. 
  All files will be named after this name.
  E.g. see section :ref:`cli-fusion-output-data`
  and figure :numref:`fig-data-fusion`.

``EXP_FUSE``:
  String (``str`` type) or list (``list`` type) of
  strings. It indicates what are the fused images directories, 
  of the form ``<PATH_EMBRYO>/FUSE/FUSE_<EXP_FUSE>``.

  .. code-block:: python

    EXP_FUSE = 'exp1'
    EXP_FUSE = ['exp1', 'exp2']

  are then both valid. 
  Default value of ``EXP_FUSE`` is ``'RELEASE'``.
  See section :ref:`cli-fusion-output-data`,
  see figure :numref:`fig-data-fusion`.

``EXP_FUSE_CHANNEL_2`` 
  see section :ref:`cli-fusion-output-data`

``EXP_FUSE_CHANNEL_3`` 
  see section :ref:`cli-fusion-output-data`

``PATH_EMBRYO``: 
  path to the *experiment*.
  If not present, the current directory is used.
  See section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  :numref:`fig-data-rawdata-3`, and :numref:`fig-data-fusion`

``acquisition_leftcam_image_prefix``  
  see section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.

``acquisition_rightcam_image_prefix``  
  see section :ref:`cli-fusion-input-data`,
  see figures :numref:`fig-data-rawdata-1`, :numref:`fig-data-rawdata-2`, 
  and :numref:`fig-data-rawdata-3`.



.. code-block:: none
  :caption: Typical organisation of mono-channel data.
  :name: fig-data-rawdata-1

   ``<PATH_EMBRYO>``/
   ├── ``<DIR_RAWDATA>``/
   │  ├── ``<DIR_LEFTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  ├── ``<acquisition_leftcam_image_prefix>001.zip``
   │  │  └── ...
   │  ├── ``<DIR_RIGHTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  ├── ``<acquisition_leftcam_image_prefix>001.zip``
   │  │  └── ...
   │  ├── ``<DIR_LEFTCAM_STACKONE>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  ├── ``<acquisition_leftcam_image_prefix>001.zip``
   │  │  └── ...
   │  └── ``<DIR_RIGHTCAM_STACKONE>``/
   │     ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │     ├── ``<acquisition_leftcam_image_prefix>001.zip``
   │     └── ...
   ...

.. code-block:: none
  :caption: Typical organisation of multi-channel data.
  :name: fig-data-rawdata-2

   ``<PATH_EMBRYO>``/
   ├── ``<DIR_RAWDATA>``/
   │  ├── ``<DIR_LEFTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_RIGHTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_LEFTCAM_STACKONE>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_RIGHTCAM_STACKONE>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_LEFTCAM_STACKZERO_CHANNEL_2>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_RIGHTCAM_STACKZERO_CHANNEL_2>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_LEFTCAM_STACKONE_CHANNEL_2>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  └── ``<DIR_RIGHTCAM_STACKONE_CHANNEL_2>``/
   │     ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │     └── ...
   ...

.. code-block:: none
  :caption: Alternative organisation of multi-channel data.mono-channel data.
  :name: fig-data-rawdata-3

   ``<PATH_EMBRYO>``/
   ├── ``<DIR_RAWDATA>``/
   │  ├── ``<DIR_LEFTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_RIGHTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_LEFTCAM_STACKONE>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  └── ``<DIR_RIGHTCAM_STACKONE>``/
   │     ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │     └── ...
   ├── ``<DIR_RAWDATA_CHANNEL_2>``/
   │  ├── ``<DIR_LEFTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_RIGHTCAM_STACKZERO>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  ├── ``<DIR_LEFTCAM_STACKONE>``/
   │  │  ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │  │  └── ...
   │  └── ``<DIR_RIGHTCAM_STACKONE>``/
   │     ├── ``<acquisition_leftcam_image_prefix>000.zip``
   │     └── ...
   ...

.. code-block:: none
  :caption: Typical organisation of fused images.
  :name: fig-data-fusion

   ``<PATH_EMBRYO>``/
   ├── ``<DIR_RAWDATA>``/
   │  └── ...
   ├── ``<FUSE>``/
   │  └── ``FUSE_<EXP_FUSE>``/
   │     ├── ``<EN>_fuse_t000.<result_image_suffix>``
   │     ├── ``<EN>_fuse_t001.<result_image_suffix>``
   │     └── ...
   ...
   




.. _cli-parameters-ace:

Ace parameters
--------------

Ace stand for *Automated Cell Extractor*. [G[L]]ACE methods aim at detecting and enhancing membranes in a 3D images
(see also section :ref:`cli-input-image-preprocessing-membrane`).

1. Hessian-based detection of 2-D manifolds, computation of a center-membrane image.  
2. Thresholding of the center-membrane image to get a binary image.
3. Reconstruction of a membrane images from the binary image through tensor voting.
   

``sigma_membrane``
  this is the gaussian sigma that is used to compute image derivatives
  (in real units), for the Hessian-based detection of 2-D manifolds.
  
``hard_thresholding``
  :python:`True` or :python:`False`.
  If set to :python:`True`, a hard threshold (set by variable 
  ``hard_threshold``) is used instead of an automated threshold.
  
``hard_threshold``

``manual``
  :python:`True` or :python:`False`.
  By default, this parameter is set to False. If failure, 
  (meaning that thresholds are very bad, meaning that the binarized 
  image is very bad), set this parameter to True and relaunch the
  computation on the test image. If the method fails again, "play" 
  with the value of ``manual_sigma`` ... and good luck.
  
``manual_sigma``
  Axial histograms fitting initialization parameter for the computation 
  of membrane image binarization axial thresholds (this parameter is 
  used if ``manual`` is set to :python:`True`). 
  One may need to test different values of 
  ``manual_sigma``. We suggest to test values between 5 and 25 
  in case of initial failure. Good luck.

``sensitivity``
  Membrane binarization parameter.
  Use larger values (smaller than or equal to 1.0) to increase 
  the quantity of binarized membranes to be used for tensor voting.

``sigma_TV``
  Parameter which defines the voting scale for membrane structures 
  propagation by tensor voting method (real coordinates). This parameter
  should be set between :math:`3 \mu m` (little cells) and 
  :math:`4.5 \mu m` (big gaps in the binarized membrane image).
  
``sigma_LF``:
  Additional smoothing parameter for reconstructed image 
  (in real coordinates).
  It seems that the default value = :math:`0.9 \mu m` is 
  ok for standard use.
  
``sample``:
  Set the fraction (in [0, 1]) of the binarized membranes further
  used for tensor voting.  
  It allows tensor voting computation speed optimisation (do not 
  touch if not bewared): the more sample, the higher the cost.

``sample_random_seed``
  Drawing a sample from the binarized membranes (see parameter 
  ``sample``) is a stochastic process. Setting this parameter 
  to some ``int`` value allows to make this stochastic process 
  reproducible.

``ace_max_value``
  Maximal value for tensor voting output.
  After smoothing by ``sigma_LF``, the values are casted into 1 or 2 bytes
  depending on this maximal value.

``bounding_box_dilation``
  Dilation radius for the cell bounding boxes
  Used to compute local histograms

``default_image_suffix``

..  seealso::
    Section :ref:`cli-input-image-preprocessing` describes the overview of the so-called
    *image reconstruction* (ie how are build input images for segmentation).

    Section :ref:`cli-input-image-preprocessing-membrane` gives some insights about membrane
    enhancement.

.. _cli-parameters-morphosnake:

Morphosnake parameters
----------------------

``dilation_iterations``
  dilation of the cell bounding box for computation purpose.

``iterations``
  maximal number of morphosnake iterations.

``delta_voxel``: 
  error on voxel count to define a stopping criteria.

``energy``
  * ``'gradient'``: uses the same formula as in 
    :cite:p:`marquez-neil:pami:2014`, as in the historical 
    astec version. But seems to be a poor choice.
  * ``'image'``: uses directly the image as the energy image.
    
``smoothing``:
  internal parameter for the morphosnake.

``balloon``:
  internal parameter for the morphosnake.

``processors``: number of processors used for the morphosnake correction.

``mimic_historical_astec``:
  :python:`True` or :python:`False`. 
  If set to :python:`True`, same implementation than the historical 
  astec version. Kept for comparison purpose.



.. _cli-parameters-preprocessing:
  
Preprocessing parameters
------------------------

The input image may be pre-processed before being used as

* either the *membrane* image (ie the height image) for watershed segmentation,
* or the *seed* image (ie the image with which the regional minima are
  computed), 
* or the *morphosnake* image (ie the image with which the morphosnake energy is
  computed).
  
For more details, see section :ref:`cli-input-image-preprocessing`.

* Ace parameters:
  see section :ref:`cli-parameters-ace`.


* ``intensity_prenormalization``: possible values are

  * ``'identity'``
  * ``'normalization_to_u8'``
  * ``'normalization_to_u16'``

  Performs a global robust normalization of the input image, prior to other
  pre-processing. The
  intensity value corresponding to the min percentile is set
  to 0, while the intensity value corresponding to the max
  percentile is set either to 255 (u8) or ``prenormalization_max_value`` (u16). In-between
  values are linearly interpolated.
  Should be left to 'identity' for integer-encoded images.
  It has been introduced for real-encoded images.

  It is governed by the variables:

  * ``prenormalization_max_percentile``:
    Percentile of the image histogram used to determine the value to
    be set to 0 (prenormalization step).
  * ``prenormalization_min_percentile``:
    Percentile of the image histogram used to determine the value to be set to
    maximal value (255 for u8, ``prenormalization_max_value`` for u16).
  * ``prenormalization_max_value``:
    Maximal output value to be issued from the prenormalization step
    (only used for the ``intensity_prenormalization = 'normalization_to_u16'`` case).

* ``intensity_transformation``:
  set the (histogram based) intensity transformation of the original image
  (see section :ref:`cli-input-image-preprocessing-histogram`)

  * ``None``: no intensity transformation of the original image is used
    to pre-process the input image. 
  * ``'identity'``: the input image is used without any transformation.
  * ``'normalization_to_u8'``: the input image (usually encoded on 16 bits)
    is normalized onto 8 bits. The values corresponding to percentiles
    given by the  variables ``normalization_min_percentile`` and
    ``normalization_max_percentile`` are mapped respectively on 0 and 255.
  * ``'normalization_to_u16'``: the input image (usually encoded on 16 bits)
    is normalized onto 8 bits or 16 bits. The values corresponding to percentiles
    given by the  variables ``normalization_min_percentile`` and
    ``normalization_max_percentile`` are mapped respectively on 0 and 255 for u8
    or ``normalization_max_value`` for u16).
    
* ``intensity_enhancement``:
  set the membrane enhancement transformation of the original image
  (see section :ref:`cli-input-image-preprocessing-membrane`)

  * ``None``: no membrane enhancement of the original image is used to
    pre-process the input image.
  * ``'GACE'``: stands for *Global Automated Cell Extractor*. It tries to
    reconstructed a membrane image through a membrane detector, an automated
    thresholding and a tensor voting step. The automated thresholding is
    computed once for the whole image.
  * ``'GLACE'``: stands for *Grouped Local Automated Cell Extractor*. It differs
    from one step from ``GACE``: the threshold of extrema image is not computed
    globally (as in ``GACE``), but one threshold is computed per cell of
    :math:`S^{\star}_{t-1} \circ \mathcal{T}_{t-1 \leftarrow t}`, from the
    extrema values of the cell bounding box.
    This can be used only with ``astec_astec`` (section
    :ref:`cli-astec-astec`).

* ``outer_contour_enhancement``:
  allows to use a fake outer contour for better segmentation of cells adjacent to the background.

  * ``None``: no contour images are used to pre-process the input image.
  * ``'from_previous_segmentation'``: contour images are built from the previous segmented time point.
    Fragile. Kept for test purposes. Obviously, works only for propagated segmentation.
    This feature has been added for tests, but has not demonstrated yet any benefit.
  * ``'from_contour_image'``: contours images named ``<EN>_contour_t<begin>.<image_suffix>``
    are provided in a separate directory
    ``<PATH_EMBRYO>/CONTOUR/CONTOUR_<EXP_CONTOUR>/``

* ``reconstruction_images_combination``:

  * ``'addition'``
  * ``'maximum'``

* ``normalization_min_percentile``
  Percentile of the image histogram used to determine the value
  to be set to 0 (normalization step).
* ``normalization_max_percentile``
  Percentile of the image histogram used to determine the value
  to be set to 255 or 'normalization_max_value' (normalization step)
* ``normalization_max_value``:
  Maximal output value to be issued from the normalization step
  (only used for the ``intensity_normalization = 'normalization_to_u16'`` case).

* ``intensity_sigma``:
  sigma (in real units) of the smoothing gaussian applied to the
  intensity-transformed image, prior its eventual combination with
  the other images (intensity enhancement, outer contours).
  Note that this variable can also be set by some watershed parameters
  (see section :ref:`cli-parameters-watershed`
  and section :ref:`cli-mars-note-parameters`).

* Registration parameters 
  (see section :ref:`cli-parameters-registration`) prefixed 
  by ``linear_registration_``
* Registration parameters 
  (see section :ref:`cli-parameters-registration`) prefixed 
  by ``nonlinear_registration_``
* ``keep_reconstruction``:
  :python:`True` or :python:`False`. If set to :python:`True`, 
  pre-processed images are kept in a ``RECONSTRUCTION/`` directory.

  These pre-processed images may be re-used in case of manual correction
  (see section :ref:`cli-manual-correction`), to extract seeds
  and to do a watershed based segmentation when a cell is to be split.
  Thus, to spare computation time, it is advised to keep them.



.. _cli-parameters-registration:

Registration parameters
-----------------------


* ``compute_registration``: :python:`True` or :python:`False`.
* ``pyramid_highest_level``:
  Registration is performed within a hierarchical scheme, ie
  an image pyramid is built, with the image dimensions
  decreasing from one pyramid level to the next (divided by 2). The
  registration starts at the highest pyramid level (the
  smallest image so the pyramid) and ends at the lowest level.
  0 is the lowest level, ie the original image itself.
  To go from level 'l' to level 'l+1', each image
  dimension is divided by 2, meaning the size of a
  3D image is divided by 8.
  Level 1 is defined by the first value of form ':math:`2^n`'
  immediately inferior to the image dimension, or the
  image dimension divided by 2 if it is already of form ':math:`2^n`'.
  Setting this variable to 6 means that registration starts with images 
  whose dimensions are 1/64th of the original image.
* ``pyramid_lowest_level``:
  Lowest level of the pyramid image for registration. Setting it
  to 0 means that the lowest level is with the image itself.
  Setting it to 1, 2 or even 3 allows to gain substantial computational time.
  See ``pyramid_highest_level`` description.
* ``gaussian_pyramid``: :python:`True` or :python:`False`.
  If True the image at one pyramid level is smoothed
  by a Gaussian kernel before building the image at
  the next level.
* ``transformation_type``
  Possible values are ``'translation'``, ``'rigid'``, ``'similitude'``,
  ``'affine'`` or ``'vectorfield'``.
* ``elastic_sigma``:
  Gaussian sigma to regularize the deformation,
  only for 'vectorfield' transformation.
  At each registration iteration, a residual deformation is
  computed. It is smoothed (regularized) by a gaussian of
  ``fluid_sigma`` parameter, then compounded with the
  previously found transformation, and the resulting
  deformation is finally smoothed (regularized) by a gaussian
  of ``elastic_sigma`` parameter.
* ``transformation_estimation_type``:
  Possible values are ``'wlts'``, ``'lts'``, ``'wls'``, or ``'ls'``.

  * ``'wlts'``: weighted least trimmed squares
  * ``'lts'``: least trimmed squares
  * ``'wls'``: weighted least squares
  * ``'ls'``: least squares

* ``lts_fraction``:
  Fraction of pairings retained to compute the transformation,
  only for robust estimation scheme (``'wlts'`` or ``'lts'``).
  Has obviously to be larger than 0.5.
* ``fluid_sigma``:
  Gaussian sigma to regularize the deformation update,
  only for 'vectorfield' transformation.
  See ``elastic_sigma`` description.
* ``normalization``: :python:`True` or :python:`False`.
  If :python:`True`, the images to be registered
  are normalized on 1 byte for computational purposes.
  This variable is kept for historical reasons. Do not change it.


.. _cli-parameters-seed-edition:

Seed edition parameters
-----------------------

* ``seed_edition_dir``:
* ``seed_edition_file``:
  if run with ``'-k'``, temporary files, including the computed 
  seeds are kept into a temporary directory, and can be corrected in
  several rounds
  
  .. code-block:: python

    seed_edition_file = [['seeds_to_be_fused_001.txt', 'seeds_to_be_created_001.txt'], \
                     ['seeds_to_be_fused_002.txt', 'seeds_to_be_created_002.txt'],
                     ...
                     ['seeds_to_be_fused_00X.txt', 'seeds_to_be_created_00X.txt']]

  Each line of a ``seeds_to_be_fused_00x.txt`` file contains 
  the labels to be fused, e.g. "10 4 2 24". A same label can be found 
  in several lines, meaning that all the labels of these lines will be 
  fused. Each line of ``seeds_to_be_created_00x.txt`` contains 
  the coordinates of a seed to be added.


  
.. _cli-parameters-watershed:

Watershed parameters
--------------------

* ``seed_sigma``:
  gaussian sigma for smoothing of initial image for seed extraction
  (real coordinates). This is a short-cut to change the value of
  ``intensity_sigma`` of the pre-processing parameters used to get the
  seed image (see section :ref:`cli-parameters-preprocessing`).
* ``seed_hmin``:
  :math:`h` value for the extraction of the :math:`h`-minima,
* ``seed_high_threshold``:
  regional minima thresholding. 
* ``membrane_sigma``:
  gaussian sigma for smoothing of reconstructed image for image 
  regularization prior to segmentation
  (real coordinates). This is a short-cut to change the value of
  ``intensity_sigma`` of the pre-processing parameters used to get the
  membrane image (see section :ref:`cli-parameters-preprocessing`).



.. _cli-parameters-diagnosis:

Diagnosis parameters
--------------------

These parameters are prefixed by ``diagnosis_``.


* ``minimal_volume``: for diagnosis on cell volume.
  Threshold on cell volume. Snapshot cells that have a volume below this threshold are displayed.

* ``maximal_volume_variation``: for diagnosis on cell volume.
  Threshold on volume variation along branches. Branches that have a volume variation
  above this threshold are displayed.
  The volume variation along a branch is calculated as
  :math:`100 * \frac{\max_{t} v(c_t) - \min_{t} v(c_t)}{\mathrm{med}_t v(c_t)}` where :math:`v(c_t)` is the volume
  of the cell :math:`c_t` and :math:`\mathrm{med}` is the median value.

* ``maximal_volume_derivative``: for diagnosis on cell volume.
  Threshold on volume derivative along branches.
  Time points along branches that have a volume derivative
  above this threshold are displayed.
  The volume derivative along a branch is calculated as
  :math:`100 * \frac{v(c_{t+1}) - v(c_{t})}{v(c_{t})}` where :math:`t` denotes the successive acquisition time points.

* ``items``: if strictly positif, number minimal of items (ie cells) to be displayed in diagnosis.

* ``minimal_length``: for diagnosis on lineage.
  Threshold on branch length. Branches that have a length below this threshold are displayed.

* ``maximal_contact_distance``: for diagnosis on cell contact surface. Threshold on cell contact surface distance
  along branches. Time points along branches that have a cell contact surface distance
  above this threshold are displayed (recall that the distance is in [0, 1]).



.. include:: ./parameters/astec_parameters_fusion.rst

.. include:: ./parameters/astec_parameters_drift.rst

.. include:: ./parameters/astec_parameters_intraregistration.rst

.. include:: ./parameters/astec_parameters_mars.rst

.. include:: ./parameters/astec_parameters_manualcorrection.rst

.. include:: ./parameters/astec_parameters_astec.rst

.. include:: ./parameters/astec_parameters_postcorrection.rst

