.. role:: python(code)
   :language: python

.. _cli-tutorial:

Tutorial
========

.. include:: ./tutorial/fusion_settings.rst

.. include:: ./tutorial/fusion.rst

.. include:: ./tutorial/fusion_drift.rst