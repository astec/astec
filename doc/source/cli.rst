
Command line interfaces
=======================

Principle
---------

The ``astec`` distribution consists in a number of command line interfaces named
``astec_something`` (eg ``astec_fusion``) while the experiment to be conducted
(both data and parameters) is described into a parameter file (eg ``parameters.py``).

Then, one only has to launch the command

.. code-block:: bash

   astec_fusion -p parameters.py

to run the experiment.


.. _cli-common-options:

Command line interfaces common options
--------------------------------------

``-h, --help``
   prints a help message
   
``-p file, --parameters file``
   set the parameter file to be parsed
   
``-e path, --embryo-rep path``
   set the
   ``path`` to the directory where the
   ``RAWDATA/`` directory is located.
   Can also be given in the parameter file by the variable ``PATH_EMBRYO``.

``-k, --keep-temporary-files``
   allows to keep the temporary files. Not to be routinely used.

``-f, --force``
   forces execution, even if (temporary) result files
   are already existing

``-v, --verbose``
   increases verboseness (both at console and in the log file)

``-nv, --no-verbose``
   no verboseness

``-d, --debug``
   increases debug information (in the log file)

``-nd, --no-debug``
   no debug information

``-pp, --print-param``
   print parameters in console and exit. A parameter file has to be provided (``-p`` option). Allows to check the parameters that will be used before any processing; it is also a means to have access to the whole parameter list. 
