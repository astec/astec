.. role:: python(code)
   :language: python

.. _tutorial-fusion:

Fusion (example)
----------------

The following parameter file performed the fusion of the 4 acquired views
with the *direct* strategy fusion
(see section :numref:`cli-fusion-direct-strategy`).

.. code-block:: python

   PATH_EMBRYO = "path/to/experiment/"
   EN = "241010-Zidane"

   DIR_RAWDATA = 'RAWDATA'
   DIR_LEFTCAM_STACKZERO = 'stack_0_channel_0_obj_left'
   DIR_RIGHTCAM_STACKZERO = 'stack_0_channel_0_obj_right'
   DIR_LEFTCAM_STACKONE = 'stack_1_channel_0_obj_left'
   DIR_RIGHTCAM_STACKONE = 'stack_1_channel_0_obj_right'
   acquisition_leftcam_image_prefix = 'Cam_left_00'
   acquisition_rightcam_image_prefix = 'Cam_right_00'

   acquisition_resolution = (0.195, 0.195, 1.0)

   acquisition_mirrors = False
   acquisition_leftcamera_z_stacking = 'inverse'
   acquisition_orientation = 'right'

   begin = 0
   end = 199

   target_resolution = 0.30
   result_image_suffix = 'mha'

   acquisition_z_cropping = True

   fusion_weighting = 'ramp'
   fusion_strategy = 'direct-fusion'

