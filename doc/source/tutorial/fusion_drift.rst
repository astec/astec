.. role:: python(code)
   :language: python

.. _tutorial-drift-compensated-fusion:

Drift compensated fusion
------------------------

A drift-compensated fusion requires several steps.

1. The fusion, with ``astec_fusion``,
   of the left and right cameras of each single acquisition angle.
   By construction, there could not be any mismatch between the two cameras, and
   the fusion will profit from the two images.
   :numref:`Section %s <tutorial-drift-compensated-fusion-fusion0>`
   and :numref:`section %s <tutorial-drift-compensated-fusion-fusion1>`
   presents examples of parameter files for this step.

2. The drift correction, with ``astec_drift``, within each fusion series.
   :numref:`Section %s <tutorial-drift-compensated-fusion-drift0>`
   and :numref:`section %s <tutorial-drift-compensated-fusion-drift1>`
   presents examples of parameter files for this step.

3. A drift correction between the two (one per angle)
   drift-compensated time series
   of stack images, still with ``astec_drift``.
   Since time series are supposed to be still (or with few motion)
   after drift compensation, it should be sufficient to register one time point,
   and not every time point.

   Drifts are compensated with respect to a reference time point (by default
   it is the first one, defined by the ``begin`` parameter, nut an other one
   can by defined by ``reference_index``).
   *It is mandatory that this reference time point is the same for the two stacks.*

   If there is no motion between the reference images of the two stacks, which
   can be easily verified by comparing the two fused images, this step
   is not necessary. However, it is required if some motion is observed.

   :numref:`Section %s <tutorial-drift-compensated-fusion-stack-drift>`
   presents an examples of parameter file for this step.

4. Once the drifts have been estimated both intra-stack and inter-stacks,
   the drift-compensated fusion can be done with ``astec_fusion``.
   :numref:`Section %s <tutorial-drift-compensated-fusion-drift-compensated-fusion>`
   presents an examples of parameter file for this step.


.. _tutorial-drift-compensated-fusion-fusion0:

Stack #0 fusion (parameters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

    PATH_EMBRYO = "."
    EN = "241010-Zidane"
    begin = 0
    end = 199

    acquisition_resolution = (0.195, 0.195, 1.0)
    acquisition_orientation = 'right'
    acquisition_mirrors = False
    acquisition_leftcamera_z_stacking = 'inverse'

    DIR_RAWDATA = 'RAWDATA'
    DIR_LEFTCAM_STACKZERO = 'stack_0_channel_0_obj_left'
    DIR_RIGHTCAM_STACKZERO = 'stack_0_channel_0_obj_right'
    DIR_LEFTCAM_STACKONE = ''
    DIR_RIGHTCAM_STACKONE = ''

    fusion_weighting = 'ramp'
    fusion_strategy = 'direct-fusion'
    target_resolution = 0.60

    EXP_FUSE = 'stack0'

* ``DIR_LEFTCAM_STACKONE`` ``DIR_RIGHTCAM_STACKONE`` and are set to ``''``, so they will be
  ignored in the fusion process.

* ``target_resolution`` is set to an high value (here ``0.6``). It will not only
  speed up the forcess of fusion, but also speed up the process of drift
  computation.



.. _tutorial-drift-compensated-fusion-fusion1:

Stack #1 fusion (parameters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This step is similar from the previous one.

.. code-block:: python

    PATH_EMBRYO = "."
    EN = "241010-Zidane"
    begin = 0
    end = 199

    acquisition_resolution = (0.195, 0.195, 1.0)
    acquisition_orientation = 'right'
    acquisition_mirrors = False
    acquisition_leftcamera_z_stacking = 'inverse'

    DIR_RAWDATA = 'RAWDATA'
    DIR_LEFTCAM_STACKZERO = ''
    DIR_RIGHTCAM_STACKZERO = ''
    DIR_LEFTCAM_STACKONE = 'stack_1_channel_0_obj_left'
    DIR_RIGHTCAM_STACKONE = 'stack_1_channel_0_obj_right'

    fusion_weighting = 'ramp'
    fusion_strategy = 'direct-fusion'
    target_resolution = 0.60

    EXP_FUSE = 'stack1'

.. warning::
    It is mandatory to specify the second stack (stack #1) left and right
    acquisition directories in the variables
    ``DIR_LEFTCAM_STACKONE`` and ``DIR_RIGHTCAM_STACKONE``, not in the
    variables  dedicated to the first stack (stack #0).

.. _tutorial-drift-compensated-fusion-drift0:

Drift estimation of stack #0 (parameters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

    PATH_EMBRYO = "."
    EN = "241010-Zidane"
    begin = 0
    end = 199

    EXP_FUSE = 'stack0'

    xy_movie_fusion_images = True
    xz_movie_fusion_images =  True
    yz_movie_fusion_images =  False

    resolution = 0.60
    template_type = 'FUSION'
    template_threshold = 140

    EXP_DRIFT = 'stack0'

* The first run of ``astec_drift`` performs

  - a first intra-series registration of the fused first stack images
  - the drift corrections of this series, based on the calculated threshold
    from the scores issued from the intra-series co-registration
    transformations
  - a second intra-series registration of the fused first stack images
    that incorporates the computed drift transformations

  However, to check the stack fusion parameters, or to adjust the ``score_threshold``
  it may be desirable to assess the 2D+t movies (in the ``ITER0-MOVIES_t<begin>-<end>``
  directory) and/or the score figure (in the ``ITER0-CO_SCORE`` directory)
  before computing the first correction round.

  To do so, the variable ``only_initialisation`` has to be set to ``True``.

* The first run of ``astec_drift``, if ``only_initialisation`` is
  set to ``False``,
  will generate ``ITER0-*`` and ``ITER1-*``
  subdirectories. To assess the quality of drift estimation, one can
  look not only at the figures generated from the scores
  (see :numref:`section %s <cli-drift-output-data>`) but also at 2D+t movies
  (of the fused images resampled after drift compensation).

  Again, these movies can be computed
  with a high resolution value (here ``resolution = 0.6``) since it aims at evaluating
  whether the co-registration is correct (not accurate).

  Obviously, 2D+t movies along other sections (with parameters
  ``xz_movie_fusion_images`` and ``yz_movie_fusion_images``) may be also computed
  as exemplified in the above parameter file.

  More important, the parameter ``template_threshold`` has to be set accordingly to
  the intensity dynamic of the fused images in order to limit the size
  of the template (see also :numref:`section %s <cli-intraregistration-template>`
  and :numref:`section %s <cli-parameters-astec-intraregistration>`).

* Based on the analysis of the first run results, it has to be decided whether
  an other iteration (an other run of ``astec_drift``) has to be made.

  The selection based on the automated threshold
  (see :numref:`section %s <cli-drift-correction-selection>`) may be too large,
  thus, corrections has to be selected with parameters ``score_threshold``,
  ``corrections_to_be_done``, or ``corrections_to_be_added``
  (see :numref:`section %s <cli-parameters-astec-drift>` for details).

* The value of parameter ``rotation_sphere_radius`` controls the number of
  initial rotations to be tested, and then the global computational cost
  of drift correction.

Once the computed drift is satisfactory, some directories or file can be removed to spare
room on the physycal device: see  :numref:`section %s <cli-drift-disk-cleaning>`.



.. _tutorial-drift-compensated-fusion-drift1:

Drift estimation of stack #1 (parameters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This step is similar from the previous one, but for the second stack/angle.

.. code-block:: python

    PATH_EMBRYO = "."
    EN = "241010-Zidane"
    begin = 0
    end = 199

    EXP_FUSE = 'stack1'

    xy_movie_fusion_images = True
    xz_movie_fusion_images =  True
    yz_movie_fusion_images =  False
    resolution = 0.60
    template_type = 'FUSION'
    template_threshold = 140

    EXP_DRIFT = 'stack1'



.. _tutorial-drift-compensated-fusion-stack-drift:

Drift estimation between stacks (parameters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the two previous sections,
``astec_drift`` was used to compute the transformations
between any two successive fused images of a given stack (obtained
by fusion of the left and right images of a given angle),
and these transformations can be further used for a drift-compensated fusion.
However, it assumes that the two drift-compensated series of stacks are already (almost)
co-registered.

Then, one has to check first
whether the two stacks are already co-registered. If not, one has to compute
a transformation between the two stacks, which is the purpose
of this section.

To check whether the two stacks are co-registered,
one has to check, by visual inspection, whether the images of
the time point ``reference_index`` (or ``begin``
if ``reference_index`` was not set)
issued from the stack fusions
(:numref:`section %s <tutorial-drift-compensated-fusion-fusion1>`
and  :numref:`section %s <tutorial-drift-compensated-fusion-fusion0>`)
can roughly be superimposed.

If yes, we can proceed to the drift-compensated fusio
:numref:`section %s <tutorial-drift-compensated-fusion-drift-compensated-fusion>`.

If no, the two stacks may have to be co-registered, which can be done
with ``astec_drift`` and the following parameter file.

.. code-block:: python

    PATH_EMBRYO = "."
    EN = "241010-Zidane"
    begin = 0
    end = 0

    EXP_FUSE = ['stack1', 'stack0']

    score_threshold = 11.13
    EXP_DRIFT = 'stack1'


* It is *mandatory* to specify the two directories in the second stack (stack #1)
  drift parameter file, and to put the fusion directory of the first stack (stack #0)
  *after* the the fusion directory of the second stack.

* ``end`` has been set to the same value than ``begin``. This way, only the
  stack-to-stack drift will be computed. If ``end`` has a larger value
  (say  :python:`end = 199`), a new iteration of intra-stack drift estimation
  (for the second stack, ie stack #1) will be performed.

* ``score_threshold`` has been set to the threshold computed at the first run.
  It will allow an earlier stop when testing the set of initial rotations.

See also :numref:`section %s <cli-drift-inter-stack>`.



.. _tutorial-drift-compensated-fusion-drift-compensated-fusion:

Drift compensated fusion (parameters)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

    PATH_EMBRYO = "."
    EN = "241010-Zidane"
    begin = 0
    end = 199

    acquisition_resolution = (0.195, 0.195, 1.0)
    acquisition_orientation = 'right'
    acquisition_mirrors = False
    acquisition_leftcamera_z_stacking = 'inverse'

    DIR_RAWDATA = 'RAWDATA'
    DIR_LEFTCAM_STACKZERO = 'stack_0_channel_0_obj_left'
    DIR_RIGHTCAM_STACKZERO = 'stack_0_channel_0_obj_right'
    DIR_LEFTCAM_STACKONE = 'stack_1_channel_0_obj_left'
    DIR_RIGHTCAM_STACKONE = 'stack_1_channel_0_obj_right'

    fusion_weighting = 'ramp'
    fusion_strategy = 'direct-fusion'
    target_resolution = 0.30

    EXP_FUSE = 'RELEASE'
    EXP_DRIFT = ['stack0', 'stack1']

* The only difference with a fusion parameter file without drift compensation is
  the line

  .. code-block:: python

     EXP_DRIFT = ['stack0', 'stack1']

  that indicates the ``DRIFT/`` subdirectories where the drift transformations
  will be serached for. The order is important, and the drift diectory for the first
  stack has to be indicated first. The stack-to-stack transformation, if any,
  will be searched in the second directory.

* The required resolution of the fusion, given by the parameter ``target_resolution``,
  can be freely chosen (independently of the ``target_resolution`` used for stack fusion
  and of the ``resolution`` value used for drift estimation).

