.. role:: python(code)
   :language: python

.. _tutorial-fusion-parameter-setting:

Fusion parameter settings
-------------------------

This section aims at explaining how to set the three parameters that
describes the acquisition protocol (see section :ref:`important parameters <cli-fusion-important-parameters>`):

* ``acquisition_mirrors``  (or ``raw_mirrors``) is a parameter indicating whether the right camera images have
  already been mirrored along the X axis (so that the X axis direction is the one of the left cameras) or not.
  Its value is either :python:`False` or :python:`True`.
  When  ``acquisition_mirrors`` is set to :python:`False`, the X-axis of the right camera images
  will be inverted.

* ``acquisition_leftcamera_z_stacking`` gives the order of stacking of in the Z direction for the left camera images.

   * ``'direct'``: *z* increases from the high-contrasted images to the blurred ones  (see figure :numref:`%s <acquisition-spim-right>`).
   * ``'inverse'``: *z* increases from the blurred images to the high-contrasted ones (see figure :numref:`%s <acquisition-spim-left>`).

  When  ``acquisition_leftcamera_z_stacking`` is set to ``'inverse'``, the Z-axis of all images
  (from both left and right cameras) will be inverted.

* ``acquisition_orientation`` (or ``raw_ori``) is a parameter describing the acquisition orientation of the
  acquisition of the stack #1 images with respect to the stack #0 ones.

   * ``'right'`` : the frame (X, Z) of the left camera of stack #0 needs to be rotated clockwise (90 degrees
     along the Y axis) to correspond to the left camera of stack #1 (see figure :numref:`%s <acquisition-spim-right>`).
   * ``'left'``: the frame (X, Z) of the left camera of stack #0 needs to be rotated counterclockwise
     (-90 degrees along the Y axis) to correspond to the left camera of stack #1
     (see figure :numref:`%s <acquisition-spim-left>`).

The experiments presented below rely upon the raw data organized as below (see also :ref:`cli-data-organization`).
They aimed at helping to set the :ref:`important parameters <cli-fusion-important-parameters>` for fusion.

.. code-block:: none

   $ /path/to/experiment/
   ├── RAWDATA/
   │  ├── stack_0_channel_0_obj_left/
   │  │  ├── Cam_left_00000.lux.h5
   │  │  ├── ...
   │  │  └── ...
   │  ├── stack_0_channel_0_obj_right/
   │  │  ├── Cam_right_00000.lux.h5
   │  │  ├── ...
   │  │  └── ...
   │  ├── stack_1_channel_0_obj_left/
   │  │  ├── Cam_left_00000.lux.h5
   │  │  ├── ...
   │  │  └── ...
   │  └── stack_1_channel_0_obj_right/
   │     ├── Cam_right_00000.lux.h5
   │     ├── ...
   │     └── ...

Setting ``acquisition_mirrors``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``acquisition_mirrors``  (or ``raw_mirrors``) is a parameter indicating whether the right camera images have
already been mirrored along the X axis (so that the X axis direction is the one of the left cameras) or not.
Looking at the acquired data provides a first means to decide how to set this parameter.

.. |zidane_stack0_left_xy145| image:: ./tutorial/figures-fusion-settings/zidane_stack0_left_xy145.png
   :width: 90%

.. |zidane_stack0_right_xy145| image:: ./tutorial/figures-fusion-settings/zidane_stack0_right_xy145.png
   :width: 90%

+--------------------------------------------------+--------------------------------------------------+
| |zidane_stack0_left_xy145|                       | |zidane_stack0_right_xy145|                      |
+--------------------------------------------------+--------------------------------------------------+
| image from ``stack_0_channel_0_obj_left``        | image from ``stack_0_channel_0_obj_right``       |
+--------------------------------------------------+--------------------------------------------------+

Above are the XY-section (Z=145) of time point 0 of Zidane series for both (left an right cameras) of
stack #0. The right camera image is symetrical with respect to the left one, demonstrating that
the X axis has to be mirrored, hence that

.. code-block:: python

   acquisition_mirrors = False

has to be specified in the parameter file.

.. |zidane_stack1_left_xy145| image:: ./tutorial/figures-fusion-settings/zidane_stack1_left_xy145.png
   :width: 90%

.. |zidane_stack1_right_xy145| image:: ./tutorial/figures-fusion-settings/zidane_stack1_right_xy145.png
   :width: 90%

+--------------------------------------------------+--------------------------------------------------+
| |zidane_stack1_left_xy145|                       | |zidane_stack1_right_xy145|                      |
+--------------------------------------------------+--------------------------------------------------+
| image from ``stack_1_channel_0_obj_left``        | image from ``stack_1_channel_0_obj_right``       |
+--------------------------------------------------+--------------------------------------------------+

Obviously, the same symetry can be observed in the images of the second stack (stack #1), acquired
after the stage rotation.

Alternatively, the value of ``acquisition_mirrors`` can also be checked by performing a first fusion.

Setting ``acquisition_leftcamera_z_stacking``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The right values of both ``acquisition_mirrors`` and ``acquisition_leftcamera_z_stacking`` can be
checked by performing a first fusion.
Since ``acquisition_mirrors`` will cause the x-mirroring of the right camera images, and
``acquisition_leftcamera_z_stacking`` will cause the z-mirroring of all image, performing a
fusion with the left and right camera images of the first stack (stack #0) is sufficient to
check whether the values of these parameters have been properly set.

Let consider the following parameter file (saved in ``parameters.py``, but any name can be chosen)

.. code-block:: python

   PATH_EMBRYO = "path/to/experiment/"
   EN = "241010-Zidane"

   DIR_RAWDATA = 'RAWDATA'
   DIR_LEFTCAM_STACKZERO = 'stack_0_channel_0_obj_left'
   DIR_RIGHTCAM_STACKZERO = 'stack_0_channel_0_obj_right'
   DIR_LEFTCAM_STACKONE = ""
   DIR_RIGHTCAM_STACKONE = ""
   acquisition_leftcam_image_prefix = 'Cam_left_00'
   acquisition_rightcam_image_prefix = 'Cam_right_00'

   acquisition_resolution = (0.195, 0.195, 1.0)

   acquisition_mirrors = True
   acquisition_leftcamera_z_stacking = 'direct'

   begin = 0
   end = 0

   target_resolution = 0.60
   result_image_suffix = 'mha'

   acquisition_z_cropping = True

   registration_transformation_type = 'translation'
   fusion_xzsection_extraction = True

   fusion_weighting = 'ramp'
   fusion_strategy = 'direct-fusion'

   EXP_FUSE = 'test_true_direct'

Some remarks.

* The first lines describes how the data are organized (see section :numref:`cli-fusion-input-data`).
  Please note that both ``DIR_LEFTCAM_STACKONE`` and ``DIR_LEFTCAM_STACKONE`` are set to ``""``. This
  allows to discard the images of the second stack (stack #1) to be used for the fusion.
  In other words, the fusion
  will be done only with the images of the first stack (stack #0).

* ``acquisition_mirrors`` and ``acquisition_leftcamera_z_stacking`` have intentionally incorrectly
  set for educational purpose.

* ``begin`` and ``end`` are both set to ``0``. Since the aim is to check the value of some parameters,
  ony one time pint will be fused (here ``0``)

* The ``target_resolution`` is set to some high value, to decrease the computational time.

* The type of the sought transformation (between the images to be fused) is set to ``'translation'``.
  It corresponds to a perfect acquisition. The ``'affine'``
  transformation type, that allows scaling, may find an extreme scaling factor if th two image to
  be registered are too far apart, yielding the interpretation more difficult.

* Very important, ``fusion_xzsection_extraction`` is set to :python:`True`.
  This will allow XZ-sections to be computed after fusion, which is the best way to check
  whether ``acquisition_leftcamera_z_stacking`` has been properly set.

* Still important, the weighting function ``fusion_weighting`` has been set to ``'ramp'``.
  An other z-depending weighting scheme could have been chosen as well.

Running the fusion comes to call the command line ``astec_fusion`` with the parameter file

.. code-block:: bash

   astec_fusion -p parameters.py

After (successful) running, a ``FUSE`` directory has appeared with a sub-directory
named after the variable ``EXP_FUSE``

.. code-block:: none

   $ /path/to/experiment/
   ├── RAWDATA/
   │  ├── stack_0_channel_0_obj_left/
   │  │  ├── Cam_left_00000.lux.h5
   │  .   .
   ├── FUSE/
   │  └── FUSE_test_true_direct/
   │     ├── 241010-Zidane_fuse_t000.mha
   │     ├── LOGS
   │     └── XZSECTION_000
   │        ├── 241010-Zidane_xz167_fuse.mha
   │        ├── 241010-Zidane_xz167_stack0_lc_reg.mha
   │        ├── 241010-Zidane_xz167_stack0_lc_weight.mha
   │        ├── 241010-Zidane_xz167_stack0_rc_reg.mha
   │        └── 241010-Zidane_xz167_stack0_rc_weight.mha

The ``FUSE_test_true_direct`` directory contains

* the fusion image(s) (here only one image, ``241010-Zidane_fuse_t000.mha``)

* a ``LOGS`` directory, that contains a copy of the parameter file, and monitoring information
  about the fusion computation, and

* a ``XZSECTION`` named after the time points, that contains a XZ-section of the fusion image,
  of the co-registered images, and of the weight image (the weighted combination of those
  image wllows to recalculate the XZ-sections of the fusion image).

* Note that result images are named after the ``EN`` (embryo name) variable.

.. |241010-Zidane_xy165_fuse_td| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xy165_fuse_td.png
   :width: 90%

.. |241010-Zidane_xz167_fuse_td| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_fuse_td.png
   :width: 90%

+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xy165_fuse_td|                    | |241010-Zidane_xz167_fuse_td|                    |
+--------------------------------------------------+--------------------------------------------------+
| XY-section of the fused image                    | XZ-section of the fused image                    |
+--------------------------------------------------+--------------------------------------------------+

The XY-section (Z=165, extracted from the file ``241010-Zidane_fuse_t000.mha``) and the XZ-section
(Y=167, automatically extracted) demonstrate the improper choice of
``acquisition_mirrors``.


.. |241010-Zidane_xz167_stack0_lc_reg_td| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_lc_reg_td.png
   :width: 90%

.. |241010-Zidane_xz167_stack0_rc_reg_td| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_rc_reg_td.png
   :width: 90%

.. |241010-Zidane_xz167_stack0_lc_weight_td| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_lc_weight_td.png
   :width: 90%

.. |241010-Zidane_xz167_stack0_rc_weight_td| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_rc_weight_td.png
   :width: 90%

Below are the other images of the ``XZSECTION_000`` directory

+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xz167_stack0_lc_reg_td|           | |241010-Zidane_xz167_stack0_rc_reg_td|           |
+--------------------------------------------------+--------------------------------------------------+
| XZ-section of left camera acquisition image      | XZ-section of right camera acquisition image     |
+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xz167_stack0_lc_weight_td|        | |241010-Zidane_xz167_stack0_rc_weight_td|        |
+--------------------------------------------------+--------------------------------------------------+
| XZ-section of left camera weight image           | XZ-section of right camera weight image          |
+--------------------------------------------------+--------------------------------------------------+

It can be seen that the lower part of the XZ-section of the
left camera image (the upper left image) is better defined than the
upper part (and conversely for the XZ-section of the
right camera image), while this lower part will be more weighted
(see the corresponding weight image, at the lower left; note that
high values correspond to white while low values correspond to black).

Let consider now the following parameter file where the values of ``acquisition_mirrors``
and ``acquisition_leftcamera_z_stacking`` are correctly set.

.. code-block:: python

   PATH_EMBRYO = "path/to/experiment/"
   EN = "241010-Zidane"

   DIR_RAWDATA = 'RAWDATA'
   DIR_LEFTCAM_STACKZERO = 'stack_0_channel_0_obj_left'
   DIR_RIGHTCAM_STACKZERO = 'stack_0_channel_0_obj_right'
   DIR_LEFTCAM_STACKONE = ""
   DIR_RIGHTCAM_STACKONE = ""
   acquisition_leftcam_image_prefix = 'Cam_left_00'
   acquisition_rightcam_image_prefix = 'Cam_right_00'

   acquisition_resolution = (0.195, 0.195, 1.0)

   acquisition_mirrors = False
   acquisition_leftcamera_z_stacking = 'inverse'

   begin = 0
   end = 0

   target_resolution = 0.60
   result_image_suffix = 'mha'

   acquisition_z_cropping = True

   registration_transformation_type = 'translation'
   fusion_xzsection_extraction = True

   fusion_weighting = 'ramp'
   fusion_strategy = 'direct-fusion'

   EXP_FUSE = 'test_false_inverse'


After (successful) running of ``astec_fusion``,
a new sub-directory appears in the  ``FUSE`` directory since the value of
``EXP_FUSE`` has chamged.

.. code-block:: none

   $ /path/to/experiment/
   ├── RAWDATA/
   │  ├── stack_0_channel_0_obj_left/
   │  │  ├── Cam_left_00000.lux.h5
   │  .  .
   ├── FUSE/
   │  ├── FUSE_test_true_direct/
   │  │  ├── ...
   │  .  │   .
   │     └── FUSE_test_false_inverse/
   │        ├── 241010-Zidane_xz167_fuse.mha
   │        .

.. |241010-Zidane_xz167_stack0_lc_reg_fi| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_lc_reg_fi.png
   :width: 90%

.. |241010-Zidane_xz167_stack0_lc_weight_fi| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_lc_weight_fi.png
   :width: 90%

.. |241010-Zidane_xz167_stack0_rc_reg_fi| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_rc_reg_fi.png
   :width: 90%

.. |241010-Zidane_xz167_stack0_rc_weight_fi| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_rc_weight_fi.png
   :width: 90%


+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xz167_stack0_lc_reg_fi|           | |241010-Zidane_xz167_stack0_rc_reg_fi|           |
+--------------------------------------------------+--------------------------------------------------+
| XZ-section of left camera acquisition image      | XZ-section of right camera acquisition image     |
+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xz167_stack0_lc_weight_fi|        | |241010-Zidane_xz167_stack0_rc_weight_fi|        |
+--------------------------------------------------+--------------------------------------------------+
| XZ-section of left camera weight image           | XZ-section of right camera weight image          |
+--------------------------------------------------+--------------------------------------------------+

First, it has to be observed that changing the value of ``acquisition_leftcamera_z_stacking``
from ``'direct'`` to ``'inverse'`` causes the acquisition images to be mirrored
along the Z direction.

Second. it can be seen that the better defined parts of the XZ-sections
now correspond to high values in the weight images.

.. |241010-Zidane_xz167_stack0_lcrc_fi_trs| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_stack0_lcrc_fi_trs.png
   :width: 90%

.. |241010-Zidane_xz167_fuse_fi_trs| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz167_fuse_fi_trs.png
   :width: 90%

+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xz167_stack0_lcrc_fi_trs|         | |241010-Zidane_xz167_fuse_fi_trs|                |
+--------------------------------------------------+--------------------------------------------------+
| XZ-section of composite left/right images        | XZ-section of the result fusion image            |
+--------------------------------------------------+--------------------------------------------------+

Above at the left, the composite view (red and green channels are respectively the XZ-sections of the left
and the right camera images) allows to visually assess the registration (recall
that this is done here by a translation only).
At the right, the XZ-section of the fused image (from the left and right cameras of
the first stack).

Setting ``acquisition_orientation``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assuming that  ``acquisition_mirrors`` and ``acquisition_leftcamera_z_stacking``
have been correctly set, a fusion image will be reconstructed with only the
left and right camera images of the second stack (after rotation of the stage,
see section :ref:`multiview acquisition <cli-fusion-multiview-lightsheet-microscope-acquisition>`).

The stage rotation axis being the Y-axis of the acquired images, looking at XZ-sections
is definitively the most efficient means to assess whether ``acquisition_orientation``
is properly set.

In the following parameter file

.. code-block:: python

   PATH_EMBRYO = "path/to/experiment/"
   EN = "241010-Zidane"

   DIR_RAWDATA = 'RAWDATA'
   DIR_LEFTCAM_STACKZERO = ''
   DIR_RIGHTCAM_STACKZERO = ''
   DIR_LEFTCAM_STACKONE = 'stack_1_channel_0_obj_left'
   DIR_RIGHTCAM_STACKONE = 'stack_1_channel_0_obj_right'
   acquisition_leftcam_image_prefix = 'Cam_left_00'
   acquisition_rightcam_image_prefix = 'Cam_right_00'

   acquisition_resolution = (0.195, 0.195, 1.0)

   acquisition_mirrors = False
   acquisition_leftcamera_z_stacking = 'inverse'

   acquisition_orientation = 'left'

   begin = 0
   end = 0

   target_resolution = 0.60
   result_image_suffix = 'mha'

   acquisition_z_cropping = True

   registration_transformation_type = 'translation'
   fusion_xzsection_extraction = True

   fusion_weighting = 'ramp'
   fusion_strategy = 'direct-fusion'

   EXP_FUSE = 'test_left'

only the data acquired from the second stack (stack #1) will be used for fusion
(notice that both ``DIR_LEFTCAM_STACKZERO`` and ``DIR_RIGHTCAM_STACKZERO`` have been
set to ``''``), ``fusion_xzsection_extraction`` have been set to :python:`True`, and
``acquisition_orientation`` has been set to ``'left'``.

A second parameter file, with ``acquisition_orientation`` has been set to ``'right'``,
is also created and both are passed to ``astec_fusion`` for computation.

.. |241010-Zidane_xz164_fuse_s1_left| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz164_fuse_s1_left.png
   :width: 90%

.. |241010-Zidane_xz164_fuse_s1_right| image:: ./tutorial/figures-fusion-settings/241010-Zidane_xz164_fuse_s1_right.png
   :width: 90%

+--------------------------------------------------+--------------------------------------------------+
| |241010-Zidane_xz164_fuse_s1_left|               | |241010-Zidane_xz164_fuse_s1_right|              |
+--------------------------------------------------+--------------------------------------------------+
| ``acquisition_orientation`` = ``'left'``         | ``acquisition_orientation`` = ``'right'``        |
+--------------------------------------------------+--------------------------------------------------+

Even if only the left and right cameras of the second stack are used for fusion,
the fused image is directly comparable to the left camera of the first stack
(whose geometry serves as reference). From above, it can be seen
that ``acquisition_orientation`` have to be set to ``'right'``
to be comparable with the fusion obtained with the camera of the first stack.



