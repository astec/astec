
.. _cli-data-organization:

Data organization
=================

It is assumed that there will be one directory per experiment (ie per embryo). This
directory contains the acquired data, but will also contain the result
data as depicted below.
	  
.. code-block:: none

   $ /path/to/experiment/
   ├── RAWDATA/
   │  └── ...
   ├── FUSE/
   │  └── FUSE_<EXP_FUSE>/
   │      ├── ...
   │      └── LOGS/
   ├── CONTOUR/
   │   └── ...
   ├── SEG/
   │  └── SEG_<EXP_SEG>/
   │      ├── ...
   │      └── LOGS/
   ├── POST/
   │  └── POST_<EXP_POST>/
   │      ├── ...
   │      └── LOGS/
   ├── REC-MEMBRANE/
   │   └── REC_<EXP_RECONSTRUCTION>/
   ├── REC-SEED/
   │   └── REC_<EXP_RECONSTRUCTION>/
   ├── REC-MORPHOSNAKE/
   │   └── REC_<EXP_RECONSTRUCTION>/
   └── INTRAREG/
      └── INTRAREG_<EXP_INTRAREG>/
          ├── ...
          └── LOGS/


* ``RAWDATA/``
  contains the raw acquisitions issued from the microscope.
  See section :ref:`cli-fusion-input-data` for their organization.

* ``FUSE/``
  will contain the images after fusion of the raw data.
  See section :ref:`cli-fusion` for details.

* ``CONTOUR/``
  may contain outer contour images (to alleviate their poor contrast).
  See section :ref:`cli-input-image-preprocessing-outercontour` for details.

* ``SEG/``
  will contain the images after segmentation.
  For the segmentation of one time point (independently from the others, typically
  the first time point), see section :ref:`cli-mars`. For the segmentation by propagation
  (from an already segmented time point), see section :ref:`cli-astec-astec`.

* ``POST/``
  will contain the images after post-segmentation.
  See section :ref:`cli-astec-postcorrection` for details.

* ``REC-MEMBRANE/``, ``REC-SEED/``, and ``REC-MORPHOSNAKE/``
  will contains the fusion image after pre-processing (may include intensity rescaling,
  membrane enhancement, fusion of outer contour, etc). These pre-processed images
  are generated at the same time than the segmentation images.
  See section :ref:`cli-input-image-preprocessing` for details.

* ``INTRAREG/``
  will contain the images after drift compensation.
  See section :ref:`cli-intraregistration` for details.

* ``..._<EXP_XXX>/``
  To compared different experiments (different settings), results are stored
  into subdirectories whose suffix can be set by the user.

* ``.../LOGS/``
  This subdirectory contains an history of the commands launched to generate the
  results, as well as a copy of the parameter file (suffixed by date and time)
  and the log of the execution (suffixed by the same date and time).
