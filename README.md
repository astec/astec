# ASTEC

This file contains instructions for installing and running the ASTEC reconstruction algorithm.

ASTEC stands for **Adaptive Segmentation and Tracking of Embryonic
Cells**.
A first implementation (no more available) was developed during L. Guignard PhD thesis,
**Quantitative analysis of animal morphogenesis: from high-throughput
laser imaging to 4D virtual embryo in ascidian**, _Léo Guignard_,
2015, https://tel.archives-ouvertes.fr/tel-01278725.
It has evolved and has been rewritten since.

This work was then published on **bioRxiv** in 2018:

**Contact-dependent cell-cell communications drive morphological invariance during ascidian embryogenesis**, _Léo Guignard, Ulla-Maj Fiuza, Bruno Leggio, Emmanuel Faure, Julien Laussu, Lars Hufnagel, Grégoire Malandain, Christophe Godin, Patrick Lemaire_, bioRxiv 2018; doi: https://doi.org/10.1101/238741.

It was later published in **Science** in 2020:

**Contact area–dependent cell communication and the morphological invariance of ascidian embryogenesis**, _Léo Guignard, Ulla-Maj Fiuza, Bruno Leggio, Julien Laussu, Emmanuel Faure, Gaël Michelin, Kilian Biasuz, Lars Hufnagel, Grégoire Malandain, Christophe Godin, Patrick Lemaire_, Science 2020; doi: https://doi.org/10.1126/science.aar5663.

Please cite this last article if you use ASTEC.


## Installation

Was tested only on Linux or MacOs systems.

1. <a href="#user-installation">User installation</a>: recommended for a basic use
2. <a href="#user-installation">User installation (with git)</a>
3. <a href="#dev-installation">Developer installation (with git)</a>
4. <a href="#doc-installation">Documentation update (with git)</a>

### [I - User installation (without git)](#user-installation)

If you do not need to get last update (ie if you have no interaction with developer(s)), this should 
be your installation of choice. Requires `conda`.

1. Create a conda environment (here the environment is named
`astec`)

    ```bash
    conda create -n astec -c morpheme -c conda-forge astec
    ```

2. Activate the built conda environment

    ```bash
    conda activate astec
    ```

    Update of packages can be done with

    ```bash
    conda update -n astec -c morpheme -c conda-forge astec
    ```

### [II - User installation (with git)](#user-installation-git)

Requires `conda` and `git`.

1. Astec code can be found at
   [gitlab.inria.fr/astec/astec](http://gitlab.inria.fr/astec/astec). It
   can be downloaded with

    ```bash
    git clone https://gitlab.inria.fr/astec/astec.git
    ```

    It creates an `astec` directory.

2. Create a conda environment named `astec`

    ```bash
    cd astec
    conda env create -f pkg/env/astec.yaml
    ```

   If already created, the conda environment can be updated with
    ```bash
    conda env update -f pkg/env/astec.yaml
    ```

3. Activate the built conda environment

    ```bash
    conda activate astec
    ```

### [III - Developer  installation (with git)](#dev-installation)

Requires `conda` and `git`.

1. Astec code can be found at
   [gitlab.inria.fr/astec/astec](http://gitlab.inria.fr/astec/astec). It
   can be downloaded with

    ```bash
    git clone https://gitlab.inria.fr/astec/astec.git
    ```

    It creates an `astec` directory.

2. Create a conda environment named `astec-dev`

    ```bash
    cd astec
    conda env create -f pkg/env/astec-dev.yaml
    ```

   If already created, the conda environment can be updated with
    ```bash
    conda env update -f pkg/env/astec-dev.yaml
    ```

3. Activate the built conda environment

    ```bash
    conda activate astec-dev
    ```

4. Install astec package for use

    Assume you are in the ``astec`` directory created at step 1:

    ```bash
    python -m pip install -e .
    ```

    The `-e` option install the package in "editable" mode, this is
    want you want if you aim at contributing to the astec
    project. This last command has to be repeated (within the conda
    environment every time the astec code has been modified).

### [IV - Documentation update (with git)](#doc-installation)

This environment only allows to update the documentation, not to test code changes (use ``astec-dev`` then).
Requires `conda` and `git`.

1. Astec code can be found at
   [gitlab.inria.fr/astec/astec](http://gitlab.inria.fr/astec/astec). It
   can be downloaded with

    ```bash
    git clone https://gitlab.inria.fr/astec/astec.git
    ```

    It creates an `astec` directory.

2. Create a conda environment named `astec-doc`

    ```bash
    cd astec
    conda env create -f pkg/env/astec-doc.yaml
    ```

   If already created, the conda environment can be updated with
    ```bash
    conda env update -f pkg/env/astec-doc.yaml
    ```

3. Activate the built conda environment

    ```bash
    conda activate astec-doc
    ```

4. Build documentation

    Assume you are in the ``astec`` directory created at step 1:
    ```bash
    cd doc
    make html
    ```

    The documentation will be built into the ``astec/doc/build/`` directory, and can be accessed
    through ``astec/doc/build/html/index.html`` with your favorite browser. 
    Documentation can also be built within the ``astec-dev`` conda environment. 



## Tutorial

A tutorial can be found at [gitlab.inria.fr/astec/astec-tutorial](https://gitlab.inria.fr/astec/astec-tutorial).

## Documentation

A documentation can be found at [astec.gitlabpages.inria.fr/astec/](https://astec.gitlabpages.inria.fr/astec/).
