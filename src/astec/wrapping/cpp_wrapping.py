##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#
##############################################################
#
#
#
##############################################################

import os
import sys
import subprocess
import shutil

from astec.io.image import imread, imsave

############################################################
#
#
#
############################################################


def _write_error_msg(text, monitoring):
    """
    Write an error message
    :param text:
    :param monitoring:
    :return:
    """
    if monitoring is not None:
        monitoring.to_log_and_console(text, 0)
    else:
        print(text)


def __find_exec(executable_file):
    """
    Try to find the executable file 'executable_file'
    :param executable_file:
    :return:
    """
    path_to_exec = shutil.which(executable_file)
    if path_to_exec == None:
        try_file = os.path.join(os.path.dirname(__file__), 'cpp', 'vt', 'build', 'bin', str(executable_file))
        if os.path.isfile(try_file):
            return try_file
        try_file = os.path.join(os.path.dirname(__file__), 'cpp', str(executable_file))
        if os.path.isfile(try_file):
            return try_file

        return None

    return path_to_exec


def _find_exec(executable_file, monitoring=None):
    """
    Try to find the executable file 'executable_file'
    :param executable_file:
    :param monitoring:
    :return:
    """
    path_to_exec = __find_exec(executable_file)

    if path_to_exec is None:
        _write_error_msg("findExec: can not find executable '" + str(executable_file) + "'", monitoring)
        _write_error_msg("... Exiting", monitoring)
        sys.exit(1)

    return path_to_exec


def path_to_cli(cli_name=None):
    if cli_name is None:
        return None
    if not isinstance(cli_name, str):
        return None
    path_to_exec = __find_exec(cli_name)
    return os.path.dirname(path_to_exec)


def path_to_vt():
    """
    """
    return path_to_cli('blockmatching')


############################################################
#
#
#
############################################################


def _launch_inline_cmd(command_line, monitoring=None):
    """

    :param command_line:
    :param monitoring:
    :return:
    """

    if monitoring is not None and (monitoring.verbose >= 2 or monitoring.debug > 0):
        monitoring.to_log("* Launch: " + command_line)
        with open(monitoring.log_filename, 'a') as logfile:
            subprocess.call(command_line, shell=True, stdout=logfile, stderr=subprocess.STDOUT)
    else:
        subprocess.call(command_line, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    return


############################################################
#
# interfaces: registrations and transformations
#
############################################################

def applyTrsf(the_image, res_image, the_transformation=None, template_image=None, res_transformation=None,
              voxel_size=None, dimensions=None, interpolation_mode='linear', cell_based_sigma=0.0,
              monitoring=None):
    """

    :param the_image: path to the image to be resampled
    :param res_image: path to the resampled image (ie result)
    :param the_transformation: path to the transformation to be used
    :param template_image: path to the template image (used to specify result image geometry)
    :param res_transformation:
    :param voxel_size: to specify the output voxel size(s); may be used to change image
                resolution by resampling
    :param dimensions: dimensions of the result image; may be used to change image
                resolution by resampling
    :param interpolation_mode:
    :param cell_based_sigma: smoothing parameter when resampling with nearest=True.
           cell_based_sigma=0.0 means no smoothing.
    :param monitoring: control structure (for verboseness and log informations)
    """

    proc = "applyTrsf"

    path_to_exec = _find_exec('applyTrsf')

    command_line = path_to_exec + " " + the_image + " " + res_image

    if the_transformation is not None:
        command_line += " -trsf " + the_transformation

    if template_image is not None:
        command_line += " -template " + template_image

    if res_transformation is not None:
        command_line += " -result-transformation " + res_transformation

    if type(interpolation_mode) == str:
        if interpolation_mode.lower() == 'linear':
            command_line += " -linear"
        elif interpolation_mode.lower() == 'nearest':
            if (type(cell_based_sigma) == int and cell_based_sigma > 0) \
                    or (type(cell_based_sigma) == float and cell_based_sigma > 0.0):
                command_line += " -cellbased"
                command_line += " -cell-based-sigma " + str(cell_based_sigma)
            else:
                command_line += " -nearest"
        else:
            # default
            pass
    else:
        # default
        pass

    if dimensions is not None:
        if type(dimensions) == tuple or type(dimensions) == list:
            if len(dimensions) == 3:
                command_line += " -template-dimensions " + str(dimensions[0]) + " " + str(dimensions[1]) + " " + str(dimensions[2])
            else:
                _write_error_msg(proc + ": unhandled length for dimensions '" + str(len(dimensions)) + "'", monitoring)
                _write_error_msg("\t Exiting", monitoring)
                sys.exit(1)
        else:
            _write_error_msg(proc + ": unhandled type for dimensions '" + str(type(dimensions)) + "'", monitoring)
            _write_error_msg("\t Exiting", monitoring)
            sys.exit(1)

        if type(voxel_size) == int or type(voxel_size) == float:
            command_line += " -vs " + str(voxel_size) + " " + str(voxel_size) + " " + str(voxel_size)
        elif type(voxel_size) == tuple or type(voxel_size) == list:
            if len(voxel_size) == 3:
                command_line += " -vs "+str(voxel_size[0]) + " " + str(voxel_size[1]) + " " + str(voxel_size[2])
            else:
                _write_error_msg(proc + ": unhandled length for voxel_size '" + str(len(voxel_size)) + "'", monitoring)
                _write_error_msg("\t Exiting", monitoring)
                sys.exit(1)

    elif voxel_size is not None:
        if type(voxel_size) == int or type(voxel_size) == float:
            command_line += " -iso " + str(voxel_size)
        elif type(voxel_size) == tuple or type(voxel_size) == list:
            if len(voxel_size) == 3:
                command_line += " -vs "+str(voxel_size[0]) + " " + str(voxel_size[1]) + " " + str(voxel_size[2])
            else:
                _write_error_msg(proc + ": unhandled length for voxel_size '" + str(len(voxel_size)) + "'", monitoring)
                _write_error_msg("\t Exiting", monitoring)
                sys.exit(1)
        else:
            _write_error_msg(proc + ": unhandled type for voxel_size '" + str(type(voxel_size)) + "'", monitoring)
            _write_error_msg("\t Exiting", monitoring)
            sys.exit(1)

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def apply_transformation(the_image, res_image, the_transformation=None, template_image=None, res_transformation=None,
                         voxel_size=None, dimensions=None, interpolation_mode='linear', cell_based_sigma=0.0,
                         monitoring=None, return_image=False):
    """

    :param the_image: path to the image to be resampled
    :param res_image: path to the resampled image (ie result)
    :param the_transformation: path to the transformation to be used
    :param template_image: path to the template image (used to specify result image geometry)
    :param res_transformation:
    :param voxel_size: to specify the output voxel size(s); may be used to change image
                resolution by resampling
    :param dimensions: dimensions of the result image; may be used to change image
                resolution by resampling
    :param interpolation_mode:
    :param cell_based_sigma: smoothing parameter when resampling with nearest=True.
           cell_based_sigma=0.0 means no smoothing.
    :param monitoring: control structure (for verboseness and log informations)
    :param return_image: if True, return the result image as an spatial image
                  (default = False; nothing is returned)
    :return: no returned value if return_image = False
            if return_image = True, return the result image as an spatial image
    """

    applyTrsf(the_image, res_image, the_transformation=the_transformation, template_image=template_image,
              res_transformation=res_transformation, voxel_size=voxel_size, dimensions=dimensions,
              interpolation_mode=interpolation_mode, cell_based_sigma=cell_based_sigma, monitoring=monitoring)

    # interpolation may put '0' values in segmentation images
    # change 0 into 1 for further computation
    # mainly useful for morphonet visualization
    if interpolation_mode.lower() == 'nearest':
        if os.path.isfile(res_image):
            path_to_exec = _find_exec('changevals')
            command_line = path_to_exec + " " + res_image + " " + res_image + " -modify 0 1"
            _launch_inline_cmd(command_line, monitoring=monitoring)

    if return_image is True:
        out = imread(res_image)
        return out

    return


def blockmatching(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf=None, py_hl=6, py_ll=3,
                  gaussian_pyramid=False, transformation_type='affine', elastic_sigma=4.0,
                  transformation_estimator='wlts', lts_fraction=0.55, fluid_sigma=4.0, normalization=True,
                  other_options=None, monitoring=None):
    """

    :param path_ref:
    :param path_flo:
    :param path_output:
    :param path_output_trsf:
    :param path_init_trsf:
    :param py_hl:
    :param py_ll:
    :param gaussian_pyramid:
    :param transformation_type:
    :param elastic_sigma:
    :param transformation_estimator:
    :param lts_fraction:
    :param fluid_sigma:
    :param normalization:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('blockmatching')

    command_line = path_to_exec + " -ref " + path_ref + " -flo " + path_flo
    if path_output is not None:
        command_line += " -res " + path_output
    if path_output_trsf is not None:
        command_line += " -res-trsf " + path_output_trsf
    if path_init_trsf is not None:
        command_line += " -left-trsf " + path_init_trsf + " -composition-with-initial"

    command_line += " -pyramid-highest-level " + str(py_hl) + " -pyramid-lowest-level " + str(py_ll)
    if gaussian_pyramid is True:
        command_line += " -py-gf"

    command_line += " -trsf-type " + transformation_type
    if transformation_type.lower() == 'vectorfield' or transformation_type.lower() == 'vectorfield2d' \
            or transformation_type.lower() == 'vectorfield3d':
        command_line += " -elastic-sigma " + str(elastic_sigma) + " " + str(elastic_sigma) + " " + str(elastic_sigma)

    command_line += " -estimator " + transformation_estimator
    command_line += " -lts-fraction " + str(lts_fraction)
    if transformation_type.lower() == 'vectorfield' or transformation_type.lower() == 'vectorfield2d' \
            or transformation_type.lower() == 'vectorfield3d':
        command_line += " -fluid-sigma " + str(fluid_sigma) + " " + str(fluid_sigma) + " " + str(fluid_sigma)

    if normalization is False:
        # monitoring.to_log_and_console("       non-normalized registration", 2)
        command_line += " -no-normalisation"

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def change_multiple_trsfs(format_input, format_output, first, last, referenceid, result_template, trsf_type='rigid',
                          resolution=None, threshold=None, margin=None, format_template=None,
                          reference_transformation=None, other_options=None, monitoring=None):
    """

    :param format_input:
    :param format_output:
    :param first:
    :param last:
    :param referenceid:
    :param result_template:
    :param trsf_type:
    :param resolution:
    :param threshold:
    :param margin:
    :param format_template:
    :param reference_transformation:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('changeMultipleTrsfs')

    command_line = path_to_exec + " -format " + format_input + " -res-format " + format_output
    command_line += " -first " + str(first) + " -last " + str(last)
    command_line += " -reference " + str(referenceid)
    command_line += " -result-template " + str(result_template)

    command_line += " -trsf-type " + trsf_type

    if resolution is not None:
        if (type(resolution) is tuple or type(resolution) is list) and len(resolution) == 3:
            command_line += " -result-voxel-size "
            command_line += str(resolution[0]) + " " + str(resolution[1]) + " " + str(resolution[2])
        elif type(resolution) is int or type(resolution) is float:
            command_line += " -result-isotropic " + str(resolution)

    if threshold is not None:
        command_line += " -threshold " + str(threshold)

    if margin is not None:
        command_line += " -margin " + str(margin)

    if format_template is not None:
        command_line += " -template-format " + str(format_template)

    if reference_transformation is not None:
        command_line += " -reference-transformation " + str(reference_transformation)

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def compose_trsf(the_trsfs, res_trsf, other_options=None, monitoring=None):
    """

    :param the_trsfs:
    :param res_trsf:
    :param other_options:
    :param monitoring:
    :return:
    """
    path_to_exec = _find_exec('composeTrsf')

    command_line = path_to_exec + " -res " + res_trsf
    command_line += " -trsfs"
    for i in range(len(the_trsfs)):
        command_line += " " + the_trsfs[i]

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def create_trsf(trsf_out, other_options=None, monitoring=None):
    """

    :param trsf_out:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('createTrsf')

    #
    #
    #
    command_line = path_to_exec + " " + trsf_out

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def linear_registration(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf=None, py_hl=6, py_ll=3,
                        transformation_type='affine', transformation_estimator='wlts', lts_fraction=0.55,
                        normalization=True, other_options=None, monitoring=None):
    """
    Compute the transformation that register the floating image onto the reference image
    :param path_ref: path to the reference image
    :param path_flo: path to the floating image
    :param path_output: path to the floating image after registration and resampling
    :param path_output_trsf: path to the computed transformation

    :param py_ll: pyramid lowes
        :param path_init_trsf: path to the initial registration (default=None)
    :param py_hl: pyramid highest level (default = 6)

    t level (default = 3)
    :param transformation_type: type of transformation to be computed (default is 'affine')
    :param transformation_estimator: transformation estimator (default is 'wlts')
    :param lts_fraction: least trimmed squares fraction (default = 0.55)
    :param normalization:
    :param other_options: other options to be passed to 'blockmatching'
           see blockmatching options for details
    :param monitoring: control structure (for verboseness and log informations)
    :return: no returned value
    """

    path_to_exec = _find_exec('blockmatching')

    command_line = path_to_exec + " -ref " + path_ref + " -flo " + path_flo
    if path_output is not None:
        command_line += " -res " + path_output
    if path_init_trsf is not None:
        command_line += " -init-res-trsf " + path_init_trsf + " -composition-with-initial"
    command_line += " -res-trsf " + path_output_trsf

    command_line += " -pyramid-highest-level " + str(py_hl) + " -pyramid-lowest-level " + str(py_ll)
    command_line += " -trsf-type " + transformation_type
    command_line += " -estimator " + transformation_estimator
    command_line += " -lts-fraction " + str(lts_fraction)
    if normalization is False:
        # monitoring.to_log_and_console("       non-normalized registration", 2)
        command_line += " -no-normalisation"

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def multiple_trsfs(format_input, format_output, first, last, reference, trsf_type='rigid', other_options=None,
                   monitoring=None):
    """

    :param format_input:
    :param format_output:
    :param first:
    :param last:
    :param reference:
    :param trsf_type:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('multipleTrsfs')

    command_line = path_to_exec + " " + format_input + " -res " + format_output
    command_line += " -method propagation "
    command_line += " -first " + str(first) + " -last " + str(last)
    command_line += " -reference " + str(reference)
    command_line += " -trsf-type " + trsf_type

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# interfaces: crop
#
############################################################

def bounding_boxes(image_labels, path_bboxes=None, monitoring=None):
    """
    Calcul des bounding-boxes de chaque label de l'image d'entree.
    Si path_bboxes est renseigne, le resultat est sauvegarde dans ce fichier.
    Output : dictionnaire D dont les cles sont les labels d'image et les
    valeurs sont les listes contenant dans l'ordre les informations de
    [volume, xmin, ymin, zmin, xmax, ymax, zmax] correspondant a ces labels
    avec volume > 0 et label > 0.
    :param image_labels:
    :param path_bboxes:
    :param monitoring:
    :return:
    """

    if path_bboxes is None:
        file_boxes = 'tmp_bounding_boxes.txt'
    else:
        file_boxes = path_bboxes

    #
    #
    #

    path_to_exec = _find_exec('boundingboxes')
    command_line = path_to_exec + " " + image_labels + " " + file_boxes
    _launch_inline_cmd(command_line, monitoring=monitoring)

    #
    #
    #

    f = open(file_boxes, 'r')
    lines = f.readlines()
    f.close()

    boxes = {}

    for line in lines:
        if not line.lstrip().startswith('#'):
            li = line.split()
            if int(li[1]):
                boxes[int(li[0])] = list(map(int, li[1:]))

    if path_bboxes is None:
        os.remove(file_boxes)

    return boxes


def crop_image(path_input, path_output, bbox, monitoring=None):
    """
    crop an image on disk
    :param path_input:
    :param path_output:
    :param bbox: [volume, xmin, ymin, zmin, xmax, ymax, zmax]
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('cropImage')
    command_line = path_to_exec + " " + path_input + " " + path_output
    command_line += " -origin " + str(bbox[1]) + " " + str(bbox[2]) + " " + str(bbox[3])
    command_line += " -dim " + str(bbox[4]-bbox[1]+1) + " " + str(bbox[5]-bbox[2]+1) + " " + str(bbox[6]-bbox[3]+1)

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def crop_sequence(format_input, path_output, firstindex, lastindex, orientation, sliceindex, monitoring=None):
    """

    :param format_input:
    :param path_output:
    :param firstindex:
    :param lastindex:
    :param orientation:
    :param sliceindex:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('cropImage')
    command_line = path_to_exec + " -format " + format_input + " " + path_output
    command_line += " -first " + str(firstindex) + " -last " + str(lastindex)
    if orientation.lower() == 'xy':
        command_line += " -xy " + str(sliceindex)
    elif orientation.lower() == 'xz':
        command_line += " -xz " + str(sliceindex)
    elif orientation.lower() == 'yz':
        command_line += " -yz " + str(sliceindex)
    else:
        return

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def ext_image(input_image, output_image, options, other_options=None, monitoring=None):

    path_to_exec = _find_exec('extImage')

    #
    #
    #

    command_line = path_to_exec

    command_line += " " + input_image
    command_line += " " + output_image
    command_line += " " + options

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# interfaces: image management
#
############################################################

def copy(path_input, path_output, other_options=None, monitoring=None):
    """

    Parameters
    ----------
    path_input
    path_output
    other_options
    monitoring

    Returns
    -------

    """

    path_to_exec = _find_exec('copy')

    command_line = path_to_exec + ' ' + path_input + ' ' + path_output
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def create_image(image_out, image_template, other_options=None, monitoring=None):
    """

    :param image_out:
    :param image_template:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('createImage')

    #
    #
    #
    command_line = path_to_exec + " " + image_out

    if image_template is not None:
        command_line += " -template " + image_template

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# interfaces: basic operation on image(s)
#
############################################################

def arithmetic_operation(path_first_input, path_second_input, path_output, other_options=None, monitoring=None):
    """

    :param path_first_input:
    :param path_second_input:
    :param path_output:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('Arit')

    #
    #
    #
    command_line = path_to_exec + " " + path_first_input + " " + path_second_input + " " + path_output
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def logical_operation(path_first_input, path_second_input, path_output, other_options=None, monitoring=None):
    """

    :param path_first_input:
    :param path_second_input:
    :param path_output:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('Logic')

    #
    #
    #
    command_line = path_to_exec
    if other_options is not None:
        command_line += " " + other_options

    command_line += " " + path_first_input

    if path_second_input is not None:
        command_line += " " + path_second_input

    command_line += " " + path_output

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def patch_logical_operation(path_first_input, path_second_input, path_output, bbox,
                            other_options=None, monitoring=None):
    """

    :param path_first_input:
    :param path_second_input:
    :param path_output:
    :param bbox: [volume, xmin, ymin, zmin, xmax, ymax, zmax]
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('patchLogic')

    #
    #
    #
    command_line = path_to_exec
    if other_options is not None:
        command_line += " " + other_options

    command_line += " " + path_first_input

    if path_second_input is not None:
        command_line += " " + path_second_input

    command_line += " " + path_output

    command_line += " -origin " + str(bbox[1]) + " " + str(bbox[2]) + " " + str(bbox[3])

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# interfaces: one image -> image
#
############################################################

def connected_components(path_input, path_output, low_threshold=1, high_threshold=None,
                         other_options=None, monitoring=None):
    """
    Label connected components
    :param path_input:
    :param path_output:
    :param low_threshold:
    :param high_threshold: high threshold for hysteresis thresholding
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('connexe')

    #
    #
    #
    command_line = path_to_exec + " " + path_input + " " + path_output
    command_line += " -lt " + str(low_threshold)
    if high_threshold is not None:
        command_line += " -ht " + str(high_threshold)

    #
    # force output type
    #
    command_line += " -labels"

    #
    # force output image type
    #
    command_line += " -o 2"

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def gradient_norm(path_input, path_output, filter_value=1.0, real_scale=False, filter_type='deriche',
                  other_options=None, monitoring=None):
    """

    :param path_input: path to the image to filter
    :param path_output: path to the output image
    :param filter_value: sigma of the gaussian filter for each axis (default is 1.0)
    :param real_scale: scale values are in 'real' units (will be divided by the voxel size to get 'voxel' values)
           if this option is at True (default=False)
    :param filter_type: gaussian type, can be ['deriche'|'fidrich'|'young-1995'|'young-2002'|...
           ...|'gabor-young-2002'|'convolution'] or None (default is 'deriche')
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('linearFilter')

    command_line = path_to_exec + " " + path_input + " " + path_output

    command_line += " -gradient-modulus"
    #
    # filter parameter value
    #
    command_line += " -sigma " + str(filter_value)
    if real_scale is True:
        command_line += " -unit real"
    else:
        command_line += " -unit voxel"

    #
    # filter type
    #
    if filter_type is not None:
        command_line += " -gaussian-type " + str(filter_type)
    command_line += " -x 0 -y 0 -z 0"

    #
    # add points at borders
    #
    command_line += " -cont 10"

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def linear_smoothing(path_input, path_output, filter_value=1.0, real_scale=False, filter_type=None, border=None,
                     other_options=None, monitoring=None):
    """

    :param path_input: path to the image to filter
    :param path_output: path to the output image
    :param filter_value: sigma of the gaussian filter for each axis (default is 1.0)
    :param real_scale: scale values are in 'real' units (will be divided by the voxel size to get 'voxel' values)
           if this option is at True (default=False)
    :param filter_type: gaussian type, can be ['deriche'|'fidrich'|'young-1995'|'young-2002'|...
           ...|'gabor-young-2002'|'convolution'] or None (default is 'deriche')
    :param border:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('linearFilter')

    command_line = path_to_exec + " " + path_input + " " + path_output

    #
    # smoothing
    #
    command_line += " -x 0 -y 0 -z 0"

    #
    # filter parameter value
    #
    command_line += " -sigma " + str(filter_value)
    if real_scale is True:
        command_line += " -unit real"
    else:
        command_line += " -unit voxel"

    #
    # filter type
    #
    if filter_type is not None:
        command_line += " -gaussian-type " + str(filter_type)

    #
    # add points at borders
    #
    if border is not None:
        command_line += " -cont " + str(border)

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def mathematical_morphology(path_input, path_output, other_options=None, monitoring=None):
    """

    :param path_input:
    :param path_output:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('morpho')
    command_line = path_to_exec + " " + path_input + " " + path_output
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def regional_maxima(path_input, path_output, h=1, other_options=None, monitoring=None):
    """

    :param path_input: path to the input image
    :param path_output: path to the output image
    :param h: h-maxima parameter value
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('regionalext')

    #
    #
    #
    command_line = path_to_exec + " " + path_input + " -diff " + path_output
    command_line += " -max -h " + str(h)

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def regional_minima(path_input, path_output, h=1, other_options=None, monitoring=None):
    """

    :param path_input: path to the input image
    :param path_output: path to the output image
    :param h: h-minima parameter value
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('regionalext')

    #
    #
    #
    command_line = path_to_exec + " " + path_input + " -diff " + path_output
    command_line += " -min -h " + str(h)

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def seuillage(path_input, path_output, low_threshold=1, high_threshold=None, other_options=None, monitoring=None):
    """

    :param path_input:
    :param path_output:
    :param low_threshold: low threshold (default: 1)
    :param high_threshold:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('seuillage')

    #
    #
    #
    command_line = path_to_exec + " " + path_input + " " + path_output
    command_line += " -sb " + str(low_threshold)
    if high_threshold is not None:
        command_line += " -sh " + str(high_threshold)

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def watershed(path_seeds, path_gradient, path_output, other_options=None, monitoring=None):
    """
    Perform the watershed operation
    :param path_seeds: path to the seeds image
    :param path_gradient: path to the intensity/gradient image
    :param path_output: path to the output image
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('watershed')

    #
    #
    #
    command_line = path_to_exec + " -seeds " + path_seeds + " -gradient " + path_gradient + " " + path_output

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# interfaces: multiple images -> image
#
############################################################

def linear_combination(the_weights, the_images, res_image, other_options=None, monitoring=None):
    """

    :param the_weights:
    :param the_images:
    :param res_image:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('mc-linearCombination')
    command_line = path_to_exec

    if isinstance(the_weights, list) and isinstance(the_images, list):
        if len(the_weights) != len(the_images):
            if monitoring is not None:
                msg = "weight and image lists do not have the same length\n"
                monitoring.to_log(msg)
            return
        command_line += " -weights"
        for i in range(len(the_weights)):
            command_line += " " + str(the_weights[i])
        command_line += " -images"
        for i in range(len(the_images)):
            command_line += " " + str(the_images[i])
    elif isinstance(the_weights, dict) and isinstance(the_images, dict):
        keys = (set(the_weights.keys())).intersection(set(the_images.keys()))
        command_line += " -weights"
        for i in keys:
            command_line += " " + str(the_weights[i])
        command_line += " -images"
        for i in keys:
            command_line += " " + str(the_images[i])

    command_line += " -res " + str(res_image)

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def mean_images(format_input, image_output, first, last, operation="maximum", other_options=None, monitoring=None):
    """

    :param format_input:
    :param image_output:
    :param first:
    :param last:
    :param operation:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('meanImages')

    command_line = path_to_exec + " -format " + format_input + " -res " + image_output
    command_line += " -operation " + operation
    command_line += " -first " + str(first) + " -last " + str(last)
    command_line += " -streaming "

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# interfaces: seed management
#
############################################################

def mc_mask_seeds(seed_image, cell_image, seed_result, other_options=None, monitoring=None):
    """

    :param seed_image:
    :param cell_image:
    :param seed_result:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('mc-maskSeeds')

    #
    #
    #
    command_line = path_to_exec

    command_line += " -seed-image " + str(seed_image)
    command_line += " -cell-image " + str(cell_image)
    command_line += " -output-image " + str(seed_result)

    if other_options is not None:
        command_line += " " + str(other_options)

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def mc_seed_edit(the_seeds, res_seeds, fusion_file=None, seeds_file=None, other_options=None, monitoring=None):
    """

    :param the_seeds:
    :param res_seeds:
    :param fusion_file:
    :param seeds_file:
    :param other_options:
    :param monitoring:
    :return:
    """
    path_to_exec = _find_exec('mc-seedEdit')

    command_line = path_to_exec + " " + the_seeds
    command_line += " " + res_seeds
    if fusion_file is not None and fusion_file != '':
        command_line += " -fusion " + fusion_file
    if seeds_file is not None and seeds_file != '':
        command_line += " -seeds " + seeds_file

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# dedicated interfaces: fusion
#
############################################################

def mip_projection_for_crop(the_image, res_xy_image, res_xz_image=None, res_zy_image=None, other_options=None,
                            monitoring=None):
    """

    :param the_image:
    :param res_xy_image:
    :param res_xz_image:
    :param res_zy_image:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('mc-extractMIPembryo')

    if res_xy_image is None and res_xz_image is None and res_zy_image is None:
        return
    command_line = path_to_exec + " " + the_image
    if res_xy_image is not None:
        command_line += " -xy " + res_xy_image
    if res_xz_image is not None:
        command_line += " -xz " + res_xz_image
    if res_zy_image is not None:
        command_line += " -zy " + res_zy_image

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def slitline_correction(the_image, res_image, output_corrections=None, input_corrections=None, other_options=None,
                        monitoring=None):
    """

    :param the_image:
    :param res_image:
    :param output_corrections
    :param input_corrections
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('mc-removeLine')

    #
    # global method
    # -xz 0.5: 50% of y lines are used to compute slit line to be corrected
    #         the ones with the smallest intensity average (assumed to be background)
    # -y 0.1: robust mean (with 10% outliers) is computed along selected y lines
    # -c 0.2: lines that contains more than 20% of outliers are subject to correction
    #

    command_line = path_to_exec + " " + the_image + " " + res_image
    if input_corrections is None:
        command_line += " -method g -xz 0.5 -y 0.1 -c 0.2"
        if output_corrections is not None:
            command_line += " -output-corrections " + output_corrections
    else:
        command_line += " -input-corrections " + input_corrections

    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# dedicated interfaces: reconstruction
#
############################################################

def global_intensity_normalization(path_input, path_output, min_percentile=0.01, max_percentile=0.99,
                                   other_options=None, monitoring=None):
    """

    :param path_input:
    :param path_output:
    :param min_percentile:
    :param max_percentile:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('mc-adhocFuse')

    #
    #
    #
    command_line = path_to_exec + " -intensity-image " + path_input + " -result-intensity-image " + path_output
    command_line += " -min-percentile " + str(min_percentile) + " -max-percentile " + str(max_percentile)
    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def outer_contour(path_input, path_output, connectivity=6, sigma=2.0, other_options=None, monitoring=None):
    """

    :param path_input:
    :param path_output:
    :param connectivity:
    :param sigma:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('cellfilter')

    #
    #
    #
    command_line = path_to_exec + " " + path_input + " " + path_output
    command_line += " -contour "
    command_line += " -con " + str(connectivity)
    command_line += " -sigma " + str(sigma)
    command_line += " -o 1 "
    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


############################################################
#
# dedicated interfaces: membrane extraction
#
############################################################

def anisotropic_histogram(path_input_extrema, path_output_histogram, path_output, path_input_mask=None,
                          manual=False, manual_sigma=7, sensitivity=0.98, other_options=None, monitoring=None):
    """
    Centerplanes image binarisation using an adaptative anisotropic threshold method detailed in [Michelin 2016]
    :param path_input_extrema: input extrema image
    :param path_output_histogram: output histogram text file
    :param path_output: output binary image
    :param path_input_mask: binary image (u8 or u16) such that the thresholding is only computed for non-null voxels
           from this mask (8 bits image of same size as input image).
    :param manual: if True, enables manual initialisation of sigma value for histograms fitting (default: False)
    :param manual_sigma: theb sigma value for histogram fitting in case of manual mode (default: 20)
    :param sensitivity: computes the anisotropic thresholds following a sensitivity criterion (true positive rate):
           threshold = #(membrane class >= threshold) / #(membrane class)
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('anisotropicHist')

    #
    #
    #
    command_line = path_to_exec + " " + path_input_extrema + " " + path_output_histogram
    command_line += " -bin-out " + path_output
    if path_input_mask is not None and os.path.isfile(path_input_mask):
        command_line += " -mask " + path_input_mask
    command_line += " -sensitivity " + str(sensitivity)
    command_line += " -v"

    if manual is True:
        from math import exp
        amplitude = 1.0 / (manual_sigma * exp(-0.5))
        lmin = manual_sigma / 3.0
        lmax = manual_sigma * 5.0
        command_line += ' -rayleighcentered ' + str(amplitude) + ' ' + str(manual_sigma)
        command_line += ' -lmin ' + str(lmin) + ' -lmax ' + str(lmax)
    else:
        command_line += " -auto"

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def membrane_extraction(path_input, prefix_output='tmp_membrane',
                        path_mask=None, scale=0.9, real_scale=True, other_options=None, monitoring=None):
    """
    Membrane (plane-like) structures enhancement and oriented centerplanes extraction
    (using the method detailed in [Michelin et al. 2014] inspired by [Krissian 2000])
    :param path_input: path to the image to filter
    :param prefix_output: Write 3 files named <prefix_output>.ext.inr,<prefix_output>.theta.inr,<prefix_output>.phi.inr
    :param path_mask: binary image (u8 or u16) such that the response function is only computed for non-null voxels
           from this mask
    :param scale: detection scale parameter (should be set as the semi-thickness of the membrane)
    :param real_scale: set as True if the scale parameter is given in real coordinates system (default),
           set as False if given in voxel coordinates
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('membrane')

    #
    #
    #
    command_line = path_to_exec + " " + path_input + " " + prefix_output
    if path_mask is not None and os.path.isfile(path_mask):
        command_line += " -mask " + path_mask
    command_line += " -single -init " + str(scale)
    if real_scale is True:
        command_line += " -real"

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return


def tensor_voting_membrane(path_input, prefix_input, path_output, path_mask=None, scale_tensor_voting=3.6,
                           sigma_smoothing=0.9, real_scale=True, sample=0.2, random_seed=None, max_value=255,
                           monitoring=None):
    """
    Grey-level image reconstruction from an image of binarised membranes associated to images of orientations.
    :param path_input: path to input image which is contains binarised planar structures,
           with associated input angle images <path_prefix>.theta.inr and <path_prefix>.phi.inr in the same folder.
    :param prefix_input: prefix name for temporary images
    :param path_output: path to the output image
    :param path_mask: mask image applied to the input image to restrict the domain of tensor voting tokens
           (must be same dimensions with input image)
    :param scale_tensor_voting: scaling for tensor voting method (only on isotropic images) (default = 3.6 um)
    :param sigma_smoothing: scale for gaussien smooting after tensor voting (default = 0.9 um)
    :param real_scale: True (default) if scale in real coordinates. False if scale in voxel coordinates.
    :param sample: multiplying parameter for decrease the time-cost of the function by diminishing the
           number of voting token; 0 < sample <= 1, default = 0.2
    :param random_seed:
    :param monitoring:
    :return:
    """

    proc = "tensor_voting_membrane"
    #
    # check whether the input image is empty
    #
    path_to_exec = _find_exec('nonZerosImage')
    command_line = path_to_exec + " " + path_input
    isnonzero = subprocess.call(command_line, shell=True)
    if isnonzero == 0:
        if monitoring is not None:
            monitoring.to_log(proc + ": '" + str(path_input).split(os.path.sep)[-1] + "' only contains 0.")
            monitoring.to_log("\t Exiting.")
        else:
            print(proc + ": '" + str(path_input).split(os.path.sep)[-1] + "' only contains 0.")
            print("\t Exiting.")
        sys.exit(1)

    #
    # tensor voting
    #

    path_to_exec = _find_exec('TVmembrane')
    command_line = path_to_exec + " " + path_input + " -output-eigenvalues " + prefix_input
    if path_mask is not None and os.path.isfile(path_mask):
        command_line += " -mask " + path_mask
    command_line += " -scale " + str(scale_tensor_voting) + " -hessian"
    command_line += " -sample " + str(sample)
    if random_seed is not None:
        command_line += " -random-seed " + str(random_seed)
    if real_scale:
        command_line += " -real"
    _launch_inline_cmd(command_line, monitoring=monitoring)

    #
    # eigenvalues substraction
    #

    arithmetic_operation(prefix_input + ".imvp3.inr", prefix_input + ".imvp1.inr", prefix_input + ".tv.inr",
                         other_options='-sub')

    #
    # smoothing
    #

    input_image = prefix_input + ".tv.inr"
    if sigma_smoothing > 0.0:
        linear_smoothing(input_image, prefix_input + ".lf.inr", filter_value=sigma_smoothing,
                         real_scale=real_scale, monitoring=monitoring)
        if not os.path.isfile(prefix_input + ".lf.inr"):
            msg = ": was unable to smooth tensor voting image. Too small standard deviation?!"
            monitoring.to_log_and_console(proc + msg)
        else:
            input_image = prefix_input + ".lf.inr"

    #
    # copy into 1-byte image
    #
    path_to_exec = _find_exec('copy')
    command_line = path_to_exec

    if isinstance(max_value, int):
        if max_value <= 0:
            msg = ": negative maximal output value?! switch to default behavior."
            monitoring.to_log_and_console(proc + msg)
            command_line += " -norma -o 1 "
        elif max_value <= 255:
            command_line += " -min 0 -max " + str(max_value) + " -o 1 "
        elif max_value <= 65535:
            command_line += " -min 0 -max " + str(max_value) + " -o 2 "
        else:
            msg = ": too large maximal output value?! switch to default behavior."
            monitoring.to_log_and_console(proc + msg)
            command_line += " -norma -o 1 "
    else:
        msg = ": non-integer output value?! switch to default behavior."
        monitoring.to_log_and_console(proc + msg)
        command_line += " -norma -o 1 "

    command_line += input_image + " " + path_output
    _launch_inline_cmd(command_line, monitoring=monitoring)

    return




############################################################
#
# dedicated interfaces: cell pproperties
#
############################################################

def cell_properties(format_input, output, first, last, diagnosis_file=None, n_processors=None,
                    cell_based_sigma=None, other_options=None, monitoring=None):
    """

    :param format_input:
    :param output:
    :param first:
    :param last:
    :param diagnosis_file:
    :param n_processors:
    :param cell_based_sigma:
    :param other_options:
    :param monitoring:
    :return:
    """

    path_to_exec = _find_exec('mc-cellProperties')

    command_line = path_to_exec + " -segmentation-format " + format_input
    command_line += " -output-xml " + output
    command_line += " -first " + str(first) + " -last " + str(last)
    if diagnosis_file is not None:
        command_line += " -diagnosis " + str(diagnosis_file)

    if n_processors is None:
        pass
    elif isinstance(n_processors, int):
        if n_processors >= 1:
            command_line += " -max-chunks-properties " + str(n_processors)
        else:
            pass

    if cell_based_sigma is None:
        pass
    elif isinstance(cell_based_sigma, (int, float)):
        command_line += " -cell-based-sigma " + str(cell_based_sigma)

    #
    # properties to be computed
    #
    prop = " -property volume surface barycenter principal-value principal-vector contact-surface"
    command_line += prop

    #
    #
    #
    if other_options is not None:
        command_line += " " + other_options

    _launch_inline_cmd(command_line, monitoring=monitoring)

    return
