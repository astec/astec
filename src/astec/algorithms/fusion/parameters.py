##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 22 jan 2025 18:04:09 CET
##############################################################
#
#
#
##############################################################

import sys
import os

import astec.utils.common as common


#
#
#
#
#


monitoring = common.Monitoring()


class FusionParameters(common.PrefixedParameter):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix="fusion_"):
        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        doc = "\n"
        doc += "Fusion parameter overview:\n"
        doc += "##########################\n"
        doc += "the fusion of the 4 acquisitions follows a number of steps\n"
        doc += "1. Optionally, a slit line correction.\n"
        doc += "   Some Y lines may appear brighter or darker in the acquisition, which\n"
        doc += "   may cause artifacts in the reconstructed (ie fused) image, which, in\n"
        doc += "   turn, may impair further segmentation steps.\n"
        doc += "2. a change of resolution in the X and Y directions only (Z remains unchanged)\n"
        doc += "   it allows to decrease the data volume if the new pixel size is larger\n"
        doc += "   than the acquisition one\n"
        doc += "3. Optionally, a crop of the resampled acquisitions\n"
        doc += "   it allows to decrease the volume of data\n"
        doc += "   the crop is based on the analysis of a MIP view (in the Z direction) of\n"
        doc += "   the volume\n"
        doc += "4. Optionally, a mirroring of the 'right' image\n"
        doc += "5. Linear registration of the 3 last images on the first one (considered as\n"
        doc += "   the reference). The reference image is resampled again, to get an\n"
        doc += "   isotropic voxel (same voxel size in the 3 directions: X, Y, Z) \n"
        doc += "6. Linear combination of images, weighted by an ad-hoc function\n"
        doc += "   The weighting functions are defined by the 'fusion_weighting' variable.\n"
        doc += "7. Crop of the fused image\n"
        doc += "   still based on the analysis of a MIP view (in the Z direction)\n"
        doc += "\n"
        self.doc['fusion_overview'] = doc

        #
        # acquisition parameters
        #

        doc = "\t possible values are 'left' or 'right'.\n"
        doc += "\t Gives the rotation (with respect to to the Y axis) of the left camera\n"
        doc += "\t frame of stack #0 to be aligned with the the left camera\n"
        doc += "\t frame of stack #1.\n"
        doc += "\t - 'right': +90 degrees\n"
        doc += "\t - 'left': -90 degrees\n"
        doc += "\t See also 'acquisition_mirrors'.\n"
        doc += "\t Since it should depend on the apparatus, this parameter should not\n"
        doc += "\t change for all acquisitions performed by the same microscope.\n"
        self.doc['acquisition_orientation'] = doc
        self.acquisition_orientation = 'left'

        doc = "\t possible values are True or False\n"
        doc += "\t If False, the right camera images are mirrored to make them similar\n"
        doc += "\t to left camera images.\n"
        doc += "\n"
        doc += "\t To determine the configuration (acquisition_orientation, acquisition_mirrors) \n"
        doc += "\t (ie ('left', False), ('left', True), ('right', False), or ('right', True)), \n"
        doc += "\t it is advised to perform the fusion for only one time point (by setting \n"
        doc += "\t 'begin' and 'end' at the same value) with a large 'target_resolution',\n"
        doc += "\t for each of those 4 choices, 'xzsection_extraction' being set to 'True'.\n"
        doc += "\t See also 'acquisition_mirrors'.\n"
        doc += "\t Since it should depend on the apparatus, this parameter should not\n"
        doc += "\t change for all acquisitions performed by the same microscope.\n"
        self.doc['acquisition_mirrors'] = doc
        self.acquisition_mirrors = False

        doc = "\t voxel size of acquired images\n"
        doc += "\t example: acquisition_resolution = (.195, .195, 1.)\n"
        self.doc['acquisition_resolution'] = doc
        self.acquisition_resolution = None

        doc = "\t possible values are 'direct' or 'inverse'.\n"
        doc += "\t Defines where are the high contrasted XZ-sections of the *left* camera\n"
        doc += "\t image of stack0.\n"
        doc += "\t - 'direct': small z are well contrasted (close to the camera), while\n"
        doc += "\t     large z are fuzzy. It is useful for direction-dependent weighting\n"
        doc += "\t     schemes.\n"
        doc += "\t - 'inverse': the other way around.\n"
        doc += "\t Changing 'direct' to 'inverse' (or the other way) implies to change\n"
        doc += "\t 'acquisition_orientation' as well.\n"
        doc += "\t Setting 'xzsection_extraction' to 'True' allows to verify whether \n"
        doc += "\t 'acquisition_stack0_leftcamera_z_stacking' is set to the right value.\n"
        doc += "\t Parameter 'acquisition_leftcamera_z_stacking' will set both\n"
        doc += "\t 'acquisition_stack0_leftcamera_z_stacking' and \n"
        doc += "\t 'acquisition_stack1_leftcamera_z_stacking'.\n"
        self.doc['acquisition_stack0_leftcamera_z_stacking'] = doc
        self.acquisition_stack0_leftcamera_z_stacking = 'direct'
        self.doc['acquisition_stack1_leftcamera_z_stacking'] = doc
        self.acquisition_stack1_leftcamera_z_stacking = 'direct'

        #
        # Correction of slit lines
        #
        doc = "\t Possible values are True or False\n"
        doc += "\t Slit lines are Y lines that appear brighter or darker in the acquisition,\n"
        doc += "\t which may cause artifacts in the reconstructed (ie fused) image, which, in\n"
        doc += "\t turn, may impair further segmentation steps.\n"
        self.doc['acquisition_slit_line_correction'] = doc
        self.acquisition_slit_line_correction = False

        #
        # fused image parameters
        #
        doc = "\t Voxel size of the reconstructed image after fusion of the four views.\n"
        doc += "\t Example: target_resolution = 0.3"
        self.doc['target_resolution'] = doc
        self.target_resolution = (0.3, 0.3, 0.3)

        #
        # fusion method
        #
        doc = "\t Possible values are 'direct-fusion' and 'hierarchical-fusion'\n"
        doc += "\t There are two ways to perform the fusion of the 4 acquisitions:\n"
        doc += "\t - 'direct-fusion': each acquisition is linearly\n"
        doc += "\t   co-registered with the first acquisition (stack #0, left camera). \n"
        doc += "\t   The transformation is computed in two steps:\n"
        doc += "\t   - with parameters prefixed by 'fusion_preregistration_'\n"
        doc += "\t     (by default deactivated).\n"
        doc += "\t   - with parameters prefixed by 'fusion_registration_'\n"
        doc += "\t     (by default 'affine' transformation).\n"
        doc += "\t   Then weights and images are transformed thanks to the \n"
        doc += "\t   computed transformations. Finally a weighted linear combination\n"
        doc += "\t   gives the result.\n"
        doc += "\t - 'hierarchical-fusion':\n"
        doc += "\t   from the couple (left camera, right camera), each stack is reconstructed,\n"
        doc += "\t   following the same scheme than the direct fusion but with only 2 images.\n"
        doc += "\t   (thus with the registration parameters prefixed by\n"
        doc += "\t   'fusion_preregistration_' and 'fusion_registration_'').\n"
        doc += "\t   Then stack#1 is co-registered with stack #0 in two steps:\n"
        doc += "\t   - with parameters prefixed by 'fusion_stack_preregistration_'\n"
        doc += "\t     (by default 'affine' transformation).\n"
        doc += "\t   - with parameters prefixed by 'fusion_stack_registration_'\n"
        doc += "\t     (by default 'vectorfield' transformation).\n"
        doc += "\t   Images and weights associated with stack#1 are then transformed. \n"
        doc += "\t   Finally a weighted linear combination gives the result.\n"
        doc += "\t fusion_preregistration_* and fusion_registration_* parameters control the\n"
        doc += "\t co-registration of two acquisitions. It is either used in the 'direct-fusion'\n"
        doc += "\t method (to co-register each acquisition onto the first one) or in the\n"
        doc += "\t 'hierarchical-fusion' method (to co-register couple of opposite acquisitions\n"
        doc += "\t to reconstruct stacks).\n"
        doc += "\t fusion_stack_preregistration_* and fusion_stack_registration_* parameters\n"
        doc += "\t control the co-registration of two stacks. It is only used in the \n"
        doc += "\t 'hierarchical-fusion' method (to co-register the reconstructed stacks).\n"
        self.doc['fusion_strategy'] = doc
        self.fusion_strategy = 'direct-fusion'

        #
        # Cropping of acquisition images (before fusion)
        #
        doc = "\t Possible values are True or False\n"
        doc += "\t If True, the acquisition images are cropped along the X and Y directions.\n"
        doc += "\t Maximum Intensity Projection (MIP) images are automatically thresholded\n"
        doc += "\t (Otsu algorithm) to determine the bounding box of the object of interest.\n"
        doc += "\t Margins are then added to the bounding box\n"
        doc += "\t - 'acquisition_cropping_margin' allow to set the siz margin values, i.e.\n"
        doc += "\t   'acquisition_cropping_margin_x_0', 'acquisition_cropping_margin_x_1',\n"
        doc += "\t   'acquisition_cropping_margin_y_0', and 'acquisition_cropping_margin_y_1' \n"
        doc += "\t   'acquisition_cropping_margin_z_0', and 'acquisition_cropping_margin_z_1' \n"
        doc += "\t - 'acquisition_cropping_margin_x' allow to set the two margin values along X, i.e.\n"
        doc += "\t   'acquisition_cropping_margin_x_0' and 'acquisition_cropping_margin_x_1',\n"
        doc += "\t - 'acquisition_cropping_margin_y' allow to set the two margin values along Y, i.e.\n"
        doc += "\t   'acquisition_cropping_margin_y_0' and 'acquisition_cropping_margin_y_1',\n"
        doc += "\t - 'acquisition_cropping_margin_z' allow to set the two margin values along Z, i.e.\n"
        doc += "\t   'acquisition_cropping_margin_z_0' and 'acquisition_cropping_margin_z_1',\n"
        self.doc['acquisition_cropping'] = doc
        self.acquisition_cropping = True

        doc = "\t Possible values are True or False\n"
        doc += "\t If True, the acquisition images are also cropped along the Z directions.\n"
        doc += "\t Margins are then added to the bounding box\n"
        self.doc['acquisition_z_cropping'] = doc
        self.acquisition_z_cropping = True

        doc = "\t Added margin of the bounding box computed for the cropping of the raw\n"
        doc += "\t acquisition image in 'left' X direction.\n"
        self.doc['acquisition_cropping_margin_x_0'] = doc
        self.acquisition_cropping_margin_x_0 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the raw\n"
        doc += "\t acquisition image in 'right' X direction.\n"
        self.doc['acquisition_cropping_margin_x_1'] = doc
        self.acquisition_cropping_margin_x_1 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the raw\n"
        doc += "\t acquisition image in 'left' Y direction.\n"
        self.doc['acquisition_cropping_margin_y_0'] = doc
        self.acquisition_cropping_margin_y_0 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the raw\n"
        doc += "\t acquisition image in 'right' Y direction.\n"
        self.doc['acquisition_cropping_margin_y_1'] = doc
        self.acquisition_cropping_margin_y_1 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the raw\n"
        doc += "\t acquisition image in 'left' Z direction.\n"
        self.doc['acquisition_cropping_margin_z_0'] = doc
        self.acquisition_cropping_margin_z_0 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the raw\n"
        doc += "\t acquisition image in 'right' Z direction.\n"
        self.doc['acquisition_cropping_margin_z_1'] = doc
        self.acquisition_cropping_margin_z_1 = 40

        doc = "\t Active when 'acquisition_cropping' is set to 'True'.\n"
        doc += "\t Perform an opening (mathematical morphology) on the thresholded MIP image.\n"
        doc += "\t It may be useful if some structures are connected to the desired one\n"
        doc += "\t (0 means no opening, else specify the structuring element size).\n"
        self.doc['acquisition_cropping_opening'] = doc
        self.acquisition_cropping_opening = 0

        #
        # Registration parameters
        # 1. acquisition_registration[] are the parameters for the registration
        #    of two acquisitions (ie a camera image)
        # 2. stack_registration are the parameters for the registration
        #    of two stacks (ie reconstruction from two opposite cameras)
        #
        self.acquisition_registration = []

        self.acquisition_registration.append(common.RegistrationParameters(prefix=[self._prefix, 'preregistration_']))
        self.acquisition_registration[0].compute_registration = False
        self.acquisition_registration[0].transformation_type = 'translation'

        self.acquisition_registration.append(common.RegistrationParameters(prefix=[self._prefix, 'registration_']))

        self.stack_registration = []

        self.stack_registration.append(common.RegistrationParameters(prefix=[self._prefix, 'stack_', 'preregistration_']))

        self.stack_registration.append(common.RegistrationParameters(prefix=[self._prefix, 'stack_', 'registration_']))
        self.stack_registration[1].transformation_type = 'vectorfield'
        self.stack_registration[1].lts_fraction = 1.0

        #
        #
        #
        doc = "\t Possible values are False, True, an integer or a list of integers.\n"
        doc += "\t If not False, XZ-sections XZ-sections of the co-registered acquisitions,\n"
        doc += "\t as well as the weighting function images, are stored in the directory\n"
        doc += "\t <PATH_EMBRYO>/FUSE/FUSE_<EXP_FUSE>/XZSECTION_<xxxx> where <xxxx> is\n"
        doc += "\t the time point index. It provides a direct and efficient means to check\n"
        doc += "\t whether the parameters 'acquisition_orientation', 'acquisition_mirrors'\n"
        doc += "\t 'acquisition_leftcamera_z_stacking' are correctly set.\n"
        doc += "\t If the value is an integer or a list of integers, it indicates the indices\n"
        doc += "\t of XZ-sections to be extracted.\n"
        self.doc['xzsection_extraction'] = doc
        self.xzsection_extraction = False

        #
        #
        #
        doc = "\t Possible values are False or True.\n"
        doc += "\t If True, the computed transformations are kept in directories\n"
        doc += "\t <PATH_EMBRYO>/FUSE/FUSE_<EXP_FUSE>/TRSF_<xxxx> where <xxxx> is"
        doc += "\t the time point index.\n"
        self.doc['keep_transformation'] = doc
        self.keep_transformation = False

        #
        # Cropping of fused image (after fusion)
        #
        doc = "\t Possible values are True or False\n"
        doc += "\t If True, the fusion image is cropped along the X and Y directions.\n"
        doc += "\t Maximum Intensity Projection (MIP) images are automatically thresholded\n"
        doc += "\t (Otsu algorithm) to determine the bounding box of the object of interest.\n"
        doc += "\t Margins are then added to the bounding box\n"
        doc += "\t - 'fusion_cropping_margin' allow to set the six margin values, i.e.\n"
        doc += "\t   'fusion_cropping_margin_x_0', 'fusion_cropping_margin_x_1',\n"
        doc += "\t   'fusion_cropping_margin_y_0', and 'fusion_cropping_margin_y_1', \n"
        doc += "\t   'fusion_cropping_margin_z_0', and 'fusion_cropping_margin_z_1' \n"
        doc += "\t - 'fusion_cropping_margin_x' allow to set the two margin values along X, i.e.\n"
        doc += "\t   'fusion_cropping_margin_x_0' and 'fusion_cropping_margin_x_1',\n"
        doc += "\t - 'fusion_cropping_margin_y' allow to set the two margin values along Y, i.e.\n"
        doc += "\t   'fusion_cropping_margin_y_0' and 'fusion_cropping_margin_y_1',\n"
        doc += "\t - 'fusion_cropping_margin_z' allow to set the two margin values along Z, i.e.\n"
        doc += "\t   'fusion_cropping_margin_z_0' and 'fusion_cropping_margin_z_1',\n"
        self.doc['fusion_cropping'] = doc
        self.fusion_cropping = True

        doc = "\t Possible values are True or False\n"
        doc += "\t If True, the fusion image is also cropped along the Z direction.\n"
        doc += "\t Margins are then added to the bounding box\n"
        self.doc['fusion_z_cropping'] = doc
        self.fusion_z_cropping = True

        doc = "\t Added margin of the bounding box computed for the cropping of the fusion\n"
        doc += "\t image in 'left' X direction.\n"
        self.doc['fusion_cropping_margin_x_0'] = doc
        self.fusion_cropping_margin_x_0 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the fusion\n"
        doc += "\t image in 'right' X direction.\n"
        self.doc['fusion_cropping_margin_x_1'] = doc
        self.fusion_cropping_margin_x_1 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the fusion\n"
        doc += "\t image in 'left' Y direction.\n"
        self.doc['fusion_cropping_margin_y_0'] = doc
        self.fusion_cropping_margin_y_0 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the fusion\n"
        doc += "\t image in 'right' Y direction.\n"
        self.doc['fusion_cropping_margin_y_1'] = doc
        self.fusion_cropping_margin_y_1 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the fusion\n"
        doc += "\t image in 'left' Z direction.\n"
        self.doc['fusion_cropping_margin_z_0'] = doc
        self.fusion_cropping_margin_z_0 = 40
        doc = "\t Added margin of the bounding box computed for the cropping of the fusion\n"
        doc += "\t image in 'right' Z direction.\n"
        self.doc['fusion_cropping_margin_z_1'] = doc
        self.fusion_cropping_margin_z_1 = 40

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# FusionParameters')
        print('#')
        print("")

        for line in self.doc['fusion_overview'].splitlines():
            print('# ' + line)

        common.PrefixedParameter.print_parameters(self)

        self.varprint('acquisition_orientation', self.acquisition_orientation, self.doc['acquisition_orientation'])
        self.varprint('acquisition_mirrors', self.acquisition_mirrors, self.doc['acquisition_mirrors'])
        self.varprint('acquisition_resolution', self.acquisition_resolution, self.doc['acquisition_resolution'])

        self.varprint('acquisition_stack0_leftcamera_z_stacking', self.acquisition_stack0_leftcamera_z_stacking,
                      self.doc['acquisition_stack0_leftcamera_z_stacking'])
        self.varprint('acquisition_stack1_leftcamera_z_stacking', self.acquisition_stack1_leftcamera_z_stacking,
                      self.doc['acquisition_stack1_leftcamera_z_stacking'])

        self.varprint('acquisition_slit_line_correction', self.acquisition_slit_line_correction,
                      self.doc['acquisition_slit_line_correction'])

        self.varprint('target_resolution', self.target_resolution, self.doc['target_resolution'])

        self.varprint('fusion_strategy', self.fusion_strategy, self.doc['fusion_strategy'])

        self.varprint('acquisition_cropping', self.acquisition_cropping, self.doc['acquisition_cropping'])
        self.varprint('acquisition_z_cropping', self.acquisition_z_cropping, self.doc['acquisition_z_cropping'])
        self.varprint('acquisition_cropping_margin_x_0', self.acquisition_cropping_margin_x_0,
                      self.doc['acquisition_cropping_margin_x_0'])
        self.varprint('acquisition_cropping_margin_x_1', self.acquisition_cropping_margin_x_1,
                      self.doc['acquisition_cropping_margin_x_1'])
        self.varprint('acquisition_cropping_margin_y_0', self.acquisition_cropping_margin_y_0,
                      self.doc['acquisition_cropping_margin_y_0'])
        self.varprint('acquisition_cropping_margin_y_1', self.acquisition_cropping_margin_y_1,
                      self.doc['acquisition_cropping_margin_y_1'])
        self.varprint('acquisition_cropping_margin_z_0', self.acquisition_cropping_margin_z_0,
                      self.doc['acquisition_cropping_margin_z_0'])
        self.varprint('acquisition_cropping_margin_z_1', self.acquisition_cropping_margin_z_1,
                      self.doc['acquisition_cropping_margin_z_1'])

        self.varprint('acquisition_cropping_opening', self.acquisition_cropping_opening,
                      self.doc['acquisition_cropping_opening'])

        for p in self.acquisition_registration:
            p.print_parameters()
        for p in self.stack_registration:
            p.print_parameters()

        self.varprint('xzsection_extraction', self.xzsection_extraction, self.doc['xzsection_extraction'])
        self.varprint('keep_transformation', self.keep_transformation, self.doc['keep_transformation'])

        self.varprint('fusion_cropping', self.fusion_cropping, self.doc['fusion_cropping'])
        self.varprint('fusion_z_cropping', self.fusion_z_cropping, self.doc['fusion_z_cropping'])
        self.varprint('fusion_cropping_margin_x_0', self.fusion_cropping_margin_x_0,
                      self.doc['fusion_cropping_margin_x_0'])
        self.varprint('fusion_cropping_margin_x_1', self.fusion_cropping_margin_x_1,
                      self.doc['fusion_cropping_margin_x_1'])
        self.varprint('fusion_cropping_margin_y_0', self.fusion_cropping_margin_y_0,
                      self.doc['fusion_cropping_margin_y_0'])
        self.varprint('fusion_cropping_margin_y_1', self.fusion_cropping_margin_y_1,
                      self.doc['fusion_cropping_margin_y_1'])
        self.varprint('fusion_cropping_margin_z_0', self.fusion_cropping_margin_z_0,
                      self.doc['fusion_cropping_margin_z_0'])
        self.varprint('fusion_cropping_margin_z_1', self.fusion_cropping_margin_z_1,
                      self.doc['fusion_cropping_margin_z_1'])
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("" + "\n")
        logfile.write('#' + "\n")
        logfile.write('# FusionParameters' + "\n")
        logfile.write('#' + "\n")
        logfile.write("" + "\n")

        for line in self.doc['fusion_overview'].splitlines():
            logfile.write('# ' + line + '\n')

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'acquisition_orientation', self.acquisition_orientation,
                      self.doc['acquisition_orientation'])
        self.varwrite(logfile, 'acquisition_mirrors', self.acquisition_mirrors, self.doc['acquisition_mirrors'])
        self.varwrite(logfile, 'acquisition_resolution', self.acquisition_resolution,
                      self.doc['acquisition_resolution'])

        self.varwrite(logfile, 'acquisition_stack0_leftcamera_z_stacking',
                      self.acquisition_stack0_leftcamera_z_stacking,
                      self.doc['acquisition_stack0_leftcamera_z_stacking'])
        self.varwrite(logfile, 'acquisition_stack1_leftcamera_z_stacking',
                      self.acquisition_stack1_leftcamera_z_stacking,
                      self.doc['acquisition_stack1_leftcamera_z_stacking'])

        self.varwrite(logfile, 'acquisition_slit_line_correction', self.acquisition_slit_line_correction,
                      self.doc['acquisition_slit_line_correction'])

        self.varwrite(logfile, 'target_resolution', self.target_resolution, self.doc['target_resolution'])

        self.varwrite(logfile, 'fusion_strategy', self.fusion_strategy, self.doc['fusion_strategy'])

        self.varwrite(logfile, 'acquisition_cropping', self.acquisition_cropping, self.doc['acquisition_cropping'])
        self.varwrite(logfile, 'acquisition_z_cropping', self.acquisition_z_cropping,
                      self.doc['acquisition_z_cropping'])
        self.varwrite(logfile, 'acquisition_cropping_margin_x_0', self.acquisition_cropping_margin_x_0,
                      self.doc['acquisition_cropping_margin_x_0'])
        self.varwrite(logfile, 'acquisition_cropping_margin_x_1', self.acquisition_cropping_margin_x_1,
                      self.doc['acquisition_cropping_margin_x_1'])
        self.varwrite(logfile, 'acquisition_cropping_margin_y_0', self.acquisition_cropping_margin_y_0,
                      self.doc['acquisition_cropping_margin_y_0'])
        self.varwrite(logfile, 'acquisition_cropping_margin_y_1', self.acquisition_cropping_margin_y_1,
                      self.doc['acquisition_cropping_margin_y_1'])
        self.varwrite(logfile, 'acquisition_cropping_margin_z_0', self.acquisition_cropping_margin_z_0,
                      self.doc['acquisition_cropping_margin_z_0'])
        self.varwrite(logfile, 'acquisition_cropping_margin_z_1', self.acquisition_cropping_margin_z_1,
                      self.doc['acquisition_cropping_margin_z_1'])

        self.varwrite(logfile, 'acquisition_cropping_opening', self.acquisition_cropping_opening,
                      self.doc['acquisition_cropping_opening'])

        for p in self.acquisition_registration:
            p.write_parameters_in_file(logfile)
        for p in self.stack_registration:
            p.write_parameters_in_file(logfile)

        self.varwrite(logfile, 'xzsection_extraction', self.xzsection_extraction, self.doc['xzsection_extraction'])
        self.varwrite(logfile, 'keep_transformation', self.keep_transformation, self.doc['keep_transformation'])

        self.varwrite(logfile, 'fusion_cropping', self.fusion_cropping, self.doc['fusion_cropping'])
        self.varwrite(logfile, 'fusion_z_cropping', self.fusion_z_cropping, self.doc['fusion_z_cropping'])
        self.varwrite(logfile, 'fusion_cropping_margin_x_0', self.fusion_cropping_margin_x_0,
                      self.doc['fusion_cropping_margin_x_0'])
        self.varwrite(logfile, 'fusion_cropping_margin_x_1', self.fusion_cropping_margin_x_1,
                      self.doc['fusion_cropping_margin_x_1'])
        self.varwrite(logfile, 'fusion_cropping_margin_y_0', self.fusion_cropping_margin_y_0,
                      self.doc['fusion_cropping_margin_y_0'])
        self.varwrite(logfile, 'fusion_cropping_margin_y_1', self.fusion_cropping_margin_y_1,
                      self.doc['fusion_cropping_margin_y_1'])
        self.varwrite(logfile, 'fusion_cropping_margin_z_0', self.fusion_cropping_margin_z_0,
                      self.doc['fusion_cropping_margin_z_0'])
        self.varwrite(logfile, 'fusion_cropping_margin_z_1', self.fusion_cropping_margin_z_1,
                      self.doc['fusion_cropping_margin_z_1'])
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):
        #
        # acquisition parameters
        #
        self.acquisition_orientation = self.read_parameter(parameters, 'raw_ori', self.acquisition_orientation)
        self.acquisition_orientation = self.read_parameter(parameters, 'acquisition_orientation',
                                                           self.acquisition_orientation)

        self.acquisition_mirrors = self.read_parameter(parameters, 'raw_mirrors', self.acquisition_mirrors)
        self.acquisition_mirrors = self.read_parameter(parameters, 'acquisition_mirrors', self.acquisition_mirrors)

        self.acquisition_stack0_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'raw_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)
        self.acquisition_stack0_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'acquisition_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)
        self.acquisition_stack1_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'raw_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)
        self.acquisition_stack1_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'acquisition_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)

        self.acquisition_stack0_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'raw_stack0_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)
        self.acquisition_stack0_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'acquisition_stack0_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)
        self.acquisition_stack1_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'raw_stack1_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)
        self.acquisition_stack1_leftcamera_z_stacking = self.read_parameter(parameters,
                                                                            'acquisition_stack1_leftcamera_z_stacking',
                                                                            self.acquisition_stack0_leftcamera_z_stacking)

        if hasattr(parameters, 'raw_resolution'):
            if parameters.raw_resolution is not None:
                if type(parameters.raw_resolution) is tuple or type(parameters.raw_resolution) is list:
                    if len(parameters.raw_resolution) == 3:
                        self.acquisition_resolution = parameters.raw_resolution
                    else:
                        monitoring.to_log_and_console("Error in reading parameters")
                        monitoring.to_log_and_console("\t 'raw_resolution' has length " +
                                                      str(len(parameters.raw_resolution)) + " instead of 3.")
                        monitoring.to_log_and_console("\t Exiting.")
                        sys.exit(1)
                else:
                    monitoring.to_log_and_console("Error in reading parameters")
                    monitoring.to_log_and_console("\t type of 'raw_resolution' (" +
                                                  str(type(parameters.raw_resolution)) + ") is not handled")
                    monitoring.to_log_and_console("\t Exiting.")
                    sys.exit(1)
        elif hasattr(parameters, 'acquisition_resolution'):
            if parameters.acquisition_resolution is not None:
                if type(parameters.acquisition_resolution) is tuple or type(parameters.acquisition_resolution) is list:
                    if len(parameters.acquisition_resolution) == 3:
                        self.acquisition_resolution = parameters.acquisition_resolution
                    else:
                        monitoring.to_log_and_console("Error in reading parameters")
                        monitoring.to_log_and_console("\t 'acquisition_resolution' has length " +
                                                      str(len(parameters.acquisition_resolution)) + " instead of 3.")
                        monitoring.to_log_and_console("\t Exiting.")
                        sys.exit(1)
                else:
                    monitoring.to_log_and_console("Error in reading parameters")
                    monitoring.to_log_and_console("\t type of 'acquisition_resolution' (" +
                                                  str(type(parameters.acquisition_resolution)) + ") is not handled")
                    monitoring.to_log_and_console("\t Exiting.")
                    sys.exit(1)

        #
        # correction of slit lines
        #
        self.acquisition_slit_line_correction = self.read_parameter(parameters, 'acquisition_slit_line_correction',
                                                                    self.acquisition_slit_line_correction)

        #
        # fused image parameters
        #
        self.target_resolution = self.read_parameter(parameters, 'target_resolution', self.target_resolution)

        #
        # fusion method
        #
        self.fusion_strategy = self.read_parameter(parameters, 'fusion_strategy', self.fusion_strategy)
        self.fusion_strategy = self.read_parameter(parameters, 'fusion_method', self.fusion_strategy)

        #
        # Cropping of acquisition images (before fusion)
        #
        self.acquisition_cropping = self.read_parameter(parameters, 'acquisition_cropping', self.acquisition_cropping)
        self.acquisition_z_cropping = self.read_parameter(parameters, 'acquisition_z_cropping',
                                                          self.acquisition_z_cropping)
        self.acquisition_cropping = self.read_parameter(parameters, 'raw_crop', self.acquisition_cropping)
        self.acquisition_z_cropping = self.read_parameter(parameters, 'raw_z_crop', self.acquisition_z_cropping)

        self.acquisition_cropping_margin_x_0 = self.read_parameter(parameters, 'acquisition_cropping_margin',
                                                                   self.acquisition_cropping_margin_x_0)
        self.acquisition_cropping_margin_x_0 = self.read_parameter(parameters, 'acquisition_cropping_margin_x',
                                                                   self.acquisition_cropping_margin_x_0)
        self.acquisition_cropping_margin_x_0 = self.read_parameter(parameters, 'acquisition_cropping_margin_x_0',
                                                                   self.acquisition_cropping_margin_x_0)
        self.acquisition_cropping_margin_x_0 = self.read_parameter(parameters, 'raw_margin_x_0',
                                                                   self.acquisition_cropping_margin_x_0)

        self.acquisition_cropping_margin_x_1 = self.read_parameter(parameters, 'acquisition_cropping_margin',
                                                                   self.acquisition_cropping_margin_x_1)
        self.acquisition_cropping_margin_x_1 = self.read_parameter(parameters, 'acquisition_cropping_margin_x',
                                                                   self.acquisition_cropping_margin_x_1)
        self.acquisition_cropping_margin_x_1 = self.read_parameter(parameters, 'acquisition_cropping_margin_x_1',
                                                                   self.acquisition_cropping_margin_x_1)
        self.acquisition_cropping_margin_x_1 = self.read_parameter(parameters, 'raw_margin_x_1',
                                                                   self.acquisition_cropping_margin_x_1)

        self.acquisition_cropping_margin_y_0 = self.read_parameter(parameters, 'acquisition_cropping_margin',
                                                                   self.acquisition_cropping_margin_y_0)
        self.acquisition_cropping_margin_y_0 = self.read_parameter(parameters, 'acquisition_cropping_margin_y',
                                                                   self.acquisition_cropping_margin_y_0)
        self.acquisition_cropping_margin_y_0 = self.read_parameter(parameters, 'acquisition_cropping_margin_y_0',
                                                                   self.acquisition_cropping_margin_y_0)
        self.acquisition_cropping_margin_y_0 = self.read_parameter(parameters, 'raw_margin_y_0',
                                                                   self.acquisition_cropping_margin_y_0)

        self.acquisition_cropping_margin_y_1 = self.read_parameter(parameters, 'acquisition_cropping_margin',
                                                                   self.acquisition_cropping_margin_y_1)
        self.acquisition_cropping_margin_y_1 = self.read_parameter(parameters, 'acquisition_cropping_margin_y',
                                                                   self.acquisition_cropping_margin_y_1)
        self.acquisition_cropping_margin_y_1 = self.read_parameter(parameters, 'acquisition_cropping_margin_y_1',
                                                                   self.acquisition_cropping_margin_y_1)
        self.acquisition_cropping_margin_y_1 = self.read_parameter(parameters, 'raw_margin_y_1',
                                                                   self.acquisition_cropping_margin_y_1)

        self.acquisition_cropping_margin_z_0 = self.read_parameter(parameters, 'acquisition_cropping_margin',
                                                                   self.acquisition_cropping_margin_z_0)
        self.acquisition_cropping_margin_z_0 = self.read_parameter(parameters, 'acquisition_cropping_margin_z',
                                                                   self.acquisition_cropping_margin_z_0)
        self.acquisition_cropping_margin_z_0 = self.read_parameter(parameters, 'acquisition_cropping_margin_z_0',
                                                                   self.acquisition_cropping_margin_z_0)
        self.acquisition_cropping_margin_z_0 = self.read_parameter(parameters, 'raw_margin_z_0',
                                                                   self.acquisition_cropping_margin_z_0)

        self.acquisition_cropping_margin_z_1 = self.read_parameter(parameters, 'acquisition_cropping_margin',
                                                                   self.acquisition_cropping_margin_z_1)
        self.acquisition_cropping_margin_z_1 = self.read_parameter(parameters, 'acquisition_cropping_margin_z',
                                                                   self.acquisition_cropping_margin_z_1)
        self.acquisition_cropping_margin_z_1 = self.read_parameter(parameters, 'acquisition_cropping_margin_z_1',
                                                                   self.acquisition_cropping_margin_z_1)
        self.acquisition_cropping_margin_z_1 = self.read_parameter(parameters, 'raw_margin_z_1',
                                                                   self.acquisition_cropping_margin_z_1)

        self.acquisition_cropping_opening = self.read_parameter(parameters, 'acquisition_cropping_opening',
                                                                self.acquisition_cropping_opening)

        #
        # registration parameters
        #
        for p in self.acquisition_registration:
            p.update_from_parameters(parameters)
        for p in self.stack_registration:
            p.update_from_parameters(parameters)

        #
        #
        #
        self.xzsection_extraction = self.read_parameter(parameters, 'xzsection_extraction', self.xzsection_extraction)
        self.xzsection_extraction = self.read_parameter(parameters, 'fusion_xzsection_extraction',
                                                        self.xzsection_extraction)

        self.keep_transformation = self.read_parameter(parameters, 'keep_transformation', self.keep_transformation)
        self.keep_transformation = self.read_parameter(parameters, 'fusion_keep_transformation',
                                                       self.keep_transformation)

        #
        # Cropping of fused image (after fusion)
        #
        self.fusion_cropping = self.read_parameter(parameters, 'fusion_cropping', self.fusion_cropping)
        self.fusion_z_cropping = self.read_parameter(parameters, 'fusion_z_cropping', self.fusion_z_cropping)
        self.fusion_cropping = self.read_parameter(parameters, 'fusion_crop', self.fusion_cropping)
        self.fusion_z_cropping = self.read_parameter(parameters, 'fusion_z_crop', self.fusion_z_cropping)

        self.fusion_cropping_margin_x_0 = self.read_parameter(parameters, 'fusion_cropping_margin',
                                                              self.fusion_cropping_margin_x_0)
        self.fusion_cropping_margin_x_0 = self.read_parameter(parameters, 'fusion_cropping_margin_x',
                                                              self.fusion_cropping_margin_x_0)
        self.fusion_cropping_margin_x_0 = self.read_parameter(parameters, 'fusion_cropping_margin_x_0',
                                                              self.fusion_cropping_margin_x_0)
        self.fusion_cropping_margin_x_0 = self.read_parameter(parameters, 'fusion_margin_x_0',
                                                              self.fusion_cropping_margin_x_0)

        self.fusion_cropping_margin_x_1 = self.read_parameter(parameters, 'fusion_cropping_margin',
                                                              self.fusion_cropping_margin_x_1)
        self.fusion_cropping_margin_x_1 = self.read_parameter(parameters, 'fusion_cropping_margin_x',
                                                              self.fusion_cropping_margin_x_1)
        self.fusion_cropping_margin_x_1 = self.read_parameter(parameters, 'fusion_cropping_margin_x_1',
                                                              self.fusion_cropping_margin_x_1)
        self.fusion_cropping_margin_x_1 = self.read_parameter(parameters, 'fusion_margin_x_1',
                                                              self.fusion_cropping_margin_x_1)

        self.fusion_cropping_margin_y_0 = self.read_parameter(parameters, 'fusion_cropping_margin',
                                                              self.fusion_cropping_margin_y_0)
        self.fusion_cropping_margin_y_0 = self.read_parameter(parameters, 'fusion_cropping_margin_y',
                                                              self.fusion_cropping_margin_y_0)
        self.fusion_cropping_margin_y_0 = self.read_parameter(parameters, 'fusion_cropping_margin_y_0',
                                                              self.fusion_cropping_margin_y_0)
        self.fusion_cropping_margin_y_0 = self.read_parameter(parameters, 'fusion_margin_y_0',
                                                              self.fusion_cropping_margin_y_0)

        self.fusion_cropping_margin_y_1 = self.read_parameter(parameters, 'fusion_cropping_margin',
                                                              self.fusion_cropping_margin_y_1)
        self.fusion_cropping_margin_y_1 = self.read_parameter(parameters, 'fusion_cropping_margin_y',
                                                              self.fusion_cropping_margin_y_1)
        self.fusion_cropping_margin_y_1 = self.read_parameter(parameters, 'fusion_cropping_margin_y_1',
                                                              self.fusion_cropping_margin_y_1)
        self.fusion_cropping_margin_y_1 = self.read_parameter(parameters, 'fusion_margin_y_1',
                                                              self.fusion_cropping_margin_y_1)

        self.fusion_cropping_margin_z_0 = self.read_parameter(parameters, 'fusion_cropping_margin',
                                                              self.fusion_cropping_margin_z_0)
        self.fusion_cropping_margin_z_0 = self.read_parameter(parameters, 'fusion_cropping_margin_z',
                                                              self.fusion_cropping_margin_z_0)
        self.fusion_cropping_margin_z_0 = self.read_parameter(parameters, 'fusion_cropping_margin_z_0',
                                                              self.fusion_cropping_margin_z_0)
        self.fusion_cropping_margin_z_0 = self.read_parameter(parameters, 'fusion_margin_z_0',
                                                              self.fusion_cropping_margin_z_0)

        self.fusion_cropping_margin_z_1 = self.read_parameter(parameters, 'fusion_cropping_margin',
                                                              self.fusion_cropping_margin_z_1)
        self.fusion_cropping_margin_z_1 = self.read_parameter(parameters, 'fusion_cropping_margin_z',
                                                              self.fusion_cropping_margin_z_1)
        self.fusion_cropping_margin_z_1 = self.read_parameter(parameters, 'fusion_cropping_margin_z_1',
                                                              self.fusion_cropping_margin_z_1)
        self.fusion_cropping_margin_z_1 = self.read_parameter(parameters, 'fusion_margin_z_1',
                                                              self.fusion_cropping_margin_z_1)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)
