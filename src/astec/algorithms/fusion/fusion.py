##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#
##############################################################
#
#
#
##############################################################

import os
import sys
import time
import platform
import subprocess
import shutil
import numpy as np

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.components.threading import waitForRunningThreadToStop
from astec.components.spatial_image import SpatialImage
from astec.io.image import imread, imsave

import astec.algorithms.fusion.direct as fdirect
import astec.algorithms.fusion.hierarchical as fhierarchical
import astec.algorithms.fusion.parameters as fparameters
import astec.algorithms.fusion.registration as fregistration
import astec.algorithms.fusion.utils as futils
import astec.algorithms.fusion.weight as fweight


#
#
#
#
#


monitoring = common.Monitoring()


########################################################################################
#
# some internal procedures
#
########################################################################################


__extension_to_be_converted__ = ['.h5', '.h5.gz', '.tif', '.tiff', '.TIF', '.TIFF']
__extension_with_resolution__ = ['.inr', '.inr.gz', '.mha', '.mha.gz']


def _read_image_name(data_path, temporary_path, file_name, resolution, default_extension='inr'):
    """
    Read an image. Eventually, unzip a compressed file, and convert the image
    to a format known by executables
    :param data_path: path to data directory
    :param temporary_path: directory for temporary file
    :param file_name: image file
    :param resolution: resolution of the result image
            required to write the output image with the right resolution
    :return:
    """

    proc = "_read_image_name"

    #
    # test whether the extension is zip
    #
    f = file_name
    full_name = os.path.join(data_path, f)

    if f[len(f)-4:len(f)] == '.zip':

        prefix = f[0:len(f)-4]

        #
        # maybe the file has already be unzipped
        #
        file_names = []
        for f in os.listdir(temporary_path):
            if len(f) <= len(prefix):
                pass
            if f[0:len(prefix)] == prefix:
                if f[len(prefix):len(f)] in common.recognized_image_extensions:
                    file_names.append(f)

        if len(file_names) > 1:
            monitoring.to_log_and_console(proc + ": already several images with name '"
                                          + str(prefix) + "' were found in '" + str(temporary_path) + "'")
            monitoring.to_log_and_console("\t " + str(file_names))
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)

        elif len(file_names) == 0:
            #
            # unzipping
            #
            monitoring.to_log_and_console("    .. unzipping '" + str(f) + "'", 2)
            #
            # there are issues with unzip
            # seems to occur when files are zipped with zip 3.0
            #
            if platform.system() == 'Linux':
                command_line = 'unzip ' + os.path.join(data_path, f) + ' -d ' + str(temporary_path)
            elif platform.system() == 'Darwin':
                command_line = 'tar xvf ' + os.path.join(data_path, f) + ' -C ' + str(temporary_path)
            else:
                command_line = 'unzip ' + os.path.join(data_path, f) + ' -d ' + str(temporary_path)
            if monitoring.verbose >= 3 or monitoring.debug > 0:
                monitoring.to_log("* Launch: " + command_line)
                with open(monitoring.log_filename, 'a') as logfile:
                    subprocess.call(command_line, shell=True, stdout=logfile, stderr=subprocess.STDOUT)
            else:
                subprocess.call(command_line, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            #
            # parsing again the temporary directory
            #
            file_names = []
            for f in os.listdir(temporary_path):
                if len(f) <= len(prefix):
                    pass
                if f[0:len(prefix)] == prefix:
                    file_names.append(f)

        if len(file_names) == 0:
            monitoring.to_log_and_console(proc + ": no image with name '" + str(prefix)
                                          + "' was found in '" + str(temporary_path) + "'")
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)

        if len(file_names) > 1:
            monitoring.to_log_and_console(proc + ": several images with name '"
                                          + str(prefix) + "' were found in '" + str(temporary_path) + "'")
            monitoring.to_log_and_console("\t " + str(file_names))
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)
        #
        #
        #
        f = file_names[0]
        full_name = os.path.join(temporary_path, f)

    #
    # test whether the file has to be converted into a more 'readable' format
    # if yes, set the resolution if required
    #

    file_has_been_converted = False
    for extension in __extension_to_be_converted__:
        if f[len(f)-len(extension):len(f)] == extension:
            prefix = f[0:len(f) - len(extension)]

            #
            # new file name
            # check whether it has already been converted
            #
            new_full_name = os.path.join(temporary_path, prefix) + '.' + str(default_extension)
            if not os.path.isfile(new_full_name):
                monitoring.to_log_and_console("    .. converting '" + str(f) + "'", 2)
                image = imread(full_name)
                if type(resolution) is tuple and len(resolution) == 3:
                    image.voxelsize = resolution
                    monitoring.to_log("    * resolution of '" + full_name + "' has been set to "
                                      + str(image.voxelsize))
                elif type(resolution) is list and len(resolution) == 3:
                    image.voxelsize = (resolution[0], resolution[1], resolution[2])
                    monitoring.to_log("    * resolution of '" + full_name + "' has been set to "
                                      + str(image.voxelsize))
                else:
                    monitoring.to_log("    * resolution of '" + full_name + "' is " + str(image.voxelsize)
                                      + "(default/read values)")
                #
                # remove unzipped file to avoid having two files in the directory
                # verify that it is not the input file!
                #
                if os.path.dirname(full_name) == temporary_path:
                    os.remove(full_name)

                #
                # save converted file
                #
                imsave(new_full_name, image)

            if default_extension in __extension_with_resolution__:
                file_has_been_converted = True

            full_name = new_full_name
            f = os.path.split(full_name)[-1]

            break

    #
    # test whether the input format is supposed to have the resolution set
    #

    if file_has_been_converted is False:
        for extension in __extension_with_resolution__:
            if f[len(f) - len(extension):len(f)] == extension:
                file_has_been_converted = True
                break

    #
    # if no conversion occurs, the resolution has not been set yet
    #

    if file_has_been_converted is False:
        if (type(resolution) is tuple or type(resolution) is list) and len(resolution) == 3:
            monitoring.to_log_and_console("    .. changing resolution of '" + str(f) + "'", 2)
            image = imread(full_name)
            if type(resolution) is tuple and len(resolution) == 3:
                image.voxelsize = resolution
                monitoring.to_log("    * resolution of '" + full_name + "' has been set to " + str(image.voxelsize))
            elif type(resolution) is list and len(resolution) == 3:
                image.voxelsize = (resolution(0), resolution(1), resolution(2))
                monitoring.to_log("    * resolution of '" + full_name + "' has been set to " + str(image.voxelsize))
            imsave(full_name, image)
        else:
            monitoring.to_log("    * resolution of '" + full_name + "' is left unchanged")

    return full_name


def _analyze_data_directory(data_dir, callfrom=None):
    """
    Parse a directory containing images
    :param data_dir:
    :return:
    1. the common prefix of image file names
    2. the number of characters used to encoded the variable part
       (time points)
    3. the list of the variable parts
    4. the common suffix of image file names
       may be longer than just the file extension
    """

    proc = "_analyze_data_directory"
    images = []
    extensions = []
    #
    # recognize images and extensions
    #
    for f in os.listdir(data_dir):
        for e in common.recognized_image_extensions:
            if f[len(f)-len(e):len(f)] == e:
                if e not in extensions:
                    extensions.append(e)
                images.append(f)
                #
                # if we found .lux.h5, do not test .h5
                #
                break
        if len(extensions) > 1:
            msg = ": several image extensions were found in '" + data_dir + "'"
            monitoring.to_log_and_console(proc + msg)
            monitoring.to_log_and_console("\t -> " + str(extensions))
            if callfrom is not None:
                monitoring.to_log_and_console("\t was called from '" + str(callfrom) + "'")
            monitoring.to_log_and_console("\t Exiting.")
            sys.exit(1)

    if len(images) == 0:
        monitoring.to_log_and_console(proc + ": no images were found in '" + data_dir + "'")
        monitoring.to_log_and_console("\t Exiting.")
        sys.exit(1)

    #
    # one image case
    #

    if len(images) == 1:
        if monitoring.debug > 0:
            msg = proc + ": only one image was found in '" + data_dir + "'"
            monitoring.to_log_and_console(msg)
        suffix = extensions[0]
        im = images[0]
        length = len(im) - 1 - len(suffix)
        # is there an additional suffix before the image extension?
        add_suffix = 0
        while add_suffix <= length:
            if '0' <= im[length - add_suffix] <= '9':
                break
            add_suffix = add_suffix + 1
        if add_suffix > 0:
            if monitoring.debug > 0:
                msg = proc + ": additional suffix of length " + str(add_suffix)
                msg += ": '" + im[len(im) - add_suffix - len(suffix):len(im) - len(suffix)] + "'"
                monitoring.to_log_and_console(msg)
        # try to figure out what the length of time encoding (ie number of digits)
        time_length = 0
        while length - add_suffix - time_length > 0:
            if '0' <= im[length - add_suffix - time_length] <= '9':
                time_length += 1
            else:
                break
        prefix = im[0:len(im) - time_length - add_suffix - len(suffix)]
        time_points = im[len(im) - time_length - add_suffix - len(suffix):len(im) - add_suffix - len(suffix)]
        suffix = im[len(im) - add_suffix - len(suffix):]
        return prefix, time_length, time_points, suffix

    #
    # several images
    # 1. check that image names are of the same length
    # 2. get prefix = common part at beginning
    # 3. get suffix = common part at end
    # 4. get length for time point encoding
    # 5. get list of time points
    #

    for i in range(1, len(images)):
        if len(images[0]) != len(images[i]):
            monitoring.to_log_and_console(proc + ": image names are of different lengths in '" + data_dir + "'")
            monitoring.to_log_and_console("\t -> " + images[0] + ", " + images[i])
            monitoring.to_log_and_console("\t Exiting.")
            sys.exit(1)

    prefix = ''
    for j in range(0, len(images[0])):
        ok = True
        for i in range(1, len(images)):
            if images[0][j] != images[i][j]:
                ok = False
                break
        if ok is True:
            prefix += images[0][j]
        else:
            break

    suffix = ''
    for j in range(len(images[0]) - 1, -1, -1):
        ok = True
        for i in range(1, len(images)):
            if images[0][j] != images[i][j]:
                ok = False
                break
        if ok is True:
            suffix += images[0][j]
        else:
            break
    suffix = suffix[::-1]

    time_length = len(images[0]) - len(prefix) - len(suffix)

    time_points = []
    for i in range(0, len(images)):
        time_points.append(images[i][len(images[i]) - time_length - len(suffix):len(images[i]) - len(suffix)])

    return prefix, time_length, time_points, suffix


########################################################################################
#
#
#
########################################################################################


def _mirroring_voxel_matrix(shape, x_mirroring=False, y_mirroring=False, z_mirroring=False):
    mat = np.identity(4)
    if x_mirroring is True:
        mat[0, 0] = -1
        mat[0, 3] = shape[0] - 1
    if y_mirroring is True:
        mat[1, 1] = -1
        mat[1, 3] = shape[1] - 1
    if z_mirroring is True:
        mat[2, 2] = -1
        mat[2, 3] = shape[2] - 1
    return mat


def _mirroring_real_matrix(shape, from_voxel, to_voxel, x_mirroring=False, y_mirroring=False, z_mirroring=False):
    #
    # mirroring matrix (in real coordinates) to resample the 'from' image into the 'to' image
    #
    mirror = _mirroring_voxel_matrix(shape, x_mirroring=x_mirroring, y_mirroring=y_mirroring, z_mirroring=z_mirroring)
    to_mat = np.identity(4)
    to_mat[0, 0] = 1.0 / to_voxel[0]
    to_mat[1, 1] = 1.0 / to_voxel[1]
    to_mat[2, 2] = 1.0 / to_voxel[2]
    from_mat = np.identity(4)
    from_mat[0, 0] = from_voxel[0]
    from_mat[1, 1] = from_voxel[1]
    from_mat[2, 2] = from_voxel[2]
    real_mirror = np.matmul(from_mat, np.matmul(mirror, to_mat))
    return real_mirror


########################################################################################
#
#
#
########################################################################################


def _preweight_process(input_images, the_images, trsf_preweight, experiment, parameters):
    proc = "_preweight_process"
    #
    # keep the transformation to transform the original data into the 'mirror' image
    # ie, the one that serves as template to compute the weights
    #
    matrix_preweight = {}

    #
    # from now on, we only process the first channel (channel #0)
    # other channels will be fused using the computed transformations
    #
    monitoring.to_log_and_console("    .. resample #1 (along X and Y)", 2)

    #
    # build the file names
    #
    res_images = {}
    trsf_resample = {}
    for i in the_images:
        res_images[i] = common.add_suffix(input_images[i], "_pre_resample",
                                          new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                          new_extension=experiment.default_image_suffix)
        trsf_resample[i] = common.add_suffix(input_images[i], "_pre_resample",
                                             new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                             new_extension="trsf")
    #
    # process
    #
    for i in the_images:

        im = imread(the_images[i])
        if type(parameters.target_resolution) == int or type(parameters.target_resolution) == float:
            resampling_resolution = [parameters.target_resolution, parameters.target_resolution, im.voxelsize[2]]
        elif (type(parameters.target_resolution) == list or type(parameters.target_resolution) == tuple) \
                and len(parameters.target_resolution) == 3:
            resampling_resolution = [parameters.target_resolution[0],
                                     parameters.target_resolution[1], im.voxelsize[2]]
        else:
            monitoring.to_log_and_console(proc + ': unable to set target resolution for first resampling', 0)
            monitoring.to_log_and_console("\t target resolution was '"+str(parameters.target_resolution)+"'", 0)
            monitoring.to_log_and_console("\t image resolution was '" + str(im.voxelsize) + "'", 0)
            monitoring.to_log_and_console("Exiting.", 0)
            sys.exit(1)
        del im

        monitoring.to_log_and_console("      .. resampling '" + the_images[i].split(os.path.sep)[-1]
                                      + "' at " + str(resampling_resolution), 2)

        if os.path.isfile(res_images[i]) and os.path.isfile(trsf_resample[i]):
            monitoring.to_log_and_console("         already existing", 2)
        else:
            cpp_wrapping.apply_transformation(the_images[i], res_images[i], the_transformation=None,
                                              res_transformation=trsf_resample[i], template_image=None,
                                              voxel_size=resampling_resolution, interpolation_mode='linear',
                                              monitoring=monitoring)

        matrix_preweight[i] = fregistration.read_transformation(trsf_resample[i])

    #
    # 3. 2D crop of resampled acquisition images
    #
    # transformation matrix is a translation
    #
    # from TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_resample.mha
    # to   TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_crop.mha
    #
    # resampling transformation TRSF_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_crop.trsf
    #
    crop_threshold = {}
    if parameters.acquisition_cropping is True:

        monitoring.to_log_and_console("    .. cropping", 2)
        the_images = res_images

        #
        # build the file names
        #
        res_images = {}
        trsf_crop = {}
        for i in the_images:
            res_images[i] = common.add_suffix(input_images[i], "_pre_crop",
                                              new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                              new_extension=experiment.default_image_suffix)
            trsf_crop[i] = common.add_suffix(input_images[i], "_pre_crop",
                                             new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                             new_extension="trsf")

        #
        # process
        #
        for i in the_images:

            monitoring.to_log_and_console("      .. cropping '"
                                          + the_images[i].split(os.path.sep)[-1] + "'", 2)

            if os.path.isfile(res_images[i]) and os.path.isfile(trsf_crop[i]):
                monitoring.to_log_and_console("         already existing", 2)
                mat = fregistration.read_transformation(trsf_crop[i])

            else:
                #
                # compute the bounding box
                #
                box, th = futils.crop_bounding_box(the_images[i], z_crop=parameters.acquisition_z_cropping,
                                                   opening=parameters.acquisition_cropping_opening)
                crop_threshold[i] = th

                #
                # apply the bounding (+margin) to the 3D image
                # box_list[i] has been set on the first channel
                #
                mat, th = futils.crop_disk_image(the_images[i], res_images[i], the_max_box=box,
                                                 z_crop=parameters.acquisition_z_cropping,
                                                 margin_x_0=parameters.acquisition_cropping_margin_x_0,
                                                 margin_x_1=parameters.acquisition_cropping_margin_x_1,
                                                 margin_y_0=parameters.acquisition_cropping_margin_y_0,
                                                 margin_y_1=parameters.acquisition_cropping_margin_y_1,
                                                 margin_z_0=parameters.acquisition_cropping_margin_z_0,
                                                 margin_z_1=parameters.acquisition_cropping_margin_z_1,
                                                 opening=parameters.acquisition_cropping_opening)
                np.savetxt(trsf_crop[i], mat)

            matrix_preweight[i] = np.matmul(matrix_preweight[i], mat)

    #
    # 4. mirroring of the 'right; camera images (parameter dependent) wrt the X axis, if required
    # 5. mirroring of all camera images (parameter dependent) wrt the Z axis, if required
    #
    # Both are done at the same time to avoid reading/writing of images
    # transformation matrix may have -1 on the diagonal (and then some translation to
    # recenter the image)
    #
    # from TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_crop.mha
    # to   TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_mirror.mha
    #
    # resampling transformation TRSF_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_mirror.trsf
    #

    if parameters.acquisition_mirrors is False \
        or parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse' \
        or parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse':

        monitoring.to_log_and_console("    .. mirroring", 2)
        the_images = res_images

        #
        # build the file names
        #
        res_images = {}
        trsf_mirror = {}
        for i in the_images:
            res_images[i] = common.add_suffix(input_images[i], "_pre_mirror",
                                              new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                              new_extension=experiment.default_image_suffix)
            trsf_mirror[i] = common.add_suffix(input_images[i], "_pre_mirror",
                                               new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                               new_extension="trsf")

        #
        # is there something to do?
        #
        for i in the_images:
            if i == 0:
                # stack 0, left camera
                if parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse':
                    pass
                else:
                    res_images[i] = the_images[i]
                    np.savetxt(trsf_mirror[i], np.identity(4))
            elif i == 1:
                # stack 0, right camera
                if parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse' \
                        or parameters.acquisition_mirrors is False:
                    pass
                else:
                    res_images[i] = the_images[i]
                    np.savetxt(trsf_mirror[i], np.identity(4))
            elif i == 2:
                # stack 1, left camera
                if parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse':
                    pass
                else:
                    res_images[i] = the_images[i]
                    np.savetxt(trsf_mirror[i], np.identity(4))
            elif i == 3:
                # stack 1, right camera
                if parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse' \
                    or parameters.acquisition_mirrors is False:
                    pass
                else:
                    res_images[i] = the_images[i]
                    np.savetxt(trsf_mirror[i], np.identity(4))
            else:
                monitoring.to_log_and_console("         weird index:'" + str(i) + "'", 2)

        #
        # process
        #
        # test has to be rewritten add (trsf_mirror[i] is not None and not os.path.isfile(trsf_mirror[i]))
        #
        for i in the_images:
            mat = None
            if i == 0:
                # stack 0, left camera
                if parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse':
                    monitoring.to_log_and_console("      .. mirroring  #" + str(i) + " '"
                                                  + the_images[i].split(os.path.sep)[-1] + "'", 2)
                    if not os.path.isfile(res_images[i]) or not os.path.isfile(trsf_mirror[i]):
                        the_im = imread(the_images[i])
                        res_im = SpatialImage(the_im.copy())[:, :, -1::-1]
                        mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                     z_mirroring=True)
                        res_im.voxelsize = the_im.voxelsize
                        imsave(res_images[i], res_im)
                        del the_im
                        del res_im
                    else:
                        monitoring.to_log_and_console("         already existing", 2)

            elif i == 1:
                # stack 0, right camera
                if parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse' \
                    or parameters.acquisition_mirrors is False:
                    monitoring.to_log_and_console("      .. mirroring  #" + str(i) + " '"
                                                  + the_images[i].split(os.path.sep)[-1] + "'", 2)
                    if not os.path.isfile(res_images[i]) or not os.path.isfile(trsf_mirror[i]):
                        the_im = imread(the_images[i])
                        if parameters.acquisition_mirrors is False:
                            if parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse':
                                res_im = SpatialImage(the_im.copy())[-1::-1, :, -1::-1]
                                mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                             x_mirroring=True, z_mirroring=True)
                            else:
                                res_im = SpatialImage(the_im.copy())[-1::-1, :, :]
                                mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                             x_mirroring=True)
                        else:
                            if parameters.acquisition_stack0_leftcamera_z_stacking.lower() == 'inverse':
                                res_im = SpatialImage(the_im.copy())[:, :, -1::-1]
                                mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                             z_mirroring=True)
                            else:
                                res_im = SpatialImage(the_im.copy())
                                mat = np.identity(4)
                        res_im.voxelsize = the_im.voxelsize
                        imsave(res_images[i], res_im)
                        del the_im
                        del res_im
                    else:
                        monitoring.to_log_and_console("         already existing", 2)

            elif i == 2:
                # stack 1, left camera
                if parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse':
                    monitoring.to_log_and_console("      .. mirroring  #" + str(i) + " '"
                                                  + the_images[i].split(os.path.sep)[-1] + "'", 2)
                    if not os.path.isfile(res_images[i]) or not os.path.isfile(trsf_mirror[i]):
                        the_im = imread(the_images[i])
                        res_im = SpatialImage(the_im.copy())[:, :, -1::-1]
                        mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                     z_mirroring=True)
                        res_im.voxelsize = the_im.voxelsize
                        imsave(res_images[i], res_im)
                        del the_im
                        del res_im
                    else:
                        monitoring.to_log_and_console("         already existing", 2)

            elif i == 3:
                # stack 1, right camera
                if parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse' \
                    or parameters.acquisition_mirrors is False:
                    monitoring.to_log_and_console("      .. mirroring  #" + str(i) + " '"
                                                  + the_images[i].split(os.path.sep)[-1] + "'", 2)
                    if not os.path.isfile(res_images[i]) or not os.path.isfile(trsf_mirror[i]):
                        the_im = imread(the_images[i])
                        if parameters.acquisition_mirrors is False:
                            if parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse':
                                res_im = SpatialImage(the_im.copy())[-1::-1, :, -1::-1]
                                mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                             x_mirroring=True, z_mirroring=True)
                            else:
                                res_im = SpatialImage(the_im.copy())[-1::-1, :, :]
                                mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                             x_mirroring=True)
                        else:
                            if parameters.acquisition_stack1_leftcamera_z_stacking.lower() == 'inverse':
                                res_im = SpatialImage(the_im.copy())[:, :, -1::-1]
                                mat = _mirroring_real_matrix(the_im.shape, the_im.voxelsize, the_im.voxelsize,
                                                             z_mirroring=True)
                            else:
                                res_im = SpatialImage(the_im.copy())
                                mat = np.identity(4)
                        res_im.voxelsize = the_im.voxelsize
                        imsave(res_images[i], res_im)
                        del the_im
                        del res_im
                    else:
                        monitoring.to_log_and_console("         already existing", 2)

            if mat is not None:
                np.savetxt(trsf_mirror[i], mat)
                matrix_preweight[i] = np.matmul(matrix_preweight[i], mat)

    #
    # at this point we have
    # - as images in TEMP_XXX/ANGLE_[0,1,2,3]/
    #
    # Below is saved TRSF_tt/ANGLE_[0,1,2,3]/ANGLE_0/Cam_[left,right]_00ttt.lux_prefusion.trsf
    # that is the product of above matrices, ie (xxx_pre_resample.trsf * xxx_pre_crop.trsf * xxx_pre_mirror.trsf)
    # and that allows to directly resample the original image
    # (Cam_[left,right]_00XXX.lux(_pre_line_corrected).mha) into Cam_[left,right]_00XXX.lux_mirror.mha
    # ie, the teemplate image for initial weigth computation
    #

    for i in trsf_preweight:
        if i not in matrix_preweight:
            msg = "    .. weird, not transformation for image #" + str(i)
            monitoring.to_log_and_console(msg, 2)
        else:
            np.savetxt(trsf_preweight[i], matrix_preweight[i])

    return res_images, crop_threshold


########################################################################################
#
#
#
########################################################################################

#
# raw data have been read and eventually converted
# do some pre-processing of each acquisition
# 1. (optional) slit line correction
# 2. resolution change (only in X and Y directions)
# 3. (optional) 2D crop
# 4. mirroring of the right camera images (parameter dependent) wrt the X axis
# 5. mirroring of the all camera images (parameter dependent) wrt the Z axis
# then call a fusion method
#

def _fusion_process(input_image_list, fused_image, time_point, experiment, parameters):
    """
    
    :param input_image_list: a list of dictionaries of images to be fused. One list per channel
           a dictionary of images to be fused contains up to 4 images, with keys
           0: the left camera of stack #0
           1: the right camera of stack #0
           2: the left camera of stack #1
           3: the right camera of stack #1
    :param fused_image: a generic name for the fusion result
           the same name will be used for each channel
    :param experiment:
    :param parameters:
    :return:
    """

    proc = 'fusion_process'

    n_channels = experiment.rawdata_dir.get_number_channels()

    if monitoring.debug > 1:
        monitoring.to_log_and_console("")
        monitoring.to_log_and_console(proc + " was called with:")
        monitoring.to_log_and_console("- input_image_list = " + str(input_image_list))
        monitoring.to_log_and_console("- fused_image = " + str(fused_image))
        if monitoring.debug > 3:
            for c in range(n_channels):
                experiment.rawdata_dir.channel[c].print_parameters()
        monitoring.to_log_and_console("")

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, fparameters.FusionParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    if (parameters.fusion_strategy.lower() == 'hierarchical-fusion' or
            parameters.fusion_strategy.lower() == 'hierarchical'):
        if len(input_image_list[0]) == 4:
            fusion_strategy = 'hierarchical-fusion'
        else:
            fusion_strategy = 'direct-fusion'
            msg = "NOTICE: not enough images for hierarchical fusion, switch to direct fusion"
            monitoring.to_log_and_console("    .. " + msg, 2)
    elif (parameters.fusion_strategy.lower() == 'direct-fusion' or
            parameters.fusion_strategy.lower() == 'direct'):
        fusion_strategy = 'direct-fusion'
    else:
        msg = "fusion strategy '" + str(parameters.fusion_strategy) + "' not handled yet. Exiting."
        monitoring.to_log_and_console("    .. " + msg, 2)
        sys.exit(1)

    #
    # nothing to do if (all) the fused image exists
    #

    do_something = False
    for c in range(n_channels):
        if os.path.isfile(os.path.join(experiment.fusion_dir.get_directory(c), fused_image)):
            if monitoring.forceResultsToBeBuilt is False:
                monitoring.to_log_and_console('    fused channel #' + str(c) + ' already existing', 2)
            else:
                monitoring.to_log_and_console('    fused channel #' + str(c) + ' already existing, but forced', 2)
                do_something = True
        else:
            do_something = True

    if do_something is False:
        return

    #
    # how to copy a list:
    # NB: 'res_images = inputImages' acts like pointers
    #

    res_image_list = input_image_list[:]

    #
    # First steps:
    # 1. (optional) slit line correction
    # 2. resolution change (only in X and Y directions)
    # 3. (optional) 2D crop
    # 4. mirroring of the right camera images (parameter dependent) wrt the X axis
    # 5. mirroring of all camera images (parameter dependent) wrt the Z axis
    #

    #
    # 1. slit line correction
    # these corrections are done on original data (without resampling) on channel[0]
    # the compute corrections are then used for the other channels
    # Crop could be done beforehand to reduce the computational burden
    #
    # from TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux.mha
    # to   TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00ttt.lux_pre_line_corrected.mha
    #
    # no resampling transformation
    #

    if parameters.acquisition_slit_line_correction is True:

        monitoring.to_log_and_console("    .. slit line correction", 2)
        the_image_list = res_image_list[:]
        res_image_list = list()
        corrections = {}

        #
        # build the file names
        #

        for c in range(n_channels):

            the_images = the_image_list[c]
            res_images = {}

            for i in the_images:
                res_images[i] = common.add_suffix(input_image_list[c][i], "_pre_line_corrected",
                                                  new_dirname=experiment.rawdata_dir.get_tmp_directory(i, c),
                                                  new_extension=experiment.default_image_suffix)
                if c == 0:
                    corrections[i] = common.add_suffix(input_image_list[c][i], "_pre_line_corrected",
                                                       new_dirname=experiment.rawdata_dir.get_tmp_directory(i, c),
                                                       new_extension='.txt')
            res_image_list.append(res_images)

        #
        # is there something to do ?
        # check whether one corrected line image is missing
        # for channel #0, check also whether the correction file is missing
        #

        do_something = {}
        for i in the_image_list[0]:
            do_something[i] = False

        for c in range(n_channels):

            the_images = the_image_list[c]
            res_images = res_image_list[c]

            for i in the_images:
                if os.path.isfile(res_images[i]):
                    if monitoring.forceResultsToBeBuilt is True:
                        do_something[i] = True
                else:
                    do_something[i] = True
                if c == 0:
                    if os.path.isfile(corrections[i]):
                        if monitoring.forceResultsToBeBuilt is True:
                            do_something[i] = True
                    else:
                        do_something[i] = True

        #
        # process
        # corrections are computed on channel #0
        # and are used for other channels
        #

        for c in range(n_channels):

            the_images = the_image_list[c]
            res_images = res_image_list[c]

            for i in the_images:
                monitoring.to_log_and_console("      .. slit line correction of '"
                                              + the_images[i].split(os.path.sep)[-1] + "'", 2)

                if do_something[i] is False:
                    monitoring.to_log_and_console("         nothing to do", 2)
                    continue

                if c == 0:
                    cpp_wrapping.slitline_correction(the_images[i], res_images[i],
                                                     output_corrections=corrections[i],
                                                     monitoring=monitoring)
                else:
                    cpp_wrapping.slitline_correction(the_images[i], res_images[i],
                                                     input_corrections=corrections[i],
                                                     monitoring=monitoring)

    #
    # keep original images (after line slit correction)
    #
    slitline_image_list = res_image_list[:]

    ##########
    #
    # from now on we process only images of channel #0 to compute the transformation
    #
    ##########

    #
    # 2. first change of resolution
    # - for X and Y: target resolution (supposed to be larger than original)
    #   => dimensions along X and Y decrease
    # - for Z: original resolution (supposed to be larger than target)
    #   => dimension along Z is the same
    #
    # 3. 2D crop of resampled acquisition images
    #
    #
    # 4. mirroring of the 'right; camera images (parameter dependent) wrt the X axis, if required
    # 5. mirroring of all camera images (parameter dependent) wrt the Z axis, if required
    #

    trsf_preweight = {}
    for i in input_image_list[0]:
        trsf_preweight[i] = common.add_suffix(res_image_list[0][i], "_preweight",
                                              new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                              new_extension="trsf")

    res_images, crop_threshold = _preweight_process(input_image_list[0], res_image_list[0], trsf_preweight, experiment,
                                                    parameters)

    matrix_preweight = {}
    for i in trsf_preweight:
        matrix_preweight[i] = fregistration.read_transformation(trsf_preweight[i])

    #
    # at this point we have
    # - as images in TEMP_XXX/ANGLE_[0,1,2,3]/
    #
    # trsf_preweight[i] are transformation TRSF_ttt/ANGLE_[0,1,2,3]/ANGLE_0/Cam_[left,right]_00ttt.lux_preweight.trsf
    # that is the product of above matrices, ie (xxx_pre_resample.trsf * xxx_pre_crop.trsf * xxx_pre_mirror.trsf)
    # and that allows to directly resample the original image into res_images[i]
    #
    # res_images[i] are images Cam_[left,right]_00XXX.lux_pre_mirror.mha
    # ie, the template image for initial weigth computation
    #

    monitoring.to_log_and_console("    .. computing initial weights for fusion", 2)

    unreg_weight_images_list = []
    weight_images_list = []

    for c in range(n_channels):
        unreg_weight_images = {}
        weight_images = {}
        cref = c
        for i in range(0, c):
            if experiment.rawdata_dir.channel[c].fusion_weighting == experiment.rawdata_dir.channel[i].fusion_weighting:
                cref = i
                break
        #
        # check if the weighting mode was used for previous channels (ie weights have already been computed)
        # cref is a previous channel that has the same weighting mode that channel #c (cref < c)
        # if cref == c, it means that there is no other previous channel that has the same weighting mode
        # weights would have to be computed
        #
        if cref == c:
            for i in res_images:
                unreg_weight_images[i] = common.add_suffix(input_image_list[c][i], "_init_weight",
                                                           new_dirname=experiment.rawdata_dir.get_tmp_directory(i, c),
                                                           new_extension=experiment.default_image_suffix)
                weight_images[i] = common.add_suffix(input_image_list[c][i], "_weight",
                                                     new_dirname=experiment.rawdata_dir.get_tmp_directory(i, c),
                                                     new_extension=experiment.default_image_suffix)
                if i % 2 == 1:
                    decreasing_weight_with_z = False
                else:
                    decreasing_weight_with_z = True
                monitoring.to_log_and_console("      .. computing initial weights for image #" + str(i) + " '" +
                                              res_images[i].split(os.path.sep)[-1] + "' of channel #" +
                                              str(c), 2)
                if not os.path.isfile(unreg_weight_images[i]):
                    #
                    #
                    #
                    fweight.build_unreg_weighting_image(res_images[i], unreg_weight_images[i],
                                                        decreasing_weight_with_z,
                                                        experiment.rawdata_dir.channel[c].fusion_weighting)
                else:
                    monitoring.to_log_and_console("            already existing", 2)
        else:
            unreg_weight_images = unreg_weight_images_list[cref]
            weight_images = weight_images_list[cref]

        unreg_weight_images_list.append(unreg_weight_images)
        weight_images_list.append(weight_images)

    #
    # reference information
    # reference image is the first found image
    #

    the_images_keys = sorted(list(res_images.keys()))
    ref_index = the_images_keys[0]
    monitoring.to_log_and_console("    .. reference image is #" + str(ref_index), 2)

    #
    # build transformations
    #
    # - matrix_* are 4x4 numpy array (linear transformation)
    # - trsf_* are transformation files
    #
    # Final transformations will be 
    # if no drift
    #    - trsf_reg_left = Id
    #    - trsf_reg_init = rotation
    #      translation has to be set so as trsf_reg_init * center(I_ref) = center(I_flo)
    #    - image to be registered = I_mirror
    # if drift
    #    - trsf_reg_left = rotation * drift * crop
    #      translation has to be set so as trsf_reg_left * center(I_crop) = center(I_mirror)
    #      (it is assumed that the object is in the center of the image)
    #    - trsf_reg_init = Id
    #    - image to be registered = I_crop = I_mirror * trsf_reg_left
    #
    # trsf_reg_init will be updated into trsf_reg_final by registration
    #

    monitoring.to_log_and_console("    .. initial transformations for fusion", 2)

    matrix_rot = {}
    matrix_drift = {}
    matrix_left = {}
    matrix_init = {}

    trsf_reg_left = {}
    trsf_reg_init = {}
    trsf_reg_final = {}

    for i in res_images:
        trsf_reg_left[i] = common.add_suffix(input_image_list[0][i], "_reg_left",
                                             new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                             new_extension="trsf")
        trsf_reg_init[i] = common.add_suffix(input_image_list[0][i], "_reg_init",
                                             new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                             new_extension="trsf")
        trsf_reg_final[i] = common.add_suffix(input_image_list[0][i], "_reg_final",
                                              new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                              new_extension="trsf")

    #
    # default angle for initial rotation matrix
    #
    if parameters.acquisition_orientation.lower() == 'left':
        default_angle = 270.0
    elif parameters.acquisition_orientation.lower() == 'right':
        default_angle = 90.0
    else:
        monitoring.to_log_and_console(proc + ": unknown acquisition orientation '"
                                      + str(parameters.acquisition_orientation) + "'", 0)
        monitoring.to_log_and_console("Exiting.", 0)
        sys.exit(1)

    #
    # rotation matrix
    #
    for i in res_images:
        #
        # set angle
        #
        if i <= 1:
            #  first stack/camera
            angle = 0.0
        else:
            # second stack/camera
            angle = default_angle
        monitoring.to_log_and_console("      .. angle used for '" + str(os.path.split(res_images[i])[-1])
                                      + "' is " + str(angle), 2)
        #
        # translation is not set
        #
        matrix_rot[i] = fregistration.init_rotation_matrix(axis="Y", angle=angle, ref_center=None, flo_center=None)

    #
    # mirror image centers and shapes
    #
    mirror_center = {}
    mirror_shape = {}
    mirror_voxelsize = {}
    for i in res_images:
        im = imread(res_images[i])
        mirror_shape[i] = im.shape[:3]
        mirror_voxelsize[i] = im.voxelsize
        mirror_center[i] = np.multiply(im.shape[:3], im.voxelsize) / 2.0
        del im

    #
    # drift matrix
    #
    monitoring.to_log_and_console("    .. looking for drifts", 2)
    is_drift = {}
    the_images = res_images
    res_images = {}

    for i in the_images:
        #
        # is there a drift transformation ?
        #
        trsf = None
        if i == 0 or i == 1:
            driftsubdir = experiment.drift_dir.get_sub_directory(i=0, verbose=False)
            if driftsubdir is not None and os.path.isdir(experiment.drift_dir.get_directory(i=0, verbose=False)):
                trsf = futils.get_drift_trsf(experiment, time_point, i=0, verbose=False)
                if trsf is None:
                    msg = "       no drift transformation found for image #" + str(time_point) + " in "
                    msg += "'" + str(driftsubdir) + "'"
                    monitoring.to_log_and_console(msg, 2)
        elif i == 2 or i == 3:
            driftsubdir = experiment.drift_dir.get_sub_directory(i=1, verbose=False)
            if driftsubdir is not None and os.path.isdir(experiment.drift_dir.get_directory(i=1, verbose=False)):
                trsf = futils.get_drift_trsf(experiment, time_point, i=1, verbose=False)
                if trsf is None:
                    msg = "       no drift transformation found for image #" + str(time_point) + " in "
                    msg += "'" + str(driftsubdir) + "'"
                    monitoring.to_log_and_console(msg, 2)
        else:
            msg = "       unexpected key '" + str(i) + "' for image to be fused"
            msg += "... exiting"
            monitoring.to_log_and_console(msg)
            sys.exit(1)

        # if no drift
        #    - trsf_reg_left = Id
        #    - trsf_reg_init = rotation
        #      translation has to be set so as trsf_reg_init * center(I_ref) = center(I_flo)
        #    - image to be registered = I_mirror
        if trsf is None:
            msg = "      .. no drift transformation for '" + trsf_reg_left[i].split(os.path.sep)[-1] + "'"
            monitoring.to_log_and_console(msg, 2)
            is_drift[i] = False

            matrix_left[i] = np.identity(4)
            np.savetxt(trsf_reg_left[i], matrix_left[i])

            matrix_init[i] = matrix_rot[i]
            trs = mirror_center[i] - (matrix_init[i][:3, :3]).dot(mirror_center[ref_index])
            matrix_init[i][0:3, 3:4] = trs.reshape(3, 1)
            np.savetxt(trsf_reg_init[i], matrix_init[i])
            res_images[i] = the_images[i]

            continue

        # if drift
        #    - trsf_reg_left = rotation * drift * crop
        #      translation has to be set so as trsf_reg_left * center(I_crop) = center(I_mirror)
        #      (it is assumed that the object is in the center of the image)
        #    - trsf_reg_init = Id
        #    - image to be registered = I_crop = I_mirror * trsf_reg_left
        is_drift[i] = True

        msg = "      .. drift transformation for '" + trsf_reg_left[i].split(os.path.sep)[-1] + "'"
        msg += " found in '" + str(driftsubdir) + "'"
        monitoring.to_log_and_console(msg, 2)
        msg = "         -> '" + os.path.join(trsf.split(os.path.sep)[-2], trsf.split(os.path.sep)[-1])
        monitoring.to_log_and_console(msg, 2)

        #
        # the initial matrix ie
        # - either used to resampled the reference image into the frame of (stack0, left camera)
        # - or used as initialization for registration
        #
        matrix_drift[i] = fregistration.read_transformation(trsf)
        matrix_left[i] = np.matmul(matrix_rot[i], matrix_drift[i])
        matrix_init[i] = np.identity(4)

        #
        # is there a stack to stack transformation?
        # this only concern images of the 2nd stack (stack #1)
        #
        if i == 2 or i == 3:
            driftsubdir = experiment.drift_dir.get_sub_directory(i=1, verbose=False)
            stack_trsf = futils.get_stack_trsf(experiment, time_point, i=1)
            if stack_trsf is None:
                msg = "      .. no stack-to-stack transformation for '" + trsf_reg_left[i].split(os.path.sep)[-1] + "'"
                monitoring.to_log_and_console(msg, 2)
            else:
                msg = "      .. stack-to-stack transformation for '" + trsf_reg_left[i].split(os.path.sep)[-1] + "'"
                msg += " found in '" + str(driftsubdir) + "'"
                monitoring.to_log_and_console(msg, 2)
                msg = "         -> '" + os.path.join(stack_trsf.split(os.path.sep)[-2], stack_trsf.split(os.path.sep)[-1])
                monitoring.to_log_and_console(msg, 2)
                mat = fregistration.read_transformation(stack_trsf)
                matrix_left[i] = np.matmul(matrix_left[i], mat)

        res_images[i] = common.add_suffix(input_image_list[0][i], "_reg_init",
                                          new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                          new_extension=experiment.default_image_suffix)

        msg = "      .. recompute drift-compensated image #" + str(i)
        monitoring.to_log_and_console(msg, 2)

        matrix_left[i] = fregistration.drifted_image(i, slitline_image_list[0][i], res_images[i],
                                                     mirror_center[i], mirror_shape[i], mirror_voxelsize[i],
                                                     matrix_preweight[i], matrix_left[i], crop_threshold[i],
                                                     experiment, parameters)
        np.savetxt(trsf_reg_left[i], matrix_left[i])
        np.savetxt(trsf_reg_init[i], matrix_init[i])

    #
    # check whether all images have a drift transformation or not
    #
    for i in the_images:
        if i not in is_drift:
            msg = "weird, no drift status for image #" + str(i) + ". Exiting."
            monitoring.to_log_and_console(proc + ": " + msg, 1)
            sys.exit(1)
        if is_drift[i] != is_drift[ref_index]:
            msg = "weird, drift status for image #" + str(i) + " is " + str(is_drift[i])
            msg += " and was " + str(is_drift[ref_index]) + " for reference image #" + str(ref_index) + ". Exiting."
            monitoring.to_log_and_console(proc + ": " + msg, 1)
            sys.exit(1)

    #
    # if there is no drift
    #   - matrix_left[i] = np.identity(4)
    #   - matrix_init[i] = rotation
    #   - res_images[i] voxel size is [parameters.target_resolution, parameters.target_resolution, no changes]
    #   - res_images[i] = TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]right_00tttt.lux_pre_mirror.mha
    # if there is some drift
    #   - matrix_left[i] = rotation o drift o crop
    #   - matrix_init[i] = np.identity(4)
    #   - res_images[i] voxel size is isotropic and equal to parameters.target_resolution
    #   - res_images[i] = TEMP_ttt/ANGLE_[0,1,2,3]/Cam_[left,right]_00tttt.lux_reg_init.mha
    #
    # (initial) weight images have the same geometry than mirrored images
    # they have to be transformaed by matrix_left[i] o matrix_init[i]

    #
    # build the file names
    #
    the_images = res_images
    res_images = {}
    for i in the_images:
        res_images[i] = common.add_suffix(input_image_list[0][i], "_reg_final",
                                          new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                          new_extension=experiment.default_image_suffix)

    #
    # the first (reference) image will remain the same whatever the fusion strategy
    #
    # set res_images[ref_index] and trsf_reg_final[ref_index]
    #   if no drift, resample 'reference' image(s) for direct fusion
    #      the result reference image is the resampled one
    #      the result transformation is the resampling one
    #   if drift
    #      the result reference image is the initial one
    #      the result transformation is the initial one
    #

    if is_drift[ref_index] is False:
        msg = "    .. isotropic resampling image #" + str(ref_index) + " for fusion"
        monitoring.to_log_and_console(msg, 2)

        #
        # resampling may induce a small translation
        #
        ref_dimension, matrix_init[ref_index] = fregistration.recompute_dimension(mirror_center[ref_index],
                                                                                  mirror_shape[ref_index],
                                                                                  mirror_voxelsize[ref_index],
                                                                                  matrix_init[ref_index],
                                                                                  parameters.target_resolution)
        np.savetxt(trsf_reg_init[ref_index], matrix_init[ref_index])
        cpp_wrapping.apply_transformation(the_images[ref_index], res_images[ref_index],
                                          the_transformation=trsf_reg_init[ref_index],
                                          res_transformation=trsf_reg_final[ref_index],
                                          template_image=None,
                                          dimensions=list(ref_dimension),
                                          voxel_size=parameters.target_resolution,
                                          interpolation_mode='linear',
                                          monitoring=monitoring)
    else:
        os.link(the_images[ref_index], res_images[ref_index])
        shutil.copy(trsf_reg_init[ref_index], trsf_reg_final[ref_index])

    #
    # in case of hierarchical fusion
    # (isotropic) resampling of left camera of stack #1 in the frame of left camera of stack #0
    # compose the initial transformation with the left transformation
    # -> the new input image is the resampled image
    # -> the new initial transformation is the identity
    #
    if fusion_strategy == 'hierarchical-fusion':
        if is_drift[ref_index] is False:
            i = 2
            msg = "    .. isotropic resampling image #" + str(i) + " for hierarchical fusion"
            monitoring.to_log_and_console(msg, 2)
            tmp_res_image = common.add_suffix(input_image_list[0][i], "_reg_init",
                                              new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                              new_extension=experiment.default_image_suffix)
            cpp_wrapping.apply_transformation(the_images[i], tmp_res_image,
                                              the_transformation=trsf_reg_init[i],
                                              res_transformation=None,
                                              template_image=res_images[ref_index],
                                              interpolation_mode='linear',
                                              monitoring=monitoring)
            the_images[i] = tmp_res_image
            matrix_left[i] = np.matmul(matrix_left[i], matrix_init[i])
            matrix_init[i] = np.identity(4)
            np.savetxt(trsf_reg_left[i], matrix_left[i])
            np.savetxt(trsf_reg_init[i], matrix_init[i])
            #
            # save the initial matrix as the final one (for hierarchical fusion purpose)
            #
            np.savetxt(trsf_reg_final[i], matrix_init[i])
        else:
            shutil.copy(trsf_reg_init[2], trsf_reg_final[2])

    #
    #
    #
    if fusion_strategy == 'direct-fusion':
        fdirect.fusion_process(input_image_list[0], the_images, res_images, trsf_reg_init, trsf_reg_final,
                               experiment, parameters)
    elif fusion_strategy == 'hierarchical-fusion':
        fhierarchical.fusion_process(input_image_list[0], unreg_weight_images_list[0], the_images, res_images,
                                     trsf_reg_left, trsf_reg_init, trsf_reg_final, experiment, parameters)
    else:
        msg = "fusion strategy '" + str(fusion_strategy) + "' not handled yet. Exiting."
        monitoring.to_log_and_console("    .. " + msg, 2)
        sys.exit(1)

    #
    # trsf_preweight * trsf_reg_left * trsf_reg_final allows to resample original data images
    # into the result one
    #
    # trsf_reg_left * trsf_reg_final allows to resample initial weight images into the result one
    #
    monitoring.to_log_and_console("    .. combining images", 2)

    #
    # combining transformations
    #
    monitoring.to_log_and_console("      .. combining transformations", 2)
    trsf_reg_full = {}
    trsf_full = {}
    for i in res_images:
        trsf_reg_full[i] = common.add_suffix(input_image_list[0][i], "_reg_full",
                                             new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                             new_extension="trsf")
        trsf_full[i] = common.add_suffix(input_image_list[0][i], "_full",
                                         new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                         new_extension="trsf")

        cpp_wrapping.compose_trsf([trsf_reg_left[i], trsf_reg_final[i]], trsf_reg_full[i], monitoring=monitoring)
        cpp_wrapping.compose_trsf([trsf_preweight[i], trsf_reg_left[i], trsf_reg_final[i]], trsf_full[i],
                                  monitoring=monitoring)

    #
    # build the file names
    #
    res_image_list = list()
    res_fusion_list = list()
    for c in range(n_channels):
        local_res_image = {}
        for i in res_images:
            local_res_image[i] = common.add_suffix(input_image_list[c][i], "_tobefused",
                                                   new_dirname=experiment.rawdata_dir.get_tmp_directory(i, c),
                                                   new_extension=experiment.default_image_suffix)
        res_image_list.append(local_res_image)

        if parameters.fusion_cropping is True:
            tmp_fused_image = common.add_suffix(fused_image, "_uncropped_fusion",
                                                new_dirname=experiment.rawdata_dir.get_tmp_directory(4, c),
                                                new_extension=experiment.default_image_suffix)
        else:
            tmp_fused_image = os.path.join(experiment.fusion_dir.get_directory(c), fused_image)
        res_fusion_list.append(tmp_fused_image)

    #
    # process fusion
    #
    for c in range(n_channels):
        monitoring.to_log_and_console("      .. computing images and weights for fusion of channel #" + str(c), 2)
        for i in res_images:
            #
            # recompute image intensity
            # it is not necessary for the reference image
            # it is perhaps not necessary for the 'direct fusion' case
            #
            cpp_wrapping.applyTrsf(slitline_image_list[c][i], res_image_list[c][i], trsf_full[i],
                                   template_image=res_images[i], interpolation_mode='linear',
                                   monitoring=monitoring)
            #
            # if we use the same weighting scheme for different channels, the weight image could already have
            # been computed
            #
            if not os.path.isfile(weight_images_list[c][i]):
                cpp_wrapping.applyTrsf(unreg_weight_images_list[c][i], weight_images_list[c][i], trsf_reg_full[i],
                                       template_image=res_images[i], interpolation_mode='linear',
                                       monitoring=monitoring)
            #
            # if intensity image is '0'
            # -> replace non-zero values in the weight image by '0'
            # if intensity image is not
            # -> replace '0' values in the weight image by '0'some minimal-value
            #
            # this is important in case of drift compensation fusion
            #
            if is_drift[ref_index] is True:
                fweight.update_weight_image(res_image_list[c][i], weight_images_list[c][i])

        #
        # linear combination
        #
        cpp_wrapping.linear_combination(weight_images_list[c], res_image_list[c], res_fusion_list[c],
                                        monitoring=monitoring)
        if not os.path.isfile(res_fusion_list[c]):
            monitoring.to_log_and_console(proc + ': fused image (channel #' + str(c) + ') has not been generated', 0)
            monitoring.to_log_and_console("Exiting.", 0)
            sys.exit(1)

        #
        #
        #
        if parameters.xzsection_extraction is not None and parameters.xzsection_extraction is not False:
            monitoring.to_log_and_console("    .. extracting xz sections", 2)
            futils.extract_xzsection(weight_images_list[c], res_image_list[c], res_fusion_list[c], c, experiment,
                                     xz_section=parameters.xzsection_extraction)

    #
    #
    #
    if parameters.fusion_cropping is False:
        return

    monitoring.to_log_and_console("    .. cropping fusion", 2)

    #
    # build file names
    #
    the_fusion_list = res_fusion_list[:]
    res_fusion_list = list()
    for c in range(n_channels):
        tmp_fused_image = os.path.join(experiment.fusion_dir.get_directory(c), fused_image)
        res_fusion_list.append(tmp_fused_image)

    #
    # compute the bounding box on the 1st channel
    # do not add margins then
    #
    box, th = futils.crop_bounding_box(the_fusion_list[0], z_crop=parameters.fusion_z_cropping, opening=0)

    #
    # apply the bounding (+margin) to the 3D image
    # box_list[i] has been set on the first channel
    #
    for c in range(n_channels):
        monitoring.to_log_and_console("      .. cropping '" + the_fusion_list[c] + "'", 2)
        trsf_crop = common.add_suffix(the_fusion_list[c], "_crop",
                                      new_dirname=experiment.rawdata_dir.get_tmp_directory(4, c),
                                      new_extension="trsf")
        mat, th = futils.crop_disk_image(the_fusion_list[c], res_fusion_list[c], the_max_box=box,
                                         z_crop=parameters.fusion_z_cropping,
                                         margin_x_0=parameters.fusion_cropping_margin_x_0,
                                         margin_x_1=parameters.fusion_cropping_margin_x_1,
                                         margin_y_0=parameters.fusion_cropping_margin_y_0,
                                         margin_y_1=parameters.fusion_cropping_margin_y_1,
                                         margin_z_0=parameters.fusion_cropping_margin_z_0,
                                         margin_z_1=parameters.fusion_cropping_margin_z_1, opening=0)
        if c == 0:
            np.savetxt(trsf_crop, mat)

    return


#
#
# read the raw data
#
#


def _fusion_preprocess(input_images, fused_image, time_point, experiment, parameters):
    """

    :param input_images: dictionary indexed by 0, 1, 2, or 3
        where 0 corresponds to angle #0 = stack_0_channel_0/Cam_Left_00
        where 1 corresponds to angle #1 = stack_0_channel_0/Cam_Right_00
        where 2 corresponds to angle #2 = stack_1_channel_0/Cam_Left_00
        where 3 corresponds to angle #3 = stack_1_channel_0/Cam_Right_00
    :param fused_image:
    :param time_point:
    :param experiment:
    :param parameters:
    :return:
    """

    proc = "fusion_preprocess"

    if monitoring.debug > 1:
        monitoring.to_log_and_console("")
        monitoring.to_log_and_console(proc + " was called with:")
        monitoring.to_log_and_console("- input_images = " + str(input_images))
        monitoring.to_log_and_console("- fused_image = " + str(fused_image))
        monitoring.to_log_and_console("- time_point = " + str(time_point))
        monitoring.to_log_and_console("")

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, fparameters.FusionParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    #
    #
    #

    monitoring.to_log_and_console('... fusion of time ' + time_point, 1)
    n_channels = experiment.rawdata_dir.get_number_channels()
    if n_channels > 1:
        monitoring.to_log_and_console('    there are ' + str(n_channels) + ' channels to be fused', 1)

    #
    # check whether there exists some unfused channel
    #
    do_something = False
    for c in range(n_channels):
        if common.find_file(experiment.fusion_dir.get_directory(c), fused_image, file_type='image', callfrom=proc,
                            local_monitoring=monitoring, verbose=False) is not None:
            if not monitoring.forceResultsToBeBuilt:
                monitoring.to_log_and_console('    channel #' + str(c) + ' already existing', 2)
            else:
                monitoring.to_log_and_console('    channel #' + str(c) + ' already existing, but forced', 2)
                do_something = True
        else:
            do_something = True

    if do_something is False:
        monitoring.to_log_and_console('    nothing to do', 2)
        return

    #
    # start processing
    #

    start_time = time.time()

    #
    # directory for auxiliary files
    #
    # ANGLE_0: LC/Stack0000 ; stack_0_channel_0/Cam_Left_*
    # ANGLE_1: RC/Stack0000 ; stack_0_channel_0/Cam_Right_*
    # ANGLE_2: LC/Stack0001 ; stack_1_channel_0/Cam_Left_*
    # ANGLE_3: RC/Stack0001 ; stack_1_channel_0/Cam_Right_*
    #
    # experiment.rawdata_dir.get_tmp_directory(i, channel_id)
    # i=0 experiment.fusion_dir.get_directory(c) / "TEMP_time_value" / "ANGLE_0"
    # i=1 experiment.fusion_dir.get_directory(c) / "TEMP_time_value" / "ANGLE_1"
    # i=2 experiment.fusion_dir.get_directory(c) / "TEMP_time_value" / "ANGLE_2"
    # i=3 experiment.fusion_dir.get_directory(c) / "TEMP_time_value" / "ANGLE_3"
    # i=4 experiment.fusion_dir.get_directory(c) / "TEMP_time_value"
    #
    experiment.set_fusion_tmp_directory(int(time_point))
    experiment.set_fusion_trsf_directory(int(time_point))
    experiment.fusion_dir.set_xzsection_directory(int(time_point))

    #
    # get image file names
    # - may involve unzipping and conversion
    #
    monitoring.to_log_and_console('    get original images', 2)

    #
    # this is the list (length = #channels) of dictionaries of images to be fused
    #
    the_image_list = list()

    # list of list of image names have been changed to list of dictionaries,
    # to manage the possibilities to fuse with only a subset of images
    #
    # images are converted if necessary
    # image resolution are set to given values if necessary

    for c in range(experiment.rawdata_dir.get_number_channels()):
        images = {}
        for i in input_images:
            images[i] = _read_image_name(experiment.rawdata_dir.channel[c].get_angle_path(i),
                                         experiment.rawdata_dir.get_tmp_directory(i, c),
                                         input_images[i],
                                         parameters.acquisition_resolution, experiment.default_image_suffix)
        the_image_list.append(images)

    #
    # the_image_list is a list of dictionary of (already translated) input images
    #

    monitoring.to_log_and_console('    fuse images', 2)

    _fusion_process(the_image_list, fused_image, time_point, experiment, parameters)

    #
    # remove temporary files if required
    #

    if monitoring.keepTemporaryFiles is False:
        experiment.remove_fusion_tmp_directory()
    if parameters.keep_transformation is False and monitoring.keepTemporaryFiles is False:
        experiment.remove_fusion_trsf_directory()

    #
    # end processing
    #

    end_time = time.time()
    monitoring.to_log_and_console('    computation time = ' + str(end_time - start_time) + ' s', 1)

    #
    # there might be 4 threads (compressing of h5 images) launched for one fusion
    # do not fuse next time point if there is more living threads
    #
    waitForRunningThreadToStop(maxthread=5)

    monitoring.to_log_and_console('', 1)

    return


#
#
# Parse the raw data directories and identify data to be fused for each time point
# - for each time point fusion_preprocess() is then called
#
#


def fusion_control(experiment, parameters):
    """

    :param experiment:
    :param parameters:
    :return:
    """

    proc = 'fusion_control'

    fdirect.monitoring.copy(monitoring)
    fhierarchical.monitoring.copy(monitoring)
    fregistration.monitoring.copy(monitoring)
    futils.monitoring.copy(monitoring)
    fweight.monitoring.copy(monitoring)

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, fparameters.FusionParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    #
    # make sure that the result directory exists
    #

    experiment.fusion_dir.make_directory()

    monitoring.to_log_and_console('', 1)

    #
    # if data directories of the main channel are different, parse them
    # else rely on the given names
    #
    # This is the historical version of the fusion. Now the muvispim has
    # standardized outputs
    # - stack_0_channel_C: contains the 'C' channel of both left and right cameras of stack #0
    # - stack_1_channel_C: contains the 'C' channel of both left and right cameras of stack #1
    #

    path_angle = {}
    is_path = {}
    for i in range(4):
        path_angle[i] = experiment.rawdata_dir.channel[0].get_angle_path(i)
        if path_angle[i] is None:
            is_path[i] = False
        else:
            is_path[i] = os.path.isdir(path_angle[i])

    #
    # test whether the angle sub-directories *that* exist are different
    # is True, it means that each *existing* sub-directory contains only data/image from only one angle
    #
    paths_are_different = True
    paths_are_different = paths_are_different and (not is_path[0] or not is_path[1] or path_angle[0] != path_angle[1])
    paths_are_different = paths_are_different and (not is_path[0] or not is_path[2] or path_angle[0] != path_angle[2])
    paths_are_different = paths_are_different and (not is_path[0] or not is_path[3] or path_angle[0] != path_angle[3])
    paths_are_different = paths_are_different and (not is_path[1] or not is_path[2] or path_angle[1] != path_angle[2])
    paths_are_different = paths_are_different and (not is_path[1] or not is_path[3] or path_angle[1] != path_angle[3])
    paths_are_different = paths_are_different and (not is_path[2] or not is_path[3] or path_angle[2] != path_angle[3])

    if paths_are_different is True:
        #
        # for each rawdata subdirectory (ie, left/right camera, stack 0/1)
        # get
        # - the common file prefix,
        # - the length of the variable part (ie the time id)
        # - the list of variable parts (ie, all the time ids)
        # - the common file suffix
        #

        # get the directories for each of the acquisition/angle. It corresponds to
        # angle #0 = stack_0_channel_0/Cam_Left_00
        # angle #1 = stack_0_channel_0/Cam_Right_00
        # angle #2 = stack_1_channel_0/Cam_Left_00
        # angle #3 = stack_1_channel_0/Cam_Right_00
        prefix = [None for _ in range(4)]
        time_length = [None for _ in range(4)]
        time_points = [None for _ in range(4)]
        suffix = [None for _ in range(4)]
        for i in range(4):
            if is_path[i]:
                prefix[i], time_length[i], time_points[i], suffix[i] = _analyze_data_directory(path_angle[i],
                                                                                               callfrom=proc)

        if not is_path[0] and not is_path[1] and not is_path[2] and not is_path[3]:
            rawdir = os.path.join(experiment.get_embryo_path(), experiment.rawdata_dir.channel[0].get_main_directory())
            monitoring.to_log_and_console("... no valid sub-directories in '" + str(rawdir) + "'?!")
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)

        if monitoring.debug > 0:
            monitoring.to_log_and_console("")
            for i in range(4):
                monitoring.to_log_and_console("analysis of directory #" + str(i) + " '" + str(path_angle[i]) + "'")
                if is_path[i]:
                    monitoring.to_log_and_console("   prefix      -> " + str(prefix[i]))
                    monitoring.to_log_and_console("   time length -> " + str(time_length[i]))
                    monitoring.to_log_and_console("   time point  -> " + str(sorted(time_points[i])))
                    monitoring.to_log_and_console("   suffix      -> " + str(suffix[i]))
                else:
                    monitoring.to_log_and_console("   is not a directory")

        #
        # compute the number of digits used for the acquisition time
        #
        # loop over acquisitions
        # 1. case where all acquisition have to be processed
        #    begin < 0 or end < 0 or begin > end or delta < 0
        # 2. only a few acquisitions have to be processed
        #
        common_time_length = None
        for i in range(4):
            if time_length[i] is None:
                continue
            if common_time_length is None:
                common_time_length = time_length[i]
            elif common_time_length != time_length[i]:
                monitoring.to_log_and_console("... weird, time lengths extracted from sub-directories are differents")
                for j in range(4):
                    monitoring.to_log_and_console("analysis of directory #" + str(i) + " '" + str(path_angle[j]) + "'")
                    if is_path[j]:
                        monitoring.to_log_and_console("   time length -> " + str(str(time_length[j])))
                    else:
                        monitoring.to_log_and_console("   is not a directory")
                if common_time_length < time_length[j]:
                    common_time_length = time_length[j]

        extra_zeros = ''
        if common_time_length < experiment.rawdata_dir.get_time_digits_for_acquisition():
            extra_zeros = (experiment.rawdata_dir.get_time_digits_for_acquisition() - common_time_length) * '0'

        #
        # no indication about the time interval to be process
        # -> process all the time ids of the list
        #

        if experiment.first_time_point < 0 or experiment.last_time_point < 0 or experiment.delta_time_point < 0 \
                or experiment.first_time_point > experiment.last_time_point:

            time_points[0].sort()
            for time_point in time_points[0]:

                #
                # fused image name
                #
                fused_image = experiment.fusion_dir.get_image_name(int(time_point)) + "." \
                              + experiment.result_image_suffix

                #
                # input image names
                #
                images = {}
                for i in range(4):
                    if not is_path[i]:
                        continue
                    im = prefix[i] + time_point + suffix[i]
                    if time_point not in time_points[i]:
                        msg = "    .. image '" + im + "' not found in '" + path_angle[i] + "'"
                        monitoring.to_log_and_console(msg, 2)
                    else:
                        images[i] = im

                #
                # check whether we found an image
                #
                if len(images) == 0:
                    monitoring.to_log_and_console("    .. no images to be fused", 2)
                    monitoring.to_log_and_console("       skip time " + str(time_point), 2)
                    continue
                #
                # process
                #

                _fusion_preprocess(images, fused_image, extra_zeros + time_point, experiment, parameters)

        else:

            if experiment.first_time_point < 0 or experiment.last_time_point < 0:
                monitoring.to_log_and_console("... time interval does not seem to be defined in the parameter file")
                monitoring.to_log_and_console("    set parameters 'begin' and 'end'")
                monitoring.to_log_and_console("\t Exiting")
                sys.exit(1)

            #
            # parse only the required time values
            #

            for time_value in range(experiment.first_time_point, experiment.last_time_point + 1,
                                    experiment.delta_time_point):

                acquisition_time = str('{:0{width}d}'.format(time_value, width=common_time_length))

                #
                # fused image name
                #

                fused_image = experiment.fusion_dir.get_image_name(time_value + experiment.delay_time_point) + "."
                fused_image += experiment.result_image_suffix

                #
                # input image names
                #

                #
                # input image names
                #
                images = {}
                for i in range(4):
                    if not is_path[i]:
                        continue
                    im = prefix[i] + acquisition_time + suffix[i]
                    if acquisition_time not in time_points[i]:
                        msg = "    .. image '" + im + "' not found in '" + path_angle[i] + "'"
                        monitoring.to_log_and_console(msg, 2)
                    else:
                        images[i] = im

                #
                # check whether we found an image
                #
                if len(images) == 0:
                    monitoring.to_log_and_console("    .. no images to be fused", 2)
                    monitoring.to_log_and_console("       skip time " + str(acquisition_time), 2)
                    continue

                #
                # process
                #

                _fusion_preprocess(images, fused_image, extra_zeros + acquisition_time, experiment, parameters)

    #
    # here data directories are not different, we have to rely on built names
    #

    else:

        if experiment.first_time_point < 0 or experiment.last_time_point < 0:
            monitoring.to_log_and_console("... time interval does not seem to be defined in the parameter file")
            monitoring.to_log_and_console("    set parameters 'begin' and 'end'")
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)

        for time_value in range(experiment.first_time_point, experiment.last_time_point+1, experiment.delta_time_point):

            acquisition_time = experiment.get_time_index(time_value)

            #
            # fused image name
            #
            fused_image = experiment.fusion_dir.get_image_name(time_value + experiment.delay_time_point) + "."
            fused_image += experiment.result_image_suffix

            #
            # input image names
            #

            images = {}

            for i in range(4):
                acquisition = "?"
                if i == 0:
                    acquisition = "left camera of stack #0"
                elif i == 1:
                    acquisition = "right camera of stack #0"
                elif i == 2:
                    acquisition = "left camera of stack #1"
                elif i == 3:
                    acquisition = "right camera of stack #1"

                sname = experiment.rawdata_dir.channel[0].get_image_name(i, time_value)
                sdir = experiment.rawdata_dir.channel[0].get_angle_path(i)
                if sdir is None:
                    monitoring.to_log_and_console("    .. no directory for " + acquisition, 2)
                else:
                    im = common.find_file(sdir, sname, file_type='image', callfrom=proc, local_monitoring=None,
                                          verbose=False)
                    if im is None:
                        monitoring.to_log_and_console("    .. image '" + sname + "' not found in '" + sdir + "'", 2)
                    else:
                        images[i] = im

            #
            # check whether we found an image
            #
            if len(images) == 0:
                monitoring.to_log_and_console("    .. no images to be fused", 2)
                monitoring.to_log_and_console("       skip time " + str(acquisition_time), 2)
                continue

            #
            # process
            #

            _fusion_preprocess(images, fused_image, acquisition_time, experiment, parameters)

    waitForRunningThreadToStop(maxthread=1)

    return
