##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 22 jan 2025 18:04:09 CET
##############################################################
#
#
#
##############################################################

import sys
import os

import astec.utils.common as common
import astec.wrapping.cpp_wrapping as cpp_wrapping

import astec.algorithms.fusion.registration as fregistration

#
#
#
#
#


monitoring = common.Monitoring()


#
# hierarchical fusion procedure
# - each stack is reconstructed
# - stack are co-registered
# - all acquisitions are fused
#
def fusion_process(input_images, unreg_weight_images, the_images, res_images, trsf_left, trsf_init, trsf_final,
                   experiment, parameters):
    """

    :param input_images: dictionary of original images of channel #0, to set names
    :param unreg_weight_images:
    :param the_images: dictionary of images to be registered. If drift is set to True, they are isotropic
        (with voxel size = parameters.target_resolution). Else, one the pixel size (along X and Y) is
        equal to parameters.target_resolution
    :param res_images: dictionary of registered images, they can be used to compute the fusion
        (ie weighted sum)
    :param trsf_left:
    :param trsf_init: initial transformations for fusion. It is the identity of drift is set to True.
    :param trsf_final: transformations computed during fusion.
    :param experiment:
    :param parameters:
    :return:
    """

    proc = "direct.fusion_process"

    # print("input_images = " + str(input_images))
    # print("the_images = " + str(the_images))
    # print("res_images = " + str(res_images))
    # print("trsf_init = " + str(trsf_init))
    # print("trsf_final = " + str(trsf_final))
    # print("time_point = " + str(time_point))

    the_images_keys = sorted(the_images.keys())
    ref_index = the_images_keys[0]
    msg = "    .. (hierarchical) fusion with respect to reference image is #" + str(ref_index)
    monitoring.to_log_and_console(msg, 2)

    #
    # build file names
    #
    trsf_intra_stack_full = {}
    stack_weight = {}
    stack = {}
    trsf_inter_stack_full = {}
    reg_stack = {}
    for i in input_images:
        trsf_intra_stack_full[i] = common.add_suffix(input_images[i], "_intra_stack_full",
                                                     new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                                     new_extension="trsf")
        stack_weight[i] = common.add_suffix(input_images[i], "_stack_weight",
                                            new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                            new_extension=experiment.default_image_suffix)
        stack[i] = common.add_suffix(input_images[i], "_stack",
                                     new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                     new_extension=experiment.default_image_suffix)
        trsf_inter_stack_full[i] = common.add_suffix(input_images[i], "_inter_stack_full",
                                                     new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                                     new_extension="trsf")
        reg_stack[i] = common.add_suffix(input_images[i], "_reg_stack",
                                         new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                         new_extension=experiment.default_image_suffix)

    #
    # first stack
    # compute the final transformation for image #1 (right camera, stack #0)
    #
    ref_index = 0
    i = 1
    monitoring.to_log_and_console("      .. reconstruct stack #0", 2)
    monitoring.to_log_and_console("        .. process '" + the_images[i].split(os.path.sep)[-1] + "' for fusion", 2)
    monitoring.to_log_and_console("           co-registering '" + the_images[i].split(os.path.sep)[-1] + "'", 2)

    #
    # please note that the reference image is *res_images[ref_index]* ie res_images[0]
    #
    if parameters.acquisition_registration[0].compute_registration is True and \
            parameters.acquisition_registration[1].compute_registration is True:
        fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_init[i],
                                          parameters=parameters.acquisition_registration[0])
        fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_final[i],
                                          parameters=parameters.acquisition_registration[1])
    elif parameters.acquisition_registration[0].compute_registration is True:
        fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_init[i],
                                          parameters=parameters.acquisition_registration[0])
    elif parameters.acquisition_registration[1].compute_registration is True:
        fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_init[i],
                                          parameters=parameters.acquisition_registration[1])
    else:
        msg = "error, no registration to be computed?! Exiting."
        monitoring.to_log_and_console(proc + ": " + msg, 0)
        sys.exit(1)

    monitoring.to_log_and_console("        .. fusion of '" + stack[0].split(os.path.sep)[-1] + "'", 2)

    for i in [0, 1]:
        cpp_wrapping.compose_trsf([trsf_left[i], trsf_final[i]], trsf_intra_stack_full[i], monitoring=monitoring)
        cpp_wrapping.applyTrsf(unreg_weight_images[i], stack_weight[i], trsf_intra_stack_full[i],
                               template_image=res_images[0], interpolation_mode='linear', monitoring=monitoring)
    cpp_wrapping.linear_combination([stack_weight[0], stack_weight[1]], [res_images[0], res_images[1]], stack[0],
                                    monitoring=monitoring)

    #
    # second stack
    #
    ref_index = 2
    i = 3
    monitoring.to_log_and_console("      .. reconstruct stack #1", 2)
    monitoring.to_log_and_console("        .. process '" + the_images[i].split(os.path.sep)[-1] + "' for fusion", 2)
    monitoring.to_log_and_console("           co-registering '" + the_images[i].split(os.path.sep)[-1] + "'", 2)

    #
    # please note that the reference image is *the_images[ref_index]* ie the_images[2]
    #
    if parameters.acquisition_registration[0].compute_registration is True and \
            parameters.acquisition_registration[1].compute_registration is True:
        fregistration.linear_registration(the_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_init[i],
                                          parameters=parameters.acquisition_registration[0])
        fregistration.linear_registration(the_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_final[i],
                                          parameters=parameters.acquisition_registration[1])
    elif parameters.acquisition_registration[0].compute_registration is True:
        fregistration.linear_registration(the_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_init[i],
                                          parameters=parameters.acquisition_registration[0])
    elif parameters.acquisition_registration[1].compute_registration is True:
        fregistration.linear_registration(the_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                          path_init_trsf=trsf_init[i],
                                          parameters=parameters.acquisition_registration[1])
    else:
        msg = "error, no registration to be computed?! Exiting."
        monitoring.to_log_and_console(proc + ": " + msg, 0)
        sys.exit(1)

    monitoring.to_log_and_console("        .. fusion of '" + stack[0].split(os.path.sep)[-1] + "'", 2)

    for i in [2, 3]:
        cpp_wrapping.compose_trsf([trsf_left[i], trsf_final[i]], trsf_intra_stack_full[i], monitoring=monitoring)
        cpp_wrapping.applyTrsf(unreg_weight_images[i], stack_weight[i], trsf_intra_stack_full[i],
                               template_image=the_images[2], interpolation_mode='linear', monitoring=monitoring)
    cpp_wrapping.linear_combination([stack_weight[2], stack_weight[3]], [the_images[ref_index], res_images[3]],
                                    stack[2], monitoring=monitoring)

    #
    # fusion of the two stacks
    #
    monitoring.to_log_and_console("      .. stack co-registration", 2)

    if parameters.stack_registration[0].compute_registration is True and \
            parameters.stack_registration[1].compute_registration is True:
        fregistration.blockmatching(stack[0], stack[2], reg_stack[2], trsf_inter_stack_full[2], path_init_trsf=None,
                                    parameters=parameters.stack_registration[0])
        fregistration.blockmatching(stack[0], stack[2], reg_stack[2], trsf_inter_stack_full[2],
                                    path_init_trsf=trsf_inter_stack_full[2],
                                    parameters=parameters.stack_registration[1])
    elif parameters.stack_registration[0].compute_registration is True:
        fregistration.blockmatching(stack[0], stack[2], reg_stack[2], trsf_inter_stack_full[2], path_init_trsf=None,
                                    parameters=parameters.stack_registration[0])
    elif parameters.stack_registration[1].compute_registration is True:
        fregistration.blockmatching(stack[0], stack[2], reg_stack[2], trsf_inter_stack_full[2], path_init_trsf=None,
                                    parameters=parameters.stack_registration[1])
    else:
        msg = "error, no registration to be computed?! Exiting."
        monitoring.to_log_and_console(proc + ": " + msg, 0)
        sys.exit(1)

    #
    #
    #
    monitoring.to_log_and_console("      .. transformation composition", 2)

    for i in [2, 3]:
        cpp_wrapping.compose_trsf([trsf_final[i], trsf_inter_stack_full[2]], trsf_final[i], monitoring=monitoring)

    return
