##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 22 jan 2025 18:04:09 CET
##############################################################
#
#
#
##############################################################

import math
import numpy as np

import astec.utils.common as common
import astec.wrapping.cpp_wrapping as cpp_wrapping

import astec.algorithms.fusion.utils as futils

#
#
#
#
#


monitoring = common.Monitoring()


########################################################################################
#
# computation of a rotation matrix
#
########################################################################################


def _axis_rotation_matrix(axis, angle, min_space=None, max_space=None):
    """ Return the transformation matrix from the axis and angle necessary
    this is a rigid transformation (rotation) that preserves the center of
    the field of view
    axis : axis of rotation ("X", "Y" or "Z")
    angle : angle of rotation (in degree)
    min_space : coordinates of the bottom point (usually (0, 0, 0))
    max_space : coordinates of the top point (usually im shape)
    """
    i = np.linalg.inv
    d = np.dot
    if axis not in ["X", "Y", "Z"]:
        raise Exception("Unknown axis : " + str(axis))
    rads = math.radians(angle)
    s = math.sin(rads)
    c = math.cos(rads)

    centering = np.identity(4)
    if min_space is None and max_space is not None:
        min_space = np.array([0., 0., 0.])

    if max_space is not None:
        space_center = (max_space-min_space)/2.
        offset = -1.*space_center
        centering[:3, 3] = offset

    rot = np.identity(4)
    if axis == "X":
        rot = np.array([[1., 0., 0., 0.],
                        [0., c, -s, 0.],
                        [0., s, c, 0.],
                        [0., 0., 0., 1.]])
    elif axis == "Y":
        rot = np.array([[c,   0., s,  0.],
                        [0., 1., 0., 0.],
                        [-s, 0., c, 0.],
                        [0., 0., 0., 1.]])

    elif axis == "Z":
        rot = np.array([[c, -s,  0., 0.],
                        [s, c, 0., 0.],
                        [0., 0., 1., 0.],
                        [0., 0., 0., 1.]])

    return d(i(centering), d(rot, centering))


def init_rotation_matrix(axis, angle, ref_center=None, flo_center=None):

    if axis not in ["X", "Y", "Z"]:
        raise Exception("Unknown axis : " + str(axis))
    rads = math.radians(angle)
    s = math.sin(rads)
    c = math.cos(rads)

    rot = np.identity(3)
    if axis == "X":
        rot = np.array([[1., 0., 0.],
                        [0., c, -s],
                        [0., s, c]])
    elif axis == "Y":
        rot = np.array([[c, 0., s],
                        [0., 1., 0.],
                        [-s, 0., c]])
    elif axis == "Z":
        rot = np.array([[c, -s, 0.],
                        [s, c, 0.],
                        [0., 0., 1.]])

    if ref_center is not None:
        if flo_center is not None:
            trs = flo_center - np.dot(rot, ref_center)
        else:
            trs = ref_center - np.dot(rot, ref_center)
    else:
        if flo_center is not None:
            trs = flo_center - np.dot(rot, flo_center)
        else:
            trs = np.zeros(3)

    mat = np.identity(4)
    mat[0:3, 0:3] = rot
    mat[0:3, 3:4] = trs.reshape(3, 1)

    return mat


def is_transformation_identity(mat, err=0.000001):
    is_identity = True
    for i in range(3):
        for j in range(3):
            if i == j:
                if mat[i, j] < 1 - err or 1 + err < mat[i, j]:
                    is_identity = False
            else:
                if mat[i, j] < (- err) or err < mat[i, j]:
                    is_identity = False
    return is_identity


def read_transformation(filename):
    #
    # copy from drift.py
    #
    proc = "_read_transformation"
    mat = np.zeros((4, 4))
    f = open(filename)

    imat = 0
    i = 0
    for line in f:
        i += 1
        # remove leading and trailing whitespaces
        li = line.strip()
        # skip comment or '(' or 'O8'
        if li.startswith('#') or li.startswith('(') or li.startswith('O8'):
            continue
        # empty line
        if len(li) == 0:
            continue
        li = li.split()

        if len(li) != 4:
            msg = "line #" + str(i) + ": '" + str(line) + " of file '" + str(filename) + "'"
            msg += "' has {:d} components?! Skip it.".format(len(li))
            monitoring.to_log_and_console(proc + ": " + msg)
            continue

        for j in range(4):
            mat[imat, j] = float(li[j])
        # print("- " + str(li))
        # print("   -> " + str(mat[imat, :]))
        imat += 1

        if imat == 4:
            break

    f.close()
    return mat


########################################################################################
#
#
#
########################################################################################


def recompute_dimension(im_center, im_shape, im_voxelsize, matrix_left, target_resolution):

    #
    # image corners
    #
    corners = np.zeros((3, 8))
    ii = 0
    for zk in range(2):
        if zk == 0:
            z = 0.0
        else:
            z = im_shape[2] * im_voxelsize[2]
        for yj in range(2):
            if yj == 0:
                y = 0.0
            else:
                y = im_shape[1] * im_voxelsize[1]
            for xi in range(2):
                if xi == 0:
                    x = 0.0
                else:
                    x = im_shape[0] * im_voxelsize[0]
                corners[:, ii] = [x, y, z]
                ii += 1

    #
    # transformed image corners
    #
    tcorners = (np.linalg.inv(matrix_left[:3, :3])).dot(corners)

    #
    # dimensions to keep the whole field of view of the transformed mirrored image
    #
    realdim = np.max(tcorners, 1) - np.min(tcorners, 1)
    ref_dimension = (np.rint(realdim / target_resolution)).astype(int)
    monitoring.to_log_and_console("         uncropped image dimensions are " + str(ref_dimension), 2)

    ref_center = (ref_dimension * target_resolution) / 2.
    # msg = "center of reference image  is " + str(ref_center)
    # monitoring.to_log_and_console("         " + msg, 2)

    #
    # set the translation so that the center of the result image get into the center of the mirrored image
    #
    trs = im_center - (matrix_left[:3, :3]).dot(ref_center)
    matrix_left[0:3, 3:4] = trs.reshape(3, 1)

    return ref_dimension, matrix_left


########################################################################################
#
#
#
########################################################################################

def drifted_image(i, the_image, res_image, im_center, im_shape, im_voxelsize, matrix_mirror, matrix_left,
                  crop_threshold, experiment, parameters):
    """
    :param i: index of the image (to get its temporary directory)
    :param the_image: original image or slit-line corrected image. Used to resample in new geometry
    :param res_image: result image
    :param im_center: center of the mirrored image (the one to be transformed)
    :param im_shape: shape of the mirrored image (the one to be transformed)
    :param im_voxelsize: voxel size of the mirrored image (the one to be transformed)
    :param matrix_mirror: transformation from mirrored image to original image (can be used to resample the
        original image into the mirrored
    :param matrix_left: transformation from result image to mirrored image. Only the vectorial part is valid
        the translation has to be re-calculated
    :param crop_threshold:
        Threshold to computed the object of interest in image, ie its bounding box to further crop the transformed
        mirror image
    :param experiment:
    :param parameters:
    :return:
    """

    #
    # recompute (image) dimensions that will contain the image after resamepling
    # as well as the resampling matrix (translation may have changed)
    #
    ref_dimension, matrix_left = recompute_dimension(im_center, im_shape, im_voxelsize, matrix_left,
                                                     parameters.target_resolution)

    #
    # do we need an temporary image for crop computation ?
    # here its is assumed that we are processing channel #0
    #
    if parameters.acquisition_cropping is False:
        tmp_image = res_image
    else:
        tmp_image = common.add_suffix(res_image, "_nocrop", new_dirname=experiment.rawdata_dir.get_tmp_directory(i, 0),
                                      new_extension=experiment.default_image_suffix)

    #
    # save the compound transformation to resample the original image into the result one
    #
    trsf = common.add_suffix(res_image, "_nocrop_from_original",
                             new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0), new_extension="trsf")
    np.savetxt(trsf, np.matmul(matrix_mirror, matrix_left))

    #
    # resample the original image into the result one (uncropped geometry)
    #
    voxel_size = [parameters.target_resolution, parameters.target_resolution, parameters.target_resolution]
    cpp_wrapping.applyTrsf(the_image, tmp_image, the_transformation=trsf, voxel_size=voxel_size,
                           dimensions=ref_dimension.tolist(), monitoring=monitoring)

    #
    # in case of no cropping
    # return the left matrix (that now links the image centers)
    #
    if parameters.acquisition_cropping is False:
        return matrix_left

    #
    # here parameters.acquisition_cropping is True:
    # crop the uncropped result image, and get the asoociated matrix (a translation)
    # to update the left matrix
    mat, th = futils.crop_disk_image(tmp_image, res_image, the_max_box=None,
                                     z_crop=parameters.acquisition_z_cropping, threshold=crop_threshold,
                                     margin_x_0=parameters.fusion_cropping_margin_x_0,
                                     margin_x_1=parameters.fusion_cropping_margin_x_1,
                                     margin_y_0=parameters.fusion_cropping_margin_y_0,
                                     margin_y_1=parameters.fusion_cropping_margin_y_1,
                                     margin_z_0=parameters.fusion_cropping_margin_z_0,
                                     margin_z_1=parameters.fusion_cropping_margin_z_1,
                                     opening=parameters.acquisition_cropping_opening)
    matrix_left = np.matmul(matrix_left, mat)
    return matrix_left

#
########################################################################################
#
#
#
########################################################################################


def blockmatching(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf=None, parameters=None):
    """

    :param path_ref:
    :param path_flo:
    :param path_output:
    :param path_output_trsf:
    :param path_init_trsf:
    :param parameters:
    :return:
    """
    #
    # add other_options="-flo-lt 1 -ref-lt 1" for drift compensated fusion,
    # to manage for zero-filled areas
    # GM, Ven 17 jan 2025 15:08:17 CET
    #
    if parameters is not None:
        cpp_wrapping.blockmatching(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf=path_init_trsf,
                                   py_hl=parameters.pyramid_highest_level,
                                   py_ll=parameters.pyramid_lowest_level,
                                   transformation_type=parameters.transformation_type,
                                   elastic_sigma=parameters.elastic_sigma,
                                   transformation_estimator=parameters.transformation_estimation_type,
                                   lts_fraction=parameters.lts_fraction,
                                   fluid_sigma=parameters.fluid_sigma,
                                   normalization=parameters.normalization,
                                   other_options="-flo-lt 1 -ref-lt 1",
                                   monitoring=monitoring)
    else:
        cpp_wrapping.blockmatching(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf=path_init_trsf,
                                   other_options="-flo-lt 1 -ref-lt 1", monitoring=monitoring)


def linear_registration(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf=None, parameters=None):
    #
    # add other_options="-flo-lt 1 -ref-lt 1" for drift compensated fusion,
    # to manage for zero-filled areas
    # GM, Ven 17 jan 2025 15:08:17 CET
    #
    if parameters is not None:
        cpp_wrapping.linear_registration(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf,
                                         py_hl=parameters.pyramid_highest_level,
                                         py_ll=parameters.pyramid_lowest_level,
                                         transformation_type=parameters.transformation_type,
                                         transformation_estimator=parameters.transformation_estimation_type,
                                         lts_fraction=parameters.lts_fraction,
                                         normalization=parameters.normalization,
                                         other_options="-flo-lt 1 -ref-lt 1",
                                         monitoring=monitoring)
    else:
        cpp_wrapping.linear_registration(path_ref, path_flo, path_output, path_output_trsf, path_init_trsf,
                                         other_options="-flo-lt 1 -ref-lt 1", monitoring=monitoring)
    return
