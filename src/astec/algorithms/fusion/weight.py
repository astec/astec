##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 22 jan 2025 18:04:09 CET
##############################################################
#
#
#
##############################################################

import numpy as np

import astec.utils.common as common
import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.components.spatial_image import SpatialImage
from astec.io.image import imread, imsave


#
#
#
#
#


monitoring = common.Monitoring()

_min_value_ = 0.1

########################################################################################
#
# function for the ad hoc computation of weights
# for the linear combination of images of the 4 cameras
#
########################################################################################


def _histogram(image, nbins=256):
    """Return histogram of image.

        Unlike `np.histogram`, this function returns the centers of bins and
        does not rebin integer arrays. For integer arrays, each integer value has
        its own bin, which improves speed and intensity-resolution.

        Parameters
        ----------
        image : array
        Input image.
        nbins : int
        Number of bins used to calculate histogram. This value is ignored for
        integer arrays.

        Returns
        -------
        hist : array
        The values of the histogram.
        bin_centers : array
        The values at the center of the bins.
        """

    proc = "_histogram"

    if not isinstance(image, SpatialImage):
        monitoring.to_log_and_console(str(proc) + ": argument image is not an ndarray")
        return None, None

    # For integer types, histogramming with bincount is more efficient.
    if np.issubdtype(image.dtype, np.integer):
        offset = 0
        if np.min(image) < 0:
            offset = np.min(image)
        hist = np.bincount(image.ravel() - offset)
        bin_centers = np.arange(len(hist)) + offset

        # clip histogram to start with a non-zero bin
        idx = np.nonzero(hist)[0][0]
        return hist[idx:], bin_centers[idx:]
    else:
        hist, bin_edges = np.histogram(image.flat, nbins)
        bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.
        return hist, bin_centers


def _threshold_otsu(image, nbins=256):
    """Return threshold value based on Otsu's method.

        Parameters
        ----------
        image : array
        Input image.
        nbins : int
        Number of bins used to calculate histogram. This value is ignored for
        integer arrays.

        Returns
        -------
        threshold : float
        Threshold value.

        References
        ----------
        .. [1] Wikipedia, http://en.wikipedia.org/wiki/Otsu's_Method
        """

    proc = "_threshold_otsu"

    if not isinstance(image, SpatialImage):
        monitoring.to_log_and_console(str(proc) + ": argument image is not an ndarray")
        return None

    #
    # kept for historical reasons.
    # Note that threshold_otsu exists in skimage.filters
    #

    hist, bin_centers = _histogram(image, nbins)
    hist = hist.astype(float)

    # class probabilities for all possible thresholds
    weight1 = np.cumsum(hist)
    weight2 = np.cumsum(hist[::-1])[::-1]
    # class means for all possible thresholds
    mean1 = np.cumsum(hist * bin_centers) / weight1
    mean2 = (np.cumsum((hist * bin_centers)[::-1]) / weight2[::-1])[::-1]

    # Clip ends to align class 1 and class 2 variables:
    # The last value of `weight1`/`mean1` should pair with zero values in
    # `weight2`/`mean2`, which do not exist.
    variance12 = weight1[:-1] * weight2[1:] * (mean1[:-1] - mean2[1:]) ** 2

    idx = np.argmax(variance12)
    threshold = bin_centers[:-1][idx]
    return threshold


def _exp_func(x, length=500, speed=5):
    """ Decay function used to take into account the remotness to the camera
    x : value to compute
    length : lenght of the function
    speed : speed of the function
    """

    #
    # has been changed to return values in [_min_value_, 1.0]
    #
    r = np.exp(-((np.float32(x) * speed) / length))
    if r > _min_value_:
        return r
    return _min_value_


def _build_guignard_weighting(image, decreasing_weight_with_z):
    """Return the mask on a given image from the decay function
    im : intensity image (SpatialImage)
    direction : if True the camera is in the side of the first slices in Z
    """
    proc = "_build_guignard_weighting"

    if not isinstance(image, SpatialImage):
        monitoring.to_log_and_console(str(proc) + ": argument image is not an ndarray")
        return None

    th = _threshold_otsu(image)
    im_th = np.zeros_like(image, dtype=np.float32)
    im_th[image > th] = 1
    if decreasing_weight_with_z is False:
        im_th = im_th[:, :, -1::-1]
    im_th_sum = np.cumsum(im_th, axis=2)
    if decreasing_weight_with_z is False:
        im_th_sum = im_th_sum[:, :, -1::-1]
    mask = _exp_func(im_th_sum, np.max(im_th_sum))

    return mask


def _build_corner_weighting(image, decreasing_weight_with_z):
    """

    :param image:
    :return:
    """
    proc = "_build_corner_weighting"

    if not isinstance(image, SpatialImage):
        monitoring.to_log_and_console(str(proc) + ": argument image is not an ndarray")
        return None

    mask = np.full_like(image, 0.1, dtype=np.float32)
    dimx = image.shape[0]
    dimz = image.shape[2]
    # build a corner-like weighting image
    # - constant z-slices (during cz slices)
    # - linear decrease along the x dimension until dimz/2 - dz is reached
    # cz : plans entiers de 1 -> [dimz-cz, dimz-1]
    # dz : decalage vers z=0 a partir de mz=dimz/2
    cz = int(dimz/8.0)
    dz = int(dimz/8.0)
    mz = int(dimz/2.0 + 0.5)
    # partie constante
    for z in range(dimz - cz, dimz):
        mask[:, :, z] = 1.0
    # partie variable
    for z in range(mz - dz, dimz - cz):
        dx = int((z - float(dimz - cz)) / float((mz - dz) - (dimz - cz)) * float(dimx / 2.0) + 0.5)
        if dimx - dx > dx:
            mask[dx:dimx-dx, :, z] = 1.0
    if decreasing_weight_with_z is True:
        mask = mask[:, :, -1::-1]
    return mask


def _build_ramp_weighting(image, decreasing_weight_with_z):
    """

    :param image:
    :return:
    """
    proc = "_build_ramp_weighting"

    if not isinstance(image, SpatialImage):
        monitoring.to_log_and_console(str(proc) + ": argument image is not an ndarray")
        return None

    mask = np.zeros_like(image, dtype=np.float32)
    dimz = image.shape[2]
    values = np.linspace(_min_value_, 1.0, num=dimz, endpoint=True)
    for z in range(0, dimz):
        mask[:, :, z] = values[z]
    if decreasing_weight_with_z is True:
        mask = mask[:, :, -1::-1]
    return mask


def _build_uniform_weighting(image):
    """

    :param image:
    :return:
    """

    proc = "_build_uniform_weighting"

    if not isinstance(image, SpatialImage):
        monitoring.to_log_and_console(str(proc) + ": argument image is not an ndarray")
        return None

    mask = np.ones_like(image, dtype=np.float32)
    return mask


def build_unreg_weighting_image(template_image_name, weighting_image_name, decreasing_weight_with_z=True,
                                fusion_weighting=None):
    """

    :param template_image_name:
    :param weighting_image_name: values are in [_min_value_(=0.1), 1.0]
    :param decreasing_weight_with_z:
    :param fusion_weighting:
    :return:
    """
    proc = "_build_unreg_weighting_image"

    im = imread(template_image_name)

    if fusion_weighting is None or not isinstance(fusion_weighting, str):
        monitoring.to_log_and_console(str(proc) + ": unknown weighting function, switch to uniform")
        unreg_weight = _build_uniform_weighting(im)
    elif fusion_weighting.lower() == 'uniform' or fusion_weighting.lower() == 'uniform-weighting':
        unreg_weight = _build_uniform_weighting(im)
    elif fusion_weighting.lower() == 'guignard' or fusion_weighting.lower() == 'guignard-weighting':
        unreg_weight = _build_guignard_weighting(im, decreasing_weight_with_z)
    elif fusion_weighting.lower() == 'corner' or fusion_weighting.lower() == 'corner-weighting':
        unreg_weight = _build_corner_weighting(im, decreasing_weight_with_z)
    elif fusion_weighting.lower() == 'ramp' or fusion_weighting.lower() == 'ramp-weighting':
        unreg_weight = _build_ramp_weighting(im, decreasing_weight_with_z)
    else:
        monitoring.to_log_and_console(str(proc) + ": unknown weighting function, switch to uniform")
        unreg_weight = _build_uniform_weighting(im)

    unreg_weight.voxelsize = im.voxelsize
    imsave(weighting_image_name, unreg_weight)

    if fusion_weighting.lower() == 'corner' or fusion_weighting.lower() == 'corner-weighting':
        cpp_wrapping.linear_smoothing(weighting_image_name, weighting_image_name, 5.0, real_scale=False,
                                      filter_type='deriche', border=10, monitoring=monitoring)

    del im
    del unreg_weight
    return


########################################################################################
#
#
#
########################################################################################


def update_weight_image(intensity_image, weight_image, new_weight_image=None):
    im = imread(intensity_image)
    we = imread(weight_image)
    #
    # do some Boolean array indexing
    #
    mask = (we == 0)
    we[mask & (im > 0)] = _min_value_
    we[im == 0] = 0
    if new_weight_image is not None and isinstance(new_weight_image, str) is True:
        imsave(new_weight_image, we)
        return
    imsave(weight_image, we)
    return
