##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 22 jan 2025 18:04:09 CET
##############################################################
#
#
#
##############################################################

import sys
import os

import astec.utils.common as common

import astec.algorithms.fusion.registration as fregistration

#
#
#
#
#


monitoring = common.Monitoring()


#
# historical fusion procedure
# each image is co-registered with the left camera acquisition of stack 30
#

def fusion_process(input_images, the_images, res_images, trsf_init, trsf_final, experiment, parameters):
    """

    :param input_images: dictionary of original images of channel #0, to set names
    :param the_images: dictionary of images to be registered.
    :param res_images: dictionary of registered images, they can be used to compute the fusion
        (ie weighted sum)
    :param trsf_init: initial transformations for fusion.
    :param trsf_final: transformations computed during fusion.
    :param experiment:
    :param parameters:
    :return:
    """

    proc = "direct.fusion_process"

    # print("input_images = " + str(input_images))
    # print("the_images = " + str(the_images))
    # print("res_images = " + str(res_images))
    # print("trsf_init = " + str(trsf_init))
    # print("trsf_final = " + str(trsf_final))
    # print("time_point = " + str(time_point))

    the_images_keys = sorted(the_images.keys())
    ref_index = the_images_keys[0]
    monitoring.to_log_and_console("    .. (direct) fusion with respect to reference image is #" + str(ref_index), 2)

    #
    #
    #
    trsf_inter = {}
    for i in the_images:
        trsf_inter[i] = common.add_suffix(input_images[i], "_reg_tmp",
                                          new_dirname=experiment.rawdata_dir.get_trsf_directory(i, 0),
                                          new_extension="trsf")
    #
    # register
    #

    for i in the_images_keys:
        if i == ref_index:
            # res_images[ref_index] = the_images[ref_index]
            continue

        monitoring.to_log_and_console("      .. process '" + the_images[i].split(os.path.sep)[-1] + "' for fusion", 2)
        monitoring.to_log_and_console("         co-registering '" + the_images[i].split(os.path.sep)[-1] + "'", 2)
        #
        # a two-fold registration, translation then affine, could be preferable
        #
        if parameters.acquisition_registration[0].compute_registration is True and \
            parameters.acquisition_registration[1].compute_registration is True:
            fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                              path_init_trsf=trsf_init[i],
                                              parameters=parameters.acquisition_registration[0])
            fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                              path_init_trsf=trsf_final[i],
                                              parameters=parameters.acquisition_registration[1])
        elif parameters.acquisition_registration[0].compute_registration is True:
            fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                              path_init_trsf=trsf_init[i],
                                              parameters=parameters.acquisition_registration[0])
        elif parameters.acquisition_registration[1].compute_registration is True:
            fregistration.linear_registration(res_images[ref_index], the_images[i], res_images[i], trsf_final[i],
                                              path_init_trsf=trsf_init[i],
                                              parameters=parameters.acquisition_registration[1])
        else:
            msg = "error, no registration to be computed?! Exiting."
            monitoring.to_log_and_console(proc + ": " + msg, 0)

        #
        # check whether the registration was successful
        #
        if not os.path.isfile(res_images[i]) or not os.path.isfile(trsf_final[i]):
            monitoring.to_log_and_console(proc + ": error when registering image " + str(i), 0)
            monitoring.to_log_and_console("   image " + str(res_images[i]) + " or transformation "
                                          + str(trsf_final[i]) + " is not existing", 0)
            monitoring.to_log_and_console("Exiting.", 0)
            sys.exit(1)

    return
