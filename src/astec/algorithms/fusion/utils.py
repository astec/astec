##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 22 jan 2025 18:04:09 CET
##############################################################
#
#
#
##############################################################

import os

import numpy as np
from scipy import ndimage as nd

import astec.utils.common as common
import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.components.spatial_image import SpatialImage
from astec.io.image import imread, imsave


#
#
#
#
#


monitoring = common.Monitoring()


########################################################################################
#
#
#
########################################################################################


def get_drift_trsf(experiment, time_point, i=0, verbose=True):

    subdir = experiment.drift_dir.get_sub_directory(i=i, verbose=verbose)
    if subdir is None:
        return None
    fulldir = os.path.join(experiment.get_embryo_path(), subdir)
    if not os.path.isdir(fulldir):
        return None

    trsfdir = None
    iteration = None
    for f in os.listdir(fulldir):
        if not os.path.isdir(os.path.join(fulldir, f)):
            continue
        if f[0:4] != 'ITER':
            continue
        pos = f.find("-TRSFS_")
        if pos == -1:
            continue
        if iteration is None:
            iteration = int(f[4:pos])
            trsfdir = f
        elif iteration < int(f[4:pos]):
            iteration = int(f[4:pos])
            trsfdir = f

    if trsfdir is None:
        return None

    trsf_name = experiment.get_embryo_name() + experiment.drift_dir.get_file_suffix() + '_t'
    trsf_name += experiment.get_time_index(int(time_point)) + '.trsf'
    trsf = os.path.join(fulldir, trsfdir, trsf_name)

    if not os.path.isfile(trsf):
        return None

    return trsf


def get_stack_trsf(experiment, time_point, i=0):
    proc = "get_stack_trsf"
    path_costack = os.path.join(experiment.get_embryo_path(), experiment.drift_dir.get_costack_path(i=i))
    if not os.path.isdir(path_costack):
        return None

    trsfs = []
    for f in os.listdir(path_costack):
        if f[-5:] == ".trsf":
            trsfs.append(f)
    if len(trsfs) == 0:
        return None
    if len(trsfs) == 1:
        return os.path.join(path_costack, trsfs[0])

    msg = "there are " + str(len(trsfs)) + " stack-to-stack transformations. Pick the first one."
    monitoring.to_log_and_console(proc + ": " + msg)

    return os.path.join(path_costack, trsfs[0])


########################################################################################
#
#
#
########################################################################################


def _get_image_shape(template_image_name):
    im = imread(template_image_name)
    shape = im.shape
    del im
    return shape


def extract_xzsection(weight_images, res_images, tmp_fused_image, channel_id, experiment, xz_section=True):
    """
    Extract XZ sections from registered raw images and weights as well as the fused image
    (before the last crop (if any))
    :param weight_images:
    :param res_images:
    :param tmp_fused_image:
    :param channel_id:
    :param experiment:
    :param xz_section:
    :return:
    """

    proc = "_extract_xzsection"
    d = experiment.fusion_dir.get_xzsection_directory(channel_id)

    xz_values = []

    if isinstance(weight_images, list) and isinstance(res_images, list):

        if xz_section is True:
            shape = _get_image_shape(res_images[0])
            xz_values = [int(shape[1] / 2)]
        elif isinstance(xz_section, int):
            xz_values = [xz_section]
        elif isinstance(xz_section, list):
            for v in xz_section:
                if isinstance(v, int):
                    xz_values += [v]
                else:
                    msg = "unhandled type for value '" + str(v) + "'. Skip it."
                    monitoring.to_log_and_console(proc + ": " + msg)
        else:
            msg = "unhandled type for 'xz_section': " + str(type(xz_section)) + ". Skip it."
            monitoring.to_log_and_console(proc + ": " + msg)
            return

        for xz in xz_values:
            options = "-xz " + str(xz)
            name = experiment.get_embryo_name() + "_xz" + str(xz)

            xzsection = os.path.join(d, name + "_stack0_lc_reg." + experiment.result_image_suffix)
            cpp_wrapping.ext_image(res_images[0], xzsection, options, monitoring=monitoring)

            xzsection = os.path.join(d, name + "_stack0_rc_reg." + experiment.result_image_suffix)
            cpp_wrapping.ext_image(res_images[1], xzsection, options, monitoring=monitoring)

            if len(res_images) == 4:
                xzsection = os.path.join(d, name + "_stack1_lc_reg." + experiment.result_image_suffix)
                cpp_wrapping.ext_image(res_images[2], xzsection, options, monitoring=monitoring)

                xzsection = os.path.join(d, name + "_stack1_rc_reg." + experiment.result_image_suffix)
                cpp_wrapping.ext_image(res_images[3], xzsection, options, monitoring=monitoring)

            xzsection = os.path.join(d, name + "_stack0_lc_weight." + experiment.result_image_suffix)
            cpp_wrapping.ext_image(weight_images[0], xzsection, options, monitoring=monitoring)

            xzsection = os.path.join(d, name + "_stack0_rc_weight." + experiment.result_image_suffix)
            cpp_wrapping.ext_image(weight_images[1], xzsection, options, monitoring=monitoring)

            if len(weight_images) == 4:
                xzsection = os.path.join(d, name + "_stack1_lc_weight." + experiment.result_image_suffix)
                cpp_wrapping.ext_image(weight_images[2], xzsection, options, monitoring=monitoring)

                xzsection = os.path.join(d, name + "_stack1_rc_weight." + experiment.result_image_suffix)
                cpp_wrapping.ext_image(weight_images[3], xzsection, options, monitoring=monitoring)

            xzsection = os.path.join(d, name + "_fuse." + experiment.result_image_suffix)
            cpp_wrapping.ext_image(tmp_fused_image, xzsection, options, monitoring=monitoring)

    elif isinstance(weight_images, dict) and isinstance(res_images, dict):

        if xz_section is True:
            keylist = list(res_images.keys())
            shape = _get_image_shape(res_images[keylist[0]])
            xz_values = [int(shape[1] / 2)]
        elif isinstance(xz_section, int):
            xz_values = [xz_section]
        elif isinstance(xz_section, list):
            for v in xz_section:
                if isinstance(v, int):
                    xz_values += [v]
                else:
                    msg = "unhandled type for value '" + str(v) + "'. Skip it."
                    monitoring.to_log_and_console(proc + ": " + msg)
        else:
            msg = "unhandled type for 'xz_section': " + str(type(xz_section)) + ". Skip it."
            monitoring.to_log_and_console(proc + ": " + msg)
            return

        for xz in xz_values:
            options = "-xz " + str(xz)
            name = experiment.get_embryo_name() + "_xz" + str(xz)

            for i in res_images:
                if i == 0:
                    xzsection = os.path.join(d, name + "_stack0_lc_reg." + experiment.result_image_suffix)
                elif i == 1:
                    xzsection = os.path.join(d, name + "_stack0_rc_reg." + experiment.result_image_suffix)
                elif i == 2:
                    xzsection = os.path.join(d, name + "_stack1_lc_reg." + experiment.result_image_suffix)
                elif i == 3:
                    xzsection = os.path.join(d, name + "_stack1_rc_reg." + experiment.result_image_suffix)
                cpp_wrapping.ext_image(res_images[i], xzsection, options, monitoring=monitoring)

            for i in res_images:
                if i == 0:
                    xzsection = os.path.join(d, name + "_stack0_lc_weight." + experiment.result_image_suffix)
                elif i == 1:
                    xzsection = os.path.join(d, name + "_stack0_rc_weight." + experiment.result_image_suffix)
                elif i == 2:
                    xzsection = os.path.join(d, name + "_stack1_lc_weight." + experiment.result_image_suffix)
                elif i == 3:
                    xzsection = os.path.join(d, name + "_stack1_rc_weight." + experiment.result_image_suffix)
                cpp_wrapping.ext_image(weight_images[i], xzsection, options, monitoring=monitoring)

            xzsection = os.path.join(d, name + "_fuse." + experiment.result_image_suffix)
            cpp_wrapping.ext_image(tmp_fused_image, xzsection, options, monitoring=monitoring)

    return


#######################################################################################
#
# cropping
#
########################################################################################


def crop_bounding_box(the_image, z_crop=False, threshold=None, opening=0, equal_xydimension=True):
    """
    Compute a bounding box to crop an image (ie extract a subimage)
    :param the_image:
    :param z_crop: True or False
    :param threshold:
    :param opening:
    :param equal_xydimension:
    :return:
    """

    #
    # build MIP projections to compute a threshold
    # threshold is MIP view average
    #

    if threshold is None:
        #
        # threshold has to be computed before any image transformation (ie by drift tranformation)
        # so that it is not polluted by 0-filled areas
        # Previously, one different threshold was computed per view (XY, YZ, and ZX), the two last being used
        # only when z-cropping wae required
        #
        # It may be preferable (no cues for that) to use one single threshold
        #
        the_xy_mip = common.add_suffix(the_image, "_xy_mip")
        the_xz_mip = common.add_suffix(the_image, "_xz_mip")
        the_zy_mip = common.add_suffix(the_image, "_zy_mip")
        cpp_wrapping.mip_projection_for_crop(the_image, the_xy_mip, the_xz_mip, the_zy_mip, monitoring=monitoring)
        mip = imread(the_xy_mip)
        xymean = np.mean(mip)
        mip = imread(the_xz_mip)
        xzmean = np.mean(mip)
        mip = imread(the_zy_mip)
        zymean = np.mean(mip)
        del mip
        local_threshold = np.mean([xymean, xzmean, zymean])
        msg = "computed thresholds for cropping were [{:.2f}, {:.2f}, {:.2f}]".format(xymean, xzmean, zymean)
        msg += ", retained threshold is {:.2f}".format(local_threshold)
    else:
        local_threshold = threshold
        msg = "threshold for cropping is {:.2f}".format(local_threshold)
    monitoring.to_log_and_console("         " + msg, 2)

    #
    # threshold the mip view, get the largest 4-connected component
    # and eventually performed a morphological opening
    #
    # Note: thresholding, connect component extraction and morphological opening can be done
    # in python. However, it was done in C because scipy.ndimage.label may have an unexpected
    # behavior when there are two many components
    #
    the_xy_selection = common.add_suffix(the_image, "_xy_cropselection")
    the_xz_selection = None
    the_zy_selection = None
    if z_crop:
        the_xz_selection = common.add_suffix(the_image, "_xz_cropselection")
        the_zy_selection = common.add_suffix(the_image, "_zy_cropselection")
    other_options = " -threshold {:.2f}".format(local_threshold)
    if opening > 0:
        other_options += " -radius {:d}".format(opening)
    cpp_wrapping.mip_projection_for_crop(the_image, the_xy_selection, the_xz_selection, the_zy_selection,
                                         other_options=other_options, monitoring=monitoring)

    #
    # above mip selection should then be binary images (values in {0, 255})
    # with one single connect component
    #

    #
    # get the bounding boxes for all objects
    # it is not necessary to searched for all labels
    # seems that there is no bounding box computed for label #0
    #
    # boundingBoxes = nd.find_objects(ccImage, max_label=maxLabel)
    # maxBox = boundingBoxes[int(maxLabel)-1]
    #
    selection = imread(the_xy_selection)
    xdim = selection.shape[0]
    ydim = selection.shape[1]
    xy_box = nd.find_objects(selection)[254]
    del selection

    xmin = xy_box[0].start
    xmax = xy_box[0].stop
    ymin = xy_box[1].start
    ymax = xy_box[1].stop

    zmin = 0
    zmax = 1
    if z_crop is True:
        selection = imread(the_xz_selection)
        xz_box = nd.find_objects(selection)[254]
        del selection
        if xmin > xz_box[0].start:
            xmin = xz_box[0].start
        if xmax < xz_box[0].stop:
            xmax = xz_box[0].stop
        zmin = xz_box[1].start
        zmax = xz_box[1].stop

        selection = imread(the_zy_selection)
        zy_box = nd.find_objects(selection)[254]
        del selection
        if zmin > zy_box[0].start:
            zmin = zy_box[0].start
        if zmax < zy_box[0].stop:
            zmax = zy_box[0].stop
        if ymin > zy_box[1].start:
            ymin = zy_box[1].start
        if ymax < zy_box[1].stop:
            ymax = zy_box[1].stop

    msg = "bounding box is [{:d}, {:d}]x[{:d}, {:d}]".format(xmin, xmax, ymin, ymax)
    if z_crop:
        msg += "x[{:d}, {:d}]".format(zmin, zmax)
    if equal_xydimension is True:
        msg = "unequal " + msg
    monitoring.to_log_and_console("         - " + msg, 2)

    if equal_xydimension is False:
        return [slice(xmin, xmax), slice(ymin, ymax), slice(zmin, zmax)], local_threshold

    # equal_dimension is True:
    xboxdim = xmax - xmin
    yboxdim = ymax - ymin
    zboxdim = zmax - zmin

    maxboxdim = max(xboxdim, yboxdim)
    if z_crop:
        maxboxdim = max(maxboxdim, zboxdim)

    if xboxdim < maxboxdim:
        xadd = int((maxboxdim - xboxdim) / 2.0)
        if xadd > 0:
            xmin = max(0, xmin - xadd)
            xmax = min(xdim, xmax + xadd)

    if yboxdim < maxboxdim:
        yadd = int((maxboxdim - yboxdim) / 2.0)
        if yadd > 0:
            ymin = max(0, ymin - yadd)
            ymax = min(ydim, ymax + yadd)

    msg = "  equal bounding box is [{:d}, {:d}]".format(xmin, xmax)
    msg += "x[{:d}, {:d}]".format(ymin, ymax)
    if z_crop:
        msg += "x[{:d}, {:d}]".format(zmin, zmax)
    monitoring.to_log_and_console("         - " + msg, 2)

    return [slice(xmin, xmax), slice(ymin, ymax), slice(zmin, zmax)], local_threshold


def crop_disk_image(the_image, res_image, the_max_box=None, z_crop=False, threshold=None,
                     margin_x_0=40, margin_x_1=40, margin_y_0=40, margin_y_1=40, margin_z_0=40, margin_z_1=40,
                     opening=0):
    """
    Crop an image on disk in XY plane
    :param the_image:
    :param res_image:
    :param the_max_box:
    :param z_crop:
    :param threshold:
    :param margin_x_0:
    :param margin_x_1:
    :param margin_y_0:
    :param margin_y_1:
    :param margin_z_0:
    :param margin_z_1:
    :param opening:
    :return:
    """

    #
    # 2D bounding box
    #
    local_threshold = None
    if the_max_box is None:
        max_box, local_threshold = crop_bounding_box(the_image, z_crop=z_crop, threshold=threshold, opening=opening)
    else:
        max_box = the_max_box

    #
    # 2D bounding box + margin
    #
    image = imread(the_image)
    voxelsize = image.get_voxelsize()

    xmin = max(max_box[0].start - margin_x_0, 0)
    xmax = min(image.shape[0], max_box[0].stop + margin_x_1)
    ymin = max(max_box[1].start - margin_y_0, 0)
    ymax = min(image.shape[1], max_box[1].stop + margin_y_1)
    zmin = 0
    zmax = image.shape[2]
    if z_crop:
        zmin = max(max_box[2].start - margin_z_0, 0)
        zmax = min(image.shape[2], max_box[2].stop + margin_z_1)

    new_box = (slice(xmin, xmax, None),
               slice(ymin, ymax, None),
               slice(zmin, zmax))

    new_image = SpatialImage(image[new_box])
    new_image.voxelsize = image.voxelsize

    imsave(res_image, new_image)

    monitoring.to_log_and_console("         crop from [0," + str(image.shape[0]) + "]x[0," + str(image.shape[1])
                                  + "]x[0," + str(image.shape[2]) + "] to [" + str(xmin) + "," + str(xmax) + "]x["
                                  + str(ymin) + "," + str(ymax) + "]x[" + str(zmin) + "," + str(zmax) + "]", 2)

    #
    # transformation matrix (translation)
    #
    mat = np.identity(4)
    mat[0, 3] = xmin * voxelsize[0]
    mat[1, 3] = ymin * voxelsize[1]
    mat[2, 3] = zmin * voxelsize[2]
    return mat, local_threshold
