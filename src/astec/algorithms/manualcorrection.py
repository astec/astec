##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#
##############################################################
#
#
#
##############################################################

import os
import sys
import time
import numpy as np
from scipy import ndimage as nd
import copy
import pickle as pkl

import astec.algorithms.mars as mars
import astec.algorithms.astec as astec
import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.utils.diagnosis as udiagnosis
import astec.utils.ioproperties as ioproperties
import astec.utils.properties as properties
import astec.utils.reconstruction as reconstruction
import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.components.spatial_image import SpatialImage
from astec.io.image import imread, imsave


#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
# classes
# - computation parameters
#
########################################################################################


class ManualCorrectionParameters(udiagnosis.DiagnosisParameters, astec.AstecParameters):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix="mancor_"):

        if "doc" not in self.__dict__:
            self.doc = {}

        udiagnosis.DiagnosisParameters.__init__(self, prefix=prefix)
        astec.AstecParameters.__init__(self, prefix=prefix)

        doc = "\n"
        doc += "Manual correction overview:\n"
        doc += "===========================\n"
        doc += "1. Fuses cells/labels from the segmentation to correct over-segmentation errors\n"
        doc += "2. Splits cells/labels from the segmentation to correct under-segmentation errors.\n"
        doc += "   Division is done by exploring a rangle of values of 'hmin' until two seeds are\n"
        doc += "     found inside the cell to be divided.\n"
        doc += "   Division is propagated for segmented time series as in first step of 'astec'\n"
        doc += "   according a lineage is present in the input segmentation directory\n"
        doc += "     a. segmentation of previous time point is deformed onto the current time point.\n"
        doc += "     b. cells are eroded to get two seeds inside the cell to be divided\n"
        doc += "     c. segmentation (watershed-based) from the deformed seeds\n"
        doc += " Transform segmentation images from the SEG/SEG_'EXP_SEG_FROM' directory to "
        doc += " segmentation images in the SEG/SEG_'EXP_SEG_TO' directory"
        self.doc['manualcorrection_overview'] = doc

        doc = "\t Directory containing the manual correction files\n"
        self.doc['manualcorrection_dir'] = doc
        self.manualcorrection_dir = None

        doc = "\t File containing the labels to be fused.\n"
        doc += "\t Each line may either indicate a comment, a division or a fusion to be made\n"
        doc += "\t - lines beginning by '#' are ignored (comment)\n"
        doc += "\t - lines with only numbers concern changes for the first time point:\n"
        doc += "\t   - one single number: label of the cell to be splitted at the first time point\n"
        doc += "\t   - several numbers: labels of the cells to be fused\n"
        doc += "\t - lines beginning by 'timevalue:' concern changes for the given time point\n"
        doc += "\t   - 'timevalue:' + one single number: label of the cell to be splitted\n"
        doc += "\t   - 'timevalue:' + several numbers: labels of the cells to be fused\n"
        doc += "\t - lines beginning by 'timevalue-timevalue:' concern changes for the given time point range\n"
        doc += "\t   - 'timevalue-timevalue:' + several numbers: labels of the cells to be fused\n"
        doc += "\t      if a lineage is present, it is used to track the cell labels to be fused\n"
        doc += "\t      else the same labels are fused at each time point of the range\n"
        self.doc['manualcorrection_file'] = doc
        self.manualcorrection_file = None

        doc = "\t Use new (unused) labels for created cells"
        self.doc['use_new_labels'] = doc
        self.use_new_labels = False

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print('')
        print('#')
        print('# ManualCorrectionParameters ')
        print('#')
        print('')

        common.PrefixedParameter.print_parameters(self)

        udiagnosis.DiagnosisParameters.print_parameters(self)
        astec.AstecParameters.print_parameters(self)

        for line in self.doc['manualcorrection_overview'].splitlines():
            print('# ' + line)

        self.varprint('manualcorrection_dir', self.manualcorrection_dir, self.doc['manualcorrection_dir'])
        self.varprint('manualcorrection_file', self.manualcorrection_file, self.doc['manualcorrection_file'])
        self.varprint('use_new_labels', self.use_new_labels, self.doc['use_new_labels'])

        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write('\n')
        logfile.write('#' + '\n')
        logfile.write('# ManualCorrectionParameters ' + '\n')
        logfile.write('#' + '\n')
        logfile.write('\n')

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        udiagnosis.DiagnosisParameters.write_parameters_in_file(self, logfile)
        astec.AstecParameters.write_parameters_in_file(self, logfile)

        for line in self.doc['manualcorrection_overview'].splitlines():
            logfile.write('# ' + line + '\n')

        self.varwrite(logfile, 'manualcorrection_dir', self.manualcorrection_dir,
                      self.doc.get('manualcorrection_dir', None))
        self.varwrite(logfile, 'manualcorrection_file', self.manualcorrection_file,
                      self.doc.get('manualcorrection_file', None))
        self.varwrite(logfile, 'use_new_labels', self.use_new_labels, self.doc.get('use_new_labels', None))
        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        udiagnosis.DiagnosisParameters.update_from_parameters(self, parameters)
        astec.AstecParameters.update_from_parameters(self, parameters)

        self.manualcorrection_dir = self.read_parameter(parameters, 'manualcorrection_dir', self.manualcorrection_dir)
        self.manualcorrection_file = self.read_parameter(parameters, 'manualcorrection_file',
                                                         self.manualcorrection_file)
        self.manualcorrection_file = self.read_parameter(parameters, 'mapping_file', self.manualcorrection_file)
        self.use_new_labels = self.read_parameter(parameters, 'use_new_labels', self.use_new_labels)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


########################################################################################
#
# some internal procedures
#
########################################################################################


def _check_division(division):
    for t in division:
        d = list(set(division[t]))
        if len(d) < len(division[t]):
            msg = "warning, there are duplicates labels for cell division at time " + str(t)
            msg += ": " + str(division[t])
            monitoring.to_log_and_console("    ... " + msg)
            division[t] = d
    return division


def _read_correction_file(dirname, filename, lineage=None, first_time_point=1, time_digits_for_cell_id=4):
    """

    Parameters
    ----------
    dirname: where to find 'filename'
    filename: filename of the corrections.
    For fusion, the admitted syntax are
    - for fusion at one given time point (if 't:' is omitted, it is applied at the first time point)
        [t:] label label [label ...]
    - for fusion at an interval of time points:
        t-t: label label [label ...]
      If no lineage is given, the same labels are used for the whole interval. If there is a lineage,
      labels at next time points are issued from the lineage.
    For division, the admitted syntax is
        [t:] label
    lineage:
    time_digits_for_cell_id

    Returns
    -------
        2 dictionaries (fusion, division) indexed either by 'first_time_point' or by an integer designing a
        time point.
        - fusion[t] is also a dictionary, whose keys are cell labels. fusion[t][c] is an array of equivalent labels
          for label 'c'
        - division[t] is a list of cell labels

    """
    proc = "_read_correction_file"
    fusion = {}
    division = {}
    separator_time_interval = ['-', ':']
    separator_time_label = [':', ',']

    if dirname is not None and len(str(dirname)) > 0:
        if not os.path.isdir(dirname):
            monitoring.to_log_and_console(proc + ": '" + dirname + "' is not a valid directory. Exiting.")
            sys.exit(1)
        else:
            local_filename = os.path.join(dirname, filename)
    else:
        local_filename = filename

    if not os.path.isfile(local_filename):
        monitoring.to_log_and_console(proc + ": '" + local_filename + "' is not a valid file. Exiting.")
        sys.exit(1)
    f = open(local_filename)
    i = 0
    for line in f:
        i += 1
        # remove leading and trailing whitespaces
        li = line.strip()
        # skip comment
        if li.startswith('#'):
            continue
        # empty line
        if len(li) == 0:
            continue
        li = li.split()

        # one component: single integer, division at first time point
        if len(li) == 1:
            if li[0].isdigit():
                division[first_time_point] = division.get(first_time_point, []) + [int(li[0])]
                continue
            msg = "line #" + str(i) + ": '" + str(line) + "' should be a single integer?! Skip the line."
            monitoring.to_log_and_console(proc + ": " + msg)
            continue

        # two or more components: only integers or "t:" + integers
        # last component(s) should be only integer
        error = False
        for i in range(1, len(li)):
            if not li[i].isdigit():
                msg = str(i+1) + "th term of line #" + str(i) + ": '" + str(line) + "' should be numbers."
                msg += " Skip the line."
                monitoring.to_log_and_console(proc + ": " + msg)
                error = True
                break
        if error:
            continue

        # only cell labels: fusion
        if li[0].isdigit():
            labels = [int(lab) for lab in li]
            if min(labels) == max(labels):
                msg = "line #" + str(i) + ": '" + str(line) + "' has the same label '" + str(li[0])
                msg += "for fusion?! ... Skip the line."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            if first_time_point not in fusion:
                fusion[first_time_point] = {}
            # use minimal label as destination label
            for lab in labels:
                if lab > min(labels):
                    fusion[first_time_point][lab] = fusion[first_time_point].get(lab, []) + [min(labels)]
            continue

        # first term is not a number, try to recognize time (interval)
        # "t:" + integer -> division
        # "t:" + integers -> fusion
        # "t-t:" + integers -> fusion
        if li[0][-1] not in separator_time_label:
            msg = "last character of first term of line #" + str(i) + ": '" + str(line) + "' should be in "
            msg += str(separator_time_label) + "."
            msg += " Skip the line."
            monitoring.to_log_and_console(proc + ": " + msg)
            continue

        # "t:" + integer(s)
        if li[0][:-1].isdigit():
            ctime = int(li[0][:-1])
            # "t:" + integer -> division
            if len(li) == 2:
                division[ctime] = division.get(ctime, []) + [int(li[1])]
                continue
            # "t:" + integers -> fusion
            labels = [int(li[i]) for i in range(1, len(li))]
            if ctime not in fusion:
                fusion[ctime] = {}
            # use minimal label as destination label
            for lab in labels:
                if lab > min(labels):
                    fusion[ctime][lab] = fusion[ctime].get(lab, []) + [min(labels)]
            continue

        # "t-t:" + integers -> fusion
        # there is one time interval separator
        count_separator_time_interval = 0
        for c in li[0][:-1]:
            if c in separator_time_interval:
                count_separator_time_interval += 1
        if count_separator_time_interval != 1:
            msg = "weird first term of line #" + str(i) + ": '" + str(line) + "'."
            msg += "No or too many time interval separator. "
            msg += " Skip the line."
            monitoring.to_log_and_console(proc + ": " + msg)
            continue

        # the time separator cuts the line into two parts (should be true if there is only one separator)
        ti = []
        for s in separator_time_interval:
            if s in li[0][:-1]:
                ti = li[0][:-1].split(s)
        if len(ti) != 2:
            msg = "weird first term of line #" + str(i) + ": '" + str(line) + "'."
            msg += "Should be cut into two pieces."
            msg += " Skip the line."
            monitoring.to_log_and_console(proc + ": " + msg)
            continue
        for t in ti:
            if not t.isdigit():
                msg = "weird first term of line #" + str(i) + ": '" + str(line) + "'."
                msg += "Should be cut into two integers."
                msg += " Skip the line."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue

        # get the time interval
        mintime = min([int(t) for t in ti])
        maxtime = max([int(t) for t in ti])

        # "t-t:" + integer -> division?
        if len(li) == 2:
            if mintime != maxtime:
                msg = "line #" + str(i) + ": '" + str(line) + "'."
                msg += "Can not do a division during a time interval."
                msg += " Skip the line."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            division[mintime] = division.get(mintime, []) + [int(li[1])]
            continue

        # "t-t:" + integers -> fusion
        labels = [int(li[i]) for i in range(1, len(li))]
        # fuse labels for a time interval
        if lineage is None:
            for ctime in range(mintime, maxtime+1):
                if ctime not in fusion:
                    fusion[ctime] = {}
                # use minimal label as destination label
                for lab in labels:
                    if lab > min(labels):
                        fusion[ctime][lab] = fusion[ctime].get(lab, []) + [min(labels)]
            continue

        # tracks labels in lineage
        for ctime in range(mintime, maxtime + 1):
            for lab in labels:
                if lab > min(labels):
                    fusion[ctime][lab] = fusion[ctime].get(lab, []) + [min(labels)]
            if ctime == maxtime:
                break
            prev_cell_ids = [10 ** time_digits_for_cell_id + lab for lab in labels]
            next_cell_ids = []
            for c in prev_cell_ids:
                if c not in lineage:
                    continue
                next_cell_ids += lineage[c]
            if len(next_cell_ids) == 0:
                msg = "line #" + str(i) + ": '" + str(line) + "'."
                msg += "There is no cells after time '" + str(ctime) + "'."
                monitoring.to_log_and_console(proc + ": " + msg)
                break
            labels = [c - (c // 10 ** time_digits_for_cell_id) for c in next_cell_ids]

    f.close()

    # correction of input file
    division = _check_division(division)

    return fusion, division

########################################################################################
#
#
#
########################################################################################


def _diagnosis_volume_image(output_image, parameters):

    im = imread(output_image)
    voxelsize = im.voxelsize
    vol = voxelsize[0] * voxelsize[1] * voxelsize[2]

    cell_label = np.unique(im)
    cell_volume = nd.sum(np.ones_like(im), im, index=np.int16(cell_label))

    volumes = zip(cell_label, cell_volume)
    volumes = sorted(volumes, key=lambda x: x[1])

    monitoring.to_log_and_console('    Number of cells: ' + str(len(cell_label)), 0)
    monitoring.to_log_and_console('    Maximal label: ' + str(np.max(im)), 0)
    # monitoring.to_log_and_console('    Cell ids: ' + str(cell_label), 0)

    monitoring.to_log_and_console('    Sorted cell volumes: ', 0)
    monitoring.to_log_and_console('      Id :    voxels          (um^3)', 0)

    if parameters.items <= 0 or parameters.items >= len(volumes):
        for v in volumes:
            msg = '    {:>4d} : {:>9d} {:>15s}'.format(v[0], int(v[1]), '({:.2f})'.format(v[1]*vol))
            monitoring.to_log_and_console(msg, 0)
    else:
        if int(parameters.items) > 0:
            for v in volumes[:parameters.items]:
                msg = '    {:>4d} : {:>9d} {:>15s}'.format(v[0], int(v[1]), '({:.2f})'.format(v[1] * vol))
                monitoring.to_log_and_console(msg, 0)
            monitoring.to_log_and_console('       ...', 0)
            for v in volumes[-parameters.items:]:
                msg = '    {:>4d} : {:>9d} {:>15s}'.format(v[0], int(v[1]), '({:.2f})'.format(v[1] * vol))
                monitoring.to_log_and_console(msg, 0)


########################################################################################
#
#
#
########################################################################################

def _image_cell_fusion(input_image, output_image, current_time, lineage_tree, fusion, time_digits_for_cell_id=4):
    """

    Parameters
    ----------
    input_image
    output_image
    current_time
    lineage_tree
    fusion
    time_digits_for_cell_id

    Returns
    -------

    """
    proc = "_image_cell_fusion"
    im = imread(input_image)
    voxelsize = im.get_voxelsize()
    datatype = im.dtype

    #
    #
    #
    immax = np.max(im)
    mapping = np.arange(immax + 1)
    for lab in fusion[current_time]:
        eqlabel = min(min(fusion[current_time][lab]), mapping[lab])
        labels = [lab] + fusion[current_time][lab] + [mapping[lab]]
        for i in range(immax + 1):
            if mapping[i] in labels:
                mapping[i] = eqlabel

    im = mapping[im]
    imsave(output_image, SpatialImage(im, voxelsize=voxelsize).astype(datatype))

    #
    #
    #
    if lineage_tree is None:
        return None

    property_keys = list(lineage_tree.keys())
    reverse_lineage = None
    if 'cell_lineage' in property_keys:
        reverse_lineage = {v: k for k, values in lineage_tree['cell_lineage'].items() for v in values}
    div = 10 ** time_digits_for_cell_id

    for i in range(immax + 1):
        if mapping[i] == i:
            continue
        label = current_time * div + i
        eqlabel = current_time * div + mapping[i]
        msg = "    - time " + str(current_time) + ": fuse cell #" + str(i) + " with cell #" + str(mapping[i])
        monitoring.to_log_and_console(msg)

        for k in property_keys:
            if k == 'cell_volume':
                if mapping[i] > 1:
                    lineage_tree['cell_volume'][eqlabel] += lineage_tree['cell_volume'][label]
                del lineage_tree['cell_volume'][label]
            elif k == 'cell_lineage':
                #
                # fusion with background
                #
                if eqlabel == 1:
                    if label in lineage_tree['cell_lineage']:
                        del lineage_tree['cell_lineage'][label]
                #
                # check wrt mother cell
                # if both are in reverse_lineage and do not have the same mother, do nothing for the lineage
                #
                elif eqlabel in reverse_lineage and label in reverse_lineage and \
                        reverse_lineage[label] != reverse_lineage[eqlabel]:
                    msg = "warning, cells " + str(i) + " and " + str(mapping[i]) + " are fused at time "
                    msg += str(current_time) + " but they have different parent cells.\n"
                    msg += "\t Do not change the lineage."
                    monitoring.to_log_and_console(proc + ": " + msg)
                else:
                    #
                    # manage mother
                    # - set label mother to eqlabel if eqlabel do not have a mother
                    # - remove label as a daughter
                    #
                    if label in reverse_lineage:
                        mother = reverse_lineage[label]
                        if eqlabel not in reverse_lineage:
                            lineage_tree['cell_lineage'][mother] += [eqlabel]
                        lineage_tree['cell_lineage'][mother].remove(label)
                    #
                    # manage label progeny, if any
                    #
                    if label in lineage_tree['cell_lineage']:
                        if eqlabel in lineage_tree['cell_lineage']:
                            lineage_tree['cell_lineage'][eqlabel] += lineage_tree['cell_lineage'][label]
                        else:
                            lineage_tree['cell_lineage'][eqlabel] = lineage_tree['cell_lineage'][label]
                        del lineage_tree['cell_lineage'][label]
            #
            # unhandled key
            #
            else:
                del lineage_tree[k]

    return lineage_tree, fusion


########################################################################################
#
#
#
########################################################################################


def _get_reconstructed_image(previous_time, current_time, experiment, reconstruction_dir, parameters):
    proc = "_get_reconstructed_image"

    #
    # used in _image_cell_division()
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(reconstruction_dir, datadir.GenericSubdirectory):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'reconstruction_dir' variable: "
                                      + str(type(reconstruction_dir)))
        sys.exit(1)

    if not isinstance(parameters, reconstruction.ReconstructionParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    #
    # is there a reconstruction image?
    # use of common.find_file allows to find any image (may have been compressed)
    #
    rec_dir = reconstruction_dir.get_directory(0)
    reconstructed_name = reconstruction_dir.get_image_name(current_time)
    reconstructed_image = common.find_file(rec_dir, reconstructed_name, file_type='image', callfrom=proc,
                                           local_monitoring=monitoring, verbose=False)

    if reconstructed_image is not None:
        reconstructed_image = os.path.join(rec_dir, reconstructed_image)
        if os.path.isfile(reconstructed_image) and monitoring.forceResultsToBeBuilt is False:
            monitoring.to_log_and_console("    .. reconstructed image is '" +
                                          str(reconstructed_image).split(os.path.sep)[-1] + "'", 2)
            return reconstructed_image

    #
    # build the reconstructed image
    #
    experiment.working_dir = experiment.astec_dir
    output_reconstructed_image = reconstruction.build_reconstructed_image(current_time, experiment,
                                                                          reconstruction_dir, parameters=parameters,
                                                                          previous_time=previous_time)
    if output_reconstructed_image is None or not os.path.isfile(output_reconstructed_image):
        monitoring.to_log_and_console("    .. " + proc + ": no reconstructed image was found/built for time "
                                      + str(current_time))
        return False
    return output_reconstructed_image


def _slices_dilation_iteration(slices, maximum):
    return tuple([slice(max(0, s.start-1), min(s.stop+1, maximum[i])) for i, s in enumerate(slices)])


def _slices_dilation(slices, maximum, iterations=1):
    for i in range(iterations):
        slices = _slices_dilation_iteration(slices, maximum)
    return slices


def _get_bounding_box(input_image, experiment, division, tmp_prefix_name, dilation_iterations=20, label_width=5):
    """
    Compute bounding boxes (including a margin) of cell of interest (cells that will divide)
    and extract subimages around these cells.
    Parameters
    ----------
    input_image
    experiment
    division
    tmp_prefix_name
    dilation_iterations

    Returns
    -------
    A dictionary of bounding boxes indexed by cell labels.
    """
    segmentation = imread(input_image)
    voxelsize = segmentation.get_voxelsize()
    datatype = segmentation.dtype

    bounding_boxes = {}
    #
    # bounding box are of the form [(slice(191, 266, None), slice(413, 471, None), slice(626, 692, None))]
    # ie [(xmin, xmax), (ymin, ymax), (zmin, zmax)]
    # see https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.find_objects.html
    # segmentation[bounding_boxes[c][0]] yields a smaller array defining by the bounding box
    #
    for c in division:
        bb = nd.find_objects(segmentation == c)
        bounding_boxes[c] = _slices_dilation(bb[0], maximum=segmentation.shape, iterations=dilation_iterations)
        cellid = str('{:0{width}d}'.format(c, width=label_width))
        cell_prefix_name = tmp_prefix_name + "_cell" + cellid + "_seg." + experiment.default_image_suffix
        if not os.path.isfile(cell_prefix_name):
            ext_segmentation = segmentation[bounding_boxes[c]]
            imsave(cell_prefix_name,  SpatialImage(ext_segmentation, voxelsize=voxelsize).astype(datatype))
            del ext_segmentation
    del segmentation
    return bounding_boxes


def _two_seeds_extraction(first_segmentation, cellid, image_for_seed, experiment, parameters):
    #
    # inspired by _cell_based_h_minima() in algorithms/astec.py
    #
    """

    Parameters
    ----------
    first_segmentation
    cellid
    image_for_seed
    parameters

    Returns
    -------

    """

    #
    # h-minima extraction with h = max value
    # the difference image is kept for further computation
    #
    h_max = parameters.watershed_seed_hmin_max_value
    wparam = mars.WatershedParameters(obj=parameters)
    wparam.seed_hmin = h_max
    h_min = h_max

    input_image = image_for_seed
    unmasked_seed_image = common.add_suffix(image_for_seed, "_unmasked_seed_h" + str('{:03d}'.format(h_min)),
                                            new_dirname=experiment.astec_dir.get_tmp_directory(),
                                            new_extension=experiment.default_image_suffix)
    seed_image = common.add_suffix(image_for_seed, "_seed_h" + str('{:03d}'.format(h_min)),
                                   new_dirname=experiment.astec_dir.get_tmp_directory(),
                                   new_extension=experiment.default_image_suffix)
    difference_image = common.add_suffix(image_for_seed, "_seed_diff_h" + str('{:03d}'.format(h_min)),
                                         new_dirname=experiment.astec_dir.get_tmp_directory(),
                                         new_extension=experiment.default_image_suffix)

    if not os.path.isfile(seed_image) or not os.path.isfile(difference_image) \
            or monitoring.forceResultsToBeBuilt is True:
        #
        # computation of labeled regional minima
        # -> keeping the 'difference' image allows to speed up the further computation
        #    for smaller values of h
        #
        mars.build_seeds(input_image, difference_image, unmasked_seed_image, experiment, wparam)
        #
        # select only the 'seeds' that are totally included in cells
        #
        cpp_wrapping.mc_mask_seeds(unmasked_seed_image, first_segmentation, seed_image)

    #
    # make a binary image (cell_segmentation) with two labels 0 and cellid
    #
    im_segmentation = imread(first_segmentation)
    cell_segmentation = np.zeros_like(im_segmentation)
    cell_segmentation[im_segmentation == cellid] = cellid
    np_unique = np.unique(cell_segmentation)
    if len(np_unique) != 2:
        monitoring.to_log_and_console('       .. weird, sub-image of cell ' + str(cellid) + ' contains '
                                      + str(len(np_unique)) + ' labels = ' + str(np_unique), 2)
    del im_segmentation

    checking = True
    while checking:

        #
        # get the seeds inside the cell
        #
        im_seed = imread(seed_image)
        labels = list(np.unique(im_seed[cell_segmentation == cellid]))
        if 0 in labels:
            labels.remove(0)

        #
        # two seeds? we're done
        # create an image with 0, 2, and 3 labels
        #
        if len(labels) == 2:
            two_seeds = (2 * (im_seed == labels[0]) + 3 * (im_seed == labels[1])).astype(im_seed.dtype)
            res_image = common.add_suffix(image_for_seed, "_two_seeds",
                                          new_dirname=experiment.astec_dir.get_tmp_directory(),
                                          new_extension=experiment.default_image_suffix)
            imsave(res_image, two_seeds)
            del two_seeds
            del im_seed
            return 2
        #
        #
        #
        elif len(labels) > 2:
            monitoring.to_log_and_console('       .. too many extrema/seeds (' + str(len(labels)) + ') for cell ' +
                                          str(cellid), 2)
            return len(labels)

        #
        # change h_min
        #
        h_min -= parameters.watershed_seed_hmin_delta_value
        if h_min < parameters.watershed_seed_hmin_min_value:
            monitoring.to_log_and_console('       .. last extrema/seeds number was (' + str(len(labels)) + ') for cell '
                                          + str(cellid), 2)
            return len(labels)

        wparam.seed_hmin = h_min

        input_image = difference_image
        unmasked_seed_image = common.add_suffix(image_for_seed, "_unmasked_seed_h" + str('{:03d}'.format(h_min)),
                                                new_dirname=experiment.astec_dir.get_tmp_directory(),
                                                new_extension=experiment.default_image_suffix)
        seed_image = common.add_suffix(image_for_seed, "_seed_h" + str('{:03d}'.format(h_min)),
                                       new_dirname=experiment.astec_dir.get_tmp_directory(),
                                       new_extension=experiment.default_image_suffix)
        difference_image = common.add_suffix(image_for_seed, "_seed_diff_h" + str('{:03d}'.format(h_min)),
                                             new_dirname=experiment.astec_dir.get_tmp_directory(),
                                             new_extension=experiment.default_image_suffix)

        if not os.path.isfile(seed_image) or not os.path.isfile(difference_image) \
                or monitoring.forceResultsToBeBuilt is True:
            mars.build_seeds(input_image, difference_image, unmasked_seed_image, experiment, wparam,
                             operation_type='max')
            cpp_wrapping.mc_mask_seeds(unmasked_seed_image, first_segmentation, seed_image)


def _two_seeds_propagation(c, oldlabels, experiment, tmp_prefix_name, label_width=4):
    proc = "_two_seeds_propagation"

    if len(oldlabels) != 2:
        msg = "\t weird, {:d} labels ".format(len(oldlabels)) + " are given as parameters: " + str(oldlabels)
        monitoring.to_log_and_console("       " + str(proc) + ": " + msg)

    #
    # try to get two seeds
    #
    cellid = str('{:0{width}d}'.format(c, width=label_width))
    cell_seg_name = tmp_prefix_name + "_cell" + cellid + "_seg." + experiment.default_image_suffix
    cell_def_seed_name = tmp_prefix_name + "_cell" + cellid + "_deformed_seeds." + experiment.default_image_suffix

    #
    #
    #
    imseg = imread(cell_seg_name)
    im_seed = imread(cell_def_seed_name)
    labels = list(np.unique(im_seed[imseg == c]))
    if 0 in labels:
        labels.remove(0)
    del imseg

    if len(labels) > 2:
        msg = "       " + str(proc) + ": weird, more than 2 labels were found"
        monitoring.to_log_and_console(msg)
        msg = "\t found " + str(labels) + " instead of " + str(oldlabels)
        monitoring.to_log_and_console(msg)
        labels_to_be_removed = []
        for la in labels:
            if la not in oldlabels:
                im_seed[im_seed == la] = 0
                labels_to_be_removed += [la]
        for la in labels_to_be_removed:
            labels.remove(la)

    #
    # two seeds? we're done
    # create an image with 0, 2, and 3 labels
    #
    if len(labels) == 2:
        two_seeds = (2 * (im_seed == labels[0]) + 3 * (im_seed == labels[1])).astype(im_seed.dtype)
        res_image = tmp_prefix_name + "_cell" + cellid + "_seeds_two_seeds." + experiment.default_image_suffix
        imsave(res_image, two_seeds)
        del two_seeds
        del im_seed
        return labels

    return labels


def _two_seeds_watershed(c, experiment, parameters, tmp_prefix_name, label_width=4):
    proc = "_two_seeds_watershed"
    #
    # try to get two seeds
    #
    cellid = str('{:0{width}d}'.format(c, width=label_width))
    cell_seg_name = tmp_prefix_name + "_cell" + cellid + "_seg." + experiment.default_image_suffix
    #
    #
    #
    cell_seed_name = tmp_prefix_name + "_cell" + cellid + "_seeds_two_seeds." + experiment.default_image_suffix
    cell_memb_name = tmp_prefix_name + "_cell" + cellid + "_membrane." + experiment.default_image_suffix
    cell_new_memb_name = tmp_prefix_name + "_cell" + cellid + "_new_membrane." + experiment.default_image_suffix
    cell_new_seed_name = tmp_prefix_name + "_cell" + cellid + "_new_seeds." + experiment.default_image_suffix
    #
    # get cell external border from cell segmentation
    #
    imseg = imread(cell_seg_name)
    binseg = np.zeros_like(imseg)
    binseg[imseg == c] = 1
    dilseg = nd.binary_dilation(binseg, iterations=2)
    cellborder = dilseg - binseg
    del imseg
    del binseg

    #
    # set membrane signal to maxmembrane: it ensures that the watershed will "stay" inside the cell
    # use also the eroded cell background as a seed
    #
    immemb = imread(cell_memb_name)
    maxmembrane = np.max(immemb) + 1
    if immemb.dtype == np.uint8 and maxmembrane > 255:
        immemb = immemb.astype(np.uint16)
    elif immemb.dtype == np.uint16 and maxmembrane > 65535:
        msg = "can not set a larger value to cell external contour (overflow at " + str(maxmembrane) + ")"
        monitoring.to_log_and_console(proc + ": " + msg)
        maxmembrane = 65535
    elif immemb.dtype == np.uint16 and maxmembrane > 32767:
        msg = "can not set a larger value to cell external contour (overflow at " + str(maxmembrane) + ")"
        monitoring.to_log_and_console(proc + ": " + msg)
        maxmembrane = 32767
    immemb[cellborder == 1] = maxmembrane
    imsave(cell_new_memb_name, immemb)
    del immemb
    del cellborder

    #
    # seed labels are [0, 2, 3]
    # add a label at 1
    #
    imseed = imread(cell_seed_name)
    imseed[dilseg == 0] = 1
    imsave(cell_new_seed_name, imseed)
    del dilseg
    del imseed

    #
    # watershed from the two seeds
    #
    cell_wate_name = tmp_prefix_name + "_cell" + cellid + "_watershed." + experiment.default_image_suffix
    mars.watershed(cell_new_seed_name, cell_new_memb_name, cell_wate_name, parameters)

    return


def _update_image_properties(im_segmentation, tmp_prefix_name, lineage_tree, experiment, new_propagated_division,
                             bounding_boxes, current_time, dividing_label, newlabels, previous_time=None,
                             previous_labels=None,
                             label_width=4, time_digits_for_cell_id=4):
    """

    :param im_segmentation:
    :param tmp_prefix_name:
    :param lineage_tree:
    :param experiment:
    :param new_propagated_division:
    :param bounding_boxes:
    :param current_time:
    :param dividing_label:
        label of the divided cell, existing cell in the current image
    :param newlabels:
        newlabels are the two labels after division
        it may be reused labels as
        newlabels[0] is a reused label
        newlabels[1] is a newly created label
    :param previous_time:
    :param previous_labels:
        in case of first division, list of one element being the cell label of the mother cell
        it is then at previous_time
    :param label_width:
    :param time_digits_for_cell_id:
    :return:
    """

    div = 10 ** time_digits_for_cell_id

    #
    # watershed has 3 labels 1, 2 and 3
    #
    cellid = str('{:0{width}d}'.format(dividing_label, width=label_width))
    cell_wate_name = tmp_prefix_name + "_cell" + cellid + "_watershed." + experiment.default_image_suffix
    #
    # update segmentation
    #
    newseg = imread(cell_wate_name)
    newseg[im_segmentation[bounding_boxes[dividing_label]] != dividing_label] = 0
    im_segmentation[bounding_boxes[dividing_label]][newseg == 2] = newlabels[0]
    im_segmentation[bounding_boxes[dividing_label]][newseg == 3] = newlabels[1]

    #
    # update volumes
    #
    cell_label = np.unique(newseg)
    cell_volume = nd.sum(np.ones_like(newseg), newseg, index=np.int16(cell_label))

    if 2 not in cell_label or 3 not in cell_label:
        msg = "weird, labels of cell watershed are " + str(cell_label)
        msg += " but should contain {2, 3}. Stop processing cell " + str(dividing_label)
        monitoring.to_log_and_console("    .. " + msg)
        return new_propagated_division
    newseg_volumes = dict(zip(cell_label, cell_volume))

    #
    # newlabels are the two labels after division
    #
    cellid1 = current_time * div + newlabels[0]
    cellid2 = current_time * div + newlabels[1]
    if cellid2 in lineage_tree['cell_volume']:
        monitoring.to_log_and_console("    .. weird, " + str(cellid2) + " was already in volume dictionary")
    lineage_tree['cell_volume'][cellid1] = int(newseg_volumes[2])
    lineage_tree['cell_volume'][cellid2] = int(newseg_volumes[3])

    #
    # remove dividing_label from volume if required
    #
    d_cellid = current_time * div + dividing_label
    if dividing_label not in newlabels:
        del lineage_tree['cell_volume'][d_cellid]

    #
    # update lineage
    # in image 'current_time' cell 'dividing_label' has been divided in (newlabels[0], newlabels[1])
    # case #1
    #   it was a first division
    #   lineage[ previous_labels[0] ] should be equal do 'dividing_label'
    #     lineage[ previous_labels[0] ] is updated to (newlabels[0], newlabels[1]) = [cellid1, cellid2]
    #   lineage[ 'dividing_label' ] is given to cellid1
    #
    # case #2
    #   it was a division propagation
    #   lineage[ previous_labels[0] ] should be equal do 'dividing_label'
    #     lineage[ previous_labels[0] ] is updated to [celldi1]
    #     lineage[ previous_labels[1] ] is updated to [celldi2]
    #   lineage[ 'dividing_label' ] is given to cellid1
    #
    if previous_labels is not None:

        #
        # mother cell id of the only one or of the first previous_labels
        # should have only one daughter and it should be 'dividing_label'
        #
        m_cellid = previous_time * div + previous_labels[0]
        if m_cellid in lineage_tree['cell_lineage']:
            if len(lineage_tree['cell_lineage'][m_cellid]) > 1:
                msg = "      .. weird, cell {:d} ".format(m_cellid)
                msg += "has {:d} daughters (only one was expected".format(len(lineage_tree['cell_lineage'][m_cellid]))
                monitoring.to_log_and_console(msg)
                msg = "         lineage is not updated"
                monitoring.to_log_and_console(msg)
                return new_propagated_division
        # dividing cell id
        # should be the only daughter of m_cellid
        d_cellid = current_time * div + dividing_label
        if lineage_tree['cell_lineage'][m_cellid][0] != d_cellid:
            msg = "      .. weird, daughter of cell {:d} ".format(m_cellid)
            msg += "has cell id {:d} ".format(lineage_tree['cell_lineage'][m_cellid][0])
            msg += "instead of {:d}".format(d_cellid)
            msg = "         lineage is not updated"
            monitoring.to_log_and_console(msg)
            return new_propagated_division

        if len(previous_labels) == 1:
            # here lineage_tree['cell_lineage'][m_cellid] = [d_cellid]
            # and d_cellid have been replaced by cellid1, cellid2
            # replace mother daughters
            lineage_tree['cell_lineage'][m_cellid] = [cellid1, cellid2]
            # give lineage of dividing cell to the first daughter
            if d_cellid in lineage_tree['cell_lineage']:
                lineage_tree['cell_lineage'][cellid1] = lineage_tree['cell_lineage'][d_cellid]
                # if dividing_label not in newlabels:
                if d_cellid not in [cellid1, cellid2]:
                    del lineage_tree['cell_lineage'][d_cellid]

        elif len(previous_labels) == 2:
            # previous_labels[0] -> cellid1
            lineage_tree['cell_lineage'][m_cellid] = [cellid1]
            # give lineage of dividing cell to the first daughter
            if d_cellid in lineage_tree['cell_lineage']:
                lineage_tree['cell_lineage'][cellid1] = lineage_tree['cell_lineage'][d_cellid]
                # if dividing_label not in newlabels:
                if d_cellid not in [cellid1, cellid2]:
                    del lineage_tree['cell_lineage'][d_cellid]
            # previous_labels[1] -> cellid2
            m_label = previous_time * div + previous_labels[1]
            lineage_tree['cell_lineage'][m_label] = [cellid2]

        else:
            msg = "      .. weird, cell(s) " + str(previous_labels) + " divides into " + str(newlabels)
            monitoring.to_log_and_console(msg)
            msg = "         lineage is not updated"
            monitoring.to_log_and_console(msg)
            return new_propagated_division

    if cellid1 in lineage_tree['cell_lineage']:
        if len(lineage_tree['cell_lineage'][cellid1]) == 1:
            t = lineage_tree['cell_lineage'][cellid1][0] // div
            label = lineage_tree['cell_lineage'][cellid1][0] - t * div
            new_propagated_division[t] = new_propagated_division.get(t, []) + [(label, newlabels)]
        else:
            msg = "      .. weird, cell " + str(cellid1) + " divides into "
            msg += str(lineage_tree['cell_lineage'][cellid1])
            monitoring.to_log_and_console(msg)
            msg = "         should only divides into one cell. Will not propagate division any further."
            monitoring.to_log_and_console(msg)

    return new_propagated_division


def _get_time_labels(lineage_tree, current_time, time_digits_for_cell_id=4, verbose=True):
    #
    # get the image labels for one time point
    #

    div = 10 ** time_digits_for_cell_id
    # cells from lineage
    lineage = lineage_tree['cell_lineage']
    allcells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    cells_from_lineage = [int(c) % div for c in allcells if (int(c) // div == current_time) and int(c) % div > 1]
    # cells from volume
    allcells = list(lineage_tree['cell_volume'].keys())
    cells_from_volume = [int(c) % div for c in allcells if (int(c) // div == current_time) and int(c) % div > 1]

    if verbose is True:
        d = len(set(cells_from_lineage) - set(cells_from_volume))
        if d > 0:
            msg = "weird, the following {:d} labels are in lineage but not in volume:".format(d)
            monitoring.to_log_and_console("    .. " + msg, 1)
            monitoring.to_log_and_console("       " + str(set(cells_from_lineage) - set(cells_from_volume)), 1)
        d = len(set(cells_from_volume) - set(cells_from_lineage))
        if d > 0:
            msg = "weird, the following {:d} labels are in volume but not in lineage:".format(d)
            monitoring.to_log_and_console("    .. " + msg, 1)
            monitoring.to_log_and_console("       " + str(set(cells_from_volume) - set(cells_from_lineage)), 1)

    return list(set(cells_from_lineage) | set(cells_from_volume))


def _image_cell_division(input_image, output_image, current_time, lineage_tree, experiment, parameters, new_division,
                         propagated_division=None, time_digits_for_cell_id=4):
    proc = "_image_cell_division"

    if lineage_tree is None:
        monitoring.to_log_and_console("    .. " + proc + ": no lineage tree is None. Exiting.")
        sys.exit(1)

    if propagated_division is None:
        propagated_division = {}

    new_propagated_division = {}

    #
    #
    #
    previous_time = current_time - experiment.delta_time_point

    #
    # set and make temporary directory
    #
    experiment.astec_dir.set_tmp_directory(current_time)
    experiment.astec_dir.make_tmp_directory()

    if parameters.membrane_reconstruction.keep_reconstruction is False:
        experiment.recmembrane_dir.set_directory(directory=experiment.astec_dir.get_tmp_directory(), force=True)
    if parameters.seed_reconstruction.keep_reconstruction is False:
        experiment.recseed_dir.set_directory(directory=experiment.astec_dir.get_tmp_directory(), force=True)
    if parameters.morphosnake_reconstruction.keep_reconstruction is False:
        experiment.recmorphosnake_dir.set_directory(directory=experiment.astec_dir.get_tmp_directory(), force=True)

    reconstruction.monitoring.copy(monitoring)

    #
    # get bounding boxes of cells of interest and extract subimages
    # => dictionary of bounding boxes indexed by cell labels.
    # tmp_prefix_name + "_cell" + cellid + "_seg." + experiment.default_image_suffix
    # save also segmentation subimages
    #
    e = common.get_image_extension(input_image)
    b = os.path.basename(input_image)
    prefix_name = "ext_" + b[0:len(b) - len(e)]
    tmp_prefix_name = os.path.join(experiment.astec_dir.get_tmp_directory(), prefix_name)

    dividing_cells = []
    if current_time in new_division:
        dividing_cells += copy.deepcopy(new_division[current_time])
    if current_time in propagated_division:
        for c, oldlabels in propagated_division[current_time]:
            dividing_cells += [c]

    #
    # compute bounding boxes and extract sub-image around cells of interest
    #
    dilation_iterations = 20
    label_width = 4
    bounding_boxes = _get_bounding_box(input_image, experiment, dividing_cells, tmp_prefix_name,
                                       dilation_iterations=dilation_iterations, label_width=label_width)
    #
    #
    #
    membrane_image = _get_reconstructed_image(previous_time, current_time, experiment, experiment.recmembrane_dir,
                                              parameters.membrane_reconstruction)
    if membrane_image is None:
        monitoring.to_log_and_console("    .. " + proc + ": no membrane image was found/built for time "
                                      + str(current_time), 2)
        return lineage_tree, new_propagated_division

    #
    # extract and save membranes sub-images
    #
    im_membrane = imread(membrane_image)
    voxelsize = im_membrane.get_voxelsize()
    datatype = im_membrane.dtype
    for c in dividing_cells:
        cellid = str('{:0{width}d}'.format(c, width=label_width))
        cell_memb_name = tmp_prefix_name + "_cell" + cellid + "_membrane." + experiment.default_image_suffix
        if not os.path.isfile(cell_memb_name):
            ext_membrane = im_membrane[bounding_boxes[c]]
            imsave(cell_memb_name,  SpatialImage(ext_membrane, voxelsize=voxelsize).astype(datatype))
            del ext_membrane
    del im_membrane

    #
    # at this point, sub-images of
    # - segmentation: ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seg.mha
    # - membrane:     ext_<EN>_[post|seg]_t<time>_cell<cell-id>_membrane.mha
    # have been saved
    #
    div = 10 ** time_digits_for_cell_id
    reverse_lineage = {v: k for k, values in lineage_tree['cell_lineage'].items() for v in values}

    #
    # de novo division: compute seeds
    #
    if current_time in new_division:

        if monitoring.debug > 0:
            if monitoring.debug == 1:
                msg = "       new cell divisions"
            else:
                msg = "       new cell divisions at time " + str(current_time) + " = "
                msg += str(new_division[current_time])
            monitoring.to_log_and_console(msg, 1)

        #
        # extract and save seeds intensity sub-images
        #
        if parameters.seed_reconstruction.is_equal(parameters.membrane_reconstruction):
            monitoring.to_log_and_console("    .. seed image is identical to membrane image", 2)
            image_for_seed = membrane_image
        else:
            image_for_seed = _get_reconstructed_image(previous_time, current_time, experiment, experiment.recseed_dir,
                                                      parameters.seed_reconstruction)
        if image_for_seed is None:
            monitoring.to_log_and_console("    .. " + proc + " no seed image was found/built for time "
                                          + str(current_time), 2)
            return lineage_tree, new_propagated_division

        if parameters.seed_reconstruction.is_equal(parameters.membrane_reconstruction):
            for c in new_division[current_time]:
                cellid = str('{:0{width}d}'.format(c, width=label_width))
                cell_memb_name = tmp_prefix_name + "_cell" + cellid + "_membrane." + experiment.default_image_suffix
                cell_seed_name = tmp_prefix_name + "_cell" + cellid + "_seeds." + experiment.default_image_suffix
                if not os.path.isfile(cell_seed_name):
                    common.copy_image(cell_memb_name, cell_seed_name)
        else:
            im_for_seeds = imread(image_for_seed)
            voxelsize = im_for_seeds.get_voxelsize()
            datatype = im_for_seeds.dtype
            for c in new_division[current_time]:
                cellid = str('{:0{width}d}'.format(c, width=label_width))
                cell_seed_name = tmp_prefix_name + "_cell" + cellid + "_seeds." + experiment.default_image_suffix
                if not os.path.isfile(cell_seed_name):
                    ext_for_seeds = im_for_seeds[bounding_boxes[c]]
                    imsave(cell_seed_name, SpatialImage(ext_for_seeds, voxelsize=voxelsize).astype(datatype))
                    del ext_for_seeds
            del im_for_seeds

        #
        # at this point, sub-images of
        # - original segmentation:   ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seg.mha
        # - reconstruction membrane: ext_<EN>_[post|seg]_t<time>_cell<cell-id>_membrane.mha
        # - reconstruction seed:     ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seeds.mha
        # have been saved
        #

        #
        # transform input_image into division_image
        #
        if current_time in propagated_division:
            division_image = common.add_suffix(input_image, "_division",
                                               new_dirname=experiment.astec_dir.get_tmp_directory(0),
                                               new_extension=experiment.default_image_suffix)
        else:
            division_image = output_image

        #
        # two seeds sub-image (labels are 0, 1 and 2)
        # tmp_prefix_name + "_cell" + cellid + "_seeds_two_seeds." + experiment.default_image_suffix
        #

        im_segmentation = imread(input_image)
        if parameters.use_new_labels is False:
            #
            # max label is the last used label in the segmentation
            #
            maxlabel = int(np.max(im_segmentation))
        else:
            #
            # max label is the last used label in the segmentation
            # at this time and at the previous time
            # [Q]: why considering the previous time?
            #
            current_labels = _get_time_labels(lineage_tree, current_time,
                                              time_digits_for_cell_id=time_digits_for_cell_id, verbose=True)
            previous_labels = _get_time_labels(lineage_tree, previous_time,
                                               time_digits_for_cell_id=time_digits_for_cell_id, verbose=False)
            maxlabel = max(max(previous_labels), max(current_labels))


        #
        # c is the label to be divided
        #
        for c in new_division[current_time]:
            #
            # try to get two seeds + watershed
            #
            cellid = str('{:0{width}d}'.format(c, width=label_width))
            cell_seg_name = tmp_prefix_name + "_cell" + cellid + "_seg." + experiment.default_image_suffix
            cell_seed_name = tmp_prefix_name + "_cell" + cellid + "_seeds." + experiment.default_image_suffix

            #
            # for computation, build temporary images
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seeds_unmasked_seed_h<h>.mha
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seeds_seed_h<h>.mha
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seeds_seed_diff_h<h>.mha
            # result image is
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_seeds_two_seeds.mha
            #   which has 3 values: 0, 2, and 3
            #

            nseeds = _two_seeds_extraction(cell_seg_name, c, cell_seed_name, experiment, parameters)
            if nseeds != 2:
                msg = "    .. (extraction) unable to find two seeds for cell " + str(c)
                msg += " at time " + str(current_time)
                monitoring.to_log_and_console(msg)
                continue

            #
            # make the following images
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_new_membrane.mha
            #   membrane sub-image where the exterior contour of the cell to be split
            #   have been enhanced
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_new_seeds.mha
            #   four values image: 0, 1 (background), 2, and 3 (for the two cells)
            # - ext_<EN>_[post|seg]_t<time>_cell<cell-id>_watershed.mha
            #   resulting watershed sub-image
            #

            _two_seeds_watershed(c, experiment, parameters, tmp_prefix_name, label_width=label_width)

            #
            # new labels
            #
            if parameters.use_new_labels is False:
                # use maximum from image
                # re-use mother label
                daughter_labels = [c, maxlabel+1]
                maxlabel += 1
            else:
                daughter_labels = [maxlabel+1, maxlabel+2]
                maxlabel += 2

            #
            # get mother cell time and label
            #
            cid = current_time * div + c
            mother_label = None
            if cid not in reverse_lineage:
                msg = "no mother cell for cell " + str(c) + " at time " + str(current_time)
                monitoring.to_log_and_console(proc + ": " + msg)
            else:
                if int(reverse_lineage[cid]) // div != previous_time:
                    msg = "weird: mother time point {:d}".format(int(reverse_lineage[cid]) // div)
                    msg += " is different from previous time {:d}".format(previous_time)
                    monitoring.to_log_and_console("       " + msg)
                mother_label = [int(reverse_lineage[cid]) % div]
                msg = "new division from time " + str(previous_time) + " to time " + str(current_time)
                msg += ": " + str(mother_label[0]) + " -> " + str(daughter_labels)
                msg += " (replace {:d})".format(c)
                monitoring.to_log_and_console("       " + msg)

            #
            # recall that temporary files have been named after mother cell id (ie c)
            #
            new_propagated_division = _update_image_properties(im_segmentation, tmp_prefix_name, lineage_tree,
                                                               experiment, new_propagated_division, bounding_boxes,
                                                               current_time, c, daughter_labels,
                                                               previous_time=previous_time,
                                                               previous_labels=mother_label, label_width=label_width,
                                                               time_digits_for_cell_id=time_digits_for_cell_id)

        #
        imsave(division_image, im_segmentation)
        if current_time not in propagated_division:
            return lineage_tree, new_propagated_division
        input_image = division_image

    #
    # division issued from a previously computed division
    # retrieve seeds from previous segmentation image
    #
    # segmentation image have been saved as 'division_image'
    # input_image is now 'division_image'
    #

    previous_time = current_time - experiment.delta_time_point

    if current_time in propagated_division:

        if monitoring.debug > 0:
            if monitoring.debug == 1:
                monitoring.to_log_and_console("       propagated cell division", 1)
            else:
                msg = "       propagated divisions at time " + str(current_time) + " = "
                msg += str(propagated_division[current_time])
            monitoring.to_log_and_console(msg, 1)

        #
        # build seeds by eroding previous segmentation and deforming it
        #
        # erosion iterations are set by default in voxel units
        # there is also a volume defined in voxel units
        #
        monitoring.to_log_and_console('    .. build seeds from previous segmentation', 2)

        previous_segmentation = experiment.get_segmentation_image(previous_time)
        if previous_segmentation is None:
            monitoring.to_log_and_console("    .. " + proc + ": no segmentation image was found for time "
                                          + str(previous_time))
            return False

        #
        # transform the previous segmentation image then erode the cells
        # adapted from astec.py
        #
        deformed_seeds = common.add_suffix(input_image, '_deformed_seeds_from_previous',
                                           new_dirname=experiment.astec_dir.get_tmp_directory(0),
                                           new_extension=experiment.default_image_suffix)
        #
        # deformed_segmentation will be needed for morphosnake correction
        # may also be used in reconstruction.py
        #
        deformed_segmentation = common.add_suffix(input_image, '_deformed_segmentation_from_previous',
                                                  new_dirname=experiment.astec_dir.get_tmp_directory(0),
                                                  new_extension=experiment.default_image_suffix)

        if not os.path.isfile(deformed_segmentation) or monitoring.forceResultsToBeBuilt is True:
            deformation = reconstruction.get_deformation_from_current_to_previous(current_time, experiment,
                                                                                  parameters.membrane_reconstruction,
                                                                                  previous_time)
            if deformation is None:
                monitoring.to_log_and_console("    .. " + proc + ": error when getting deformation field")
                return False

            cpp_wrapping.apply_transformation(previous_segmentation, deformed_segmentation, deformation,
                                              interpolation_mode='nearest', monitoring=monitoring)

        astec.build_seeds_from_previous_segmentation(deformed_segmentation, deformed_seeds, parameters)

        #
        # save sub-images of the deformed seeds
        # the cell to be divided is 'c' in the current image
        # its predecessor have been divided in previous image and replace by oldlabels
        # - use 'c' to build names
        # - use bounding_boxes[c] to mask seed image
        #
        im_for_seeds = imread(deformed_seeds)
        voxelsize = im_for_seeds.get_voxelsize()
        datatype = im_for_seeds.dtype
        for c, oldlabels in propagated_division[current_time]:
            cellid = str('{:0{width}d}'.format(c, width=label_width))
            cell_seed_name = tmp_prefix_name + "_cell" + cellid + "_deformed_seeds." + experiment.default_image_suffix
            if not os.path.isfile(cell_seed_name):
                ext_for_seeds = im_for_seeds[bounding_boxes[c]]
                imsave(cell_seed_name, SpatialImage(ext_for_seeds, voxelsize=voxelsize).astype(datatype))
                del ext_for_seeds
        del im_for_seeds

        im_segmentation = imread(input_image)
        if parameters.use_new_labels is False:
            #
            # max label is the last used label in the segmentation
            #
            maxlabel = int(np.max(im_segmentation))
        else:
            #
            # max label is the last used label in the segmentation
            # at this time and at the previous time
            #
            current_labels = _get_time_labels(lineage_tree, current_time,
                                              time_digits_for_cell_id=time_digits_for_cell_id, verbose=True)
            previous_labels = _get_time_labels(lineage_tree, previous_time,
                                               time_digits_for_cell_id=time_digits_for_cell_id, verbose=False)
            maxlabel = max(max(previous_labels), max(current_labels))


        for c, oldlabels in propagated_division[current_time]:
            #
            # previous_labels are the labels in previous image that will correspond to [2, 3]
            # should be equal to oldlabels
            #
            previous_labels = _two_seeds_propagation(c, oldlabels, experiment, tmp_prefix_name, label_width=label_width)
            #
            # previous_labels are encoded as numpy.uint16, which may yield an overflow
            # in _image_cell_division (line: m_cellid = previous_time * 10 ** 4 + previous_labels[0])
            # reason is due to changes in python/numpy
            #   - python 3.9  : (np.uint16) * (int) -> (np.int64)
            #   - python 3.11 : (np.uint16) * (int) -> (np.uint16)
            # convert into np.int64 to prevent overflow
            #
            previous_labels = [np.int64(c) for c in previous_labels]
            if len(previous_labels) != 2:
                msg = "    .. (propagation) unable to find two seeds for cell " + str(c)
                msg += " at time " + str(current_time)
                monitoring.to_log_and_console(msg)
                msg = "\t found " + str(len(previous_labels)) + " labels: " + str(previous_labels)
                monitoring.to_log_and_console(msg, 3)
                continue

            if len(set(previous_labels) ^ set(oldlabels)) > 0:
                msg = "    .. found seeds for cell " + str(c) + " are " + str(previous_labels)
                msg += " instead of " + str(oldlabels)
            #
            # new labels
            #
            if parameters.use_new_labels is False:
                # use maximum from image
                # re-use mother label
                daughter_labels = [c, maxlabel+1]
                maxlabel += 1
            else:
                daughter_labels = [-1, -1]
                if current_time * 10 ** time_digits_for_cell_id + oldlabels[0] not in lineage_tree['cell_lineage']:
                    daughter_labels[0] = oldlabels[0]
                else:
                    daughter_labels[0] = maxlabel+1
                    maxlabel += 1
                if current_time * 10 ** time_digits_for_cell_id + oldlabels[1] not in lineage_tree['cell_lineage']:
                    daughter_labels[1] = oldlabels[1]
                else:
                    daughter_labels[1] = maxlabel+1
                    maxlabel += 1

            msg = "propagated division from time " + str(previous_time) + " to time " + str(current_time)
            msg += ": " + str(previous_labels) + " -> " + str(daughter_labels)
            msg += " (replace {:d})".format(c)
            monitoring.to_log_and_console("       " + msg)

            #
            # the returned image ext_<EN>_[post|seg]_t<time>_cell<cell-id>_watershed.mha
            # has 3 labels (1, 2, 3)
            #
            _two_seeds_watershed(c, experiment, parameters, tmp_prefix_name, label_width=label_width)

            new_propagated_division = _update_image_properties(im_segmentation, tmp_prefix_name, lineage_tree,
                                                               experiment, new_propagated_division, bounding_boxes,
                                                               current_time, c, daughter_labels,
                                                               previous_time=previous_time,
                                                               previous_labels=previous_labels, label_width=label_width,
                                                               time_digits_for_cell_id=time_digits_for_cell_id)

        imsave(output_image, im_segmentation)
        del im_segmentation

    return lineage_tree, new_propagated_division


########################################################################################
#
#
#
########################################################################################

#
#
#
#
#
def _fill_volumes(lineage, image, current_time=0, time_digits_for_cell_id=4):
    mul = 10 ** time_digits_for_cell_id

    if lineage is not None and lineage != {}:
        if ioproperties.keydictionary['volume']['output_key'] in lineage:
            tmp = lineage[ioproperties.keydictionary['volume']['output_key']]
            for k in tmp:
                if k / mul == current_time:
                    monitoring.to_log_and_console("    .. cell volumes found for time #" + str(current_time), 1)
                    return lineage
    #
    # no volumes found, compute them
    #
    monitoring.to_log_and_console("    .. computes cell volumes for time #" + str(current_time), 1)
    return properties.update_cell_volume_properties(lineage, image, current_time=current_time,
                                                    time_digits_for_cell_id=time_digits_for_cell_id)


########################################################################################
#
#
#
########################################################################################

def _get_io_images(current_time, seg_name, experiment):
    proc = "_get_io_images"

    #
    # input image is in EXP_SEG_FROM ie MARS subdirectory
    # - it can be suffixed either by 'mars' or by 'seg'
    # output image will be in EXP_SEG_TO ie ASTEC subdirectory
    #
    mars_dir = experiment.mars_dir.get_directory(0)
    astec_dir = experiment.astec_dir.get_directory(0)
    mars_name = experiment.mars_dir.get_image_name(current_time)

    #
    #
    #
    input_image = None
    seg_image = common.find_file(mars_dir, mars_name, file_type='image', callfrom=proc, local_monitoring=None,
                                 verbose=False)
    if seg_image is not None:
        input_image = os.path.join(mars_dir, seg_image)
    else:
        seg_image = common.find_file(mars_dir, seg_name, file_type='image', callfrom=proc, local_monitoring=None,
                                     verbose=False)
        if seg_image is not None:
            input_image = os.path.join(mars_dir, seg_image)
    if input_image is None:
        return None, None
        monitoring.to_log_and_console("    .. " + proc + ": no segmentation image was found for time "
                                      + str(current_time))
        monitoring.to_log_and_console("    .. exiting.")
        sys.exit(1)

    #
    # output image
    #
    seg_image = common.find_file(astec_dir, seg_name, file_type='image', callfrom=proc, local_monitoring=None,
                                 verbose=False)
    #
    # input_image is the full name of input image (or None if not found)
    # seg_image is the file name in the output directory (or None if not found)
    #
    return input_image, seg_image


def correction_process(input_image, output_image, current_time, lineage_tree, experiment, parameters, fusion,
                       new_division, propagated_division=None):

    proc = "correction_process"

    if propagated_division is None:
        propagated_division = {}

    #
    # something to do
    #
    new_propagated_division = {}
    monitoring.to_log_and_console("    correction of '" + str(input_image).split(os.path.sep)[-1] + "'", 1)
    time_digits_for_cell_id = experiment.get_time_digits_for_cell_id()

    #
    # cell fusion
    #
    if current_time in fusion:
        if current_time in new_division or current_time in propagated_division:
            fusedcell_image = common.add_suffix(input_image, experiment.result_image_suffix + "_fusedcell",
                                                new_dirname=experiment.astec_dir.get_tmp_directory(0),
                                                new_extension=experiment.default_image_suffix)
        else:
            fusedcell_image = output_image
        if monitoring.debug > 0:
            monitoring.to_log_and_console("    process cell fusion", 1)

        #
        # compute volume if required
        #
        lineage_tree = _fill_volumes(lineage_tree, input_image, current_time,
                                     time_digits_for_cell_id=time_digits_for_cell_id)
        lineage_tree, fusion = _image_cell_fusion(input_image, fusedcell_image, current_time, lineage_tree, fusion,
                                                  time_digits_for_cell_id=time_digits_for_cell_id)
        input_image = fusedcell_image

    #
    # cell division
    #
    if current_time in new_division or current_time in propagated_division:
        if monitoring.debug > 0:
            monitoring.to_log_and_console("    process cell division", 1)
        lineage_tree, new_propagated_division = _image_cell_division(input_image, output_image, current_time,
                                                                     lineage_tree, experiment, parameters, new_division,
                                                                     propagated_division,
                                                                     time_digits_for_cell_id=time_digits_for_cell_id)

    return lineage_tree, new_propagated_division

#
#
#
#
#


def correction_control(experiment, parameters):
    """

    :param experiment:
    :param parameters:
    :return:
    """

    proc = "correction_control"
    total_start_time = time.time()
    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, ManualCorrectionParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    #
    # make sure that the result directory exists
    #

    experiment.astec_dir.make_directory()
    if parameters.membrane_reconstruction.keep_reconstruction is True:
        experiment.recmembrane_dir.make_directory()
    if parameters.seed_reconstruction.keep_reconstruction is True:
        experiment.recseed_dir.make_directory()
    if parameters.morphosnake_reconstruction.keep_reconstruction is True:
        experiment.recmorphosnake_dir.make_directory()

    monitoring.to_log_and_console('', 1)

    mars_dir = experiment.mars_dir.get_directory(0)
    astec_dir = experiment.astec_dir.get_directory(0)

    #
    # set last time point if required
    #

    if (experiment.first_time_point is None or experiment.first_time_point < 0) and \
            (experiment.last_time_point is None or experiment.last_time_point < 0):
        monitoring.to_log_and_console("... time interval does not seem to be defined in the parameter file")
        monitoring.to_log_and_console("    set parameters 'begin' and 'end'")
        monitoring.to_log_and_console("\t Exiting")
        sys.exit(1)

    if experiment.first_time_point is not None and experiment.first_time_point >= 0:
        if experiment.last_time_point is None:
            experiment.last_time_point = experiment.first_time_point
        elif experiment.last_time_point < experiment.first_time_point:
            monitoring.to_log_and_console("... weird time interval: 'begin' = " + str(experiment.first_time_point)
                                          + ", 'end' = " + str(experiment.last_time_point))

    #
    # read lineage
    # use experiment.astec_dir.get_file_name("_lineage") to have a well-formed file name
    #
    lineage = {}
    lineage_tree_file = common.find_file(mars_dir, experiment.astec_dir.get_file_name("_lineage"),
                                         file_type='lineage', callfrom=proc, verbose=False)
    if lineage_tree_file is not None:
        msg = "found file '" + str(lineage_tree_file) + "' in " + str(mars_dir)
        monitoring.to_log_and_console(proc + ": " + msg, 2)
    else:
        msg = "does not found file '" + str(experiment.astec_dir.get_file_name("_lineage")) + "' in " + str(mars_dir)
        monitoring.to_log_and_console(proc + ": " + msg, 2)

    if lineage_tree_file is not None and os.path.isfile(os.path.join(mars_dir, lineage_tree_file)):
        lineage_tree_path = os.path.join(mars_dir, lineage_tree_file)
        lineage_tree = ioproperties.read_dictionary(lineage_tree_path)
        #
        # will only update volume and lineage
        #
        keylist = list(lineage_tree.keys())
        for k in keylist:
            if k != 'cell_lineage' and k != 'cell_volume':
                del lineage_tree[k]
        if 'cell_lineage' in lineage_tree:
            lineage = lineage_tree['cell_lineage']
        else:
            msg = "weird, property file '" + lineage_tree_path + "' has no lineage data."
            monitoring.to_log_and_console(proc + ": " + msg)
        if 'cell_volume' not in lineage_tree:
            msg = "weird, property file '" + lineage_tree_path + "' has no cell volume data."
            monitoring.to_log_and_console(proc + ": " + msg)
        #
        #
        #
    else:
        lineage_tree = None
    #    property_file = experiment.astec_dir.get_file_name("_lineage")
    #    monitoring.to_log_and_console(proc + ": no property file '" + property_file + "' ?! Exiting.")
    #    sys.exit(1)

    #
    #
    #
    fusion, division = _read_correction_file(parameters.manualcorrection_dir, parameters.manualcorrection_file,
                                             lineage=lineage, first_time_point=experiment.first_time_point,
                                             time_digits_for_cell_id=experiment.get_time_digits_for_cell_id())

    propagated_division = {}
    restarted_correction = False
    for current_time in range(experiment.first_time_point, experiment.last_time_point + 1, experiment.delta_time_point):

        monitoring.to_log_and_console('... manual correction of time #' + str(current_time), 1)
        start_time = time.time()

        #
        # get file names
        # input_image is the full name of input image (or None if not found)
        # seg_image is the file name in the output directory (or None if not found)
        #
        seg_name = experiment.astec_dir.get_image_name(current_time)
        input_image, output_image = _get_io_images(current_time, seg_name, experiment)
        if input_image is None:
            monitoring.to_log_and_console("    .. " + proc + ": no segmentation image was found for time "
                                          + str(current_time))
            monitoring.to_log_and_console("    .. exiting.")
            sys.exit(1)

        #
        # output image already exists
        #
        if output_image is not None and monitoring.forceResultsToBeBuilt is False:
            restarted_correction = True
            monitoring.to_log_and_console("    corrected image '" + str(output_image) + "' exists", 2)
            continue

        #
        # output image does not exist or has to be recomputed
        #
        if output_image is None:
            output_image = os.path.join(astec_dir, seg_name + '.' + experiment.result_image_suffix)
        else:
            output_image = os.path.join(astec_dir, output_image)

        #
        # previous output images have been found, this a restarted correction
        # -> get temporary lineage and propagated division files
        #
        if restarted_correction:
            previous_time = current_time - experiment.delta_time_point
            timeid = str('{:0{width}d}'.format(previous_time, width=experiment.get_time_digits_for_filename()))

            msg = "restart corrections from time " + str(previous_time)
            monitoring.to_log_and_console("    " + msg, 1)

            tmplineage = os.path.join(astec_dir, "tmp_lineage_" + timeid + ".pkl")
            if not os.path.isfile(tmplineage):
                msg = "weird, no temporary lineage file for time " + str(previous_time) + "?!"
                monitoring.to_log_and_console("    " + msg, 1)
            else:
                monitoring.to_log_and_console("    reading '" + str(tmplineage) + "'", 1)
                inputfile = open(tmplineage, 'rb')
                lineage_tree = pkl.load(inputfile)
                inputfile.close()

            tmpdivision = os.path.join(astec_dir, "tmp_division_" + timeid + ".pkl")
            if not os.path.isfile(tmpdivision):
                msg = "weird, no temporary division file for time " + str(previous_time) + "?!"
                monitoring.to_log_and_console("    " + msg, 1)
            else:
                monitoring.to_log_and_console("    reading '" + str(tmpdivision) + "'", 1)
                inputfile = open(tmpdivision, 'rb')
                propagated_division = pkl.load(inputfile)
                inputfile.close()

            restarted_correction = False

        #
        # is there something to do? if not, copy the image
        #
        if current_time not in fusion and current_time not in division and current_time not in propagated_division:
            cpp_wrapping.copy(input_image, output_image)
            monitoring.to_log_and_console("    no corrections to be done for time " + str(current_time), 2)
            continue

        #
        # here, we have corrections to be done
        #
        lineage_tree, propagated_division = correction_process(input_image, output_image, current_time, lineage_tree,
                                                               experiment, parameters, fusion, division,
                                                               propagated_division)
        #
        # save temporary lineage and propagated division here
        #
        if lineage_tree is not None:
            timeid = str('{:0{width}d}'.format(current_time, width=experiment.get_time_digits_for_filename()))
            tmplineage = os.path.join(astec_dir, "tmp_lineage_" + timeid + ".pkl")
            outputfile = open(tmplineage, 'wb')
            pkl.dump(lineage_tree, outputfile)
            outputfile.close()
        if propagated_division is not None:
            timeid = str('{:0{width}d}'.format(current_time, width=experiment.get_time_digits_for_filename()))
            tmpdivision = os.path.join(astec_dir, "tmp_division_" + timeid + ".pkl")
            outputfile = open(tmpdivision, 'wb')
            pkl.dump(propagated_division, outputfile)
            outputfile.close()

        #
        # cleaning
        #
        if monitoring.keepTemporaryFiles is False:
            experiment.astec_dir.rmtree_tmp_directory()

        end_time = time.time()

        monitoring.to_log_and_console('    computation time = ' + str(end_time - start_time) + ' s', 1)
        monitoring.to_log_and_console('', 1)

    #
    # save lineage here
    #
    if lineage_tree is not None:
        lineage_tree_path = os.path.join(astec_dir, experiment.astec_dir.get_file_name("_lineage") + "."
                                         + experiment.result_lineage_suffix)
        ioproperties.write_dictionary(lineage_tree_path, lineage_tree, verbose=True)

    #
    # remove temporary files
    #
    for current_time in range(experiment.first_time_point, experiment.last_time_point + 1, experiment.delta_time_point):
        timeid = str('{:0{width}d}'.format(current_time, width=experiment.get_time_digits_for_filename()))
        tmplineage = os.path.join(astec_dir, "tmp_lineage_" + timeid + ".pkl")
        if os.path.isfile(tmplineage):
            os.remove(tmplineage)
        tmpdivision = os.path.join(astec_dir, "tmp_division_" + timeid + ".pkl")
        if os.path.isfile(tmpdivision):
            os.remove(tmpdivision)

    total_end_time = time.time()
    monitoring.to_log_and_console('... total computation time = ' + str(total_end_time - total_start_time) + ' s', 1)
    monitoring.to_log_and_console('', 1)
