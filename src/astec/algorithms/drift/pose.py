##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2025
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Dim  9 fév 2025 18:48:23 CET
##############################################################
#
#
#
##############################################################

import os
import shutil
import sys
import time
import copy
import math
import random

import numpy as np
from scipy import ndimage as nd
from skimage.filters import threshold_otsu

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.wrapping.cpp_wrapping as cpp_wrapping
import astec.algorithms.intraregistration as intraregistration

from astec.components.spatial_image import SpatialImage
from astec.io.image import imread


#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
# should have a look to https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.transform.Rotation.html
# particularly to compute the rotation vector from the rotation matrix
#
########################################################################################


def _size_center_from_radius(r):
    #
    # copy from ascidian/src/ascidian/core/vector_distribution.py
    #
    ir = math.ceil(r)
    s = int(2 * ir + 1 + 2)
    return s, ir + 1


def _sphere_matrix(r=3):
    #
    # similar to ascidian/src/ascidian/core/vector_distribution.py
    #
    """
    Draw a discrete sphere in a 3D numpy array. Sphere points will have the 2 value.

    Parameters
    ----------
    r : float, default=3
        sphere radius
        radius =  2.0:    26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees
        radius =  2.3:    38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees
        radius =  2.5:    54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees
        radius =  2.9:    66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees
        radius =  3.0:    90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
        radius =  3.5:    98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees
        radius =  3.7:   110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees
        radius =  3.8:   134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees
        radius =  4.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        radius =  5.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        radius = 10.0:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
        radius = 15.0:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
        radius = 20.0:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
        radius = 25.0:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
        radius = 30.0:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
        radius = 35.0: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
        radius = 40.0: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees

    Returns
    -------
    a 3D numpy ndarray where outer sphere points have non-zero value, and the center coordinates

    """

    #
    # r is radius
    # matrix size will be 2*r + 1 (center) + 2 (margins)
    # center is r+1
    #
    s, c = _size_center_from_radius(r)
    m = np.zeros((s, s, s), dtype=np.int8)
    # fill the sphere
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                di = float(i) - float(c)
                dj = float(j) - float(c)
                dk = float(k) - float(c)
                d = math.sqrt(di * di + dj * dj + dk * dk)
                if d <= r:
                    m[i][j][k] = 1
    return m, (c, c, c)


def rotation_vector_distribution(sphere_radius, verbose=True):
    m, c = _sphere_matrix(sphere_radius)

    #
    # points of the outer sphere have a value of 2, all other points are at 0
    #

    rotation_vector = []
    vmax = 0
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 0:
                    continue
                v = np.array([(i - c[0]), (j - c[1]), (k - c[2])])
                rotation_vector += [v]
                vn = np.linalg.norm(v)
                if vmax < vn:
                    vmax = vn
    #
    # dtheta is the angle between two aligned vectors with 1 as difference in norm
    # the two opposite vectors will also have an angle of dtheta between them
    #
    dtheta = np.pi / (vmax + 0.5)
    for i, v in enumerate(rotation_vector):
        rotation_vector[i] = dtheta * rotation_vector[i]

    if verbose:
        msg = "       .. direction distribution support build with r = " + str(sphere_radius)
        monitoring.to_log_and_console(msg, 2)
        monitoring.to_log_and_console("          vectors = " + str(len(rotation_vector)), 2)
        msg = "          angle increment is {:.2f} rad ({:.2f} degrees)".format(dtheta, dtheta * 180.0 / np.pi)
        monitoring.to_log_and_console(msg, 2)
    return rotation_vector


def _rotation_matrix_from_rotation_vector(rot):
    #
    # copy from ascidian/src/ascidian/core/icp.py
    #

    # Euler-Rodrigues formula
    # R = I + f(theta) X(r) + g(theta) [X(r) * X(r)]
    # theta: rotation angle (modulus of rotation vector),
    # g(theta) = (1 - cos(theta)) / (theta * theta)
    # f(theta) = sin(theta) / theta
    # X(r): matrix of the cross product by r
    mat = np.zeros((3, 3))

    t2 = rot[0] * rot[0] + rot[1] * rot[1] + rot[2] * rot[2]
    theta = math.sqrt(t2)

    if theta > 1e-8:
        f = math.sin(theta) / theta
        g = (1.0 - math.cos(theta)) / t2

        mat[0, 0] = 1.0 - g * (rot[1] * rot[1] + rot[2] * rot[2])
        mat[1, 1] = 1.0 - g * (rot[2] * rot[2] + rot[0] * rot[0])
        mat[2, 2] = 1.0 - g * (rot[0] * rot[0] + rot[1] * rot[1])

        mat[0, 1] = g * rot[0] * rot[1]
        mat[0, 2] = g * rot[0] * rot[2]
        mat[1, 2] = g * rot[2] * rot[1]

        mat[1, 0] = mat[0, 1]
        mat[2, 0] = mat[0, 2]
        mat[2, 1] = mat[1, 2]

        mat[0, 1] -= f * rot[2]
        mat[0, 2] += f * rot[1]
        mat[1, 2] -= f * rot[0]

        mat[1, 0] += f * rot[2]
        mat[2, 0] -= f * rot[1]
        mat[2, 1] += f * rot[0]

    else:
        mat[0, 0] = 1.0
        mat[1, 1] = 1.0
        mat[2, 2] = 1.0

    return mat


def rotation_vector_from_rotation_matrix(mat):
    #
    # trace mat = 2 cos theta - 1
    #               [ (mat[2, 1] - mat[1, 2]) / 2 ]
    # sin theta n = [ (mat[0, 2] - mat[2, 0]) / 2 ]
    #               [ (mat[1, 0] - mat[0, 1]) / 2 ]
    # dos not work for sin theta = 0 obviously
    #
    trace = mat[0, 0] + mat[1, 1] + mat[2, 2]
    c = (trace - 1.0) / 2.0
    theta = math.acos(c)
    vec = np.zeros(3)
    vec[0] = (mat[2, 1] - mat[1, 2]) / 2.0
    vec[1] = (mat[0, 2] - mat[2, 0]) / 2.0
    vec[2] = (mat[1, 0] - mat[0, 1]) / 2.0

    s = math.sin(theta)
    if math.fabs(s) > 0.000001:
        vec = theta * vec / s
    return vec, theta


def rotation_matrix_from_vectors(rotation_vector):
    rotation_matrix = []
    for v in rotation_vector:
        mat = np.identity(4)
        mat[0:3, 0:3] = _rotation_matrix_from_rotation_vector(v)
        rotation_matrix += [mat]
    return rotation_matrix


def initialize_translations(rotation_matrix, ref_center=None, flo_center=None):
    for i, mat in enumerate(rotation_matrix):
        if ref_center is not None:
            if flo_center is not None:
                trs = flo_center - np.dot(mat[0:3, 0:3], ref_center)
            else:
                trs = ref_center - np.dot(mat[0:3, 0:3], ref_center)
        else:
            if flo_center is not None:
                trs = flo_center - np.dot(mat[0:3, 0:3], flo_center)
            else:
                trs = np.zeros(3)
        rotation_matrix[i][0:3, 3] = trs
    return rotation_matrix


########################################################################################
#
#
#
########################################################################################


def _threshold_from_quantile(image_path, quantile):
    proc = "_threshold_from_quantile"
    im = imread(image_path)
    if isinstance(quantile, (float, np.float64)):
        threshold = np.quantile(im, 1.0-quantile)
    elif isinstance(quantile, list):
        threshold = []
        for q in quantile:
            threshold.append(np.quantile(im, 1.0-q))
    else:
        msg = "unhandled type for 'quantile': " + str(type(quantile))
        monitoring.to_log_and_console(proc + ": " + msg)
        sys.exit(1)
    del im
    return threshold


def _radius2connexity(radius):
    proc = "_radius2connexity"
    if radius <= 0:
        return [0, 0, 0]
    elif radius == 1:
        return [0, 1, 0]
    elif radius == 2:
        return [1, 0, 1]
    elif radius == 3:
        return [2, 0, 1]
    elif radius == 4:
        return [2, 2, 0]
    elif radius == 5:
        return [3, 0, 2]
    elif radius == 6:
        return [3, 2, 1]
    elif radius == 7:
        return [2, 2, 2]
    elif radius == 8:
        return [5, 0, 3]
    elif radius == 9:
        return [3, 3, 2]
    elif radius == 10:
        return [4, 2, 3]
    else:
        monitoring.to_log_and_console(str(proc) + ": radius value too large '" + str(radius) + "'")
        sys.exit(1)


def morphology(thearray, operation, radius):
    proc = "_morphology"

    if radius <= 0:
        return thearray
    if operation == 'opening':
        resarray = morphology(thearray, 'erosion', radius)
        return morphology(resarray, 'dilation', radius)
    elif operation == 'closing':
        resarray = morphology(thearray, 'dilation', radius)
        return morphology(resarray, 'erosion', radius)
    elif operation != 'dilation' and operation != 'erosion':
        monitoring.to_log_and_console(str(proc) + ": unknown operation '" + str(operation) + "'")
        sys.exit(1)

    resarray = copy.deepcopy(thearray)
    #
    # connexities is an array of 3 values corresponding to the iterations
    # of respectively 6-, 18- and 26- structuring element that are closest
    # to the euclidean ball
    #
    connexities = _radius2connexity(radius)
    #
    # 6-connexity
    #
    if connexities[0] > 0:
        struct = nd.generate_binary_structure(3, 1)
        if connexities[0] > 1:
            struct = nd.iterate_structure(struct, connexities[0])
        if operation == 'dilation':
            resarray = nd.grey_dilation(resarray, footprint=struct)
        elif operation == 'erosion':
            resarray = nd.grey_erosion(resarray, footprint=struct)
    #
    # 18-connexity
    #
    if connexities[1] > 0:
        struct = nd.generate_binary_structure(3, 2)
        if connexities[1] > 1:
            struct = nd.iterate_structure(struct, connexities[1])
        if operation == 'dilation':
            resarray = nd.grey_dilation(resarray, footprint=struct)
        elif operation == 'erosion':
            resarray = nd.grey_erosion(resarray, footprint=struct)
    #
    # 26-connexity
    #
    if connexities[2] > 0:
        struct = nd.generate_binary_structure(3, 3)
        if connexities[2] > 1:
            struct = nd.iterate_structure(struct, connexities[2])
        if operation == 'dilation':
            resarray = nd.grey_dilation(resarray, footprint=struct)
        elif operation == 'erosion':
            resarray = nd.grey_erosion(resarray, footprint=struct)

    return SpatialImage(resarray, thearray.voxelsize)


def _score(a):
    mean = np.mean(a)
    # std = np.std(a)
    # r = [a.shape[0], float(mean), float(std)]
    #
    # positif = (a > 0)
    # ap = a[positif]
    # pmean = np.mean(ap)
    # pstd = np.std(ap)
    # r += [ap.shape[0], float(pmean), float(pstd)]
    return float(mean)


def score_from_precomputed(threshold, floating_registered, dilated_ref):
    #
    # floating_registered is a image file name
    # dilated_ref is a np.array
    #

    imflo = imread(floating_registered)
    if isinstance(threshold, (float, np.float64)):
        imthreshold = (imflo >= threshold)
    elif isinstance(threshold, list):
        imthreshold = (imflo >= threshold[0]) & (imflo <= threshold[1])
    membrane = (np.maximum(imflo, dilated_ref) - dilated_ref)[imthreshold]
    s = _score(membrane)
    del imflo
    del imthreshold
    del membrane
    return s


def score_from_image_names(parameters, floating_image, floating_registered, reference_image):
    if parameters.intensity_upper_quantile is not None and \
            isinstance(parameters.intensity_upper_quantile, (float, np.float64)):
        threshold = _threshold_from_quantile(floating_image, [parameters.intensity_lower_quantile,
                                                              parameters.intensity_upper_quantile])
    else:
        threshold = _threshold_from_quantile(floating_image, parameters.intensity_lower_quantile)

    imref = imread(reference_image)
    #
    # dilatation allows to compensate for local mis-registration or evolution
    # max(registered_image, dil(reference_image)) - dil(reference_image)
    # allows to select where the registered_image is above the (dilated) reference one
    # ideally, all points should be near 0 if registration is successful
    # (and intensities are comparable)
    #
    dilated_ref = morphology(imref, "dilation", parameters.dilation_radius)
    del imref

    s = score_from_precomputed(threshold, floating_registered, dilated_ref)
    del dilated_ref
    return s
