##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2025
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun 10 fév 2025 14:52:41 CET
##############################################################
#
#
#
##############################################################

import sys
import os

import astec.utils.common as common
import astec.algorithms.intraregistration as intraregistration

#
#
#
#
#


monitoring = common.Monitoring()


class PoseParameters(common.PrefixedParameter):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):

        if prefix is None:
            prefix = ['drift_']
        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        # how to compute the score between 2 images
        #

        doc = "\t defines the radius of the structuring element for dilation\n"
        doc += "\t Dilation will enlarge the membrane of the 'i+1' image, thus\n"
        doc += "\t allow to compute the score wrt of the 'i' image defined as \n"
        doc += "\t the enlarged membrane minus the 'i' image.\n"
        self.doc['dilation_radius'] = doc
        self.dilation_radius = 5

        doc = "\t defines the upper quantile of the fusion image to be used where computing\n"
        doc += "\t the scores. It allows to select the hyper-intense areas (ie membranes).\n"
        self.doc['intensity_lower_quantile'] = doc
        self.intensity_lower_quantile = 0.1

        doc = "\t defines the upper quantile of the fusion image to be used where *not* computing\n"
        doc += "\t the scores. It allows to select the hyper-hyper-intense areas (ie fragments)\n"
        doc += "\t that are not membranes.\n"
        self.doc['intensity_upper_quantile'] = doc
        self.intensity_upper_quantile = 0.002
        #
        # how to define the rotation
        #

        doc = "\t Sphere radius to build the support of the rotation vector distribution.\n"
        doc += "\t radius = 3:   123 vectors, angle increment is 51.43 degrees\n"
        doc += "\t radius = 3.2: 147 vectors, angle increment is 49.15 degrees\n"
        doc += "\t radius = 3.4: 171 vectors, angle increment is 47.16 degrees\n"
        doc += "\t radius = 3.5: 179 vectors, angle increment is 45.41 degrees\n"
        doc += "\t radius = 3.7: 203 vectors, angle increment is 43.84 degrees\n"
        doc += "\t radius = 4.0: 257 vectors, angle increment is 40.00 degrees\n"
        doc += "\t radius = 4.2: 305 vectors, angle increment is 38.93 degrees\n"
        doc += "\t radius = 4.3: 341 vectors, angle increment is 37.95 degrees\n"
        doc += "\t radius = 4.4: 365 vectors, angle increment is 37.05 degrees\n"
        doc += "\t radius = 4.5: 389 vectors, angle increment is 36.20 degrees\n"
        doc += "\t radius = 4.6: 437 vectors, angle increment is 35.42 degrees\n"
        doc += "\t radius = 4.7: 461 vectors, angle increment is 34.68 degrees\n"
        doc += "\t radius = 4.9: 485 vectors, angle increment is 33.34 degrees\n"
        doc += "\t radius = 5.0: 515 vectors, angle increment is 32.73 degrees\n"
        doc += "\t radius = 5.1: 587 vectors, angle increment is 32.15 degrees\n"
        doc += "\t radius = 5.2: 619 vectors, angle increment is 31.60 degrees\n"
        doc += "\t radius = 5.4: 691 vectors, angle increment is 30.59 degrees\n"
        doc += "\t radius = 5.5: 739 vectors, angle increment is 30.11 degrees\n"
        doc += "\t radius = 5.7: 751 vectors, angle increment is 29.24 degrees\n"
        doc += "\t radius = 5.8: 799 vectors, angle increment is 28.83 degrees\n"
        doc += "\t radius = 5.9: 847 vectors, angle increment is 28.43 degrees\n"
        doc += "\t radius = 6.0: 925 vectors, angle increment is 27.69 degrees\n"
        self.doc['rotation_sphere_radius'] = doc
        self.rotation_sphere_radius = 4.0

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# PoseParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('dilation_radius', self.dilation_radius, self.doc['dilation_radius'])
        self.varprint('intensity_lower_quantile', self.intensity_lower_quantile, self.doc['intensity_lower_quantile'])
        self.varprint('intensity_upper_quantile', self.intensity_upper_quantile, self.doc['intensity_upper_quantile'])
        self.varprint('rotation_sphere_radius', self.rotation_sphere_radius)

        print("")
        return

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write('#\n')
        logfile.write('# PoseParameters\n')
        logfile.write('#\n')
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'dilation_radius', self.dilation_radius, self.doc['dilation_radius'])
        self.varwrite(logfile, 'intensity_lower_quantile', self.intensity_lower_quantile,
                      self.doc['intensity_lower_quantile'])
        self.varwrite(logfile, 'intensity_upper_quantile', self.intensity_upper_quantile,
                      self.doc['intensity_upper_quantile'])
        self.varwrite(logfile, 'rotation_sphere_radius', self.rotation_sphere_radius,
                      self.doc.get('rotation_sphere_radius', None))

        logfile.write("\n")
        return

    def write_parameters(self, log_filename=None):
        if log_filename is not None:
            local_log_filename = log_filename
        else:
            local_log_filename = monitoring.log_filename
        if local_log_filename is not None:
            with open(local_log_filename, 'a') as logfile:
                self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        #
        #
        #
        self.dilation_radius = self.read_parameter(parameters, 'dilation_radius', self.dilation_radius)
        self.intensity_lower_quantile = self.read_parameter(parameters, 'intensity_lower_quantile',
                                                            self.intensity_lower_quantile)
        self.intensity_upper_quantile = self.read_parameter(parameters, 'intensity_upper_quantile',
                                                            self.intensity_upper_quantile)
        self.rotation_sphere_radius = self.read_parameter(parameters, 'rotation_sphere_radius',
                                                          self.rotation_sphere_radius)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


class DriftParameters(intraregistration.IntraRegParameters, PoseParameters):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):

        if prefix is None:
            prefix = ['drift_']
        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        PoseParameters.__init__(self, prefix=prefix)
        intraregistration.IntraRegParameters.__init__(self, prefix=prefix)
        self.movie_fusion_images = True
        self.xy_movie_fusion_images = True
        self.template_type = 'FUSION'

        #
        #
        #

        doc = "\t If True, do not compute the corrections after the first intra-series\n"
        doc += "\t registration. It allows to check whether the drift computation is required,\n"
        doc += "\t or whether the fusion parameters have to be adjusted, or whether the \n"
        doc += "\t drift parameters (eg score_threshold) have to be adjusted.\n"
        self.doc['only_initialisation'] = doc
        self.only_initialisation = False

        doc = "\t defines a threshold on the 'co-registration' score. It allows first to\n"
        doc += "\t select couples of images to be co-registered, and second to stop\n"
        doc += "\t the co-registration procedure. If None, the threshold is automatically\n"
        doc += "\t computed as the Otsu threshold from the scores.\n"
        self.doc['score_threshold'] = doc
        self.score_threshold = None

        doc = "\t suffix used to named the above python files as well as the generated figures.\n"
        self.doc['figurefile_suffix'] = doc
        self.figurefile_suffix = ""

        doc = "\t acquisition time points to be corrected. Time points above the \n"
        doc += "\t threshold are ignored. For 'i' in the list, it means that the ith\n"
        doc += "\t image is considered not being correctly registered with the\n"
        doc += "\t (i+1)th one.\n"
        self.doc['corrections_to_be_done'] = doc
        self.corrections_to_be_done = []

        doc = "\t acquisition time points to be corrected (even if the\n"
        doc += "\t corresponding score is below the threshold), *in addition* to the\n"
        doc += "\t time points above the computed or given threshold. For 'i' in\n"
        doc += "\t the list, it means that the ith image is not correctly registered\n"
        doc += "\t  with the (i+1)th one.\n"
        self.doc['corrections_to_be_added'] = doc
        self.corrections_to_be_added = []

        doc = "\t acquisition time points *not* to be corrected (even if the\n"
        doc += "\t corresponding score is above the threshold). For 'i' in the \n"
        doc += "\t list, it means that the ith image is correctly registered with \n"
        doc += "\t the (i+1)th one.\n"
        self.doc['corrections_to_be_skipped'] = doc
        self.corrections_to_be_skipped = []

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# DriftParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        intraregistration.IntraRegParameters.print_parameters(self)
        PoseParameters.print_parameters(self)

        #
        #
        #

        self.varprint('only_initialisation', self.only_initialisation, self.doc['only_initialisation'])
        self.varprint('score_threshold', self.score_threshold, self.doc['score_threshold'])

        self.varprint('figurefile_suffix', self.figurefile_suffix)

        self.varprint('corrections_to_be_done', self.corrections_to_be_done)
        self.varprint('corrections_to_be_added', self.corrections_to_be_added)
        self.varprint('corrections_to_be_skipped', self.corrections_to_be_skipped)

        print("")
        return

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write('#\n')
        logfile.write('# DriftParameters\n')
        logfile.write('#\n')
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        intraregistration.IntraRegParameters.write_parameters_in_file(self, logfile)
        PoseParameters.write_parameters_in_file(self, logfile)

        #
        #
        #

        self.varwrite(logfile, 'only_initialisation', self.only_initialisation, self.doc['only_initialisation'])
        self.varwrite(logfile, 'score_threshold', self.score_threshold, self.doc['score_threshold'])

        self.varwrite(logfile, 'figurefile_suffix', self.figurefile_suffix, self.doc.get('figurefile_suffix', None))

        self.varwrite(logfile, 'corrections_to_be_done', self.corrections_to_be_done,
                      self.doc.get('corrections_to_be_done', None))
        self.varwrite(logfile, 'corrections_to_be_added', self.corrections_to_be_added,
                      self.doc.get('corrections_to_be_added', None))
        self.varwrite(logfile, 'corrections_to_be_skipped', self.corrections_to_be_skipped,
                      self.doc.get('corrections_to_be_skipped', None))

        logfile.write("\n")
        return

    def write_parameters(self, log_filename=None):
        if log_filename is not None:
            local_log_filename = log_filename
        else:
            local_log_filename = monitoring.log_filename
        if local_log_filename is not None:
            with open(local_log_filename, 'a') as logfile:
                self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        intraregistration.IntraRegParameters.update_from_parameters(self, parameters)
        PoseParameters.update_from_parameters(self, parameters)
        #
        #
        #

        self.only_initialisation = self.read_parameter(parameters, 'only_initialisation', self.only_initialisation)
        self.score_threshold = self.read_parameter(parameters, 'score_threshold', self.score_threshold)

        self.figurefile_suffix = self.read_parameter(parameters, 'figurefile_suffix', self.figurefile_suffix)

        self.corrections_to_be_done = self.read_parameter(parameters, 'corrections_to_be_done',
                                                          self.corrections_to_be_done)
        self.corrections_to_be_added = self.read_parameter(parameters, 'corrections_to_be_added',
                                                           self.corrections_to_be_added)
        self.corrections_to_be_skipped = self.read_parameter(parameters, 'corrections_to_be_skipped',
                                                             self.corrections_to_be_skipped)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)
