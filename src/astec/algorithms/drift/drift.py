##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2025
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#
##############################################################
#
#
#
##############################################################

import os
import shutil
import sys
import time
import random

import numpy as np
from skimage.filters import threshold_otsu

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.wrapping.cpp_wrapping as cpp_wrapping
import astec.algorithms.intraregistration as intraregistration
import astec.algorithms.drift.parameters as dparameters
import astec.algorithms.drift.pose as dpose

from astec.io.image import imread


#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
#
#
########################################################################################


def _read_transformation(filename):
    proc = "_read_transformation"
    mat = np.zeros((4, 4))
    f = open(filename)

    imat = 0
    i = 0
    for line in f:
        i += 1
        # remove leading and trailing whitespaces
        li = line.strip()
        # skip comment or '(' or 'O8'
        if li.startswith('#') or li.startswith('(') or li.startswith('O8'):
            continue
        # empty line
        if len(li) == 0:
            continue
        li = li.split()

        if len(li) != 4:
            msg = "line #" + str(i) + ": '" + str(line) + " of file '" + str(filename) + "'"
            msg += "' has {:d} components?! Skip it.".format(len(li))
            monitoring.to_log_and_console(proc + ": " + msg)
            continue

        for j in range(4):
            mat[imat, j] = float(li[j])
        imat += 1

        if imat == 4:
            break

    f.close()
    return mat


########################################################################################
#
#
#
########################################################################################


def _get_coscore_path_name(experiment, path_coscore, floating_index, reference_index):
    flo = experiment.get_time_index(floating_index)
    ref = experiment.get_time_index(reference_index)
    co_score_name = experiment.get_embryo_name() + '_score_flo' + str(flo) + '_ref' + str(ref) + '.py'
    return os.path.join(path_coscore, co_score_name)


########################################################################################
#
#
#
########################################################################################


def _exists_result(experiment, prefix=None, verbose=False):
    # collect the co-registrations transformations from corrections
    # and complete them with the initial ones

    #
    # coregistration transformation directory
    #
    if verbose:
        monitoring.to_log_and_console("    .. check results prefixed by '" + str(prefix) + "'", 2)
    path_cotrsf = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_cotrsf_path(prefix=prefix))
    if not os.path.isdir(path_cotrsf):
        if verbose:
            msg = "directory '" + str(path_cotrsf) + "' does not exist."
            monitoring.to_log_and_console("       " + msg, 2)
        return False

    #
    # score directory
    #
    path_coscore = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_coscore_path(prefix=prefix))
    if not os.path.isdir(path_coscore):
        if verbose:
            msg = "directory '" + str(path_coscore) + "' does not exist."
            monitoring.to_log_and_console("       " + msg, 2)
        return False

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    # loop on time
    #
    # what is needed to compute the next iteration are
    # - the co-registration transformations between successive images
    # - the score files of these co-registrations
    #
    # should we test whether the result transformations (ie in directory ITER<i>-TRSFS_t<begin>-<end>)
    # are present?
    #

    for reference_time in range(first_time_point + experiment.delta_time_point,
                                last_time_point + 1, experiment.delta_time_point):

        floating_time = reference_time - experiment.delta_time_point

        #
        # transformation file between successive images
        #
        trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_cotrsf, floating_time, reference_time)
        trsf_split = os.path.split(trsf_name)

        if not os.path.isfile(trsf_name):
            if verbose:
                msg = "transformation '" + str(trsf_split[1]) + "' does not exist."
                monitoring.to_log_and_console("       " + msg, 2)
            return False

        #
        # score file
        #
        score_name = _get_coscore_path_name(experiment, path_coscore, floating_time, reference_time)
        score_split = os.path.split(score_name)

        if not os.path.isfile(score_name):
            if verbose:
                msg = "score '" + str(score_split[1]) + "' does not exist."
                monitoring.to_log_and_console("       " + msg, 2)
            return False

    if verbose:
        monitoring.to_log_and_console("       results are complete", 2)
    return True


########################################################################################
#
#
#
########################################################################################


def _info(a, name):
    print("type of '" + str(name) + "' is " + str(type(a)))
    print("\t shape is " + str(a.shape))
    print("\t dtype is " + str(a.dtype))


########################################################################################
#
#
#
########################################################################################


def _write_array(f, varname, a, length=2):
    proc = "_write_array"

    f.write(str(varname) + " = ")
    last = len(a) - 1
    f.write("[")
    for i, v in enumerate(a):
        if isinstance(v, (int, np.int64)):
            form = "{:d}"
        elif isinstance(v, (float, np.float64)):
            form = "{:1." + str(length) + "f}"
        else:
            msg = "type '" + str(type(v)) + "' not handled yet"
            monitoring.to_log_and_console(proc + ": " + msg)
            continue
        f.write(form.format(v))
        if i < last:
            f.write(", ")
    f.write("]")
    f.write("\n")


def _save_score(score, floating_time, reference_time, score_name, suffix=None):
    f = open(score_name, "w")
    f.write("#\n")
    f.write("# [(floating_time+reference_time)/2, #pts, mean, stddev] \n")
    # f.write("# [(floating_time+reference_time)/2, #pts, mean, stddev, #positifs, mean, stddev] \n")
    f.write("#\n")
    if suffix is not None:
        hist_name = "score_{:03d}_on_{:03d}".format(floating_time, reference_time) + str(suffix)
    else:
        hist_name = "score_{:03d}_on_{:03d}".format(floating_time, reference_time)
    position = float(floating_time + reference_time)/2.0
    _write_array(f, hist_name, [position, score])
    f.write("\n")
    f.close()


########################################################################################
#
#
#
########################################################################################


def _coregistration_score(experiment, parameters, prefix=None, verbose=False):
    """
    Compute the scores between co-registered pairs of successive images
    Resulting transformations are computed with fused image at t as floating image
    and fused image at t+delta_t as reference image (the one that will be dilated
    for the score calculation)

    :param experiment:
    :param parameters:
    :return:
    """

    proc = "_coregistration_score"

    #
    # create directories is required
    #
    path_cotrsf = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_cotrsf_path(prefix=prefix))
    path_coregistered = os.path.join(experiment.get_embryo_path(),
                                     experiment.working_dir.get_coregistered_path(prefix=prefix))
    if not os.path.isdir(path_coregistered):
        os.makedirs(path_coregistered)

    path_coscore = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_coscore_path(prefix=prefix))
    if not os.path.isdir(path_coscore):
        os.makedirs(path_coscore)

    #
    #
    #
    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    path_fusion = experiment.fusion_dir.get_directory()

    #
    # loop on time
    #

    for reference_time in range(first_time_point + experiment.delta_time_point,
                                last_time_point + 1, experiment.delta_time_point):

        floating_time = reference_time - experiment.delta_time_point

        #
        # is there a score file?
        #
        score_file = _get_coscore_path_name(experiment, path_coscore, floating_time, reference_time)
        if os.path.isfile(score_file):
            if verbose:
                score_name = score_file.split(os.path.sep)[-1]
                monitoring.to_log_and_console("       - score '" + str(score_name) + "' already existing", 2)
            continue

        #
        # image name
        #
        floating_prefix = experiment.fusion_dir.get_image_name(floating_time)
        floating_name = common.find_file(path_fusion, floating_prefix, file_type='image', callfrom=proc,
                                         local_monitoring=monitoring)
        if floating_name is None:
            msg = ": error, image '" + str(floating_prefix) + "' was not found"
            monitoring.to_log_and_console(proc + msg, 1)
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)

        reference_prefix = experiment.fusion_dir.get_image_name(reference_time)
        reference_name = common.find_file(path_fusion, reference_prefix, file_type='image', callfrom=proc,
                                          local_monitoring=monitoring)
        if reference_name is None:
            msg = ": error, image '" + str(reference_prefix) + "' was not found"
            monitoring.to_log_and_console(proc + msg, 1)
            monitoring.to_log_and_console("\t Exiting")
            sys.exit(1)

        floating_image = os.path.join(path_fusion, floating_name)
        reference_image = os.path.join(path_fusion, reference_name)

        #
        #
        #
        msg = "score of '" + str(floating_name) + "' versus '" + str(reference_name) + "'"
        monitoring.to_log_and_console("       - " + msg, 2)

        #
        # is there a co-registered (ie resampled) floating image?
        #
        floating_registered = intraregistration.get_coregistered_path_name(experiment, path_coregistered, floating_time,
                                                                           reference_time)
        if not os.path.isfile(floating_registered):
            registered_name = common.find_file(path_coregistered, floating_registered, file_type='image', callfrom=proc,
                                               local_monitoring=monitoring)
            if registered_name is None:

                #
                # resample image
                #
                trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_cotrsf, floating_time,
                                                                   reference_time)
                if not os.path.isfile(trsf_name):
                    msg = ": error, transformation '" + str(trsf_name) + "' was not found"
                    monitoring.to_log_and_console(proc + msg, 1)
                    monitoring.to_log_and_console("\t Exiting")
                    sys.exit(1)

                monitoring.to_log_and_console("           resample image '" + floating_name + "'", 2)
                cpp_wrapping.apply_transformation(floating_image, floating_registered, trsf_name, reference_image,
                                                  interpolation_mode='linear',
                                                  monitoring=monitoring)
                if not os.path.isfile(floating_registered):
                    msg = ": error, image '" + str(floating_registered) + "' was not computed"
                    monitoring.to_log_and_console(proc + msg, 1)
                    monitoring.to_log_and_console("\t Exiting")
                    sys.exit(1)
            else:
                floating_registered = os.path.join(path_coregistered, registered_name)
                monitoring.to_log_and_console("           image '" + registered_name + "' already existing", 4)
        else:
            registered_name = floating_registered.split(os.path.sep)[-1]
            monitoring.to_log_and_console("           image '" + registered_name + "' already existing", 4)

        registered_name = floating_registered.split(os.path.sep)[-1]
        #
        # [#pts, mean, stddev]
        #
        monitoring.to_log_and_console("           computing score of " + str(registered_name), 2)
        score = dpose.score_from_image_names(parameters, floating_image, floating_registered, reference_image)
        _save_score(score, floating_time, reference_time, score_file)

        #
        # cleaning
        #

    return


########################################################################################
#
#
#
########################################################################################


def _threshold_otsu_scores(scores):
    fullmeans = [scores[i][2] for i in scores]
    return threshold_otsu(np.array(fullmeans))


def _figure_coregistration_analyze(scores, angles, threshold, parameters, figuredir=None, prefix=None):
    filename = 'figure_coregistration_analyze'
    figname = 'coregistration_analyze'

    strprefix = None
    if prefix is not None and isinstance(prefix, str):
        strprefix = prefix.lower()
        strprefix = strprefix.replace('-', '_')
        filename = 'figure_' + strprefix + 'coregistration_analyze'
        figname = strprefix + 'coregistration_analyze'
    if figuredir is not None and isinstance(figuredir, str):
        filename = os.path.join(figuredir, filename)

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    f = open(filename, "w")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("import numpy as np\n")
    f.write("from skimage.filters import threshold_otsu\n")

    f.write("\n")
    if strprefix is None:
        f.write("scores = " + str(scores) + "\n")
    else:
        f.write(strprefix + "scores = " + str(scores) + "\n")

    f.write("\n")
    angles_radians = [a*180.0/float(np.pi) for a in angles]
    if strprefix is None:
        varangles = 'angles'
    else:
        varangles = strprefix + 'angles'
    _write_array(f, varangles, angles_radians, length=2)

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    if strprefix is None:
        f.write("fullmeans = [scores[i][1] for i in scores]\n")
        f.write("x = [scores[i][0] for i in scores]\n")
    else:
        f.write("fullmeans = [" + strprefix + "scores[i][1] for i in " + strprefix + "scores]\n")
        f.write("x = [" + strprefix + "scores[i][0] for i in " + strprefix + "scores]\n")

    f.write("\n")
    f.write("tmin = min(x) - 0.5\n")
    f.write("tmax = max(x) + 0.5\n")

    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(16, 16), constrained_layout=True)\n")

    f.write("\n")
    f.write("ax1.plot(x, fullmeans, linewidth=2)\n")
    f.write("ax1.plot([tmin, tmax], [{:f}, {:f}]".format(threshold, threshold))
    f.write(", linestyle='dashed', color='red', linewidth=2)\n")

    f.write("\n")
    f.write("ax1.set_ylabel('Average score', fontsize=20)\n")
    f.write("ax1.tick_params(labelsize=20)\n")
    f.write("ax1.set_xlim(tmin, tmax)\n")
    f.write("# ax1.set_ylim(0, 20)\n")
    f.write("# ax1.set_xticks(np.arange(tmin, tmax, 5))\n")
    f.write("title = \"scores (dilation radius = " + str(parameters.dilation_radius) + ")")
    if parameters.score_threshold is None:
        f.write(", Otsu threshold = {:.2f}\"\n".format(threshold))
    else:
        f.write(", threshold = {:.2f}\"\n".format(parameters.score_threshold))
    f.write("ax1.set_title(title, fontsize=24)\n")

    f.write("\n")
    f.write("ax2.plot(x, " + str(varangles) + ", linewidth=2)\n")

    f.write("\n")
    f.write("ax2.set_ylabel('Rotation angles (degrees)', fontsize=20)\n")

    f.write("\n")
    f.write("# ax2.set_xticks(range(tmin, tmax, 10))\n")
    f.write("ax2.tick_params(labelsize=20)\n")
    f.write("ax2.set_xlim(tmin, tmax)\n")
    f.write("# ax2.set_ylim(0, 180)\n")
    f.write("# ax2.set_xticks(np.arange(tmin, tmax, 5))\n")
    f.write("ax2.set_xlabel('Acquisition time', fontsize=20)\n")

    f.write("\n")
    f.write("ax1.xaxis.grid(True)\n")
    f.write("ax2.xaxis.grid(True)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname)
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")
    f.write("\n")

    f.close()


def _coregistration_analyze(experiment, parameters, prefix=None):
    """
    Collect scores, compute a threshold and generate a figure
    :param experiment:
    :param parameters:
    :param prefix:
    :return:
    """
    proc = "_coregistration_analyze"

    #
    # create directories is required
    #
    path_cotrsf = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_cotrsf_path(prefix=prefix))
    path_coscore = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_coscore_path(prefix=prefix))

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    # loop on time
    #
    scores = {}
    angles = []
    #
    # read  data
    #
    for reference_time in range(first_time_point + experiment.delta_time_point,
                                last_time_point + 1, experiment.delta_time_point):

        floating_time = reference_time - experiment.delta_time_point

        #
        # is there a score file?
        #
        score_name = _get_coscore_path_name(experiment, path_coscore, floating_time, reference_time)
        if not os.path.isfile(score_name):
            monitoring.to_log_and_console("           weird, score '" + score_name + "' is not existing", 2)
            sys.exit(1)

        #
        #
        #
        hist_name = "score_{:03d}_on_{:03d}".format(floating_time, reference_time)

        score = common.load_source(score_name)
        if hasattr(score, hist_name):
            scores[floating_time] = getattr(score, hist_name)
        else:
            msg = "weird, file '" + str(score_name) + "' does not contain variable '"
            msg += hist_name + "'"
            monitoring.to_log_and_console(proc + ": " + msg, 2)
            sys.exit(1)

        #
        # transformation
        #
        trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_cotrsf, floating_time, reference_time)
        if not os.path.isfile(trsf_name):
            monitoring.to_log_and_console("           weird, transformation '" + trsf_name + "' is not existing", 2)
            sys.exit(1)
        mat = _read_transformation(trsf_name)
        v, a = dpose.rotation_vector_from_rotation_matrix(mat)
        angles += [a]
    #
    #
    #
    fullmeans = [scores[i][1] for i in scores]
    threshold = threshold_otsu(np.array(fullmeans))
    monitoring.to_log_and_console("    .. threshold from registration scores = {:.2f}".format(threshold), 2)
    corrections = [i for i in scores if scores[i][1] > threshold]
    if parameters.score_threshold is not None:
        monitoring.to_log_and_console("       induced corrections: " + str(corrections))
        threshold = parameters.score_threshold
        monitoring.to_log_and_console("    .. use threshold from parameters = {:.2f}".format(threshold), 2)
    corrections = [i for i in scores if scores[i][1] > threshold]
    monitoring.to_log_and_console("       corrections to be done: " + str(corrections))
    #
    #
    #
    _figure_coregistration_analyze(scores, angles, threshold, parameters, figuredir=path_coscore, prefix=prefix)
    return corrections, threshold


########################################################################################
#
#
#
########################################################################################


def register(experiment, parameters, ref_image, flo_image, rotation_matrix=None, floating_registered=None,
             trsf_name=None, score_threshold=None, score_previous=None, early_stop=True):
    proc = "register"

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, dparameters.DriftParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))

    monitoring.to_log_and_console("          pre-computations", 2)

    #
    # precomputed from reference image
    #
    im = imread(ref_image)
    dilated_ref = dpose.morphology(im, "dilation", parameters.dilation_radius)
    ref_center = np.multiply(im.shape[:3], im.voxelsize) / 2.0
    del im

    #
    # precomputed from floating image
    #
    im = imread(flo_image)
    if parameters.intensity_upper_quantile is not None and \
            isinstance(parameters.intensity_upper_quantile, (float, np.float64)):
        floating_threshold = [np.quantile(im, 1.0 - parameters.intensity_lower_quantile),
                              np.quantile(im, 1.0 - parameters.intensity_upper_quantile)]
    else:
        floating_threshold = np.quantile(im, 1.0 - parameters.intensity_lower_quantile)
    flo_center = np.multiply(im.shape[:3], im.voxelsize) / 2.0
    del im

    #
    # rotation_matrix is a array of (4,4) np.array where the [0:3,0:3] part is the rotation
    # set the translation according to image centers
    #
    if rotation_matrix is None:
        rotation_vector = dpose.rotation_vector_distribution(parameters.rotation_sphere_radius)
        rotation_matrix = dpose.rotation_matrix_from_vectors(rotation_vector)

    rotation_matrix = dpose.initialize_translations(rotation_matrix, ref_center=ref_center, flo_center=flo_center)
    n_rotation_matrix = len(rotation_matrix)
    ntransformations = list(range(n_rotation_matrix))

    #
    # add some randomness
    #
    random.shuffle(ntransformations)

    #
    # file names in temporary directory
    #
    init_trsf = []
    res_trsf = []
    reg_flo = []

    tmpdir = experiment.working_dir.get_tmp_directory(0)

    for i in ntransformations:
        init_trsf += [common.add_suffix(flo_image, "_init_{:04d}".format(i), new_dirname=tmpdir, new_extension="trsf")]
        res_trsf += [common.add_suffix(flo_image, "_res_{:04d}".format(i), new_dirname=tmpdir, new_extension="trsf")]
        reg_flo += [common.add_suffix(flo_image, "_res_{:04d}".format(i), new_dirname=tmpdir)]
    #
    #
    #
    monitoring.to_log_and_console("          co-registering '" + str(flo_image) + "'", 2)

    ibest = None
    sbest = None

    for j, i in enumerate(ntransformations):
        np.savetxt(init_trsf[i], rotation_matrix[i])
        cpp_wrapping.linear_registration(ref_image, flo_image, reg_flo[i], res_trsf[i], init_trsf[i],
                                         py_hl=parameters.registration.pyramid_highest_level,
                                         py_ll=parameters.registration.pyramid_lowest_level,
                                         transformation_type=parameters.registration.transformation_type,
                                         transformation_estimator=parameters.registration.transformation_estimation_type,
                                         lts_fraction=parameters.registration.lts_fraction,
                                         normalization=parameters.registration.normalization, monitoring=None)
        s = dpose.score_from_precomputed(floating_threshold, reg_flo[i], dilated_ref)

        msg = "transformation #{:4d} ({:4d}/{:4d}), score = {:.2f}".format(i, j, n_rotation_matrix, s)
        if ibest is None:
            ibest = i
            sbest = s
        elif sbest > s:
            msg += ", score improved"
            ibest = i
            sbest = s
        if early_stop and score_threshold is not None and sbest < score_threshold:
            msg += " and inferior to {:.2f}, break".format(score_threshold)
        monitoring.to_log_and_console("          - " + msg, 2)
        if early_stop and score_threshold is not None and sbest < score_threshold:
            break

    if ibest is not None:
        msg = "Retained score is {:.2f}".format(sbest)
        if score_previous is not None:
            msg += " (previous score was {:.2f})".format(score_previous)
        msg += ", found for transformation #{:d}".format(ibest)
        monitoring.to_log_and_console("          " + msg, 2)

        if score_previous is not None and score_previous < sbest:
            msg = "Retained score does not improved the previous one, ignore it"
            monitoring.to_log_and_console("          - " + msg, 2)
        else:
            if trsf_name is not None:
                shutil.copy(res_trsf[ibest], trsf_name)
            if floating_registered is not None:
                shutil.copy(reg_flo[ibest], floating_registered)

    del dilated_ref
    return sbest


########################################################################################
#
#
#
########################################################################################


def _intrastack_registration(experiment, parameters, reference_time, floating_time, trsf_name, floating_registered,
                             rotation_matrix, threshold=None, init_prefix=None, corrected_prefix=None, early_stop=True):
    proc = "_intrastack_registration"

    path_init_coscore = os.path.join(experiment.get_embryo_path(),
                                     experiment.working_dir.get_coscore_path(prefix=init_prefix))
    path_corr_coscore = os.path.join(experiment.get_embryo_path(),
                                     experiment.working_dir.get_coscore_path(prefix=corrected_prefix))
    #
    # previous score
    #
    sprevious = None
    score_name = _get_coscore_path_name(experiment, path_init_coscore, floating_time, reference_time)
    if not os.path.isfile(score_name):
        monitoring.to_log_and_console("           weird, previous score '" + score_name + "' is not existing", 2)
    else:
        hist_name = "score_{:03d}_on_{:03d}".format(floating_time, reference_time)
        score = common.load_source(score_name)
        if hasattr(score, hist_name):
            sprevious = getattr(score, hist_name)[1]
        else:
            msg = "weird, file '" + str(score_name) + "' does not contain variable '"
            msg += hist_name + "'"
            monitoring.to_log_and_console(proc + ": " + msg, 2)
    if sprevious is not None:
        monitoring.to_log_and_console("          - previous score was {:.2f}".format(sprevious), 2)

    #
    # image name
    #
    path_fusion = experiment.fusion_dir.get_directory()

    floating_prefix = experiment.fusion_dir.get_image_name(floating_time)
    floating_name = common.find_file(path_fusion, floating_prefix, file_type='image', callfrom=proc,
                                     local_monitoring=monitoring)
    if floating_name is None:
        msg = ": error, image '" + str(floating_prefix) + "' was not found"
        monitoring.to_log_and_console(proc + msg, 1)
        monitoring.to_log_and_console("\t Exiting")
        sys.exit(1)

    reference_prefix = experiment.fusion_dir.get_image_name(reference_time)
    reference_name = common.find_file(path_fusion, reference_prefix, file_type='image', callfrom=proc,
                                      local_monitoring=monitoring)
    if reference_name is None:
        msg = ": error, image '" + str(reference_prefix) + "' was not found"
        monitoring.to_log_and_console(proc + msg, 1)
        monitoring.to_log_and_console("\t Exiting")
        sys.exit(1)

    flo_image = os.path.join(path_fusion, floating_name)
    ref_image = os.path.join(path_fusion, reference_name)

    score = register(experiment, parameters, ref_image, flo_image, rotation_matrix=rotation_matrix,
                     floating_registered=floating_registered, trsf_name=trsf_name, score_threshold=threshold,
                     score_previous=sprevious, early_stop=early_stop)

    if sprevious is None or sprevious >= score:
        score_name = _get_coscore_path_name(experiment, path_corr_coscore, floating_time, reference_time)
        _save_score(score, floating_time, reference_time, score_name)

    return


########################################################################################
#
#
#
########################################################################################


def _coregistration_correction(experiment, parameters, corrections, threshold=None, init_prefix=None,
                               corrected_prefix=None):
    """
    Perform the co-registration of any couple of two successive images
    Resulting transformations are computed with fused image at t as floating image
    and used image at t+delta_t as reference image

    :param experiment:
    :param parameters:
    :return:
    """

    proc = "_coregistration_correction"

    #
    # set random seed
    #
    seed = time.time_ns()
    monitoring.to_log_and_console("       .. random seed = " + str(seed), 2)
    random.seed(a=seed)

    #
    # rotation_vector is an array of np.array of shape 3
    # rotation_matrix is an array of np.array of shape (4,4) where the [0:3, 0:3] part
    #   is the rotation matrix
    #
    rotation_vector = dpose.rotation_vector_distribution(parameters.rotation_sphere_radius)
    rotation_matrix = dpose.rotation_matrix_from_vectors(rotation_vector)

    #
    # create directories is required
    #
    path_cotrsf = os.path.join(experiment.get_embryo_path(),
                               experiment.working_dir.get_cotrsf_path(prefix=corrected_prefix))
    if not os.path.isdir(path_cotrsf):
        os.makedirs(path_cotrsf)

    path_coregistered = os.path.join(experiment.get_embryo_path(),
                                     experiment.working_dir.get_coregistered_path(prefix=corrected_prefix))
    if not os.path.isdir(path_coregistered):
        os.makedirs(path_coregistered)

    path_coscore = os.path.join(experiment.get_embryo_path(),
                                experiment.working_dir.get_coscore_path(prefix=corrected_prefix))
    if not os.path.isdir(path_coscore):
        os.makedirs(path_coscore)

    #
    #
    #
    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    # loop on time
    #

    for reference_time in range(first_time_point + experiment.delta_time_point,
                                last_time_point + 1, experiment.delta_time_point):
        floating_time = reference_time - experiment.delta_time_point

        if floating_time not in corrections and floating_time not in parameters.corrections_to_be_added:
            continue
        added_correction = (floating_time in parameters.corrections_to_be_added)

        #
        # set and make temporary directory
        # - there is one temporary directory per timepoint
        #
        experiment.working_dir.set_tmp_directory(floating_time)
        experiment.working_dir.make_tmp_directory()

        #
        #
        #
        msg = "correction of time '" + str(floating_time) + "' versus '" + str(reference_time) + "'"
        monitoring.to_log_and_console("       .. " + msg, 2)

        #
        # is there a co-registered (ie resampled) floating image?
        #
        floating_registered = intraregistration.get_coregistered_path_name(experiment, path_coregistered, floating_time,
                                                                           reference_time)
        if not os.path.isfile(floating_registered):
            registered_name = common.find_file(path_coregistered, floating_registered, file_type='image', callfrom=proc,
                                               local_monitoring=monitoring)
            if registered_name is None:

                #
                # resample image
                #
                trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_cotrsf, floating_time,
                                                                   reference_time)
                if os.path.isfile(trsf_name):
                    monitoring.to_log_and_console("           transformation '" + trsf_name + "' already exists", 2)
                else:
                    _intrastack_registration(experiment, parameters, reference_time, floating_time, trsf_name,
                                             floating_registered, rotation_matrix, threshold=threshold,
                                             init_prefix=init_prefix, corrected_prefix=corrected_prefix,
                                             early_stop=not added_correction)
                    # if not os.path.isfile(floating_registered):
                    #     msg = ": error, image '" + str(floating_registered) + "' was not computed"
                    #     monitoring.to_log_and_console(proc + msg, 1)
                    #     monitoring.to_log_and_console("\t Exiting")
                    #     sys.exit(1)
            else:
                monitoring.to_log_and_console("           image '" + registered_name + "' already exists", 2)
        else:
            registered_name = floating_registered.split(os.path.sep)[-1]
            monitoring.to_log_and_console("           image '" + registered_name + "' already exists", 2)

        #
        # cleaning
        #

        if monitoring.keepTemporaryFiles is False:
            experiment.working_dir.rmtree_tmp_directory()

    #
    #
    #
    return


########################################################################################
#
#
#
########################################################################################


def _coregistration_collect(experiment, init_prefix=None, corrected_prefix=None, result_prefix=None, verbose=False):
    #
    # collect the co-registrations transformations from corrections
    # and complete them with the initial ones
    #
    proc = "_coregistration_collect"

    #
    # transformations
    #
    path_init_cotrsf = os.path.join(experiment.get_embryo_path(),
                                    experiment.working_dir.get_cotrsf_path(prefix=init_prefix))
    if not os.path.isdir(path_init_cotrsf):
        msg = "weird: directory '" + str(path_init_cotrsf) + "' does not exist. Exiting"
        monitoring.to_log_and_console(proc + ": " + msg, 1)
        sys.exit(1)

    path_corr_cotrsf = os.path.join(experiment.get_embryo_path(),
                                    experiment.working_dir.get_cotrsf_path(prefix=corrected_prefix))
    if not os.path.isdir(path_corr_cotrsf):
        msg = "weird: directory '" + str(path_corr_cotrsf) + "' does not exist. Exiting"
        monitoring.to_log_and_console(proc + ": " + msg, 1)
        sys.exit(1)

    path_res_cotrsf = os.path.join(experiment.get_embryo_path(),
                                   experiment.working_dir.get_cotrsf_path(prefix=result_prefix))

    if not os.path.isdir(path_res_cotrsf):
        os.makedirs(path_res_cotrsf)

    #
    # scores
    #
    path_init_coscore = os.path.join(experiment.get_embryo_path(),
                                     experiment.working_dir.get_coscore_path(prefix=init_prefix))
    if not os.path.isdir(path_init_coscore):
        msg = "weird: directory '" + str(path_init_coscore) + "' does not exist. Exiting"
        monitoring.to_log_and_console(proc + ": " + msg, 1)
        sys.exit(1)

    path_corr_coscore = os.path.join(experiment.get_embryo_path(),
                                     experiment.working_dir.get_coscore_path(prefix=corrected_prefix))
    if not os.path.isdir(path_corr_coscore):
        msg = "weird: directory '" + str(path_corr_coscore) + "' does not exist. Exiting"
        monitoring.to_log_and_console(proc + ": " + msg, 1)
        sys.exit(1)

    path_res_coscore = os.path.join(experiment.get_embryo_path(),
                                    experiment.working_dir.get_coscore_path(prefix=result_prefix))
    if not os.path.isdir(path_res_coscore):
        os.makedirs(path_res_coscore)

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    # loop on time
    #

    for reference_time in range(first_time_point + experiment.delta_time_point,
                                last_time_point + 1, experiment.delta_time_point):

        floating_time = reference_time - experiment.delta_time_point

        #
        # result transformation
        #
        res_trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_res_cotrsf, floating_time,
                                                               reference_time)
        trsf_split = os.path.split(res_trsf_name)

        #
        # get input transformation
        #
        trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_corr_cotrsf, floating_time,
                                                           reference_time)
        msg = ""
        if os.path.isfile(trsf_name):
            if verbose:
                msg = "copy '" + str(trsf_split[1]) + "' from '" + str(path_corr_cotrsf) + "'"
        else:
            trsf_name = intraregistration.get_cotrsf_path_name(experiment, path_init_cotrsf, floating_time,
                                                               reference_time)
            if os.path.isfile(trsf_name):
                if verbose:
                    msg = "copy '" + str(trsf_split[1]) + "' from '" + str(path_init_cotrsf) + "'"
            else:
                msg = "weird, transformation '" + str(trsf_split[1]) + "' was found neither in "
                msg += "'" + str(path_corr_cotrsf) + "' nor in '" + str(path_init_cotrsf) + "'. Exiting."
                monitoring.to_log_and_console(proc + ": " + msg, 1)
                sys.exit(1)

        #
        #
        #
        if verbose:
            monitoring.to_log_and_console("       .. " + msg, 2)
        shutil.copy(trsf_name, res_trsf_name)

        #
        # result score
        #
        res_score_name = _get_coscore_path_name(experiment, path_res_coscore, floating_time, reference_time)
        score_split = os.path.split(res_score_name)

        #
        # get input score
        #
        score_name = _get_coscore_path_name(experiment, path_corr_coscore, floating_time, reference_time)
        if os.path.isfile(score_name):
            if verbose:
                msg = "copy '" + str(score_split[1]) + "' from '" + str(path_corr_coscore) + "'"
        else:
            score_name = _get_coscore_path_name(experiment, path_init_coscore, floating_time, reference_time)
            if os.path.isfile(score_name):
                if verbose:
                    msg = "copy '" + str(score_split[1]) + "' from '" + str(path_init_coscore) + "'"
            else:
                msg = "weird, score '" + str(score_split[1]) + "' was found neither in "
                msg += "'" + str(path_corr_coscore) + "' nor in '" + str(path_init_cotrsf) + "'. Exiting."
                monitoring.to_log_and_console(proc + ": " + msg, 1)
                sys.exit(1)

        #
        #
        #
        if verbose:
            monitoring.to_log_and_console("       .. " + msg, 2)
        shutil.copy(score_name, res_score_name)
    return


########################################################################################
#
#
#
########################################################################################


def intrastack_drift_control(experiment, parameters):
    """

    :param experiment:
    :param parameters:
    :return:
    """

    proc = "intrastack_drift_control"
    common_prefix = "ITER"

    if isinstance(experiment, datadir.Experiment) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'experiment' parameter", 1)
        sys.exit(1)
    if isinstance(parameters, dparameters.DriftParameters) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'parameters' parameter", 1)
        sys.exit(1)

    dpose.monitoring.copy(monitoring)
    #
    # start processing
    #
    start_time = time.time()

    #
    #
    #
    iteration = 0
    prefix = common_prefix + str(iteration) + '-'
    corrections = []
    threshold = None
    corrections_are_computed = False

    #
    # first computation (iteration #0)
    #
    if not _exists_result(experiment, prefix=prefix, verbose=False):
        #
        # - co-registration of any 2 successive images
        # - transformation composition
        # - temporal movies of [xy,yz,xz] slices
        #
        # will generate ITER0- prefixed directories
        # - ITER0-CO-TRSFS: (rigid) transformations (T_{(i+1)<-(i)}) between successive images
        #                  allow to resample the ith image into the (i+1)th frame
        # - ITER0-CO-REGISTERED: ith images resampled into the (i+1)th frame
        #                  allow to visually check whether the registration is right
        # - ITER0-TRSFS_t(first)-(last): (rigid) transformations (T_{(ref)<-(i)}) towards a reference frame
        #                  allow to resample fused image (in the FUSE/ directory)
        #
        monitoring.to_log_and_console("", 2)
        monitoring.to_log_and_console(" .. iteration #" + str(iteration) + " computation", 2)
        parameters.save_coregistered_image = True
        intraregistration.intraregistration_control(experiment, parameters, prefix=prefix)

        #
        # compute co-registration scores
        # will generate the INIT-CO-SCORE/ directory
        #
        monitoring.to_log_and_console("    .. compute scores", 2)
        _coregistration_score(experiment, parameters, prefix=prefix)

        #
        # global analysis of all scores
        #
        monitoring.to_log_and_console("    .. analyze scores", 2)
        corrections, threshold = _coregistration_analyze(experiment, parameters, prefix=prefix)
        monitoring.to_log_and_console("       acquisition time above threshold: " + str(corrections), 2)
        corrections_are_computed = True

        if parameters.only_initialisation is True:
            end_time = time.time()
            monitoring.to_log_and_console('    computation time = ' + str(end_time - start_time) + ' s', 1)
            monitoring.to_log_and_console('', 1)
            return

    #
    # what is the last iteration
    #
    while True:
        prefix = common_prefix + str(iteration) + '-'
        if not _exists_result(experiment, prefix=prefix, verbose=True):
            break
        iteration += 1

    #
    #
    #
    if iteration == 0:
        msg = " .. iteration #" + str(iteration) + " computation has failed ?! Exiting."
        monitoring.to_log_and_console(msg, 2)
        _exists_result(experiment, prefix=prefix, verbose=True)
        sys.exit(0)

    #
    #
    #
    previous_iteration = iteration - 1
    init_prefix = common_prefix + str(previous_iteration) + '-'
    corrected_prefix = "CORRECTED" + str(previous_iteration) + '-'

    #
    # re-computation of some coregistration transformations
    # - CORRECTED-CO-TRSFS: (recomputed) re(rigid) transformations (T_{(i+1)<-(i)}) between successive images
    # - CORRECTED-CO-REGISTERED: (recomputed) ith images resampled into the (i+1)th frame
    #
    monitoring.to_log_and_console("", 2)
    monitoring.to_log_and_console(" .. correction of iteration #" + str(previous_iteration), 2)

    if corrections_are_computed is False:
        corrections, threshold = _coregistration_analyze(experiment, parameters, prefix=init_prefix)
        monitoring.to_log_and_console("       acquisition time above threshold: " + str(corrections), 2)

    if len(parameters.corrections_to_be_done) > 0:
        corrections = parameters.corrections_to_be_done
    if len(parameters.corrections_to_be_skipped) > 0:
        corrections = sorted(list(set(corrections) - set(parameters.corrections_to_be_skipped)))

    monitoring.to_log_and_console("       acquisition time to be corrected: " + str(corrections), 2)

    monitoring.to_log_and_console("    .. co-registration correction", 2)
    _coregistration_correction(experiment, parameters, corrections, threshold=threshold, init_prefix=init_prefix,
                               corrected_prefix=corrected_prefix)

    #
    #
    #
    monitoring.to_log_and_console("    .. co-registration collect", 2)
    _coregistration_collect(experiment, init_prefix=init_prefix, corrected_prefix=corrected_prefix,
                            result_prefix=prefix, verbose=False)

    #
    #
    #
    monitoring.to_log_and_console("", 2)
    monitoring.to_log_and_console(" .. iteration #" + str(iteration) + " computation", 2)
    parameters.save_coregistered_image = False
    intraregistration.intraregistration_control(experiment, parameters, prefix=prefix)

    #
    #
    #
    monitoring.to_log_and_console("    .. compute scores", 2)
    _coregistration_score(experiment, parameters, prefix=prefix)

    #
    #
    #
    monitoring.to_log_and_console("    .. analyze scores", 2)
    corrections, threshold = _coregistration_analyze(experiment, parameters, prefix=prefix)

    #
    # end processing
    #

    end_time = time.time()

    monitoring.to_log_and_console('    computation time = ' + str(end_time - start_time) + ' s', 1)
    monitoring.to_log_and_console('', 1)

    return


########################################################################################
#
#
#
########################################################################################


def interstack_drift_control(experiment, parameters):
    """

    :param experiment:
    :param parameters:
    :return:
    """

    proc = "interstack_drift_control"

    if isinstance(experiment, datadir.Experiment) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'experiment' parameter", 1)
        sys.exit(1)
    if isinstance(parameters, dparameters.DriftParameters) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'parameters' parameter", 1)
        sys.exit(1)

    dpose.monitoring.copy(monitoring)

    #
    # image name
    #
    if parameters.reference_index is None:
        reference_index = experiment.first_time_point
    else:
        reference_index = parameters.reference_index


    path_floating = experiment.fusion_dir.get_directory(i=0)
    path_reference = experiment.fusion_dir.get_directory(i=1)

    image_prefix = experiment.fusion_dir.get_image_name(reference_index)
    floating_name = common.find_file(path_floating, image_prefix, file_type='image', callfrom=proc,
                                     local_monitoring=monitoring)
    flo_image = os.path.join(path_floating, floating_name)

    reference_name = common.find_file(path_reference, image_prefix, file_type='image', callfrom=proc,
                                     local_monitoring=monitoring)
    ref_image = os.path.join(path_reference, reference_name)

    msg = "registration of " + str(flo_image)
    monitoring.to_log_and_console(" .. " + msg)
    msg = "\t onto " + str(ref_image)
    monitoring.to_log_and_console("    " + msg)

    #
    #
    #
    path_costack = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_costack_path())
    if not os.path.isdir(path_costack):
        os.makedirs(path_costack)

    experiment.working_dir.set_tmp_directory(reference_index)
    experiment.working_dir.make_tmp_directory()

    floating_registered = common.add_suffix(flo_image, "_costack", new_dirname=path_costack)
    trsf_name = common.add_suffix(flo_image, "_costack", new_dirname=path_costack, new_extension="trsf")

    register(experiment, parameters, ref_image, flo_image, rotation_matrix=None,
             floating_registered=floating_registered, trsf_name=trsf_name, score_threshold=parameters.score_threshold,
             score_previous=None, early_stop=True)

    if monitoring.keepTemporaryFiles is False:
        experiment.working_dir.rmtree_tmp_directory()

    return
