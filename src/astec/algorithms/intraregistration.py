import os
import shutil
import sys
import time

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.utils.ioproperties as ioproperties
import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.io.image import imread

#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
# classes
# - computation parameters
#
########################################################################################


class IntraRegParameters(common.PrefixedParameter):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):

        if prefix is None:
            prefix = ['intra_registration_']
        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        # Co-registration parameters
        #

        self.registration = common.RegistrationParameters(prefix=self._prefix)
        self.registration.transformation_type = 'rigid'

        doc = "\t If True, save the ith image resampled in the (i+1)th image frame\n"
        doc += "\t Useful for extreme image drift correction\n"
        doc += "\t \n"
        self.doc['save_coregistered_image'] = doc
        self.save_coregistered_image = False

        #
        # intra-sequence transformation parameters
        # reference, ie image to be still (up to a translation)
        # while composing transformation
        # 'reference_transformation_file' is the transformation
        # that will be resampled the whole series afterwards
        # (defined for the reference image)
        #

        doc = "\t defines the still image after transformation compositions\n"
        doc += "\t it will only translated, except if 'reference_transformation_file'\n"
        doc += "\t or 'reference_transformation_angles' are given\n"
        doc += "\t \n"
        self.doc['reference_index'] = doc
        self.reference_index = None

        doc = "\t resampling transformation to be applied to the reference\n"
        doc += "\t image (and to the whole serie) after transformation \n"
        doc += "\t compositions.\n"
        doc += "\t \n"
        self.doc['reference_transformation_file'] = doc
        self.reference_transformation_file = None

        doc = "\t list of rotations wrt the X, Y,or Z axis that defines the\n"
        doc += "\t resampling transformation. \n"
        doc += "\t syntax: 'X 30 Y 50' means a rotation of 30 degree around\n"
        doc += "\t X followed by a rotation of 50 around Y\n"
        doc += "\t beware: rotation composition depends on the order, so\n"
        doc += "\t 'X 30 Y 50' is not equivalent to 'Y 50 X 30'\n"
        self.doc['reference_transformation_angles'] = doc
        self.reference_transformation_angles = None

        #
        # intra-sequence transformation parameters
        # input template, ie how to define the useful information to be kept
        #
        doc = "\t Possible values are 'FUSION', 'SEGMENTATION', or 'POST-SEGMENTATION'\n"
        doc += "\t The template is built so that the useful information of\n"
        doc += "\t all resampled images fits into it. Useful information\n"
        doc += "\t can be issued from either the fused sequence, the segmentation\n"
        doc += "\t sequence or the post-segmentation sequence. \n"
        self.doc['template_type'] = doc
        self.template_type = "FUSION"

        doc = "\t Giving a threshold with the 'template_type', only points\n"
        doc += "\t above the threshold are considered to be included in the\n"
        doc += "\t template after resampling, this allows to reduce the template.\n"
        doc += "\t According the background value is either 0 or 1 in both the\n"
        doc += "\t segmentation and the post-segmentation sequences, setting\n"
        doc += "\t this threshold to 2 for these sequences allows to keep the\n"
        doc += "\t entire embryo in the resampled/reconstructed sequence.\n"
        self.doc['template_threshold'] = doc
        self.template_threshold = None

        doc = "\t In addition, a margin can be given for a more comfortable\n"
        doc += "\t visualization. By default, it is 0 when only fusion\n"
        doc += "\t images are used, and 10 if either segmentation or\n"
        doc += "\t post-segmentation images are also used.\n"
        self.doc['margin'] = doc
        self.margin = None

        #
        # output template
        #
        doc = "\t gives the resulting (isotropic) voxel size (as the \n"
        doc += "\t 'target_resolution' gives the voxel size of the fused images).\n"
        doc += "\t However, for visualization purposes, it may be indicated to\n"
        doc += "\t have a larger voxel size (hence the 0.6 instead of 0.3)\n"
        self.doc['resolution'] = doc
        self.resolution = 0.3

        #
        # force rebuilding of template and of transformations versus a reference
        # useful when transformations have already been computed for fused image as template
        # they can re-used for segmentation images as template
        #
        doc = "\t Possible values are True or False\n"
        doc += "\t if True, force to recompute the template as well as the \n"
        doc += "\t transformations from the co-registrations (that are not\n"
        doc += "\t re-computed). It is useful when a first intra-registration\n"
        doc += "\t has been done with only the fusion images: a second\n"
        doc += "\t  intra-registration with the segmentation images as template \n"
        doc += "\t can be done without recomputing the co-registration\n"
        self.doc['rebuild_template'] = doc
        self.rebuild_template = False

        #
        # resampling parameters
        #
        doc = "\t Sigma to smooth (post-)segmentation images when resampling\n"
        self.doc['sigma_segmentation_images'] = doc
        self.sigma_segmentation_images = 1.0

        doc = "\t Possible values are True or False\n"
        self.doc['resample_fusion_images'] = doc
        self.resample_fusion_images = True
        doc = "\t Possible values are True or False\n"
        self.doc['resample_reconstruction_images'] = doc
        self.resample_reconstruction_images = False
        doc = "\t Possible values are True or False \n"
        self.doc['resample_segmentation_images'] = doc
        self.resample_segmentation_images = False
        doc = "\t Possible values are True or False \n"
        self.doc['resample_post_segmentation_images'] = doc
        self.resample_post_segmentation_images = False

        doc = "\t Possible values are True or False\n"
        doc += "\t To build 2D+t movies from the resampled fusion images.\n"
        self.doc['movie_fusion_images'] = doc
        self.movie_fusion_images = True

        doc = "\t Possible values are True or False\n"
        doc += "\t To build 2D+t movies from the resampled segmentation .\n"
        doc += "\t images\n"
        self.doc['movie_segmentation_images'] = doc
        self.movie_segmentation_images = False

        doc = "\t Possible values are True or False\n"
        doc += "\t To build 2D+t movies from the resampled post-segmentation.\n"
        doc += "\t images\n"
        self.doc['movie_post_segmentation_images'] = doc
        self.movie_post_segmentation_images = False

        doc = "\t Either a list of XY-sections used to build the 2D+t movies\n"
        doc += "\t eg 'xy_movie_fusion_images = [100, 200]', or a boolean value\n"
        doc += "\t where 'True' indicates the xy slice at the middle of the volume.\n"
        self.doc['xy_movie_fusion_images'] = doc
        self.xy_movie_fusion_images = []

        doc = "\t List of XZ-sections used to build the 2D+t movies\n"
        doc += "\t or a boolean value\n"
        self.doc['xz_movie_fusion_images'] = doc
        self.xz_movie_fusion_images = []

        doc = "\t List of YZ-sections used to build the 2D+t movies\n"
        doc += "\t or a boolean value\n"
        self.doc['yz_movie_fusion_images'] = doc
        self.yz_movie_fusion_images = []

        doc = "\t Either a list of XY-sections used to build the 2D+t movies\n"
        doc += "\t eg 'xy_movie_segmentation_images = [100, 200]', or a boolean value\n"
        doc += "\t where 'True' indicates the xy slice at the middle of the volume.\n"
        self.doc['xy_movie_segmentation_images'] = doc
        self.xy_movie_segmentation_images = []

        doc = "\t List of XZ-sections used to build the 2D+t movies\n"
        doc += "\t or a boolean value\n"
        self.doc['xz_movie_segmentation_images'] = doc
        self.xz_movie_segmentation_images = []

        doc = "\t List of YZ-sections used to build the 2D+t movies\n"
        doc += "\t or a boolean value\n"
        self.doc['yz_movie_segmentation_images'] = doc
        self.yz_movie_segmentation_images = []

        doc = "\t Either a list of XY-sections used to build the 2D+t movies\n"
        doc += "\t eg 'xy_movie_post_segmentation_images = [100, 200]', or a \n"
        doc += "\t boolean value where 'True' indicates the xy slice at the \n"
        doc += "\t middle of the volume.\n"
        self.doc['xy_movie_post_segmentation_images'] = doc
        self.xy_movie_post_segmentation_images = []

        doc = "\t List of XZ-sections used to build the 2D+t movies\n"
        doc += "\t or a boolean value\n"
        self.doc['xz_movie_post_segmentation_images'] = doc
        self.xz_movie_post_segmentation_images = []

        doc = "\t List of YZ-sections used to build the 2D+t movies\n"
        doc += "\t or a boolean value\n"
        self.doc['yz_movie_post_segmentation_images'] = doc
        self.yz_movie_post_segmentation_images = []

        doc = "\t Possible values are True or False\n"
        doc += "\t build a maximum image from the resampled series\n"
        doc += "\t it may be useful to define a cropping valid area\n"
        doc += "\t for the whole sequence\n"
        self.doc['maximum_fusion_images'] = doc
        self.maximum_fusion_images = False

        doc = "\t Possible values are True or False\n"
        doc += "\t build a maximum image from the resampled series\n"
        doc += "\t it may be useful to define a cropping valid area\n"
        doc += "\t for the whole sequence\n"
        self.doc['maximum_segmentation_images'] = doc
        self.maximum_segmentation_images = False

        doc = "\t Possible values are True or False\n"
        doc += "\t build a maximum image from the resampled series\n"
        doc += "\t it may be useful to define a cropping valid area\n"
        doc += "\t for the whole sequence\n"
        self.doc['maximum_post_segmentation_images'] = doc
        self.maximum_post_segmentation_images = False

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# IntraRegParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        #
        # Co-registration parameters
        #

        self.registration.print_parameters()

        #
        # intra-sequence transformation parameters
        #

        self.varprint('save_coregistered_image', self.save_coregistered_image, self.doc['save_coregistered_image'])

        self.varprint('reference_index', self.reference_index, self.doc['reference_index'])
        self.varprint('reference_transformation_file', self.reference_transformation_file,
                      self.doc['reference_transformation_file'])
        self.varprint('reference_transformation_angles', self.reference_transformation_angles,
                      self.doc['reference_transformation_angles'])

        self.varprint('template_type', self.template_type, self.doc['template_type'])
        self.varprint('template_threshold', self.template_threshold, self.doc['template_threshold'])
        self.varprint('margin', self.margin, self.doc['margin'])

        self.varprint('resolution', self.resolution, self.doc['resolution'])

        self.varprint('rebuild_template', self.rebuild_template, self.doc['rebuild_template'])

        #
        # resampling parameters
        #

        self.varprint('sigma_segmentation_images', self.sigma_segmentation_images,
                      self.doc['sigma_segmentation_images'])
        self.varprint('resample_fusion_images', self.resample_fusion_images, self.doc['resample_fusion_images'])
        self.varprint('resample_reconstruction_images', self.resample_reconstruction_images,
                      self.doc['resample_reconstruction_images'])
        self.varprint('resample_segmentation_images', self.resample_segmentation_images,
                      self.doc['resample_segmentation_images'])
        self.varprint('resample_post_segmentation_images', self.resample_post_segmentation_images,
                      self.doc['resample_post_segmentation_images'])

        #
        # movie parameters
        #

        self.varprint('movie_fusion_images', self.movie_fusion_images, self.doc['movie_fusion_images'])
        self.varprint('movie_segmentation_images', self.movie_segmentation_images,
                      self.doc['movie_segmentation_images'])
        self.varprint('movie_post_segmentation_images', self.movie_post_segmentation_images,
                      self.doc['movie_post_segmentation_images'])

        self.varprint('xy_movie_fusion_images', self.xy_movie_fusion_images, self.doc['xy_movie_fusion_images'])
        self.varprint('xz_movie_fusion_images', self.xz_movie_fusion_images, self.doc['xz_movie_fusion_images'])
        self.varprint('yz_movie_fusion_images', self.yz_movie_fusion_images, self.doc['yz_movie_fusion_images'])

        self.varprint('xy_movie_segmentation_images', self.xy_movie_segmentation_images,
                      self.doc['xy_movie_segmentation_images'])
        self.varprint('xz_movie_segmentation_images', self.xz_movie_segmentation_images,
                      self.doc['xz_movie_segmentation_images'])
        self.varprint('yz_movie_segmentation_images', self.yz_movie_segmentation_images,
                      self.doc['yz_movie_segmentation_images'])

        self.varprint('xy_movie_post_segmentation_images', self.xy_movie_post_segmentation_images,
                      self.doc['xy_movie_post_segmentation_images'])
        self.varprint('xz_movie_post_segmentation_images', self.xz_movie_post_segmentation_images,
                      self.doc['xz_movie_post_segmentation_images'])
        self.varprint('yz_movie_post_segmentation_images', self.yz_movie_post_segmentation_images,
                      self.doc['yz_movie_post_segmentation_images'])

        self.varprint('maximum_fusion_images', self.maximum_fusion_images, self.doc['maximum_fusion_images'])
        self.varprint('maximum_segmentation_images', self.maximum_segmentation_images,
                      self.doc['maximum_segmentation_images'])
        self.varprint('maximum_post_segmentation_images', self.maximum_post_segmentation_images,
                      self.doc['maximum_post_segmentation_images'])

        print("")
        return

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write('#\n')
        logfile.write('# IntraRegParameters\n')
        logfile.write('#\n')
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        #
        # Co-registration parameters
        #

        self.registration.write_parameters_in_file(logfile)

        #
        # intra-sequence transformation parameters
        #

        self.varwrite(logfile, 'save_coregistered_image', self.save_coregistered_image,
                      self.doc['save_coregistered_image'])

        self.varwrite(logfile, 'reference_index', self.reference_index, self.doc['reference_index'])
        self.varwrite(logfile, 'reference_transformation_file', self.reference_transformation_file,
                      self.doc['reference_transformation_file'])
        self.varwrite(logfile, 'reference_transformation_angles', self.reference_transformation_angles,
                      self.doc['reference_transformation_angles'])

        self.varwrite(logfile, 'template_type', self.template_type, self.doc['template_type'])
        self.varwrite(logfile, 'template_threshold', self.template_threshold, self.doc['template_threshold'])
        self.varwrite(logfile, 'margin', self.margin, self.doc['margin'])

        self.varwrite(logfile, 'resolution', self.resolution, self.doc['resolution'])

        self.varwrite(logfile, 'rebuild_template', self.rebuild_template, self.doc['rebuild_template'])

        #
        # resampling parameters
        #

        self.varwrite(logfile, 'sigma_segmentation_images', self.sigma_segmentation_images,
                      self.doc['sigma_segmentation_images'])
        self.varwrite(logfile, 'resample_fusion_images', self.resample_fusion_images,
                      self.doc['resample_fusion_images'])
        self.varwrite(logfile, 'resample_reconstruction_images', self.resample_reconstruction_images,
                      self.doc['resample_reconstruction_images'])
        self.varwrite(logfile, 'resample_segmentation_images', self.resample_segmentation_images,
                      self.doc['resample_segmentation_images'])
        self.varwrite(logfile, 'resample_post_segmentation_images', self.resample_post_segmentation_images,
                      self.doc['resample_post_segmentation_images'])

        #
        # movie parameters
        #

        self.varwrite(logfile, 'movie_fusion_images', self.movie_fusion_images, self.doc['movie_fusion_images'])
        self.varwrite(logfile, 'movie_segmentation_images', self.movie_segmentation_images,
                      self.doc['movie_segmentation_images'])
        self.varwrite(logfile, 'movie_post_segmentation_images', self.movie_post_segmentation_images,
                      self.doc['movie_post_segmentation_images'])

        self.varwrite(logfile, 'xy_movie_fusion_images', self.xy_movie_fusion_images,
                      self.doc['xy_movie_fusion_images'])
        self.varwrite(logfile, 'xz_movie_fusion_images', self.xz_movie_fusion_images,
                      self.doc['xz_movie_fusion_images'])
        self.varwrite(logfile, 'yz_movie_fusion_images', self.yz_movie_fusion_images,
                      self.doc['yz_movie_fusion_images'])

        self.varwrite(logfile, 'xy_movie_segmentation_images', self.xy_movie_segmentation_images,
                      self.doc['xy_movie_segmentation_images'])
        self.varwrite(logfile, 'xz_movie_segmentation_images', self.xz_movie_segmentation_images,
                      self.doc['xz_movie_segmentation_images'])
        self.varwrite(logfile, 'yz_movie_segmentation_images', self.yz_movie_segmentation_images,
                      self.doc['yz_movie_segmentation_images'])

        self.varwrite(logfile, 'xy_movie_post_segmentation_images', self.xy_movie_post_segmentation_images,
                      self.doc['xy_movie_post_segmentation_images'])
        self.varwrite(logfile, 'xz_movie_post_segmentation_images', self.xz_movie_post_segmentation_images,
                      self.doc['xz_movie_post_segmentation_images'])
        self.varwrite(logfile, 'yz_movie_post_segmentation_images', self.yz_movie_post_segmentation_images,
                      self.doc['yz_movie_post_segmentation_images'])

        self.varwrite(logfile, 'maximum_fusion_images', self.maximum_fusion_images, self.doc['maximum_fusion_images'])
        self.varwrite(logfile, 'maximum_segmentation_images', self.maximum_segmentation_images,
                      self.doc['maximum_segmentation_images'])
        self.varwrite(logfile, 'maximum_post_segmentation_images', self.maximum_post_segmentation_images,
                      self.doc['maximum_post_segmentation_images'])

        logfile.write("\n")
        return

    def write_parameters(self, log_filename=None):
        if log_filename is not None:
            local_log_filename = log_filename
        else:
            local_log_filename = monitoring.log_filename
        if local_log_filename is not None:
            with open(local_log_filename, 'a') as logfile:
                self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_args(self, args):
        self.reference_transformation_file = args.reference_transformation_file
        self.reference_transformation_angles = args.reference_transformation_angles

    def update_from_parameters(self, parameters):

        margin_is_updated = False

        #
        # co-registration parameters
        #

        self.registration.update_from_parameters(parameters)

        #
        # intra-sequence transformation parameters
        #
        self.save_coregistered_image = self.read_parameter(parameters, 'save_coregistered_image',
                                                           self.save_coregistered_image)

        self.reference_index = self.read_parameter(parameters, 'reference_index', self.reference_index)
        self.reference_transformation_file = self.read_parameter(parameters, 'reference_transformation_file',
                                                                 self.reference_transformation_file)
        self.reference_transformation_angles = self.read_parameter(parameters, 'reference_transformation_angles',
                                                                   self.reference_transformation_angles)

        self.template_type = self.read_parameter(parameters, 'template_type', self.template_type)
        self.template_threshold = self.read_parameter(parameters, 'template_threshold', self.template_threshold)
        old_margin = self.margin
        self.margin = self.read_parameter(parameters, 'margin', self.margin)
        if self.margin != old_margin:
            margin_is_updated = True

        self.resolution = self.read_parameter(parameters, 'resolution', self.resolution)

        self.rebuild_template = self.read_parameter(parameters, 'rebuild_template', self.rebuild_template)

        #
        # resampling parameters
        #

        self.sigma_segmentation_images = self.read_parameter(parameters, 'sigma_segmentation_images',
                                                             self.sigma_segmentation_images)

        self.resample_fusion_images = self.read_parameter(parameters, 'resample_fusion_images',
                                                          self.resample_fusion_images)
        self.resample_reconstruction_images = self.read_parameter(parameters, 'resample_reconstruction_images',
                                                                  self.resample_reconstruction_images)
        self.resample_segmentation_images = self.read_parameter(parameters, 'resample_segmentation_images',
                                                                self.resample_segmentation_images)
        self.resample_post_segmentation_images = self.read_parameter(parameters, 'resample_post_segmentation_images',
                                                                     self.resample_post_segmentation_images)

        self.movie_fusion_images = self.read_parameter(parameters, 'movie_fusion_images', self.movie_fusion_images)
        self.movie_segmentation_images = self.read_parameter(parameters, 'movie_segmentation_images',
                                                             self.movie_segmentation_images)
        self.movie_post_segmentation_images = self.read_parameter(parameters, 'movie_post_segmentation_images',
                                                                  self.movie_post_segmentation_images)

        self.xy_movie_fusion_images = self.read_parameter(parameters, 'xy_movie_fusion_images',
                                                          self.xy_movie_fusion_images)
        self.xz_movie_fusion_images = self.read_parameter(parameters, 'xz_movie_fusion_images',
                                                          self.xz_movie_fusion_images)
        self.yz_movie_fusion_images = self.read_parameter(parameters, 'yz_movie_fusion_images',
                                                          self.yz_movie_fusion_images)

        self.xy_movie_segmentation_images = self.read_parameter(parameters, 'xy_movie_segmentation_images',
                                                                self.xy_movie_segmentation_images)
        self.xz_movie_segmentation_images = self.read_parameter(parameters, 'xz_movie_segmentation_images',
                                                                self.xz_movie_segmentation_images)
        self.yz_movie_segmentation_images = self.read_parameter(parameters, 'yz_movie_segmentation_images',
                                                                self.yz_movie_segmentation_images)

        self.xy_movie_post_segmentation_images = self.read_parameter(parameters, 'xy_movie_post_segmentation_images',
                                                                     self.xy_movie_post_segmentation_images)
        self.xz_movie_post_segmentation_images = self.read_parameter(parameters, 'xz_movie_post_segmentation_images',
                                                                     self.xz_movie_post_segmentation_images)
        self.yz_movie_post_segmentation_images = self.read_parameter(parameters, 'yz_movie_post_segmentation_images',
                                                                     self.yz_movie_post_segmentation_images)

        self.maximum_fusion_images = self.read_parameter(parameters, 'maximum_fusion_images',
                                                         self.maximum_fusion_images)
        self.maximum_segmentation_images = self.read_parameter(parameters, 'maximum_segmentation_images',
                                                               self.maximum_segmentation_images)
        self.maximum_post_segmentation_images = self.read_parameter(parameters, 'maximum_post_segmentation_images',
                                                                    self.maximum_post_segmentation_images)

        #
        # default for margin is none (ie no margin)
        # which is convenient for fusion images
        # however, when dealing with segmentation images as template, a little margin will be great
        # thus, define the default as 10 (if no specification from the user)
        #
        if hasattr(parameters, 'intra_registration_template_type'):
            if parameters.intra_registration_template_type.lower() == 'segmentation' \
                    or parameters.intra_registration_template_type.lower() == 'seg' \
                    or parameters.intra_registration_template_type.lower() == 'post-segmentation' \
                    or parameters.intra_registration_template_type.lower() == 'post_segmentation' \
                    or parameters.intra_registration_template_type.lower() == 'post':
                if margin_is_updated is False:
                    parameters.intra_registration_margin = 10
        if hasattr(parameters, 'template_type'):
            if parameters.template_type.lower() == 'segmentation' \
                    or parameters.template_type.lower() == 'seg' \
                    or parameters.template_type.lower() == 'post-segmentation' \
                    or parameters.template_type.lower() == 'post_segmentation' \
                    or parameters.template_type.lower() == 'post':
                if margin_is_updated is False:
                    parameters.intra_registration_margin = 10

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


########################################################################################
#
# define paths and file names
#
########################################################################################



def get_cotrsf_path_name(experiment, path_cotrsf, floating_index, reference_index):
    flo = experiment.get_time_index(floating_index)
    ref = experiment.get_time_index(reference_index)
    #
    # the use of experiment.working_dir.get_file_suffix() allows more flexibility
    # can be called for extrem drift correction
    #
    co_trsf_name = experiment.get_embryo_name() + experiment.working_dir.get_file_suffix()
    co_trsf_name += '_flo' + str(flo) + '_ref' + str(ref) + '.trsf'
    return os.path.join(path_cotrsf, co_trsf_name)


def get_cotrsf_path_format(experiment, path_cotrsf):
    form = experiment.get_time_format()
    co_trsf_format = experiment.get_embryo_name() + experiment.working_dir.get_file_suffix()
    #
    # the use of experiment.working_dir.get_file_suffix() allows more flexibility
    # can be called for extrem drift correction
    #
    co_trsf_format += '_flo' + form + '_ref' + form + '.trsf'
    return os.path.join(path_cotrsf, co_trsf_format)


def get_coregistered_path_name(experiment, path_coregistered, floating_index, reference_index):
    flo = experiment.get_time_index(floating_index)
    ref = experiment.get_time_index(reference_index)
    co_registered_name = experiment.get_embryo_name() + '_flo' + str(flo) + '_in_ref' + str(ref)
    co_registered_name += '.' + experiment.default_image_suffix

    return os.path.join(path_coregistered, co_registered_name)


def _get_trsf_name(experiment, index):
    """

    :param experiment:
    :param index:
    :return:
    """
    ind = experiment.get_time_index(index)
    trsf_name = experiment.get_embryo_name() + experiment.working_dir.get_file_suffix() + '_t' + str(ind) + '.trsf'
    return trsf_name


def _get_trsf_format(experiment):
    """

    :param experiment:
    :return:
    """
    form = experiment.get_time_format()
    trsf_format = experiment.get_embryo_name() + experiment.working_dir.get_file_suffix() + '_t' + form + '.trsf'
    return trsf_format


def _get_template_path_name(experiment, path_trsf):
    """

    :param experiment:
    :return:
    """
    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point
    suffix = "_t" + experiment.get_time_index(first_time_point) + "-" + experiment.get_time_index(last_time_point)
    result_template = "template" + suffix + "." + experiment.result_image_suffix
    return os.path.join(path_trsf, result_template)


########################################################################################
#
# some internal procedures
#
########################################################################################

def _check_data(experiment, suffix=None):
    """
    Check whether all the images (from the first time point to the last one) exist
    :param experiment:
    :param suffix:
    :return:
    """

    proc = "_check_data"

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    path_fusion = os.path.join(experiment.get_embryo_path(), experiment.fusion_dir.get_directory())

    for current_time in range(first_time_point + experiment.delta_time_point,
                              last_time_point + 1, experiment.delta_time_point):

        input_name = experiment.get_image_name(current_time, 'fuse')

        if suffix is None:
            input_image = common.find_file(path_fusion, input_name, file_type='image', callfrom=proc,
                                           local_monitoring=monitoring)

            if input_image is None:
                monitoring.to_log_and_console("    .. image '" + input_name + "' not found in '"
                                              + str(path_fusion) + "'", 2)
                return False

        else:
            input_image = input_name + '.' + suffix
            if not os.path.isfile(os.path.join(path_fusion, input_image)):
                monitoring.to_log_and_console("    .. image '" + input_image + "' not found in '"
                                              + str(path_fusion) + "'", 2)
                return False

    return True


########################################################################################
#
#
#
########################################################################################


def _coregistration_control(experiment, parameters, prefix=None):
    """
    Perform the co-registration of any couple of two successive images
    Resulting transformations are computed with fused image at t as floating image
    and used image at t+delta_t as reference image

    :param experiment:
    :param parameters:
    :return:
    """

    proc = "_coregistration_control"

    #    if _check_data(experiment) is False:
    #        monitoring.to_log_and_console(proc + ": error, some fused data are missing", 1)
    #        monitoring.to_log_and_console("\t Exiting")
    #        sys.exit(1)

    sub_intrareg_cotrsf = experiment.working_dir.get_cotrsf_path(prefix=prefix)
    path_intrareg_cotrsf = os.path.join(experiment.get_embryo_path(), sub_intrareg_cotrsf)
    if not os.path.isdir(path_intrareg_cotrsf):
        os.makedirs(path_intrareg_cotrsf)

    sub_intrareg_coreg = experiment.working_dir.get_coregistered_path(prefix=prefix)
    path_intrareg_coreg = os.path.join(experiment.get_embryo_path(), sub_intrareg_coreg)
    if parameters.save_coregistered_image is True:
        if not os.path.isdir(path_intrareg_coreg):
            os.makedirs(path_intrareg_coreg)

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    path_fusion = experiment.fusion_dir.get_directory()

    #
    # loop on time
    #
    regparam = parameters.registration

    for reference_time in range(first_time_point + experiment.delta_time_point,
                                last_time_point + 1, experiment.delta_time_point):

        floating_time = reference_time - experiment.delta_time_point
        trsf_name = get_cotrsf_path_name(experiment, path_intrareg_cotrsf, floating_time, reference_time)

        if not os.path.isfile(trsf_name) or monitoring.forceResultsToBeBuilt is True:

            floating_prefix = experiment.fusion_dir.get_image_name(floating_time)
            floating_name = common.find_file(path_fusion, floating_prefix, file_type='image', callfrom=proc,
                                             local_monitoring=monitoring)
            if floating_name is None:
                monitoring.to_log_and_console(proc + ": error, image '" + str(floating_prefix) + "' was not found", 1)
                monitoring.to_log_and_console("\t Exiting")
                sys.exit(1)

            reference_prefix = experiment.fusion_dir.get_image_name(reference_time)
            reference_name = common.find_file(path_fusion, reference_prefix, file_type='image', callfrom=proc,
                                              local_monitoring=monitoring)
            if reference_name is None:
                monitoring.to_log_and_console(proc + ": error, image '" + str(reference_prefix) + "' was not found", 1)
                monitoring.to_log_and_console("\t Exiting")
                sys.exit(1)

            floating_image = os.path.join(path_fusion, floating_name)
            reference_image = os.path.join(path_fusion, reference_name)
            floating_reg = None
            if parameters.save_coregistered_image is True:
                floating_reg = get_coregistered_path_name(experiment, sub_intrareg_coreg, floating_time, reference_time)

            monitoring.to_log_and_console("       co-registering '" + floating_name + "'", 2)
            cpp_wrapping.linear_registration(reference_image, floating_image, floating_reg, trsf_name, None,
                                             py_hl=regparam.pyramid_highest_level,
                                             py_ll=regparam.pyramid_lowest_level,
                                             transformation_type=regparam.transformation_type,
                                             transformation_estimator=regparam.transformation_estimation_type,
                                             lts_fraction=regparam.lts_fraction,
                                             normalization=regparam.normalization,
                                             monitoring=monitoring)

    return


def _transformations_from_reference(experiment, parameters, temporary_dir, prefix=None):
    """
    Combine the transformations issued from the co-registration of pairs of successive images
    to get transformations from one given image
    :param experiment:
    :param parameters:
    :return:
    """

    if not os.path.isdir(temporary_dir):
        os.makedirs(temporary_dir)

    sub_intrareg_cotrsf = experiment.working_dir.get_cotrsf_path(prefix=prefix)
    path_intrareg_cotrsf = os.path.join(experiment.get_embryo_path(), sub_intrareg_cotrsf)

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    #
    #
    build_trsf = False
    if monitoring.forceResultsToBeBuilt is True:
        build_trsf = True
    else:
        for i in range(first_time_point + experiment.delta_time_point, last_time_point + 1,
                       experiment.delta_time_point):
            trsf_name = _get_trsf_name(experiment, i)
            if not os.path.isfile(os.path.join(temporary_dir, trsf_name)):
                build_trsf = True
                break

    #
    #
    #
    if build_trsf is True:
        if parameters.reference_index is None:
            reference_index = first_time_point
        else:
            reference_index = parameters.reference_index

        format_input = get_cotrsf_path_format(experiment, path_intrareg_cotrsf)

        trsf_format = _get_trsf_format(experiment)
        format_output = os.path.join(temporary_dir, trsf_format)

        cpp_wrapping.multiple_trsfs(format_input, format_output, first_time_point, last_time_point,
                                    reference_index, trsf_type=parameters.registration.transformation_type,
                                    monitoring=monitoring)

    return


def _is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def _get_reference_transformation(parameters, temporary_dir):
    """

    :param parameters:
    :return:
    """

    proc = "_get_reference_transformation"

    if parameters.reference_transformation_file is not None:
        if os.path.isfile(parameters.reference_transformation_file):
            return parameters.reference_transformation_file

    if parameters.reference_transformation_angles is not None:
        if type(parameters.reference_transformation_angles) == str:
            s = parameters.reference_transformation_angles.split(' ')
            create_trsf_option = "-angle-unit degree"
            le = len(create_trsf_option)
            i = 0
            while i < len(s):
                if s[i].lower() == 'x':
                    if i + 1 < len(s) and _is_float(s[i + 1]):
                        create_trsf_option += ' -xrotation ' + s[i + 1]
                    else:
                        monitoring.to_log_and_console(proc + ": weird value for 'X rotation' -> " + str(s[i + 1]), 1)
                elif s[i].lower() == 'y':
                    if i + 1 < len(s) and _is_float(s[i + 1]):
                        create_trsf_option += ' -yrotation ' + s[i + 1]
                    else:
                        monitoring.to_log_and_console(proc + ": weird value for 'Y rotation' -> " + str(s[i + 1]), 1)
                elif s[i].lower() == 'z':
                    if i + 1 < len(s) and _is_float(s[i + 1]):
                        create_trsf_option += ' -zrotation ' + s[i + 1]
                    else:
                        monitoring.to_log_and_console(proc + ": weird value for 'Z rotation' -> " + str(s[i + 1]), 1)
                i += 2

            if len(create_trsf_option) > le:
                trsf_name = os.path.join(temporary_dir, "reference_transformation.trsf")
                cpp_wrapping.create_trsf(trsf_name, other_options=create_trsf_option, monitoring=monitoring)
                if os.path.isfile(trsf_name):
                    return trsf_name
                else:
                    monitoring.to_log_and_console(proc + ": unable to compute reference transformation", 1)
            else:
                monitoring.to_log_and_console(proc + ": unable to translate reference transformation angles", 1)

    return None


def _transformations_and_template(experiment, parameters, temporary_dir, prefix=None):
    """
    From transformations from one given image, compute the template to resample all images.
    :param experiment:
    :param parameters:
    :param temporary_dir:
    :return:
    """

    proc = "_transformations_and_template"

    if isinstance(parameters, IntraRegParameters) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'parameters' parameter", 1)
        sys.exit(1)

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point
    suffix = "_t" + experiment.get_time_index(first_time_point) + "-" + experiment.get_time_index(last_time_point)
    result_dir = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_trsf_path(prefix=prefix,
                                                                                                 suffix=suffix))
    result_template = _get_template_path_name(experiment, result_dir)

    if os.path.isfile(result_template) and monitoring.forceResultsToBeBuilt is not True \
            and parameters.rebuild_template is not True:
        return

    monitoring.to_log_and_console("       warning: this stage may be long", 2)

    if not os.path.isdir(result_dir):
        os.makedirs(result_dir)

    #
    # which format to use?
    # if requested, try to use segmentation image (threshold should be set to 2)
    # else use fusion images
    #
    template_format = None

    if parameters.template_type.lower() == 'segmentation' or parameters.template_type.lower() == 'seg':
        #
        # check whether segmentation image share a common suffix
        #
        path_template_format = os.path.join(experiment.get_embryo_path(), experiment.astec_dir.get_directory())
        suffix = common.get_file_suffix(experiment, path_template_format, experiment.astec_dir.get_image_format(),
                                        flag_time=experiment.get_time_format())

        if suffix is None:
            monitoring.to_log_and_console(proc + ": no consistent naming was found in '"
                                          + experiment.astec_dir.get_directory() + "'", 1)
            monitoring.to_log_and_console("\t switch to fused images as templates", 1)

        else:
            monitoring.to_log_and_console("       ... build template from segmentation images of '"
                                          + experiment.astec_dir.get_directory() + "'", 2)
            template_name = experiment.astec_dir.get_image_format() + '.' + suffix
            template_format = os.path.join(path_template_format, template_name)

    elif parameters.template_type.lower() == 'post-segmentation' \
            or parameters.template_type.lower() == 'post_segmentation' or parameters.template_type.lower() == 'post':
        #
        # check whether post-corrected segmentation image share a common suffix
        #
        path_template_format = os.path.join(experiment.get_embryo_path(), experiment.post_dir.get_directory())
        suffix = common.get_file_suffix(experiment, path_template_format, experiment.post_dir.get_image_format(),
                                        flag_time=experiment.get_time_format())

        if suffix is None:
            monitoring.to_log_and_console(proc + ": no consistent naming was found in '"
                                          + experiment.post_dir.get_directory() + "'", 1)
            monitoring.to_log_and_console("\t switch to fused images as templates", 1)

        else:
            monitoring.to_log_and_console("       ... build template from post-segmentation images of '"
                                          + experiment.post_dir.get_directory() + "'", 2)
            template_name = experiment.post_dir.get_image_format() + '.' + suffix
            template_format = os.path.join(path_template_format, template_name)

    #
    # use fusion images to build the template
    #
    if template_format is None:
        #
        # check whether fusion image share a common suffix
        #
        path_template_format = os.path.join(experiment.get_embryo_path(), experiment.fusion_dir.get_directory())
        suffix = common.get_file_suffix(experiment, path_template_format, experiment.fusion_dir.get_image_format(),
                                        flag_time=experiment.get_time_format())
        if suffix is None:
            monitoring.to_log_and_console(proc + ": no consistent naming was found in '"
                                          + experiment.fusion_dir.get_directory() + "'", 1)
            monitoring.to_log_and_console("\t Exiting", 1)
            sys.exit(1)
        else:
            monitoring.to_log_and_console("       ... build template from fusion images of '"
                                          + experiment.fusion_dir.get_directory() + "'", 2)
            template_name = experiment.fusion_dir.get_image_format() + '.' + suffix
            template_format = os.path.join(path_template_format, template_name)

    #
    # other parameters
    #

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    trsf_format = _get_trsf_format(experiment)
    format_input = os.path.join(temporary_dir, trsf_format)
    format_output = os.path.join(result_dir, trsf_format)

    if parameters.reference_index is None:
        reference_index = first_time_point
    else:
        reference_index = parameters.reference_index

    #
    #
    #

    cpp_wrapping.change_multiple_trsfs(format_input, format_output, first_time_point, last_time_point, reference_index,
                                       result_template, trsf_type=parameters.registration.transformation_type,
                                       resolution=parameters.resolution, threshold=parameters.template_threshold,
                                       margin=parameters.margin, format_template=template_format,
                                       reference_transformation=_get_reference_transformation(parameters,
                                                                                              temporary_dir),
                                       monitoring=monitoring)

    return


def _resample_images(experiment, parameters, template_image, directory_type, interpolation_mode='linear', prefix=None):
    """
    resample all images given a set of transformations and a template
    :param experiment:
    :param parameters:
    :param template_image:
    :param directory_type:
    :param interpolation_mode:
    :return:
    """

    proc = "_resample_images"

    #
    # in case the template has been gziped, or copied into an other format
    #

    b = os.path.basename(template_image)
    d = os.path.dirname(template_image)
    local_template_name = common.find_file(d, b, file_type='image', callfrom=proc, local_monitoring=monitoring)
    if local_template_name is None:
        monitoring.to_log_and_console(proc + ": template '" + str(b) + "' was not found in '" + str(d) + "'", 1)
        monitoring.to_log_and_console("\t resampling will not be done")
        return
    local_template_image = os.path.join(d, local_template_name)

    #
    #
    #

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    #
    #

    if directory_type.lower() == 'fuse':
        working_dir = experiment.fusion_dir
    elif directory_type.lower() == 'post':
        working_dir = experiment.post_dir
    elif directory_type.lower() == 'seg':
        working_dir = experiment.astec_dir
    elif directory_type.lower() == 'membrane-reconstruction':
        working_dir = experiment.recmembrane_dir
    elif directory_type.lower() == 'seed-reconstruction':
        working_dir = experiment.recseed_dir
    else:
        monitoring.to_log_and_console(proc + ": unknown directory type '" + str(directory_type) + "'", 1)
        monitoring.to_log_and_console("\t resampling will not be done")
        return

    #
    # loop on directories
    #
    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point
    suffix = "_t" + experiment.get_time_index(first_time_point) + "-" + experiment.get_time_index(last_time_point)
    trsf_dir = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_trsf_path(prefix=prefix,
                                                                                               suffix=suffix))

    for idir in range(working_dir.get_number_directories()):

        dir_input = working_dir.get_directory(idir)
        monitoring.to_log_and_console("     . resampling '" + str(dir_input) + "'", 2)
        dir_input = os.path.join(experiment.get_embryo_path(), working_dir.get_sub_directory(idir))
        if not os.path.isdir(dir_input):
            monitoring.to_log_and_console("       '" + str(dir_input) + "' does not exist, skip it.", 2)
            continue

        subdir = working_dir.get_sub_directory(idir)
        if prefix is not None and isinstance(prefix, str):
            subdir = prefix + subdir
        dir_output = os.path.join(experiment.working_dir.get_directory(), subdir)
        if not os.path.isdir(dir_output):
            os.makedirs(dir_output)

        #
        # loop on images
        #

        for t in range(first_time_point + experiment.delay_time_point,
                       last_time_point + experiment.delay_time_point + 1, experiment.delta_time_point):

            input_name = working_dir.get_image_name(t)

            output_name = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                          working_dir.get_file_suffix() + experiment.working_dir.get_time_prefix() + \
                          experiment.get_time_index(t)

            output_image = common.find_file(dir_output, output_name, file_type='image', callfrom=proc,
                                            local_monitoring=None, verbose=False)

            if output_image is None or monitoring.forceResultsToBeBuilt is True or parameters.rebuild_template is True:

                input_image = common.find_file(dir_input, input_name, file_type='image', callfrom=proc,
                                               local_monitoring=monitoring)
                output_image = os.path.join(dir_output, output_name + '.' + str(experiment.result_image_suffix))
                trsf_name = os.path.join(trsf_dir, _get_trsf_name(experiment, t))

                if input_image is None:
                    msg = "- image '" + str(input_name) + "' was not found in '" + str(dir_input) + "', skip it"
                    monitoring.to_log_and_console("       " + msg, 1)
                elif not os.path.isfile(trsf_name):
                    monitoring.to_log_and_console(proc + ": transformation '" + str(_get_trsf_name(experiment, t))
                                                  + "' was not found in '" + str(trsf_dir) + "'", 1)
                else:
                    monitoring.to_log_and_console("       resampling '" + str(input_image) + "'", 2)
                    input_image = os.path.join(dir_input, input_image)
                    cpp_wrapping.apply_transformation(input_image, output_image, trsf_name, local_template_image,
                                                      interpolation_mode=interpolation_mode,
                                                      cell_based_sigma=parameters.sigma_segmentation_images,
                                                      monitoring=monitoring)

    return


def _copy_lineage(experiment, directory_type):
    proc = "_copy_lineage"

    if directory_type.lower() == 'post':
        working_dir = experiment.post_dir
    elif directory_type.lower() == 'seg':
        working_dir = experiment.astec_dir
    else:
        monitoring.to_log_and_console(proc + ": unknown directory type '" + str(directory_type) + "'", 1)
        monitoring.to_log_and_console("\t lineage will not be copied")
        return

    for idir in range(working_dir.get_number_directories()):

        #
        # lineage input file name
        #
        segmentation_dir = working_dir.get_directory()

        lineage_tree_file = common.find_file(segmentation_dir, working_dir.get_file_name("_lineage"),
                                             file_type='lineage', callfrom=proc, verbose=False)
        if lineage_tree_file is None:
            monitoring.to_log_and_console(str(proc) + ": unable to find lineage file in " + str(segmentation_dir))
            return
        elif os.path.isfile(os.path.join(segmentation_dir, lineage_tree_file)):
            lineage_tree_path = os.path.join(segmentation_dir, lineage_tree_file)
            lineage_tree_information = ioproperties.read_dictionary(lineage_tree_path)
        else:
            monitoring.to_log_and_console(str(proc) + ": " + str(lineage_tree_file) + " is not a file?")
            return

        #
        # keep nothing except lineage
        #
        new_lineage_tree_information = {'cell_lineage': lineage_tree_information['cell_lineage']}

        #
        # lineage output file name
        #
        dir_output = os.path.join(experiment.working_dir.get_directory(), working_dir.get_sub_directory(idir))
        output_name = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                      working_dir.get_file_suffix() + "_lineage" + "." + experiment.result_lineage_suffix
        new_lineage_tree_path = os.path.join(dir_output, output_name)

        ioproperties.write_dictionary(new_lineage_tree_path, new_lineage_tree_information)
    return


def _image_extension(experiment, dir_input):
    #
    # check image extension
    #
    extensions = []
    for f in os.listdir(dir_input):
        for e in common.recognized_image_extensions:
            if f[len(f) - len(e):len(f)] == e:
                extensions.append(e)
    extensions = list(set(extensions))
    if len(extensions) == 1:
        if extensions[0][1:] != experiment.result_image_suffix:
            msg = "use '" + str(extensions[0]) + "' as extension instead of '"
            msg += str(experiment.result_image_suffix) + "'"
            monitoring.to_log_and_console("     . " + msg, 2)
        return str(extensions[0])
    else:
        if len(extensions) > 1:
            msg = "several extensions were found in '" + str(dir_input) + "': " + str(extensions)
            monitoring.to_log_and_console("     . " + msg, 2)
            msg = "use '" + str(experiment.result_image_suffix) + "' as extension"
            monitoring.to_log_and_console("     . " + msg, 2)
        return '.' + str(experiment.result_image_suffix)


def _movie_to_be_made(xy_movie, xz_movie, yz_movie):
    xy_bool = False
    if isinstance(xy_movie, bool):
        if xy_movie is True:
            xy_bool = True
    elif isinstance(xy_movie, list):
        if len(xy_movie) > 0:
            xy_bool = True
    xz_bool = False
    if isinstance(xz_movie, bool):
        if xz_movie is True:
            xz_bool = True
    elif isinstance(xz_movie, list):
        if len(xz_movie) > 0:
            xz_bool = True
    yz_bool = False
    if isinstance(yz_movie, bool):
        if yz_movie is True:
            yz_bool = True
    elif isinstance(yz_movie, list):
        if len(yz_movie) > 0:
            yz_bool = True
    return xy_bool or xz_bool or yz_bool


def _make_movies(experiment, parameters, directory_type, xylist, xzlist, yzlist, prefix=None):
    """

    :param experiment:
    :param parameters:
    :param directory_type:
    :param xylist:
    :param xzlist:
    :param yzlist:
    :return:
    """

    proc = "_make_movies"

    #
    #
    #

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    #
    #

    if directory_type.lower() == 'fuse':
        working_dir = experiment.fusion_dir
    elif directory_type.lower() == 'post':
        working_dir = experiment.post_dir
    elif directory_type.lower() == 'seg':
        working_dir = experiment.astec_dir
    else:
        monitoring.to_log_and_console(proc + ": unknown directory type '" + str(directory_type) + "'", 1)
        monitoring.to_log_and_console("\t movies will not be done")
        return

    #
    # should we switch to default behavior?
    #

    xy = []
    xz = []
    yz = []

    if isinstance(xylist, list):
        xy = xylist
    if isinstance(xzlist, list):
        xz = xzlist
    if isinstance(yzlist, list):
        yz = yzlist

    #
    # loop on directories
    #
    suffix = "_t" + experiment.get_time_index(first_time_point) + "-" + experiment.get_time_index(last_time_point)
    moviedir = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_movie_path(prefix=prefix,
                                                                                                suffix=suffix))

    for idir in range(working_dir.get_number_directories()):

        subdir = working_dir.get_sub_directory(idir)
        if prefix is not None and isinstance(prefix, str):
            subdir = prefix + subdir
        dir_input = os.path.join(experiment.working_dir.get_directory(), subdir)
        monitoring.to_log_and_console("     . movies from '" + str(dir_input) + "'", 2)
        dir_output = os.path.join(moviedir, working_dir.get_sub_directory(idir))

        if not os.path.isdir(dir_output):
            os.makedirs(dir_output)

        #
        # have an image to be read
        # - if all (xy, xz, yz) are empty list
        #   => select the middle slice for xy (default historical behavior)
        # - if one (xy, xz, yz) is bool and True
        #   => select the middle slice for this one
        #
        xymiddle = isinstance(xy, list) and len(xy) == 0 and isinstance(xz, list) and len(xz) == 0 and \
                   isinstance(yz, list) and len(xz) == 0
        xymiddle = xymiddle or (isinstance(xylist, bool) and xylist is True)
        xzmiddle = isinstance(xzlist, bool) and xzlist is True
        yzmiddle = isinstance(yzlist, bool) and yzlist is True

        if xymiddle or xzmiddle or yzmiddle:
            #
            # read the first image and set XY slice in the middle
            #
            first_prefix = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                           working_dir.get_file_suffix() + experiment.working_dir.get_time_prefix() + \
                           experiment.get_time_index(first_time_point)
            first_name = common.find_file(dir_input, first_prefix, file_type='image', callfrom=proc,
                                          local_monitoring=None, verbose=False)
            if first_name is None:
                monitoring.to_log_and_console(proc + ": no file '" + str(first_prefix) + "' in '" + str(dir_input) +
                                              "'", 1)
                monitoring.to_log_and_console("\t movies will not be done")
                return
            first_image = imread(os.path.join(dir_input, first_name))
            if xymiddle is True:
                xy.append(int(first_image.shape[2] / 2))
            if xzmiddle is True:
                xz.append(int(first_image.shape[1] / 2))
            if yzmiddle is True:
                yz.append(int(first_image.shape[0] / 2))
            del first_image

        input_format = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                       working_dir.get_file_suffix() + experiment.working_dir.get_time_prefix() + \
                       experiment.get_time_format()
        #
        # check image extension
        #
        input_format = os.path.join(dir_input, input_format + _image_extension(experiment, dir_input))

        #
        # processing
        #

        name_prefix = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                      working_dir.get_file_suffix() + experiment.working_dir.get_time_prefix() + \
                      experiment.get_time_index(first_time_point) + '-' + \
                      experiment.get_time_index(last_time_point)
        if len(xy) > 0:
            for s in xy:
                name_output = name_prefix + '_xy' + '{:0{width}d}'.format(s, width=4)
                name_output = os.path.join(dir_output, name_output + '.' + str(experiment.result_image_suffix))
                if os.path.isfile(name_output) is False or monitoring.forceResultsToBeBuilt is True \
                        or parameters.rebuild_template is True:
                    monitoring.to_log_and_console("       process xy=" + str(s), 2)
                    cpp_wrapping.crop_sequence(input_format, name_output, first_time_point, last_time_point, 'xy', s,
                                               monitoring=monitoring)
                if os.path.isfile(name_output) is False:
                    monitoring.to_log_and_console("       generating '" + str(name_output) + "' has failed", 2)

        if len(xz) > 0:
            for s in xz:
                name_output = name_prefix + '_xz' + '{:0{width}d}'.format(s, width=4)
                name_output = os.path.join(dir_output, name_output + '.' + str(experiment.result_image_suffix))
                if os.path.isfile(name_output) is False or monitoring.forceResultsToBeBuilt is True \
                        or parameters.rebuild_template is True:
                    monitoring.to_log_and_console("       process xz=" + str(s), 2)
                    cpp_wrapping.crop_sequence(input_format, name_output, first_time_point, last_time_point, 'xz', s,
                                               monitoring=monitoring)
                if os.path.isfile(name_output) is False:
                    monitoring.to_log_and_console("       generating '" + str(name_output) + "' has failed", 2)

        if len(yz) > 0:
            for s in yz:
                name_output = name_prefix + '_yz' + '{:0{width}d}'.format(s, width=4)
                name_output = os.path.join(dir_output, name_output + '.' + str(experiment.result_image_suffix))
                if os.path.isfile(name_output) is False or monitoring.forceResultsToBeBuilt is True \
                        or parameters.rebuild_template is True:
                    monitoring.to_log_and_console("       process yz=" + str(s), 2)
                    cpp_wrapping.crop_sequence(input_format, name_output, first_time_point, last_time_point, 'yz', s,
                                               monitoring=monitoring)
                if os.path.isfile(name_output) is False:
                    monitoring.to_log_and_console("       generating '" + str(name_output) + "' has failed", 2)

    return


def _make_maximum(experiment, directory_type, prefix=None):
    """

    :param experiment:
    :param directory_type:
    :return:
    """

    proc = "_make_maximum"

    #
    #
    #

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    #
    #
    #

    if directory_type.lower() == 'fuse':
        working_dir = experiment.fusion_dir
    elif directory_type.lower() == 'post':
        working_dir = experiment.post_dir
    elif directory_type.lower() == 'seg':
        working_dir = experiment.astec_dir
    else:
        monitoring.to_log_and_console(proc + ": unknown directory type '" + str(directory_type) + "'", 1)
        monitoring.to_log_and_console("\t maximum will not be done")
        return

    #
    # loop on directories
    #
    suffix = "_t" + experiment.get_time_index(first_time_point) + "-" + experiment.get_time_index(last_time_point)
    maxdir = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_maximum_path(suffix=suffix))

    for idir in range(working_dir.get_number_directories()):

        subdir = working_dir.get_sub_directory(idir)
        if prefix is not None and isinstance(prefix, str):
            subdir = prefix + subdir
        dir_input = os.path.join(experiment.working_dir.get_directory(), subdir)
        monitoring.to_log_and_console("     . maximum from '" + str(dir_input) + "'", 2)
        dir_output = os.path.join(maxdir, working_dir.get_sub_directory(idir))
        if not os.path.isdir(dir_output):
            os.makedirs(dir_output)

        input_format = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                       working_dir.get_file_suffix() + experiment.working_dir.get_time_prefix() + \
                       experiment.get_time_format()
        input_format = os.path.join(dir_input, input_format + _image_extension(experiment, dir_input))

        #
        # processing
        #

        name_prefix = experiment.working_dir.get_file_prefix() + experiment.working_dir.get_file_suffix() + \
                      working_dir.get_file_suffix() + experiment.working_dir.get_time_prefix() + \
                      experiment.get_time_index(first_time_point) + '-' + \
                      experiment.get_time_index(last_time_point)
        name_output = name_prefix + '_maximum'
        name_output = os.path.join(dir_output, name_output + '.' + str(experiment.result_image_suffix))
        cpp_wrapping.mean_images(input_format, name_output, first_time_point, last_time_point, operation="maximum",
                                 monitoring=monitoring)
    return


########################################################################################
#
#
#
########################################################################################


def intraregistration_control(experiment, parameters, prefix=None):
    """

    :param experiment:
    :param parameters:
    :param prefix: used for fusionmotion  call
    :return:
    """

    proc = "intraregistration_control"

    if isinstance(experiment, datadir.Experiment) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'experiment' parameter", 1)
        sys.exit(1)
    if isinstance(parameters, IntraRegParameters) is False:
        monitoring.to_log_and_console(proc + ": bad type for 'parameters' parameter", 1)
        sys.exit(1)

    #
    # start processing
    #
    start_time = time.time()

    #
    # if template does not exists,
    # 1. compute transformations between successive images
    #    INTRAREG/INTRAREG_<EXP_INTRAREG>/CO-TRSFS directory
    # 2. compose transformations wrt a reference (default = first time point)
    #    INTRAREG/INTRAREG_<EXP_INTRAREG>/CO-TRSFS/TEMP directory
    # 3. re-compute transformations (ie translations) and template that includes all transformed images
    #    INTRAREG/INTRAREG_<EXP_INTRAREG>/TRSFS_t<first>-<last> directory
    #

    sub_intrareg_cotrsf = experiment.working_dir.get_cotrsf_path(prefix=prefix)
    path_intrareg_cotrsf = os.path.join(experiment.get_embryo_path(), sub_intrareg_cotrsf)

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point
    suffix = "_t" + experiment.get_time_index(first_time_point) + "-" + experiment.get_time_index(last_time_point)
    result_dir = os.path.join(experiment.get_embryo_path(), experiment.working_dir.get_trsf_path(prefix=prefix,
                                                                                                 suffix=suffix))
    result_template = _get_template_path_name(experiment, result_dir)


    if not os.path.isdir(result_dir) or not os.path.isfile(result_template) \
            or parameters.rebuild_template is True or monitoring.forceResultsToBeBuilt is True:
        if experiment.delta_time_point > 1:
            monitoring.to_log_and_console(proc + ": warning, delta_time=" + str(experiment.delta_time_point)
                                          + ", this step may be fragile", 1)
        #
        # co-registration of any 2 successive images
        # will fill the INTRAREG/INTRAREG_<EXP_INTRAREG>/CO-TRSFS directory
        #

        monitoring.to_log_and_console("    .. co-registrations", 2)
        _coregistration_control(experiment, parameters, prefix=prefix)

        #
        # composition of transformations by propagation
        # results in a set of transformation from the reference image (given by the reference_index)
        # towards each image
        # will fill the INTRAREG/INTRAREG_<EXP_INTRAREG>/TEMP directory
        #

        monitoring.to_log_and_console("    .. transformation composition", 2)

        temporary_dir = os.path.join(path_intrareg_cotrsf, 'TEMP')
        _transformations_from_reference(experiment, parameters, temporary_dir, prefix=prefix)

        #
        # re-composition of transformations
        # template creation for further resampling operations
        # will fill the INTRAREG/INTRAREG_<EXP_INTRAREG>/TRSFS_t<first>-<last> directory
        #

        monitoring.to_log_and_console("    .. transformation recomposition and template generation", 2)

        _transformations_and_template(experiment, parameters, temporary_dir, prefix=prefix)

        if monitoring.keepTemporaryFiles is False:
            shutil.rmtree(temporary_dir)

    #
    # template image and resampling transformations have been computed
    # resample images if required
    #
    # NOTE: il y a un probleme de coherence, puisque la premiere image de segmentation peut etre issue
    # de mars et donc etre nommee differemment. Pour le reechantillonage, on pourrait utiliser
    # reconstruction.get_segmentation_image().
    #
    _fusion_movie_ = parameters.movie_fusion_images
    _fusion_movie_ = _fusion_movie_ or _movie_to_be_made(parameters.xy_movie_fusion_images,
                                                         parameters.xz_movie_fusion_images,
                                                         parameters.yz_movie_fusion_images)
    _segmentation_movie_ = parameters.movie_segmentation_images
    _segmentation_movie_ = _segmentation_movie_ or _movie_to_be_made(parameters.xy_movie_segmentation_images,
                                                                     parameters.xz_movie_segmentation_images,
                                                                     parameters.yz_movie_segmentation_images)
    _post_segmentation_movie_ = parameters.movie_post_segmentation_images
    _post_segmentation_movie_ = _post_segmentation_movie_ or _movie_to_be_made(
        parameters.xy_movie_post_segmentation_images,
        parameters.xz_movie_post_segmentation_images,
        parameters.yz_movie_post_segmentation_images)

    if parameters.resample_fusion_images is True or _fusion_movie_ is True:
        monitoring.to_log_and_console("    .. resampling fusion images", 2)
        _resample_images(experiment, parameters, result_template, 'fuse', prefix=prefix)

    if parameters.resample_reconstruction_images is True:
        monitoring.to_log_and_console("    .. resampling reconstruction membrane images", 2)
        _resample_images(experiment, parameters, result_template, 'membrane-reconstruction')
        monitoring.to_log_and_console("    .. resampling reconstruction seed images", 2)
        _resample_images(experiment, parameters, result_template, 'seed-reconstruction', prefix=prefix)

    if parameters.resample_segmentation_images is True or _segmentation_movie_ is True:
        monitoring.to_log_and_console("    .. resampling segmentation images", 2)
        _resample_images(experiment, parameters, result_template, 'seg', interpolation_mode='nearest', prefix=prefix)
        monitoring.to_log_and_console("    .. copying lineage file", 2)
        _copy_lineage(experiment, 'seg')

    if parameters.resample_post_segmentation_images is True or _post_segmentation_movie_ is True:
        monitoring.to_log_and_console("    .. resampling post-segmentation images", 2)
        _resample_images(experiment, parameters, result_template, 'post', interpolation_mode='nearest', prefix=prefix)
        monitoring.to_log_and_console("    .. copying lineage file", 2)
        _copy_lineage(experiment, 'post')

    #
    # make 3D=2D+t images = movie of evolving slices with respect to time
    #

    if _fusion_movie_ is True:
        monitoring.to_log_and_console("    .. movies from fusion images", 2)
        _make_movies(experiment, parameters, 'fuse', parameters.xy_movie_fusion_images,
                     parameters.xz_movie_fusion_images, parameters.yz_movie_fusion_images, prefix=prefix)

    if _segmentation_movie_ is True:
        monitoring.to_log_and_console("    .. movies from segmentation images", 2)
        _make_movies(experiment, parameters, 'seg', parameters.xy_movie_segmentation_images,
                     parameters.xz_movie_segmentation_images, parameters.yz_movie_segmentation_images, prefix=prefix)

    if _post_segmentation_movie_ is True:
        monitoring.to_log_and_console("    .. movies from post-segmentation images", 2)
        _make_movies(experiment, parameters, 'post', parameters.xy_movie_post_segmentation_images,
                     parameters.xz_movie_post_segmentation_images, parameters.yz_movie_post_segmentation_images,
                     prefix=prefix)

    #
    # make maximum images
    #

    if parameters.maximum_fusion_images is True:
        monitoring.to_log_and_console("    .. maximum from fusion images", 2)
        monitoring.to_log_and_console("       warning: this stage may be long", 2)
        _make_maximum(experiment, 'fuse', prefix=prefix)

    if parameters.maximum_segmentation_images is True:
        monitoring.to_log_and_console("    .. maximum from fusion images", 2)
        monitoring.to_log_and_console("       warning: this stage may be long", 2)
        _make_maximum(experiment, 'seg', prefix=prefix)

    if parameters.maximum_post_segmentation_images is True:
        monitoring.to_log_and_console("    .. maximum from fusion images", 2)
        monitoring.to_log_and_console("       warning: this stage may be long", 2)
        _make_maximum(experiment, 'post', prefix=prefix)

    #
    # end processing
    #

    end_time = time.time()

    monitoring.to_log_and_console('    computation time = ' + str(end_time - start_time) + ' s', 1)
    monitoring.to_log_and_console('', 1)

    return
