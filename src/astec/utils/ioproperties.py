##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#
##############################################################
#
#
#
##############################################################

import os
import sys
import time
import getpass

import numpy as np
import pickle as pkl
import xml.etree.ElementTree as ElementTree


class ToyMonitoring(object):

    def __init__(self):
        self.verbose = -1
        self.debug = 0
        self.log_filename = None

    @staticmethod
    def to_console(text):
        print(text)

    def to_log(self, text):
        if self.log_filename is not None:
            with open(self.log_filename, 'a') as logfile:
                logfile.write(text+'\n')

    def to_log_and_console(self, text, verboseness=0):
        if self.verbose >= verboseness or self.debug > 0:
            self.to_console(text)
        self.to_log(text)


import importlib.metadata
try:
    importlib.metadata.version("astec")
    import astec.utils.common as common
    monitoring = common.Monitoring()
except PackageNotFoundError :
    monitoring = ToyMonitoring()


########################################################################################
#
# key correspondences for astec-related characteristics
# input_keys are possible dictionary keys in input files
# output_key is the "standardised" output key
#
# some keys are obsolete. They were used in previous/historical versions of astec
#
# Examples
# - from 'full_properties_Samson_MN20.pkl', keys are
#   ['volumes information',
#    'Cells labels in time',
#    'Barycenters',
#    'Fate',
#    'All Cells',
#    'Principal values',
#    'Names',
#    'cell_cell_contact_information',
#    'Lineage tree',
#    'Cells history',
#    'Principal vectors']
# - from 'new_lineage_tree_MN20.pkl', keys are cell labels
# - from '140317-Patrick-St8_seg_lineage.pkl', keys are
#   ['h_mins_information', 'lin_tree', 'volumes_information', 'sigmas_information']
#
#
########################################################################################

_astec_keys = {'all_cells': {'output_key': 'all_cells',
                             'input_keys': ['all_cells', 'All Cells', 'All_Cells', 'all cells', 'tot_cells']},
               'barycenter': {'output_key': 'cell_barycenter',
                              'input_keys': ['cell_barycenter', 'Barycenters', 'barycenters']},
               'compactness': {'output_key': 'cell_compactness',
                               'input_keys': ['cell_compactness', 'Cell Compactness', 'compacity', 'cell_sphericity']},
               'contact_surface': {'output_key': 'cell_contact_surface',
                                   'input_keys': ['cell_contact_surface', 'cell_cell_contact_information']},
               'contact_edge': {'output_key': 'cell_contact_edge',
                                'input_keys': ['cell_contact_edge']},
               'contact_edge_length': {'output_key': 'cell_contact_edge_length',
                                       'input_keys': ['cell_contact_edge_length']},
               'contact_edge_segment': {'output_key': 'cell_contact_edge_segment',
                                        'input_keys': ['cell_contact_edge_segment']},
               'h_min': {'output_key': 'cell_h_min',
                         'input_keys': ['cell_h_min', 'h_mins_information']},
               'history': {'output_key': 'cell_history',
                           'input_keys': ['cell_history', 'Cells history', 'cell_life', 'life']},
               'labels_in_time': {'output_key': 'cell_labels_in_time',
                                  'input_keys': ['cell_labels_in_time', 'Cells labels in time', 'time_labels']},
               'lineage': {'output_key': 'cell_lineage',
                           'input_keys': ['lineage_tree', 'lin_tree', 'Lineage tree', 'cell_lineage']},
               'name': {'output_key': 'cell_name',
                        'input_keys': ['cell_name', 'Names', 'names', 'cell_names']},
               'naming_score': {'output_key': 'cell_naming_score',
                                'input_keys': ['cell_naming_score', 'Scores', 'scores']},
               'principal_values': {'output_key': 'cell_principal_values',
                                    'input_keys': ['cell_principal_values', 'Principal values']},
               'principal_vectors': {'output_key': 'cell_principal_vectors',
                                     'input_keys': ['cell_principal_vectors', 'Principal vectors']},
               'problematic_cells': {'output_key': 'problematic_cells',
                                     'input_keys': ['problematic_cells']},
               'sigma': {'output_key': 'cell_sigma',
                           'input_keys': ['cell_sigma', 'sigmas_information', 'sigmas']},
               'surface': {'output_key': 'cell_surface',
                           'input_keys': ['cell_surface', 'cell surface']},
               'tissue_fate': {'output_key': 'cell_tissue_fate',
                               'input_keys': ['cell_tissue_fate', 'cell_fate', 'Fate']},
               'unknown': {'output_key': 'unknown_key',
                           'input_keys': ['unknown_key']},
               'volume': {'output_key': 'cell_volume',
                          'input_keys': ['cell_volume', 'volumes_information', 'volumes information', 'vol']}}


########################################################################################
#
# morphonet type for astec-related characteristics
#
########################################################################################


_astec_mn_type = {'contact_surface': 'dict', 'lineage': 'time', 'name': 'string', 'surface' : 'float',
                 'tissue_fate': 'string', 'volume': 'float'}


########################################################################################
#
# key correspondences for urchin-related characteristics
#
########################################################################################


_urchin_keys = {'apicobasal_length': {'output_key': 'cell_apicobasal_length',
                                     'input_keys': ['cell_apicobasal_length']},
               'urchin_adjacency': {'output_key': 'urchin_cell_adjacency',
                                    'input_keys': ['urchin_cell_adjacency']},
               'urchin_apical_contact_edge_length': {'output_key': 'urchin_apical_contact_edge_length',
                                                     'input_keys': ['urchin_apical_contact_edge_length']},
               'urchin_apical_contact_edge_segment': {'output_key': 'urchin_apical_contact_edge_segment',
                                                      'input_keys': ['urchin_apical_contact_edge_segment']},
               'urchin_apical_surface': {'output_key': 'urchin_apical_surface',
                                         'input_keys': ['urchin_apical_surface']},
               'urchin_apical_surface_barycenter': {'output_key': 'urchin_apical_surface_barycenter',
                                                    'input_keys': ['urchin_apical_surface_barycenter']},
               'urchin_apicobasal_length': {'output_key': 'urchin_cell_apicobasal_length',
                                            'input_keys': ['urchin_cell_apicobasal_length']},
               'urchin_apicobasal_segment': {'output_key': 'urchin_cell_apicobasal_segment',
                                             'input_keys': ['urchin_cell_apicobasal_segment']},
               'urchin_basal_contact_edge_length': {'output_key': 'urchin_basal_contact_edge_length',
                                                    'input_keys': ['urchin_basal_contact_edge_length']},
               'urchin_basal_contact_edge_segment': {'output_key': 'urchin_basal_contact_edge_segment',
                                                     'input_keys': ['urchin_basal_contact_edge_segment']},
               'urchin_basal_surface': {'output_key': 'urchin_basal_surface',
                                        'input_keys': ['urchin_basal_surface']},
               'urchin_basal_surface_barycenter': {'output_key': 'urchin_basal_surface_barycenter',
                                                   'input_keys': ['urchin_basal_surface_barycenter']},
               'urchin_vegetal_distance': {'output_key': 'urchin_vegetal_distance',
                                           'input_keys': ['urchin_vegetal_distance']}}


########################################################################################
#
# ascidian-related characteristics:
# - key correspondences
# - morphonet type
#
########################################################################################

_ascidian_keys = {'init_naming_neighborhood_assessment': {'output_key': 'init_naming_neighborhood_assessment',
                                                          'input_keys': ['init_naming_neighborhood_assessment',
                                                                         'morphonet_float_init_naming_neighborhood_assessment']},
                  'init_naming_neighborhood_assessment_quality': {
                      'output_key': 'init_naming_neighborhood_assessment_quality',
                      'input_keys': ['init_naming_neighborhood_assessment_quality',
                                     'morphonet_float_init_naming_neighborhood_assessment_quality']},
                  'leave_one_out_errors': {'output_key': 'leave_one_out_errors',
                                           'input_keys': ['leave_one_out_errors',
                                                          'morphonet_label_leave_one_out_errors',
                                                          'morphonet_selection_leave_one_out_errors']},
                  'name_choice_agreement': {'output_key': 'name_choice_agreement',
                                            'input_keys': ['morphonet_float_name_choice_agreement',
                                                           'name_choice_agreement']},
                  'name_choice_min_distance': {'output_key': 'name_choice_min_distance',
                                               'input_keys': ['morphonet_float_name_choice_min_distance',
                                                              'name_choice_min_distance']},
                  'name_choice_min_distance_homogeneize': {'output_key': 'name_choice_min_distance_homogeneize',
                                               'input_keys': ['morphonet_float_name_choice_min_distance_homogeneize',
                                                              'name_choice_min_distance_homogeneize']},
                  'tissuefate_map' : {'output_key': 'tissuefate_map',
                                      'input_keys': ['tissuefate_map', 'morphonet_selection_fate_map',
                                                     'morphonet_label_tissuefate_map',
                                                     'morphonet_label_tissuefate_lemaire_2009',
                                                     'morphonet_selection_tissuefate_lemaire_2009',
                                                     'morphonet_selection_tissuefate_guignard_2020']},
                  'unequal_division_errors': {'output_key': 'unequal_division_errors',
                                              'input_keys': ['morphonet_label_unequal_division_errors',
                                                             'morphonet_selection_unequal_division_errors',
                                                             'unequal_division_errors']}}


_ascidian_mn_type = {'init_naming_neighborhood_assessment': 'float',
                     'init_naming_neighborhood_assessment_quality': 'float',
                     'leave_one_out_errors': 'label',
                     'name_choice_agreement': 'float',
                     'name_choice_min_distance': 'float',
                     'name_choice_min_distance_homogeneize': 'float',
                     'tissuefate_map': 'label',
                     'unequal_division_errors': 'label'}


keydictionary = {**_astec_keys, **_urchin_keys, **_ascidian_keys}


mn_typedictionary = {**_astec_mn_type, **_ascidian_mn_type}


#
# for documentation/help purposes
# list all recognized keys into a string
#
def recognized_features():
    ret = ""
    ret += "\n [Astec features]: "
    for i, k in enumerate(_astec_keys):
        ret += "'{:s}'".format(k)
        if i < len(_astec_keys) - 1:
            ret += "; "
    ret += "\n [Urchin features]: "
    for i, k in enumerate(_urchin_keys):
        ret += "'{:s}'".format(k)
        if i < len(_urchin_keys) - 1:
            ret += "; "
    ret += "\n [Ascidian features]: "
    for i, k in enumerate(_ascidian_keys):
        ret += "'{:s}'".format(k)
        if i < len(_ascidian_keys) - 1:
            ret += "; "
    ret += "\n"
    return ret


#
#
#

def _get_key_from_input_key(input_key, reverse_dict=None):
    proc = "_get_key_from_input_key"

    if input_key is None:
        return None
    if not isinstance(input_key, str):
        msg = ": unexpected type for 'input_key' (" + str(type(input_key)) + "), will ignore it"
        monitoring.to_log_and_console(str(proc) + msg, 1)
        return None

    if reverse_dict is None:
        reverse_dict = {i: k for k in keydictionary for i in keydictionary[k]['input_keys']}
    if input_key in reverse_dict:
        return reverse_dict[input_key]

    msg = "   ... feature '" + str(input_key) + "' is not recognized or handled, skip it."
    monitoring.to_log_and_console(msg, 1)
    return None


def _get_keys_from_input_keys(input_keylist):
    proc = "_get_keys_from_input_keys"

    if input_keylist is None:
        return []
    if isinstance(input_keylist, list) and len(input_keylist) == 0:
        return []
    if not isinstance(input_keylist, list):
        msg = ": unexpected type for 'input_keylist' (" + str(type(input_keylist)) + "), will ignore it"
        monitoring.to_log_and_console(str(proc) + msg, 1)
        return []

    output_keylist = []
    reverse_dict = {i: k for k in keydictionary for i in keydictionary[k]['input_keys']}
    for k in input_keylist:
        r = _get_key_from_input_key(k, reverse_dict=reverse_dict)
        if r is not None:
            output_keylist += [r]
    return list(set(output_keylist))


########################################################################################
#
#
#
########################################################################################


def _normalize_dictionary_keys(inputdict):
    """

    :param inputdict:
    :return:
    """

    if inputdict == {}:
        return {}

    outputdict = {}

    for inputkey in inputdict:
        foundkey = False
        for k in keydictionary:
            # print "       compare '" + str(tmpkey) + "' with '" + str(k) + "'"
            if inputkey in keydictionary[k]['input_keys']:
                outputkey = keydictionary[k]['output_key']
                # monitoring.to_log_and_console("   ... recognized key '" + str(outputkey) + "'", 4)
                #
                # update if key already exists, else just create the dictionary entry
                #
                outputdict[outputkey] = inputdict[inputkey]
                foundkey = True
                break

        if foundkey is False:
            outputdict[inputkey] = inputdict[inputkey]

    return outputdict


def get_dictionary_entry(inputdict, keystring):
    proc = 'get_dictionary_entry'
    if keystring not in keydictionary:
        msg = "keystring '" + str(keystring) + "' not in keydictionary keys"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
        monitoring.to_log_and_console("\t list of keys is " + str(list(keydictionary.keys())))
        return {}
    for k in keydictionary[keystring]['input_keys']:
        if k in inputdict:
            return inputdict[k]
    else:
        monitoring.to_log_and_console(str(proc) + ": '" + str(keystring) + "' was not found in input dictionary", 1)
        monitoring.to_log_and_console("    keys were: " + str(list(inputdict.keys())), 1)
        return {}


########################################################################################
#
# to translate a dictionary into XML
#
########################################################################################

#
# from stackoverflow.com
# questions/3095434/inserting-newlines-in-xml-file-generated-via-xml-etree-elementtree-in-python
#
#
# used for pretty printing
#


def _indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            _indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


########################################################################################
#
# types
# 'lineage':  'lineage_tree' :
#     dictionary de liste de int
#     lineage_tree.590002 = <type 'list'>
# 'h_min' : 'cell_h_min' :
# 'volume' : 'cell_volume' :
#     dictionary de int
#     cell_volume.590002 = <type 'int'>
#     590002: 236936
# 'sigma': 'cell_sigma':
# 'label_in_time': 'cell_labels_in_time'
#     dictionary de liste de numpy.int64
#     cell_labels_in_time.1 = <type 'list'>
#     1: [10002, 10003, 10004, ..., 10082]
# 'barycenter': 'cell_barycenter'
#     dictionary de numpy.ndarray de numpy.float64
#     cell_barycenter.590002 = <type 'numpy.ndarray'>
#     590002: array([ 258.41037242,  226.74975943,  303.67167927])
# 'fate': 'cell_fate'
#     dictionary de str
#     cell_fate.590002 = <type 'str'>
#     590002: 'Mesoderm Notochord 1'
# 'all-cells': 'all_cells'  # liste de toutes les cellules ?
#     liste de numpy.int64
#     all_cells = <type 'list'>
# 'principal-value': 'cell_principal_values'
#     dictionary de liste de numpy.float64
#     cell_principal_values.590002 = <type 'list'>
#     590002: [1526.0489371146978, 230.60881177650205, 91.063513300019849]
# 'name': 'cell_name'
#     dictionary de str
#     cell_name.590002 = <type 'str'>
#     590002: 'a9.0012_'
# 'contact': 'cell_contact_surface',
#     dictionary de dictionary de int
#     cell_contact_surface.590002.590019 = <type 'int'>
#     590002: {590001: 1808, 590003: 1436, 590004: 5012, ..., 590225: 2579}
# 'history': 'cell_history'
#     dictionary de numpy.ndarray de numpy.int64
#     cell_history.590002 = <type 'numpy.ndarray'>
#     590002: array([510002, 520002, 530002, 540002, 550002, 560002, 570002, 580002,
#         590002, 600002, 610002, 620002, 630002, 640002, 650002, 660002,
#         670002, 680002, 690002, 700002, 710002, 720002, 730002, 740002,
#         750002, 760002, 770002, 780002, 790002, 800002, 810002, 820002,
#         830002, 840002, 850002, 860002, 870002, 880002])
# 'principal-vector': 'cell_principal_vectors'    # liste de numpy.ndarray
#     dictionary de liste de numpy.ndarray de numpy.float64
#     cell_principal_vectors.590002 = <type 'list'>
#     590002: [array([ 0.17420991, -0.74923203,  0.63898534]),
#         array([-0.24877611,  0.59437038,  0.7647446 ]),
#         array([ 0.95276511,  0.29219037,  0.08284582])]
#
########################################################################################

def _list_to_text(value):
    proc = "_list_to_text"

    #
    # list of numbers
    #
    if isinstance(value[0], (int, float, np.int64, np.float64)):
        # element.text = str(value)
        # GM: why sorting
        # value.sort()
        return repr(value)

    #
    # list of strings
    #
    elif isinstance(value[0], str):
        value.sort()
        text = "["
        for i in range(len(value)):
            text += "'" + value[i] + "'"
            if i < len(value) - 1:
                text += ", "
                if i > 0 and i % 5 == 0:
                    text += "\n  "
        text += "]"
        return text
    #
    # 'principal-vector' case
    #  liste de numpy.ndarray de numpy.float64
    #
    elif isinstance(value[0], np.ndarray):
        text = "["
        for i in range(len(value)):
            if not isinstance(value[i], np.ndarray):
                msg = "weird, element #" + str(i) + " is not a np.ndarray, but should be. Skip it."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            if value[i].ndim == 0:
                msg = "weird, element #" + str(i) + " has no dimension. Skip it."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            # text += str(list(value[i]))
            text += repr(list(value[i]))
            if i < len(value) - 1:
                text += ", "
                if i > 0 and i % 10 == 0:
                    text += "\n  "
        text += "]"
        return text

    elif isinstance(value[0], list):
        text = "["
        for i in range(len(value)):
            # text += str(list(value[i]))
            text += _list_to_text(value[i])
            if i < len(value) - 1:
                text += ", "
                if i > 0 and i % 5 == 0:
                    text += "\n  "
        text += "]"
        return text
    else:
        monitoring.to_log_and_console(proc + ": error, element list type ('" + str(type(value[0]))
                                      + "') not handled yet")
    return ""


def _set_xml_element_text(element, value):
    """

    :param element:
    :param value:
    :return:
    """
    proc = "_set_xml_element_text"

    #
    # dictionary : recursive call
    #   dictionary element may be list, int, numpy.ndarray, str
    # list : may be int, numpy.int64, numpy.float64, numpy.ndarray
    #

    if type(value) == dict:
        # print proc + ": type is dict"
        keylist = list(value.keys())
        keylist.sort()
        for k in keylist:
            _dict2xml(element, k, value[k])

    elif type(value) == list:

        #
        # empty list
        #

        if len(value) == 0:
            element.text = repr(value)
        #
        # 'lineage', 'label_in_time', 'all-cells', 'principal-value'
        #

        else:
            element.text = _list_to_text(value)

    #
    # 'barycenter', 'cell_history'
    #
    elif type(value) == np.ndarray:
        # element.text = str(list(value))
        element.text = repr(list(value))

    #
    # 'volume', 'contact'
    #
    elif type(value) in (int, float, np.int64, np.float64):
        # element.text = str(value)
        element.text = repr(value)

    #
    # 'fate', 'name'
    #
    elif type(value) == str:
        element.text = repr(value)

    else:
        monitoring.to_log_and_console(proc + ": element type '" + str(type(value))
                                      + "' not handled yet, uncomplete translation")


def _dict2xml(parent, tag, value):
    """

    :param parent:
    :param tag:
    :param value:
    :return:
    """
    proc = "_dict2xml"
    #
    # integers can not be XML tags
    #
    if type(tag) in (int, np.int64):
        child = ElementTree.Element('cell', attrib={'cell-id': str(tag)})
    else:
        monitoring.to_log_and_console("   ... " + proc + ": process tag = '" + str(tag) + "'", 3)
        name = _get_key_from_input_key(tag)
        mn_type = None
        if name in _ascidian_mn_type:
            mn_type = _ascidian_mn_type[name]
        if mn_type is not None:
            msg = "field of name '" + str(name) + "' and morphonet type '" + str(mn_type) + "'"
            monitoring.to_log_and_console("                  " + msg, 3)
            child = ElementTree.Element(name, attrib={'mn_type': str(mn_type)})
        else:
            msg = "field of name '" + str(name) + "'"
            monitoring.to_log_and_console("                  " + msg, 3)
            child = ElementTree.Element(str(tag))

    _set_xml_element_text(child, value)

    parent.append(child)
    return parent


#
# procedure d'appel et creation de la racine
#


def dict2xml(dictionary, defaultroottag='data'):
    """

    :param dictionary:
    :param defaultroottag:
    :return:
    """

    proc = "dict2xml"

    if type(dictionary) is not dict:
        monitoring.to_log_and_console(proc + ": error, input is of type '" + str(type(dictionary)) + "'")
        return None

    #
    # s'il n'y a qu'un seul element dans le dictionnaire, on appelle la racine
    # d'apres celui-ci (eg lineage_tree), sinon on cree une racine qui contient
    # tous les elements
    #

    # if len(dictionary) == 1:
    #    roottag = list(dictionary.keys())[0]
    #    root = ElementTree.Element(roottag)
    #    _set_xml_element_text(root, dictionary[roottag])
    # elif len(dictionary) > 1:

    if len(dictionary) >= 1:

        root = ElementTree.Element(defaultroottag)
        for k, v in dictionary.items():
            _dict2xml(root, k, v)

    else:
        monitoring.to_log_and_console(proc + ": error, empty dictionary ?!")
        return None

    _indent(root)
    tree = ElementTree.ElementTree(root)

    return tree


########################################################################################
#
# to translate a XML tree into dictionary
#
########################################################################################


def _set_dictionary_value(root):
    """

    :param root:
    :return:
    """

    if len(root) == 0:

        #
        # pas de branche, on renvoie la valeur
        #

        # return ast.literal_eval(root.text)
        if root.text is None:
            return None
        else:
            return eval(root.text)

    else:

        dictionary = {}

        for child in root:

            # print("child.tag=" + str(child.tag))
            # print "len(child)=" + str(len(child))
            # print "child.text=" + str(child.text)

            key = child.tag
            if child.tag == 'cell':
                # do not cast into numpy int64, it seems to cause a mess since
                # the 2.0 version of numpy
                # key = np.int64(child.attrib['cell-id'])
                key = int(child.attrib['cell-id'])
            elif 'mn_type' in child.attrib:
                key = 'morphonet_' + child.attrib['mn_type'] + "_" + str(child.tag)
            dictionary[key] = _set_dictionary_value(child)

    return dictionary


def xml2dict(tree):
    """

    :param tree:
    :return:
    """

    proc = "xml2dict"

    root = tree.getroot()

    dictionary = {}

    for k, v in keydictionary.items():

        if root.tag == v['output_key']:
            monitoring.to_log_and_console("   ... " + proc + ": process root.tag = '" + str(root.tag) + "'", 3)
            dictionary[str(root.tag)] = _set_dictionary_value(root)
            break
    else:
        for child in root:
            monitoring.to_log_and_console("   ... " + proc + ": process child.tag = '" + str(child.tag) + "'", 3)
            value = _set_dictionary_value(child)
            if value is None:
                monitoring.to_log_and_console("       " + proc + ": empty property '" + str(child.tag) + "' ?! "
                                              + " ... skip it", 1)
            else:
                key = child.tag
                if child.tag == 'cell':
                    # do not cast into numpy int64, it seems to cause a mess since
                    # the 2.0 version of numpy
                    # key = np.int64(child.attrib['cell-id'])
                    key = int(child.attrib['cell-id'])
                elif 'mn_type' in child.attrib:
                    key = 'morphonet_' + child.attrib['mn_type'] + "_" + str(child.tag)
                dictionary[key] = value

    return dictionary


########################################################################################
#
# to read a set of files into a dictionary
#
########################################################################################


#
# update dictionary from what has been read
#

def _update_read_dictionary(propertiesdict, tmpdict, filename, verbose=True):
    """

    :param propertiesdict:
    :param tmpdict:
    :return:
    """
    proc = "_update_read_dictionary"
    unknownkeys = []

    for tmpkey in tmpdict:
        foundkey = False

        for k in keydictionary:
            # print "       compare '" + str(tmpkey) + "' with '" + str(k) + "'"
            if tmpkey in keydictionary[k]['input_keys']:
                outputkey = keydictionary[k]['output_key']
                if verbose:
                    monitoring.to_log_and_console("   ... recognized key '" + str(outputkey) + "'", 2)
                #
                # update if key already exists, else just create the dictionary entry
                #
                if outputkey in propertiesdict:
                    if type(propertiesdict[outputkey]) is dict and type(tmpdict[tmpkey]) is dict:
                        propertiesdict[outputkey].update(tmpdict[tmpkey])
                    elif type(propertiesdict[outputkey]) is list and type(tmpdict[tmpkey]) is list:
                        propertiesdict[outputkey] += tmpdict[tmpkey]
                    else:
                        monitoring.to_log_and_console(proc + ": error, can not update property '" + str(outputkey)
                                                      + "'")
                else:
                    propertiesdict[outputkey] = tmpdict[tmpkey]
                foundkey = True
                break

        if foundkey is False:
            unknownkeys.append(tmpkey)

    if len(unknownkeys) > 0 and len(unknownkeys) == len(list(tmpdict.keys())):
        #
        # no key was found
        # it is assumed it's a lineage tree: add some test here ?
        #
        monitoring.to_log_and_console("   ... assume '" + str(filename) + "' is a lineage", 1)
        outputkey = keydictionary['lineage']['output_key']
        if outputkey in propertiesdict:
            if type(propertiesdict[outputkey]) is dict and type(tmpdict) is dict:
                propertiesdict[outputkey].update(tmpdict)
            else:
                monitoring.to_log_and_console(proc + ": error, can not update property '" + str(outputkey) + "'")
        else:
            propertiesdict[outputkey] = tmpdict

    elif len(unknownkeys) > 0:
        #
        # some unknown keys were found
        #
        monitoring.to_log_and_console("   ... unrecognized key(s) are '" + str(unknownkeys) + "'", 1)

        # previous behavior: use keydictionary['unknown']['output_key'] as key
        # for *one* unknown property
        #
        #  outputkey = keydictionary['unknown']['output_key']
        # if len(unknownkeys) == 1:
        #     tmpkey = unknownkeys[0]
        #     if outputkey in propertiesdict:
        #         if type(propertiesdict[outputkey]) is dict and type(tmpdict[tmpkey]) is dict:
        #             propertiesdict[outputkey].update(tmpdict[tmpkey])
        #         elif type(propertiesdict[outputkey]) is list and type(tmpdict[tmpkey]) is list:
        #             propertiesdict[outputkey] += tmpdict[tmpkey]
        #         else:
        #             monitoring.to_log_and_console(proc + ": error, can not update property '" + str(outputkey)
        #                                           + "'")
        #     else:
        #         propertiesdict[outputkey] = tmpdict[tmpkey]
        # else:
        #     monitoring.to_log_and_console(proc + ": error, can not update many unknown properties")

        #
        # use unknown keys as such
        #
        for k in unknownkeys:
            propertiesdict[k] = tmpdict[k]

    return propertiesdict


#
# legacy from historal astec version
# types issued from the reading of xml files may be erroneous
# fix it
#

def _convert_to_nparray(propertiesdict):
    """

    :param propertiesdict:
    :return:
    """

    if propertiesdict == {}:
        return {}

    if 'cell_barycenter' in propertiesdict:
        do_translation = False
        for c in propertiesdict['cell_barycenter']:
            if type(propertiesdict['cell_barycenter'][c]) == list:
                do_translation = True
            break
        msg = "   ... type translation of 'cell_barycenter'"
        if do_translation is True:
            monitoring.to_log_and_console(msg + " from list to np.ndarray", 3)
            #
            # translate list of float to np.array of np.float64
            #
            for c in propertiesdict['cell_barycenter']:
                propertiesdict['cell_barycenter'][c] = np.array(propertiesdict['cell_barycenter'][c])
        else:
            monitoring.to_log_and_console(msg + " already done", 3)

    if 'cell_principal_vectors' in propertiesdict:
        do_translation = False
        for c in propertiesdict['cell_principal_vectors']:
            for v in range(len(propertiesdict['cell_principal_vectors'][c])):
                if type(propertiesdict['cell_principal_vectors'][c][v]) == list:
                    do_translation = True
                break
            break
        msg = "   ... type translation of 'cell_principal_vectors'"
        if do_translation is True:
            monitoring.to_log_and_console(msg + " from list to np.ndarray", 3)
            #
            # translate list of list of float to a list of np.array of np.float64
            #
            for c in propertiesdict['cell_principal_vectors']:
                for v in range(len(propertiesdict['cell_principal_vectors'][c])):
                    propertiesdict['cell_principal_vectors'][c][v] = \
                        np.array(propertiesdict['cell_principal_vectors'][c][v])
        else:
            monitoring.to_log_and_console(msg + " already done", 3)

    return propertiesdict


def _convert_from_nparray(propertiesdict):
    """

    :param propertiesdict:
    :return:
    """

    if propertiesdict == {}:
        return {}

    if 'cell_barycenter' in propertiesdict:
        do_translation = False
        for c in propertiesdict['cell_barycenter']:
            if type(propertiesdict['cell_barycenter'][c]) == np.ndarray:
                do_translation = True
            break
        msg = "   ... type translation of 'cell_barycenter'"
        if do_translation is True:
            monitoring.to_log_and_console(msg + " from np.ndarray to list", 3)
            #
            # translate list of float to np.array of np.float64
            #
            for c in propertiesdict['cell_barycenter']:
                propertiesdict['cell_barycenter'][c] = propertiesdict['cell_barycenter'][c].astype(float).tolist()
        else:
            monitoring.to_log_and_console(msg + " already done", 3)

    if 'cell_principal_vectors' in propertiesdict:
        do_translation = False
        for c in propertiesdict['cell_principal_vectors']:
            for v in range(len(propertiesdict['cell_principal_vectors'][c])):
                if type(propertiesdict['cell_principal_vectors'][c][v]) == np.ndarray:
                    do_translation = True
                break
            break
        msg = "   ... type translation of 'cell_principal_vectors'"
        if do_translation is True:
            monitoring.to_log_and_console(msg + " from np.ndarray to list", 3)
            #
            # translate list of list of float to a list of np.array of np.float64
            #
            for c in propertiesdict['cell_principal_vectors']:
                for v in range(len(propertiesdict['cell_principal_vectors'][c])):
                    propertiesdict['cell_principal_vectors'][c][v] = \
                        propertiesdict['cell_principal_vectors'][c][v].astype(float).tolist()
        else:
            monitoring.to_log_and_console(msg + " already done", 3)

    return propertiesdict


def _read_xml_file(filename, propertiesdict, verbose=True):
    monitoring.to_log_and_console("... reading '" + str(filename) + "'", 1)
    inputxmltree = ElementTree.parse(filename)
    tmpdict = xml2dict(inputxmltree)
    propertiesdict = _update_read_dictionary(propertiesdict, tmpdict, filename, verbose=verbose)
    del tmpdict
    return propertiesdict


def _read_pkl_file(filename, propertiesdict,verbose=True):
    monitoring.to_log_and_console("... reading '" + str(filename) + "'", 1)
    inputfile = open(filename, 'rb')
    tmpdict = pkl.load(inputfile)
    inputfile.close()
    propertiesdict = _update_read_dictionary(propertiesdict, tmpdict, filename, verbose=verbose)
    del tmpdict
    return propertiesdict


#
# generic I/O procedures
#

def read_dictionary(inputfilenames, inputpropertiesdict=None, verbose=True):
    """

    :param inputfilenames:
    :param inputpropertiesdict:
    :return:
    """
    proc = 'read_dictionary'

    if inputfilenames is None:
        monitoring.to_log_and_console(proc + ": error, no input files")
        return {}

    if isinstance(inputpropertiesdict, dict):
        propertiesdict = inputpropertiesdict
    else:
        propertiesdict = {}

    #
    #
    #

    if type(inputfilenames) == str:
        if not os.path.isfile(inputfilenames):
            monitoring.to_log_and_console(proc + ": error, file '" + str(inputfilenames) + "' does not exist")
            return propertiesdict

        if inputfilenames.endswith("xml") is True:
            propertiesdict = _read_xml_file(inputfilenames, propertiesdict, verbose=verbose)
            propertiesdict = _convert_to_nparray(propertiesdict)
        elif inputfilenames.endswith("pkl") is True:
            propertiesdict = _read_pkl_file(inputfilenames, propertiesdict, verbose=verbose)
        else:
            monitoring.to_log_and_console(proc + ": error: extension not recognized for '" + str(inputfilenames) + "'")

        propertiesdict = _normalize_dictionary_keys(propertiesdict)
        return propertiesdict

    #
    # here, we assume type(inputfilenames) == list
    #

    #
    # read xml files
    #

    for filename in inputfilenames:

        if not os.path.isfile(filename):
            monitoring.to_log_and_console(proc + ": error, file '" + str(filename) + "' does not exist")
            continue

        if filename.endswith("xml") is True:
            propertiesdict = _read_xml_file(filename, propertiesdict)

    #
    # translation of xml may take place here
    #

    propertiesdict = _convert_to_nparray(propertiesdict)

    #
    # read pkl files
    #

    for filename in inputfilenames:

        if not os.path.isfile(filename):
            monitoring.to_log_and_console(proc + ": error, file '" + str(filename) + "' does not exist")
            continue

        if filename.endswith("pkl") is True:
            propertiesdict = _read_pkl_file(filename, propertiesdict)

    #
    #
    #

    for filename in inputfilenames:
        if filename[len(filename) - 3:len(filename)] == "xml":
            continue
        elif filename[len(filename) - 3:len(filename)] == "pkl":
            continue
        else:
            monitoring.to_log_and_console(proc + ": error: extension not recognized for '" + str(filename) + "'")

    propertiesdict = _normalize_dictionary_keys(propertiesdict)
    return propertiesdict


def write_dictionary(filename, inputpropertiesdict, verbose=True):
    """

    :param filename:
    :param inputpropertiesdict:
    :return:
    """
    proc = 'write_dictionary'

    if verbose is True:
        monitoring.to_log_and_console("... writing '" + str(filename) + "'", 1)

    inputpropertiesdict = _convert_from_nparray(inputpropertiesdict)

    if filename.endswith("pkl") is True:
        lineagefile = open(filename, 'wb')
        pkl.dump(inputpropertiesdict, lineagefile)
        lineagefile.close()
    elif filename.endswith("xml") is True:
        xmltree = dict2xml(inputpropertiesdict)
        xmltree.write(filename)
        del xmltree
    elif filename.endswith("tlp") is True:
        write_tlp_file(filename, inputpropertiesdict)
    else:
        msg = "error when writing lineage file. Extension not recognized for '" + os.path.basename(filename) + "'"
        monitoring.to_log_and_console(str(proc) + ": " + msg, 1)
    return


########################################################################################
#
# morphonet writing procedure
#
########################################################################################


def _write_morphonet_dict(f, property, key, time_digits_for_cell_id=4):
    proc = "_write_morphonet_dict"
    div = int(10 ** time_digits_for_cell_id)
    for c in property[key]:
        #
        # object tuple (OTP): t, id, ch,
        # specifying the time point (t),
        # the visualisation channel (ch) [can be skipped]
        # and the id of the specific object given in the obj file (id).
        #
        otp = "{:d}".format(int(c) // div) + ",{:d}".format(int(c) % div) + ":"
        if not isinstance(property[key][c], dict):
            f.write(otp + str(property[key][c]) + "\n")
            msg = ": element type'" + str(type(property[key][c])) + "'"
            msg += "(key = '" + str(key) + "')"
            msg += " not handled yet. Skip it."
            monitoring.to_log_and_console(proc + msg)
            continue
        for d in property[key][c]:
            otp2 = "{:d}".format(int(d) // div) + ",{:d}".format(int(d) % div) + ":"
            if isinstance(property[key][c][d], (float, np.float64, int, np.int64)):
                f.write(otp + otp2 + str(property[key][c][d]) + "\n")
            elif isinstance(property[key][c][d], (list, np.ndarray)):
                for v in property[key][c][d]:
                    if isinstance(v, (float, np.float64, int, np.int64)):
                        f.write(otp + str(v) + "\n")
                    else:
                        msg = ": list element type'" + str(type(v)) + "'"
                        msg += "(key = '" + str(key) + "')"
                        msg += " not handled yet. Skip it."
                        monitoring.to_log_and_console(proc + msg)
            else:
                msg = ": list element type'" + str(type(property[key][c][d])) + "'"
                msg += "(key = '" + str(key) + "')"
                msg += " not handled yet. Skip it."
                monitoring.to_log_and_console(proc + msg)
    return


def _write_morphonet_float(f, property, key, time_digits_for_cell_id=4):
    proc = "_write_morphonet_float"
    div = int(10 ** time_digits_for_cell_id)
    for c in property[key]:
        #
        # object tuple (OTP): t, id, ch,
        # specifying the time point (t),
        # the visualisation channel (ch) [can be skipped]
        # and the id of the specific object given in the obj file (id).
        #
        otp = "{:d}".format(int(c) // div) + ",{:d}".format(int(c) % div) + ":"
        if isinstance(property[key][c], (float, np.float64, int, np.int64)):
            f.write(otp + str(property[key][c]) + "\n")
        elif isinstance(property[key][c], (list, np.ndarray)):
            for v in property[key][c]:
                if isinstance(v, (float, np.float64, int, np.int64)):
                    f.write(otp + str(v) + "\n")
                else:
                    msg = ": list element type'" + str(type(v)) + "'"
                    msg += "(key = '" + str(key) + "')"
                    msg += " not handled yet. Skip it."
                    monitoring.to_log_and_console(proc + msg)
        else:
            msg = ": element type'" + str(type(property[key][c])) + "'"
            msg += "(key = '" + str(key) + "')"
            msg += " not handled yet. Skip it."
            monitoring.to_log_and_console(proc + msg)
    return


def _write_morphonet_label(f, property, key, time_digits_for_cell_id=4):
    proc = "_write_morphonet_label"
    div = int(10 ** time_digits_for_cell_id)
    for c in property[key]:
        #
        # object tuple (OTP): t, id, ch,
        # specifying the time point (t),
        # the visualisation channel (ch) [can be skipped]
        # and the id of the specific object given in the obj file (id).
        #
        otp = "{:d}".format(int(c) // div) + ",{:d}".format(int(c) % div) + ":"
        if isinstance(property[key][c], (int, np.int64)):
            f.write(otp + str(property[key][c]) + "\n")
        elif isinstance(property[key][c], (list, np.ndarray)):
            for v in property[key][c]:
                if isinstance(v, (int, np.int64)):
                    f.write(otp + str(v) + "\n")
                else:
                    msg = ": list element type'" + str(type(v)) + "'"
                    msg += "(key = '" + str(key) + "')"
                    msg += " not handled yet. Skip it."
                    monitoring.to_log_and_console(proc + msg)
        else:
            msg = ": element type'" + str(type(property[key][c])) + "'"
            msg += "(key = '" + str(key) + "')"
            msg += " not handled yet. Skip it."
            monitoring.to_log_and_console(proc + msg)
    return


def _write_morphonet_string(f, property, key, time_digits_for_cell_id=4):
    proc = "_write_morphonet_string"
    div = int(10 ** time_digits_for_cell_id)
    for c in property[key]:
        #
        # object tuple (OTP): t, id, ch,
        # specifying the time point (t),
        # the visualisation channel (ch) [can be skipped]
        # and the id of the specific object given in the obj file (id).
        #
        otp = "{:d}".format(int(c) // div) + ",{:d}".format(int(c) % div) + ":"
        if isinstance(property[key][c], str):
            f.write(otp + str(property[key][c]) + "\n")
        elif isinstance(property[key][c], (list, np.ndarray)):
            for v in property[key][c]:
                if isinstance(v, str):
                    f.write(otp + str(v) + "\n")
                else:
                    msg = ": list element type'" + str(type(v)) + "'"
                    msg += "(key = '" + str(key) + "')"
                    msg += " not handled yet. Skip it."
                    monitoring.to_log_and_console(proc + msg)
        else:
            msg = ": element type'" + str(type(property[key][c])) + "'"
            msg += "(key = '" + str(key) + "')"
            msg += " not handled yet. Skip it."
            monitoring.to_log_and_console(proc + msg)
    return


def _write_morphonet_time(f, property, key, time_digits_for_cell_id=4):
    proc = "_write_morphonet_time"
    div = int(10 ** time_digits_for_cell_id)
    for c in property[key]:
        #
        # object tuple (OTP): t, id, ch,
        # specifying the time point (t),
        # the visualisation channel (ch) [can be skipped]
        # and the id of the specific object given in the obj file (id).
        #
        otp = "{:d}".format(int(c) // div) + ",{:d}".format(int(c) % div) + ":"
        if not isinstance(property[key][c], list):
            f.write(otp + str(property[key][c]) + "\n")
            msg = ": element type'" + str(type(property[key][c])) + "'"
            msg += "(key = '" + str(key) + "')"
            msg += " not handled yet. Skip it."
            monitoring.to_log_and_console(proc + msg)
            continue
        for d in property[key][c]:
            otp2 = "{:d}".format(int(d) // div) + ",{:d}".format(int(d) % div)
            f.write(otp + otp2 + "\n")
    return


def write_morphonet_file(d, output_features=None, directory=None, file_prefix=None, file_suffix=None,
                         time_digits_for_cell_id=4):
    proc = "write_morphonet_file"

    #
    # get the features to printed out
    #
    key_list = _get_keys_from_input_keys(output_features)
    if len(key_list) == 0:
        #
        # get all keys from the files
        # intersect with all recognized morphonet types
        #
        key_list = set(_get_keys_from_input_keys(list(d.keys())))
        key_list = list(set(key_list) & set(mn_typedictionary.keys()))

    #
    #
    #
    for feature in key_list:
        d_key = None
        for searchedkey in keydictionary[feature]['input_keys']:
            if searchedkey in d:
                d_key = searchedkey
                break
        if d_key is None:
            msg = "do not find feature '" + str(feature) + "' in property dictionary. Will not write it."
            monitoring.to_log_and_console(proc + ": " + msg, 1)
            continue
        d_type = mn_typedictionary[feature]

        #
        # here, we have d[d_key] to write out with morphonet d_type
        # build the file name
        #
        filename = str(keydictionary[feature]['output_key'])
        if file_prefix is not None:
            if isinstance(file_prefix, str) and len(file_prefix) > 0:
                filename = file_prefix + filename
        if file_suffix is not None:
            if isinstance(file_suffix, str) and len(file_suffix) > 0:
                filename += file_suffix
        filename += '.txt'
        if directory is not None and isinstance(directory, str):
            if not os.path.isdir(directory):
                if not os.path.exists(directory):
                    os.makedirs(directory)
                else:
                    monitoring.to_log_and_console(proc + ": '" + str(directory) + "' is not a directory ?!")
            if os.path.isdir(directory):
                filename = os.path.join(directory, filename)

        if monitoring.debug > 0:
            msg = "writing '" + str(feature) + "' into morphonet file '" + filename + "'"
            monitoring.to_log_and_console("... " + msg, 1)

        #
        # write (see utils.datadir.py)
        #
        f = open(filename, "w")
        f.write("#\n")
        f.write("# command line: '" + " ".join(sys.argv) + "'\n")
        f.write("# executed at " + time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + "\n")
        f.write("# by '" + str(getpass.getuser()) + "'\n")
        f.write("#\n")
        f.write("# " + str(d_key) + "\n")
        f.write("#\n")
        f.write("type:{:s}\n".format(d_type))

        #
        #
        #
        if d_type == 'dict':
            _write_morphonet_dict(f, d, d_key, time_digits_for_cell_id=time_digits_for_cell_id)
        elif d_type == 'float':
            _write_morphonet_float(f, d, d_key, time_digits_for_cell_id=time_digits_for_cell_id)
        elif d_type == 'label':
            _write_morphonet_label(f, d, d_key, time_digits_for_cell_id=time_digits_for_cell_id)
        elif d_type == 'string':
            _write_morphonet_string(f, d, d_key, time_digits_for_cell_id=time_digits_for_cell_id)
        elif d_type == 'time':
            _write_morphonet_time(f, d, d_key, time_digits_for_cell_id=time_digits_for_cell_id)
        else:
            msg = "morphonet type'" + str(d_type) + "'"
            msg += "(feature = '" + str(d_key) + "')"
            msg += " not handled yet. Skip it."
            f.write("# " + msg + "\n")
            monitoring.to_log_and_console(proc + ": " + msg)
        f.close()


########################################################################################
#
# write tlp file
# this was inspired from pkl2tlp from L. Guignard
#
########################################################################################

def write_tlp_file(tlpfilename, dictionary):
    """

    :param tlpfilename:
    :param dictionary:
    :return:
    """

    proc = "write_tlp_file"

    #
    # is there a lineage
    #
    if keydictionary['lineage']['output_key'] in dictionary:
        lineage = dictionary[keydictionary['lineage']['output_key']]
    else:
        monitoring.to_log_and_console(proc + ": no lineage was found.")
        return

    #
    # open file
    #
    f = open(tlpfilename, "w")
    f.write("(tlp \"2.0\"\n")

    #
    # write nodes = lineage.keys() + lineage.values()
    #
    nodes = set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values]))
    f.write("(nodes ")
    for n in nodes:
        f.write(str(n) + " ")
    f.write(")\n")

    #
    # write edges
    #
    count_edges = 0
    for m, ds in lineage.items():
        count_edges += 1
        for d in ds:
            f.write("(edge " + str(count_edges) + " " + str(m) + " " + str(d) + ")\n")

    #
    # write node ids
    #
    f.write("(property 0 int \"id\"\n")
    f.write("\t(default \"0\" \"0\")\n")
    for node in nodes:
        f.write("\t(node " + str(node) + str(" \"") + str(node) + "\")\n")
    f.write(")\n")

    #
    #
    #
    for p in dictionary:
        if p == keydictionary['lineage']['output_key']:
            pass
        elif p == keydictionary['all_cells']['output_key']:
            pass
        #
        # property as single double
        #
        elif p == keydictionary['volume']['output_key'] or p == keydictionary['surface']['output_key'] \
                or p == keydictionary['apicobasal_length']['output_key'] \
                or p == keydictionary['compactness']['output_key']:
            prop = dictionary[p]
            default = np.median(list(prop.values()))
            f.write("(property 0 double \"" + str(p) + "\"\n")
            f.write("\t(default \"" + str(default) + "\" \"0\")\n")
            for node in nodes:
                f.write("\t(node " + str(node) + str(" \"") + str(prop.get(node, default)) + "\")\n")
            f.write(")\n")
        #
        # property as string
        #
        elif p == keydictionary['tissue_fate']['output_key'] or p == keydictionary['name']['output_key']:
            prop = dictionary[p]
            f.write("(property 0 string \"" + str(p) + "\"\n")
            f.write("\t(default \"" + "no string" + "\" \"0\")\n")
            for node in nodes:
                f.write("\t(node " + str(node) + str(" \"") + str(prop.get(node, "no string")) + "\")\n")
            f.write(")\n")
        #
        #
        #
        elif p == keydictionary['h_min']['output_key'] or p == keydictionary['sigma']['output_key'] \
                or p == keydictionary['labels_in_time']['output_key'] \
                or p == keydictionary['barycenter']['output_key'] \
                or p == keydictionary['principal_values']['output_key'] \
                or p == keydictionary['contact_surface']['output_key'] \
                or p == keydictionary['history']['output_key'] \
                or p == keydictionary['principal_vectors']['output_key'] \
                or p == keydictionary['naming_score']['output_key']:
            pass
        else:
            monitoring.to_log_and_console(proc + ": property '" + str(p) + "' not handled yet for writing.")

    #
    # close file
    #
    f.write(")")
    f.write("(nodes ")

    f.close()
