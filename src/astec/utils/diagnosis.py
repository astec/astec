

from operator import itemgetter
import copy
import statistics
import os
import sys

import astec.utils.common as common
import astec.utils.ioproperties as ioproperties


#
#
#
#
#

monitoring = common.Monitoring()

########################################################################################
#
#
#
########################################################################################


class DiagnosisParameters(common.PrefixedParameter):

    def __init__(self, prefix=None):

        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        # parametres pour le diagnostic sur le volume
        #
        doc = "\t for diagnosis on cell volume. Threshold on cell volume. Snapshot cells\n"
        doc += "\t that have a volume below this threshold are displayed."
        self.doc['minimal_volume'] = doc
        self.minimal_volume = 0
        doc = "\t for diagnosis on cell volume. Threshold on volume variation along branches.\n"
        doc += "\t Branches that have a volume variation above this threshold are displayed.\n"
        doc += "\t The volume variation along a branch is calculated as\n"
        doc += "\t (max v(c) - min v(c)) / median v(c)\n"
        doc += "\t where v(c) is the volume of the cell c"
        self.doc['maximal_volume_variation'] = doc
        self.maximal_volume_variation = 0.4
        doc = "\t for diagnosis on cell volume. Threshold on volume derivative along branches.\n"
        doc += "\t Time points along branches that have a volume derivative above this threshold\n"
        doc += "\t are displayed. The volume derivative along a branch is calculated as\n"
        doc += "\t (v(c_{t+1}) - v(c_{t}))/v(c_{t}) where t denotes the successive acquisition\n"
        doc += "\t time points."
        self.doc['maximal_volume_derivative'] = doc
        self.maximal_volume_derivative = 0.3

        #
        # nombre d'items a imprimer
        #
        doc = "\t if strictly positif, number of items (ie cells) to be displayed in diagnosis.\n"
        self.doc['items'] = doc
        self.items = 10
        
        #
        # diagnostic pour le lignage
        #
        doc = "\t for diagnosis on lineage. Threshold on branch length. Branches that have a length\n"
        doc += "\t below this threshold are displayed."
        self.doc['minimal_length'] = doc
        self.minimal_length = 10

    def print_parameters(self):
        print("")
        print('#')
        print('# DiagnosisParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('minimal_volume', self.minimal_volume, self.doc.get('minimal_volume', None))
        self.varprint('maximal_volume_variation', self.maximal_volume_variation,
                      self.doc.get('maximal_volume_variation', None))
        self.varprint('maximal_volume_derivative', self.maximal_volume_derivative,
                      self.doc.get('maximal_volume_derivative', None))

        self.varprint('items', self.items, self.doc.get('items', None))

        self.varprint('minimal_length', self.minimal_length, self.doc.get('minimal_length', None))

        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# DiagnosisParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'minimal_volume', self.minimal_volume, self.doc.get('minimal_volume', None))
        self.varwrite(logfile, 'maximal_volume_variation', self.maximal_volume_variation,
                      self.doc.get('maximal_volume_variation', None))
        self.varwrite(logfile, 'maximal_volume_derivative', self.maximal_volume_derivative,
                      self.doc.get('maximal_volume_derivative', None))

        self.varwrite(logfile, 'items', self.items, self.doc.get('items', None))

        self.varwrite(logfile, 'minimal_length', self.minimal_length, self.doc.get('minimal_length', None))

        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    def update_from_parameters(self, parameters):

        self.minimal_volume = self.read_parameter(parameters, 'minimal_volume', self.minimal_volume)
        self.maximal_volume_variation = self.read_parameter(parameters, 'maximal_volume_variation',
                                                            self.maximal_volume_variation)
        self.maximal_volume_derivative = self.read_parameter(parameters, 'maximal_volume_derivative',
                                                             self.maximal_volume_derivative)

        self.items = self.read_parameter(parameters, 'items', self.items)

        self.minimal_length = self.read_parameter(parameters, 'minimal_length', self.minimal_length)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

    def update_from_args(self, args):
        if args.diagnosis_minimal_volume is not None:
            self.minimal_volume = int(args.diagnosis_minimal_volume)
        if args.diagnosis_items is not None:
            self.items = int(args.diagnosis_items)


############################################################
#
# for cell ids diagnosis
#
############################################################


def _get_nodes(prop, key):
    """

    Parameters
    ----------
    prop: property dictionary
    key: property for which the list of cell ids is required

    Returns
    -------
    The list of all cell ids for the given property, or None

    """
    proc = "_get_nodes"
    if key == 'cell_lineage' or key == 'cell_contact_surface':
        d = prop[key]
        nodes = list(set(d.keys()).union(set([v for values in list(d.values()) for v in values])))
        return nodes
    if key == 'cell_volume' or key == 'cell_surface' or key == 'cell_compactness' or key == 'cell_barycenter' or \
            key == 'cell_principal_values' or key == 'cell_name' or key == 'cell_principal_vectors':
        return list(prop[key].keys())
    if key == 'all_cells':
        return prop[key]
    if monitoring.debug > 0:
        monitoring.to_log_and_console(proc + ": property '" + str(key) + "' not handled yet")
    return None


def _find_node(prop, key, cell_id):

    keyname = None
    keynameset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keynameset) > 0:
        keyname = list(keynameset)[0]

    if key in ioproperties.keydictionary['lineage']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['h_min']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['volume']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['surface']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['compactness']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['sigma']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['label_in_time']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['barycenter']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['fate']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['all-cells']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['principal-value']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['name']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['contact']['input_keys']:
        if cell_id in prop[key]:
            msg = "    - " + str(cell_id)
            if keyname is not None:
                if cell_id in prop[keyname]:
                    msg += " (" + str(prop[keyname][cell_id]) + ")"
            msg += " is indexed in '" + str(key) + "' dictionary"
            monitoring.to_log_and_console(msg)
        cells = []
        for c in prop[key]:
            if cell_id in prop[key][c]:
                cells += [c]
        if len(cells) > 0:
            msg = "    - " + str(cell_id)
            msg += " is a contact cell of"
            for c in cells:
                msg += " " + str(c) + ""
                if keyname is not None:
                    if c in prop[keyname]:
                        msg += " (" + str(prop[keyname][c]) + ")"
                if len(cells) == 2 and cells.index(c) == 0:
                    msg += " and"
                elif len(cells) > 2:
                    if cells.index(c) <= len(cells) - 2:
                        msg += ","
                    if cells.index(c) == len(cells) - 2:
                        msg += " and"

            monitoring.to_log_and_console(msg)

    elif key in ioproperties.keydictionary['history']['input_keys']:
        pass
    elif key in ioproperties.keydictionary['principal-vector']['input_keys']:
        pass
    else:
        monitoring.to_log_and_console("    unknown key '" + str(key) + "' for diagnosis", 1)


############################################################
#
# for lineage diagnosis
#
############################################################


def _get_time_interval_from_lineage(direct_lineage, time_digits_for_cell_id=4):
    nodes = list(set(direct_lineage.keys()).union(set([v for values in list(direct_lineage.values()) for v in values])))
    first_time = min(nodes) // 10 ** time_digits_for_cell_id
    last_time = max(nodes) // 10 ** time_digits_for_cell_id

    return first_time, last_time


def _print_list(prop, tab, nitems=-1, time_digits_for_cell_id=4, verbose=2):
    keyname = None
    keynameset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keynameset) > 0:
        keyname = list(keynameset)[0]

    for i, c in enumerate(tab):
        t = c // 10 ** time_digits_for_cell_id
        cell_id = c % 10 ** time_digits_for_cell_id
        msg = "    - cell #" + str(cell_id) + " of time " + str(t)
        if keyname is not None:
            if c in prop[keyname]:
                msg += " (" + str(prop[keyname][c]) + ")"
        if nitems < 0 or (nitems > 0 and i < nitems):
            monitoring.to_log_and_console(msg, verbose)
        else:
            monitoring.to_log(msg)
    if 0 < nitems < len(tab):
        monitoring.to_log_and_console("    ...", verbose)


############################################################
#
# for volume diagnosis
#
############################################################


def _decode_cell_id(s, time_digits_for_cell_id=4):
    div = 10 ** time_digits_for_cell_id
    return "cell #{:4d}".format(int(s) % div) + " of image #{:4d}".format(int(s) // div)


########################################################################################
#
#
#
########################################################################################


def _cell_id(c, time_digits_for_cell_id=4):
    t = c // 10 ** time_digits_for_cell_id
    c -= t * 10 ** time_digits_for_cell_id
    return c


########################################################################################
#
# cell ids diagnosis
#
########################################################################################


def _diagnosis_cellids(prop, diagnosis_parameters, time_digits_for_cell_id=4):

    monitoring.to_log_and_console("  === " + "cell ids" + " diagnosis === ", 1)

    div = 10 ** time_digits_for_cell_id

    #
    # get keys indexed in ioproperties.keydictionary
    #
    keyname = None
    keynameset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keynameset) > 0:
        keyname = list(keynameset)[0]

    # get nodes (ie cells) for each recognized property
    # remove background
    nodes = {}
    for k in prop:
        nodes[k] = _get_nodes(prop, k)
        if nodes[k] is None:
            del nodes[k]
            continue

    #
    # background cells may remain, remove them again
    # check whether the label 0 exists
    #
    zeros = {}
    for k in nodes:
        nodes[k] = [c for c in nodes[k] if int(c) % div != 1]
        for c in nodes[k]:
            if int(c) % div == 0:
                zeros[k] = zeros.get(k, []) + [c]
                nodes[k].remove(c)
    if len(zeros) > 0:
        for k in zeros:
            msg = "  - property '" + str(k) + "': found " + str(len(zeros[k])) + " '0' labels"
            monitoring.to_log_and_console(msg, 1)
            monitoring.to_log_and_console("    " + str(zeros[k]), 1)
            for z in zeros[k]:
                _find_node(prop, k, z)
    del zeros

    #
    # check whether the same set of cell ids are indexed or used in each property
    #
    unodes = set()
    for k in nodes:
        unodes = unodes.union(set(nodes[k]))
    inodes = copy.deepcopy(unodes)
    for k in nodes:
        inodes = inodes.intersection(set(nodes[k]))
    if len(unodes) > len(inodes):
        diff = sorted(unodes - inodes)
        msg = "  - " + str(len(diff))
        msg += " cells (excluding 1s and 0s) are not in all features"
        monitoring.to_log_and_console(msg, 1)

        if int(diagnosis_parameters.items) > 0:
            if len(diff) <= int(diagnosis_parameters.items):
                length = len(diff)
            else:
                length = int(diagnosis_parameters.items)
        else:
            length = 0

        for i in range(length):
            msg = "    - " + str(diff[i])
            if keyname is not None:
                if diff[i] in prop[keyname]:
                    msg += " (" + str(prop[keyname][diff[i]]) + ")"
            insets = [k for k in nodes if diff[i] in nodes[k]]
            outsets = [k for k in nodes if diff[i] not in nodes[k]]
            if len(insets) > 0:
                msg += " is in " + str(insets)
            if len(outsets) > 0:
                if len(insets) > 0:
                    msg += " and"
                msg += " is not in " + str(outsets)
            monitoring.to_log_and_console(msg, 1)
        if length < len(diff):
            msg = "    ... "
            monitoring.to_log_and_console(msg, 1)

    monitoring.to_log_and_console("")

    return prop


########################################################################################
#
#
#
########################################################################################


def _diagnosis_lineage(prop, description, diagnosis_parameters, time_digits_for_cell_id=4):

    keylineage = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['lineage']['input_keys'])
    if len(keyset) > 0:
        keylineage = list(keyset)[0]

    keyname = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keyset) > 0:
        keyname = list(keyset)[0]

    keydiagnosis = 'morphonet_selection_diagnosis_lineage'
    if keydiagnosis in prop:
        del prop[keydiagnosis]
    prop[keydiagnosis] = {}

    monitoring.to_log_and_console("  === " + str(description) + " diagnosis === ", 1)

    direct_lineage = prop[keylineage]
    first_time, last_time = _get_time_interval_from_lineage(direct_lineage,
                                                            time_digits_for_cell_id=time_digits_for_cell_id)
    monitoring.to_log_and_console("  - estimated time interval = [" + str(first_time) + ", " + str(last_time) + "]", 1)

    #
    # cell with more than 2 daughters
    #
    multiple_daughters = [cell for cell in direct_lineage if len(direct_lineage[cell]) > 2]
    divisions = [cell for cell in direct_lineage if len(direct_lineage[cell]) >= 2]

    #
    # get cells without daughters, remove cells from the last time point
    #
    direct_nodes = list(set(direct_lineage.keys()).union(set([v for values in list(direct_lineage.values())
                                                              for v in values])))
    leaves = set(direct_nodes) - set(direct_lineage.keys())
    early_leaves = [leave for leave in leaves if (leave/10**time_digits_for_cell_id) < last_time]

    #
    # count cells per time
    #
    div = 10 ** time_digits_for_cell_id
    cells_per_time = {}
    for c in direct_nodes:
        t = int(c) // div
        if t not in cells_per_time:
            cells_per_time[t] = [c]
        else:
            cells_per_time[t].append(c)

    monitoring.to_log_and_console("    at time " + str(first_time) + ", #cells = " +
                                  str(len(cells_per_time[first_time])), 1)
    monitoring.to_log_and_console("    at time " + str(last_time) + ", #cells = " +
                                  str(len(cells_per_time[last_time])), 1)

    #
    # build a reverse lineage
    #
    reverse_lineage = {}
    for k, values in direct_lineage.items():
        for v in values:
            reverse_lineage[v] = reverse_lineage.get(v, []) + [k]

    #
    # branch lengths
    #
    length = {}
    lastleave = {}
    daughters = []
    for c in direct_lineage:
        if len(direct_lineage[c]) <= 1:
            continue
        daughters += direct_lineage[c]
    for d in daughters:
        length[d] = 0
        lastleave[d] = d
        c = d
        while c in direct_lineage and len(direct_lineage[c]) == 1:
            length[d] += 1
            c = direct_lineage[c][0]
            lastleave[d] = c
        if int(c) // div == last_time:
            del length[d]
            del lastleave[d]

    short_length = [[c, length[c], lastleave[c]] for c in length if length[c] <= diagnosis_parameters.minimal_length]
    if len(short_length) > 0:
        short_length = sorted(short_length, key=itemgetter(1))

    #
    # get cells with more than 1 mother
    #
    multiple_mothers = [cell for cell in reverse_lineage if len(reverse_lineage[cell]) > 1]

    #
    # get cells without mother, remove cells from the first time point
    #
    reverse_nodes = list(set(reverse_lineage.keys()).union(set([v for values in list(reverse_lineage.values())
                                                                for v in values])))
    orphans = set(reverse_nodes) - set(reverse_lineage.keys())
    late_orphans = [orphan for orphan in orphans if (orphan // 10 ** time_digits_for_cell_id) > first_time]

    monitoring.to_log_and_console("  - found " + str(len(direct_nodes)) + " cells", 1)

    nitems = 10
    if isinstance(diagnosis_parameters, DiagnosisParameters):
        nitems = int(diagnosis_parameters.items)

    if len(late_orphans) > 0:
        late_orphans.sort()
        cell_verbose = monitoring.verbose
        if len(late_orphans) <= nitems:
            cell_verbose = 1
        monitoring.to_log_and_console("  - " + str(len(late_orphans))
                                      + " lineage branches starting after the first time point", 1)
        _print_list(prop, late_orphans, time_digits_for_cell_id=time_digits_for_cell_id, verbose=cell_verbose)
        for c in late_orphans:
            prop[keydiagnosis][c] = 10

    if len(multiple_mothers) > 0:
        multiple_mothers.sort()
        cell_verbose = monitoring.verbose
        if len(multiple_mothers) <= nitems:
            cell_verbose = 1
        monitoring.to_log_and_console("  - " + str(len(multiple_mothers)) + " cells with multiple mother cells", 1)
        _print_list(prop, multiple_mothers, time_digits_for_cell_id=time_digits_for_cell_id, verbose=cell_verbose)
        for c in multiple_mothers:
            prop[keydiagnosis][c] = 20

    if len(leaves) > 0:
        monitoring.to_log_and_console("  - " + str(len(leaves))
                                      + " lineage terminal branches", 1)
    if len(early_leaves) > 0:
        early_leaves.sort()
        cell_verbose = monitoring.verbose
        if len(early_leaves) <= nitems:
            cell_verbose = 1
        monitoring.to_log_and_console("  - " + str(len(early_leaves))
                                      + " lineage terminal branches ending before the last time point", 1)
        _print_list(prop, early_leaves, nitems=nitems, time_digits_for_cell_id=time_digits_for_cell_id,
                    verbose=cell_verbose)
        for c in early_leaves:
            prop[keydiagnosis][c] = 30

    if len(divisions) > 0:
        divisions.sort()
        monitoring.to_log_and_console("  - " + str(len(divisions)) + " cell divisions", 1)
    if len(multiple_daughters) > 0:
        multiple_daughters.sort()
        cell_verbose = monitoring.verbose
        if len(multiple_daughters) <= nitems:
            cell_verbose = 1
        monitoring.to_log_and_console("  - " + str(len(multiple_daughters))
                                      + " divisions yielding more than 2 branches", 1)
        _print_list(prop, multiple_daughters, time_digits_for_cell_id=time_digits_for_cell_id, verbose=cell_verbose)
        for c in multiple_daughters:
            prop[keydiagnosis][c] = 40

    if len(short_length) > 0:
        monitoring.to_log_and_console("  - " + str(len(short_length)) + " non-terminal branch length < " +
                                      str(diagnosis_parameters.minimal_length), 1)
        if len(short_length) <= diagnosis_parameters.items:
            nitems = len(short_length)
        else:
            nitems = int(diagnosis_parameters.items)
        for i in range(nitems):
            msg = "    - " + str(short_length[i][0])
            if keyname is not None:
                if short_length[i][0] in prop[keyname]:
                    msg += " (" + str(prop[keyname][short_length[i][0]]) + ")"
            msg += " has a branch/life length of " + str(short_length[i][1])
            msg += " (ended at " + str(short_length[i][2]) + ")"
            monitoring.to_log_and_console(msg, 1)
        if nitems < len(short_length):
            msg = "    ... "
            monitoring.to_log_and_console(msg, 1)
        for c in short_length:
            prop[keydiagnosis][c[0]] = 50

    monitoring.to_log_and_console("")
    return prop


def _diagnosis_volume(prop, description, diagnosis_parameters, time_digits_for_cell_id=4):
    """

    :param prop:
    :param description:
    :param diagnosis_parameters:
    :return:
    """

    #     dictionary de int
    #     cell_volume.590002 = <type 'int'>
    #     590002: 236936

    keyname = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keyset) > 0:
        keyname = list(keyset)[0]

    keylineage = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['lineage']['input_keys'])
    if len(keyset) > 0:
        keylineage = list(keyset)[0]

    keyvolume = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['volume']['input_keys'])
    if len(keyset) > 0:
        keyvolume = list(keyset)[0]

    keydiagnosis = 'morphonet_float_diagnosis_volume'
    if keydiagnosis in prop:
        del prop[keydiagnosis]
    prop[keydiagnosis] = {}

    monitoring.to_log_and_console("  === " + str(description) + " diagnosis === ", 1)

    all_cell_with_volume = set(prop[keyvolume].keys())
    cell_with_volume = [c for c in all_cell_with_volume if _cell_id(c, time_digits_for_cell_id) != 1]
    background_with_volume = [c for c in all_cell_with_volume if _cell_id(c, time_digits_for_cell_id) == 1 or
                              _cell_id(c, time_digits_for_cell_id) == 0]

    first_time = min(all_cell_with_volume) // 10 ** time_digits_for_cell_id
    last_time = max(all_cell_with_volume) // 10 ** time_digits_for_cell_id

    monitoring.to_log_and_console("  - estimated time interval = [" + str(first_time) + ", " + str(last_time) + "]", 1)

    volume = [[c, prop[keyvolume][c]] for c in cell_with_volume]
    volume = sorted(volume, key=itemgetter(1))

    monitoring.to_log_and_console("    found " + str(len(background_with_volume)) + " background cells with volume", 1)
    monitoring.to_log_and_console("    found " + str(len(cell_with_volume)) + " cells with volume", 1)

    d = DiagnosisParameters()
    n = int(d.items)
    v = int(d.minimal_volume)

    monitoring.to_log_and_console("")
    monitoring.to_log_and_console("  - smallest volumes", 1)

    if isinstance(diagnosis_parameters, DiagnosisParameters):
        n = int(diagnosis_parameters.items)
        v = int(diagnosis_parameters.minimal_volume)

    for i in range(len(list(prop[keyvolume].keys()))):
        if (n > 0 and i < n) or (v > 0 and int(volume[i][1]) <= v):
            msg = _decode_cell_id(volume[i][0])
            if keyname is not None:
                if volume[i][0] in prop[keyname]:
                    msg += " (" + str(prop[keyname][volume[i][0]]) + ")"
            msg += " has volume = " + str(volume[i][1])
            monitoring.to_log_and_console("    " + msg, 1)

    volume = sorted(volume, key=itemgetter(1), reverse=True)

    monitoring.to_log_and_console("")
    monitoring.to_log_and_console("  - largest volumes", 1)

    for i in range(len(list(prop[keyvolume].keys()))):
        if n > 0 and i < n:
            msg = _decode_cell_id(volume[i][0])
            if keyname is not None:
                if volume[i][0] in prop[keyname]:
                    msg += " (" + str(prop[keyname][volume[i][0]]) + ")"
            msg += " has volume = " + str(volume[i][1])
            monitoring.to_log_and_console("    " + msg, 1)

    #
    # volume variation
    #
    if keylineage is None:
        return prop

    lineage = prop[keylineage]
    div = 10 ** time_digits_for_cell_id
    first_time, last_time = _get_time_interval_from_lineage(lineage, time_digits_for_cell_id=time_digits_for_cell_id)
    first_cells = [lineage[c][0] for c in lineage if len(lineage[c]) == 2 and int(c) // div < last_time-1]
    first_cells += [lineage[c][1] for c in lineage if len(lineage[c]) == 2 and int(c) // div < last_time-1]

    volume_along_time = {}
    volume_derivative = {}

    for cell in first_cells:
        if cell not in prop[keyvolume] or cell not in lineage:
            continue
        volume_along_time[cell] = volume_along_time.get(cell, []) + [prop[keyvolume][cell]]
        volume_derivative[cell] = []
        pcell = cell
        while True:
            ncell = lineage[pcell][0]
            if ncell not in prop[keyvolume]:
                break
            volume_along_time[cell] += [prop[keyvolume][ncell]]
            volume_derivative[cell] += [(prop[keyvolume][ncell] - prop[keyvolume][pcell]) / prop[keyvolume][pcell]]
            pcell = ncell
            if pcell not in lineage or len(lineage[pcell]) > 1:
                break

    volume_variation = {}
    volume_max_derivative = {}
    for cell in volume_along_time:
        if len(volume_along_time[cell]) <= 1:
            msg = "    * weird, " + _decode_cell_id(cell)
            if keyname is not None:
                if cell in prop[keyname]:
                    msg += " (" + str(prop[keyname][cell]) + ")"
            msg += " has a branch (life) length of " + str(len(volume_along_time[cell]))
            monitoring.to_log_and_console(msg, 1)
            continue
        volume_variation[cell] = (max(volume_along_time[cell]) - min(volume_along_time[cell])) / \
                                  statistics.median(volume_along_time[cell])
        if max(volume_derivative[cell]) > 0:
            volume_max_derivative[cell] = max(volume_derivative[cell])
        if min(volume_derivative[cell]) < 0:
            volume_max_derivative[cell] = max(-min(volume_derivative[cell]), volume_max_derivative.get(cell, 0.0))

    volume = [[c, volume_variation[c]] for c in volume_variation]
    volume = sorted(volume, key=itemgetter(1), reverse=True)

    monitoring.to_log_and_console("")
    msg = "  - largest volume variation [(max-min)/median]"
    msg += ": " + str(len(volume_variation.keys())) + " branchs"
    monitoring.to_log_and_console(msg, 1)

    if isinstance(diagnosis_parameters, DiagnosisParameters):
        n = int(diagnosis_parameters.items)
        v = diagnosis_parameters.maximal_volume_variation

    for i in range(len(volume_variation.keys())):
        if (n > 0 and i < n) or (0 < v <= volume[i][1]):
            msg = _decode_cell_id(volume[i][0])
            if keyname is not None:
                if volume[i][0] in prop[keyname]:
                    msg += " (" + str(prop[keyname][volume[i][0]]) + ")"
            msg += " has volume variation = {:.4f}".format(volume[i][1])
            msg += " [branch length = " + str(len(volume_along_time[volume[i][0]])) + "]"
            if n > 0 and i < n:
                monitoring.to_log_and_console("    " + msg, 1)
            else:
                monitoring.to_log("    " + msg)
    if len(volume_variation.keys()) > 0 and n < len(volume_variation.keys()):
        monitoring.to_log_and_console("     ...", 1)

    volume = [[c, volume_max_derivative[c]] for c in volume_max_derivative]
    volume = sorted(volume, key=itemgetter(1), reverse=True)

    monitoring.to_log_and_console("")
    msg = "  - largest volume derivative [(v[t+1]-v[t])/v[t]]"
    msg += ": " + str(len(volume_max_derivative.keys())) + " branchs"
    monitoring.to_log_and_console(msg, 1)

    if isinstance(diagnosis_parameters, DiagnosisParameters):
        n = int(diagnosis_parameters.items)
        v = diagnosis_parameters.maximal_volume_derivative

    for i in range(len(volume_max_derivative.keys())):
        if (n > 0 and i < n) or (0 < v <= volume[i][1]):
            t = volume[i][0] // div
            msg = _decode_cell_id(volume[i][0])
            if keyname is not None:
                if volume[i][0] in prop[keyname]:
                    msg += " (" + str(prop[keyname][volume[i][0]]) + ")"
            msg += " has volume maximal derivative = {:.4f}".format(volume[i][1])
            msg += " [branch length = " + str(len(volume_along_time[volume[i][0]])) + "]"
            if n > 0 and i < n:
                monitoring.to_log_and_console("    " + msg, 1)
            else:
                monitoring.to_log("    " + msg)
            cell = volume[i][0]
            c = cell
            for j in range(len(volume_derivative[cell])):
                if (0 < v <= abs(volume_derivative[cell][j])) or abs(volume_derivative[cell][j]) > volume[i][1] - 0.1:
                    msg = "    - from time " + str(t+j) + " to " + str(t+j+1)
                    msg += ", volume evolves from " + str(volume_along_time[cell][j]) + " to "
                    msg += str(volume_along_time[cell][j+1])
                    msg += " (derivative = {:.4f}".format(volume_derivative[cell][j]) + ")"
                    if n > 0 and i < n:
                        monitoring.to_log_and_console(msg, 1)
                    else:
                        monitoring.to_log(msg)
                    if c not in prop[keydiagnosis]:
                        prop[keydiagnosis][c] = abs(volume_derivative[cell][j])
                    else:
                        prop[keydiagnosis][c] = max(prop[keydiagnosis][c], abs(volume_derivative[cell][j]))
                    prop[keydiagnosis][lineage[c][0]] = abs(volume_derivative[cell][j])
                    c = lineage[c][0]
    if len(volume_max_derivative.keys()) > 0 and n < len(volume_max_derivative.keys()):
        monitoring.to_log_and_console("    ...", 1)

    monitoring.to_log_and_console("")
    return prop


def _diagnosis_one_feature(prop, feature, diagnosis_parameters, time_digits_for_cell_id=4):

    if feature in ioproperties.keydictionary['lineage']['input_keys']:
        prop = _diagnosis_lineage(prop, feature, diagnosis_parameters=diagnosis_parameters,
                                  time_digits_for_cell_id=time_digits_for_cell_id)
    elif feature in ioproperties.keydictionary['volume']['input_keys']:
        prop = _diagnosis_volume(prop, feature, diagnosis_parameters=diagnosis_parameters,
                                 time_digits_for_cell_id=time_digits_for_cell_id)
    else:
        if monitoring.debug > 0:
            monitoring.to_log_and_console("    unknown key '" + str(feature) + "' for diagnosis", 1)

    return prop


def diagnosis(prop, features=None, parameters=None, time_digits_for_cell_id=4, verbose=True):
    """

    :param prop: property dictionary
    :param features:
    :param parameters:
    :param time_digits_for_cell_id:
    :param verbose:
    :return:
    """

    if parameters is not None and isinstance(parameters, DiagnosisParameters):
        diagnosis_parameters = parameters
    else:
        diagnosis_parameters = DiagnosisParameters()

    if verbose:
        monitoring.to_log_and_console("... diagnosis", 1)

    #
    # test keys
    #
    prop = _diagnosis_cellids(prop, diagnosis_parameters, time_digits_for_cell_id=time_digits_for_cell_id)

    #
    # diagnosis on all features of the property dictionary, or only on requested features
    #
    propkeys = list(prop.keys())
    if features is None or (isinstance(features, list) and len(features) == 0) or \
            (isinstance(features, str) and len(features) == 0):
        for k in propkeys:
            prop = _diagnosis_one_feature(prop, k, diagnosis_parameters,
                                          time_digits_for_cell_id=time_digits_for_cell_id)
    else:
        if isinstance(features, str):
            prop = _diagnosis_one_feature(prop, features, diagnosis_parameters,
                                          time_digits_for_cell_id=time_digits_for_cell_id)
        elif isinstance(features, list):
            for f in features:
                prop = _diagnosis_one_feature(prop, f, diagnosis_parameters,
                                              time_digits_for_cell_id=time_digits_for_cell_id)

    if verbose:
        monitoring.to_log_and_console("  === end of diagnosis === ", 1)
        monitoring.to_log_and_console("")

    return prop
