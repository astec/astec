
import os
import sys
import importlib.util

import time

import shutil
import itertools

import astec.wrapping.cpp_wrapping as cpp_wrapping


##################################################
#
#
#
##################################################


def load_source(path):
    proc = "load_source"
    if path is None:
        return None
    if not os.path.isfile(path):
        print(proc + ": '" + path + "' is not a valid file.")
        print("\t Exiting.")
        sys.exit(1)

    spec = importlib.util.spec_from_file_location('parameters', path)
    parameters = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(parameters)
    return parameters


def read_parameter(parameters, parameter_description, default_value):
    """
    get a parameter value from a (already read parameter file)
    :param parameters:
    :param parameter_description:
    :param default_value:
    :return:
    """
    #
    # read the parameter
    #
    # print("entering read_parameter")
    # print("\t prefixes = " + str(self._prefixes))

    returned_attr = default_value
    if hasattr(parameters, parameter_description):
        returned_attr = getattr(parameters, parameter_description)
    return returned_attr


##################################################
#
#
#
##################################################

def _flat_list(input_list):
    output_list = []
    # iterating over the data
    for element in input_list:
        # checking for list
        if type(element) == list:
            # calling the same function with current element as new argument
            output_list += element
        else:
            output_list += [element]
    return output_list


def str_variable(name, value):
    # print("   - value = " + str(value) + " type=" + str(type(value)))
    s = "# " + name + " = "
    if value is None:
        s += str(value)
    elif type(value) == str:
        s += "'" + value + "'"
    elif type(value) == bool:
        s += str(value)
    elif type(value) == float:
        s += str(value)
    elif type(value) == int:
        s += str(value)
    elif type(value) == list:
        s += "["
        if len(value) > 0:
            for i in range(len(value)):
                if type(value[i]) == str:
                    s += "'" + value[i] + "'"
                else:
                    s += str(value[i])
                if i + 1 < len(value):
                    s += ", "
        else:
            s += 'empty list'
        s += "]"
    elif type(value) == tuple:
        s += str(value)
    else:
        s += "unhandled type"
    return s


class PrefixedParameter(object):
    def __init__(self, prefix=None):
        if prefix is None:
            self._prefix = ""
            self._full_prefix = ""
        elif type(prefix) is str:
            self._prefix = prefix
            self._full_prefix = ''.join(self._prefix)
        elif type(prefix) is list:
            output_list = _flat_list(prefix)
            self._prefix = []
            for p in output_list:
                if type(p) is str:
                    self._prefix.append(p)
                elif type(p) is list:
                    self._prefix.extend(p)
                else:
                    monitoring.to_log_and_console("PrefixedParameter.init: unexpected type for 'p':" + str(type(p)))
                    monitoring.to_log_and_console("Exiting")
                    sys.exit(0)
            self._full_prefix = ''.join(self._prefix)
        else:
            monitoring.to_log_and_console("PrefixedParameter.init: unexpected type for 'prefix':" + str(type(prefix)))
            monitoring.to_log_and_console("Exiting")
            sys.exit(0)
        self._set_prefixes()
        return

    def _set_prefixes(self):
        prefix = self._prefix
        if prefix is None or prefix == "":
            self._prefixes = [""]
        if type(prefix) is str and prefix != "":
            self._prefixes = ["", prefix]
        if type(prefix) is list:
            prefixes = [""]
            prefixes.extend(prefix)
            for length in range(2, len(prefix)+1):
                for c in itertools.combinations(prefix, length):
                    prefixes.append(''.join(c))
            self._prefixes = prefixes
        return

    def read_parameter(self, parameters, parameter_description, default_value):
        """
        get a parameter value from a (already read parameter file)
        :param self:
        :param parameters:
        :param parameter_description:
        :param default_value:
        :return:
        """
        #
        # read the parameter
        #
        # print("entering read_parameter")
        # print("\t prefixes = " + str(self._prefixes))

        returned_attr = default_value
        for p in self._prefixes:
            desc = p + parameter_description
            if hasattr(parameters, desc):
                if isinstance(default_value, list) and len(default_value) > 0:
                    returned_attr = default_value + getattr(parameters, desc)
                else:
                    returned_attr = getattr(parameters, desc)
        return returned_attr

    def _fulldesc(self, desc):
        if self._prefix is None or self._prefix == '':
            return '- ' + desc + ' = '
        elif type(self._prefix) is str:
            return '- [' + str(self._prefix) + ']' + desc + ' = '
        elif type(self._prefix) is list:
            return '- ' + str(self._prefix) + desc + ' = '
        else:
            return '- ' + desc + ' = '
        # if self._full_prefix is not None:
        #     name = self._full_prefix + desc
        # else:
        #     name = desc
        # return '- ' + name + ' = '

    def confprint(self, name, value, spaces=0):
        print(spaces * ' ' + self._fulldesc(name) + str(value))

    def print_configuration(self, spaces=0):
        if type(self._prefixes) is list and len(self._prefixes) > 1:
            print(spaces * ' ' + "- _prefix = " + str(self._prefix))
            print(spaces * ' ' + "- _full_prefix = " + str(self._full_prefix))
            print(spaces * ' ' + "- _prefixes = " + str(self._prefixes))

    def confwrite(self, logfile, name, value, spaces=0):
        logfile.write(spaces * ' ' + self._fulldesc(name) + str(value) + '\n')

    def write_configuration_in_file(self, logfile, spaces=0):
        if type(self._prefixes) is list and len(self._prefixes) > 1:
            logfile.write(spaces * ' ' + "- _prefix = " + str(self._prefix) + '\n')
            logfile.write(spaces * ' ' + "- _full_prefix = " + str(self._full_prefix) + '\n')
            logfile.write(spaces * ' ' + "- _prefixes = " + str(self._prefixes) + '\n')

    def varprint(self, name, value, doc=None):
        print(str_variable(self._full_prefix + name, value))
        if doc is not None and isinstance(doc, str) and len(doc):
            for line in doc.splitlines():
                print('# ' + line)

    def print_parameters(self):
        if type(self._prefixes) is list and len(self._prefixes) > 1:
            print('# _prefix      = ' + str(self._prefix))
            print('# _full_prefix = ' + str(self._full_prefix))
            print('# _prefixes    = ' + str(self._prefixes))
            print('#')

    def varwrite(self, logfile, name, value, doc=None):
        logfile.write(str_variable(self._full_prefix + name, value) + '\n')
        if doc is not None and isinstance(doc, str) and len(doc):
            for line in doc.splitlines():
                logfile.write('# ' + line + '\n')

    def write_parameters_in_file(self, logfile):
        if type(self._prefixes) is list and len(self._prefixes) > 1:
            logfile.write('# _prefix      = ' + str(self._prefix) + '\n')
            logfile.write('# _full_prefix = ' + str(self._full_prefix) + '\n')
            logfile.write('# _prefixes    = ' + str(self._prefixes) + '\n')
            logfile.write('#' + '\n')


##################################################
#
# Monitoring processing
#
##################################################

def timestamp_to_str(timestamp=None):
    """
    build a string from a time stamp, e.g. time.localtime()
    :param timestamp:
    :return:
    """
    if timestamp is None:
        timestamp = time.localtime()
    d = time.strftime("%Y-%m-%d-%H-%M-%S", timestamp)
    return d


class Monitoring(object):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self):
        self.verbose = 1
        self.debug = 0
        self.log_filename = None
        self.keepTemporaryFiles = False
        self.forceResultsToBeBuilt = False

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_configuration(self):
        print("")
        print('Monitoring configuration')
        print('- verbose is ' + str(self.verbose))
        print('- debug is ' + str(self.debug))
        print('- log_filename is ' + str(self.log_filename))
        print('- keepTemporaryFiles is ' + str(self.keepTemporaryFiles))
        print('- forceResultsToBeBuilt is ' + str(self.forceResultsToBeBuilt))
        print("")

    def write_configuration(self):
        if self.log_filename is not None:
            with open(self.log_filename, 'a') as logfile:
                logfile.write("\n")
                logfile.write('Monitoring status\n')
                logfile.write('- verbose is ' + str(self.verbose)+'\n')
                logfile.write('- debug is ' + str(self.debug)+'\n')
                logfile.write('- log_filename is ' + str(self.log_filename)+'\n')
                logfile.write('- keepTemporaryFiles is ' + str(self.keepTemporaryFiles)+'\n')
                logfile.write('- forceResultsToBeBuilt is ' + str(self.forceResultsToBeBuilt)+'\n')
                logfile.write("\n")
        return

    def update_execution_time(self, start_time=None, end_time=None):
        if self.log_filename is not None:
            with open(self.log_filename, 'a') as logfile:
                logfile.write('# Total execution time = ' + str(time.mktime(end_time) - time.mktime(start_time))
                              + ' sec\n')
                logfile.write("\n\n")
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_args(self, args):
        self.verbose = args.verbose
        self.debug = args.debug
        self.keepTemporaryFiles = args.keepTemporaryFiles
        self.forceResultsToBeBuilt = args.forceResultsToBeBuilt

    ############################################################
    #
    # setters
    #
    ############################################################

    def set_log_filename(self, experiment=None, cli_name=None, timestamp=None, log_filename=None):
        """
        set the full logfile name. The log directory name is built
        :param experiment:
        :param cli_name:
        :param timestamp:
        :param log_filename:
        :return:
        """
        if log_filename is not None and isinstance(log_filename, str):
            self.log_filename = log_filename
            return

        if experiment is not None:
            log_dirname = experiment.get_log_dirname()
            if not os.path.isdir(log_dirname):
                os.makedirs(log_dirname)
        else:
            log_dirname = os.getcwd()

        if cli_name is None:
            local_executable = 'unknown'
        else:
            local_executable = os.path.basename(cli_name)
            if local_executable[-3:] == '.py':
                local_executable = local_executable[:-3]
        log_filename = local_executable + '-' + timestamp_to_str(timestamp) + '.log'

        self.log_filename = os.path.join(log_dirname, log_filename)
        return

    ############################################################
    #
    # misc
    #
    ############################################################

    def copy(self, m):
        """
        make a copy from an other object
        :param m:
        :return:
        """
        if m is None:
            return
        self.verbose = m.verbose
        self.debug = m.debug
        self.log_filename = m.log_filename
        self.keepTemporaryFiles = m.keepTemporaryFiles
        self.forceResultsToBeBuilt = m.forceResultsToBeBuilt

    @staticmethod
    def to_console(text):
        print(text)

    def to_log(self, text):
        if self.log_filename is not None:
            with open(self.log_filename, 'a') as logfile:
                logfile.write(text+'\n')

    def to_log_and_console(self, text, verboseness=0):
        if self.verbose >= verboseness or self.debug > 0:
            self.to_console(text)
        self.to_log(text)


monitoring = Monitoring()


########################################################################################
#
# Registration
#
########################################################################################


def _fullname(prefix, desc):
    if prefix is not None:
        return prefix + desc
    else:
        return desc


def _fulldesc(prefix, desc):
    return '- ' + _fullname(prefix, desc) + ' = '


class RegistrationParameters(PrefixedParameter):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):
        PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        #
        #
        doc = "\t possible values are True or False\n"
        self.doc['compute_registration'] = doc
        self.compute_registration = True

        #
        # parameters
        #
        doc = "\t Highest level of the pyramid image use for registration\n"
        doc += "\t \n"
        doc += "\t Registration is performed within a hierarchical scheme, ie\n"
        doc += "\t an image pyramid is built, with the image dimensions \n"
        doc += "\t decreasing from one pyramid level to the next (divided by 2). The \n"
        doc += "\t registration starts at the highest pyramid level (the \n"
        doc += "\t smallest image so the pyramid) and ends at the lowest\n"
        doc += "\t level.\n"
        doc += "\t 0 is the lowest level, ie the original image itself.\n"
        doc += "\t To go from level 'l' to level 'l+1', each image \n"
        doc += "\t dimension is divided by 2, meaning the size of a \n"
        doc += "\t 3D image is divided by 8.\n"
        doc += "\t Level 1 is defined by the first value of form '2^n' \n"
        doc += "\t immediately inferior to the image dimension, or the\n"
        doc += "\t image dimension divided by 2 if it is already of\n"
        doc += "\t form 2^n.\n"
        doc += "\t Setting this variable to 6 means that registration starts\n"
        doc += "\t with images whose dimensions are 1/64th of the original image.\n"
        self.doc['pyramid_highest_level'] = doc
        self.pyramid_highest_level = 6

        doc = "\t Lowest level of the pyramid image use for registration\n"
        doc += "\t Setting it to 0 means that the lowest level is with\n"
        doc += "\t the image itself. Setting it to 1 or even 2 allows\n"
        doc += "\t to gain computational time.\n"
        doc += "\t See 'pyramid_highest_level' description.\n"
        self.doc['pyramid_lowest_level'] = doc
        self.pyramid_lowest_level = 3

        doc = "\t possible values are True or False\n"
        doc += "\t If True the image at one pyramid level is smoothed\n"
        doc += "\t by a Gaussian kernel before building the image at\n"
        doc += "\t the next level\n"
        self.doc['gaussian_pyramid'] = doc
        self.gaussian_pyramid = False

        doc = "\t Possible values are 'translation', 'rigid', 'similitude', \n"
        doc += "\t 'affine' or 'vectorfield'\n"
        self.doc['transformation_type'] = doc
        self.transformation_type = 'affine'

        doc = "\t Gaussian sigma to regularize the deformation,\n"
        doc += "\t only for 'vectorfield' transformation.\n"
        doc += "\t \n"
        doc += "\t At each registration iteration, a residual deformation is\n"
        doc += "\t computed. It is smoothed (regularized) by a gaussian of \n"
        doc += "\t 'fluid_sigma' parameter, then compounded with the \n"
        doc += "\t previously found transformation, and the resulting\n"
        doc += "\t deformation is finally smoothed (regularized) by a gaussian\n"
        doc += "\t of 'elastic_sigma' parameter.\n"
        self.doc['elastic_sigma'] = doc
        self.elastic_sigma = 4.0

        doc = "\t Possible values are 'wlts', 'lts', 'wls', or 'ls'\n"
        doc += "\t - 'wlts': weighted least trimmed squares\n"
        doc += "\t - 'lts': least trimmed squares\n"
        doc += "\t - 'wls': weighted least squares\n"
        doc += "\t - 'ls': least squares \n"
        self.doc['transformation_estimation_type'] = doc
        self.transformation_estimation_type = 'wlts'

        doc = "\t Fraction of pairings retained to compute the transformation,\n"
        doc += "\t only for robust estimation scheme ('wlts' or 'lts').\n"
        doc += "\t Has obviously to be larger than 0.5.\n"
        self.doc['lts_fraction'] = doc
        self.lts_fraction = 0.55

        doc = "\t Gaussian sigma to regularize the deformation update.\n"
        doc += "\t Only for 'vectorfield' transformation.\n"
        doc += "\t See 'elastic_sigma' description.\n"
        self.doc['fluid_sigma'] = doc
        self.fluid_sigma = 4.0

        doc = "\t possible values are True or False. If True, the images to be registered\n"
        doc += "\t are normalized on 1 byte for computational purposes.\n"
        doc += "\t This variable is kept for historical reasons. Do not change it.\n"
        self.doc['normalization'] = doc
        self.normalization = False

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# RegistrationParameters')
        print('#')
        print("")

        PrefixedParameter.print_parameters(self)

        self.varprint('compute_registration', self.compute_registration, self.doc['compute_registration'])

        self.varprint('pyramid_highest_level', self.pyramid_highest_level, self.doc['pyramid_highest_level'])
        self.varprint('pyramid_lowest_level', self.pyramid_lowest_level, self.doc['pyramid_lowest_level'])
        self.varprint('gaussian_pyramid', self.gaussian_pyramid, self.doc['gaussian_pyramid'])

        self.varprint('transformation_type', self.transformation_type, self.doc['transformation_type'])

        self.varprint('elastic_sigma', self.elastic_sigma, self.doc['elastic_sigma'])

        self.varprint('transformation_estimation_type', self.transformation_estimation_type,
                      self.doc['transformation_estimation_type'])
        self.varprint('lts_fraction', self.lts_fraction, self.doc['lts_fraction'])
        self.varprint('fluid_sigma', self.fluid_sigma, self.doc['fluid_sigma'])

        self.varprint('normalization', self.normalization, self.doc['normalization'])
        print("")
        return

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# RegistrationParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'compute_registration', self.compute_registration, self.doc['compute_registration'])

        self.varwrite(logfile, 'pyramid_highest_level', self.pyramid_highest_level, self.doc['pyramid_highest_level'])
        self.varwrite(logfile, 'pyramid_lowest_level', self.pyramid_lowest_level, self.doc['pyramid_lowest_level'])
        self.varwrite(logfile, 'gaussian_pyramid', self.gaussian_pyramid, self.doc['gaussian_pyramid'])

        self.varwrite(logfile, 'transformation_type', self.transformation_type, self.doc['transformation_type'])

        self.varwrite(logfile, 'elastic_sigma', self.elastic_sigma, self.doc['elastic_sigma'])

        self.varwrite(logfile, 'transformation_estimation_type', self.transformation_estimation_type,
                      self.doc['transformation_estimation_type'])
        self.varwrite(logfile, 'lts_fraction', self.lts_fraction, self.doc['lts_fraction'])
        self.varwrite(logfile, 'fluid_sigma', self.fluid_sigma, self.doc['fluid_sigma'])

        self.varwrite(logfile, 'normalization', self.normalization, self.doc['normalization'])

        logfile.write("\n")
        return

    def write_parameters(self, log_filename=None):
        if log_filename is not None:
            local_log_filename = log_filename
        else:
            local_log_filename = monitoring.log_filename
        if local_log_filename is not None:
            with open(local_log_filename, 'a') as logfile:
                self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):
        self.compute_registration = self.read_parameter(parameters, 'compute_registration', self.compute_registration)

        self.pyramid_highest_level = self.read_parameter(parameters, 'pyramid_highest_level',
                                                         self.pyramid_highest_level)
        self.pyramid_lowest_level = self.read_parameter(parameters, 'pyramid_lowest_level', self.pyramid_lowest_level)
        self.gaussian_pyramid = self.read_parameter(parameters, 'gaussian_pyramid', self.gaussian_pyramid)

        self.transformation_type = self.read_parameter(parameters, 'transformation_type', self.transformation_type)

        self.elastic_sigma = self.read_parameter(parameters, 'elastic_sigma', self.elastic_sigma)

        self.transformation_estimation_type = self.read_parameter(parameters, 'transformation_estimation_type',
                                                                  self.transformation_estimation_type)
        self.lts_fraction = self.read_parameter(parameters, 'lts_fraction', self.lts_fraction)
        self.fluid_sigma = self.read_parameter(parameters, 'fluid_sigma', self.fluid_sigma)
        self.normalization = self.read_parameter(parameters, 'normalization', self.normalization)

    def update_from_parameter_file(self, parameter_file):

        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = load_source(parameter_file)
        self.update_from_parameters(parameters)

    ############################################################
    #
    #
    #
    ############################################################

    def is_equal(self, p):
        if self.compute_registration != p.compute_registration:
            return False
        if self.pyramid_highest_level != p.pyramid_highest_level:
            return False
        if self.pyramid_lowest_level != p.pyramid_lowest_level:
            return False
        if self.gaussian_pyramid != p.gaussian_pyramid:
            return False
        if self.transformation_type != p.transformation_type:
            return False
        if self.elastic_sigma != p.elastic_sigma:
            return False
        if self.transformation_estimation_type != p.transformation_estimation_type:
            return False
        if self.lts_fraction != p.lts_fraction:
            return False
        if self.fluid_sigma != p.fluid_sigma:
            return False
        if self.normalization != p.normalization:
            return False

        return True


def blockmatching(image_ref, image_flo, image_output, trsf_output, trsf_init, parameters, other_options=None):
    """

    :param image_ref:
    :param image_flo:
    :param image_output:
    :param trsf_output:
    :param trsf_init:
    :param parameters:
    :param other_options:
    :return:
    """

    proc = "common.blockmatching"

    #
    # parameter type checking
    #

    if not isinstance(parameters, RegistrationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    cpp_wrapping.blockmatching(image_ref, image_flo, image_output, trsf_output, trsf_init,
                               py_hl=parameters.pyramid_highest_level, py_ll=parameters.pyramid_lowest_level,
                               gaussian_pyramid=parameters.gaussian_pyramid,
                               transformation_type=parameters.transformation_type,
                               elastic_sigma=parameters.elastic_sigma,
                               transformation_estimator=parameters.transformation_estimation_type,
                               lts_fraction=parameters.lts_fraction, fluid_sigma=parameters.fluid_sigma,
                               normalization=parameters.normalization, other_options=other_options,
                               monitoring=monitoring)
    return


########################################################################################
#
#
#
########################################################################################

def get_parameter_file(parameter_file):
    """
    check if the given parameter file is valid, otherwise ask for a file name
    :param parameter_file: the parameter file name to be tested
    :return: the parameter file name
    """
    if parameter_file is not None and os.path.isfile(parameter_file):
        return parameter_file
    new_parameter_file = input('   Provide the parameter file: ')
    if new_parameter_file is None or new_parameter_file == '':
        print("getParameterFile: no parameter file. Exiting.")
        sys.exit(1)
    if not os.path.isfile(new_parameter_file):
        print("getParameterFile: '" + new_parameter_file + "' is not a valid file. Exiting.")
        sys.exit(1)
    return new_parameter_file


########################################################################################
#
#
#
########################################################################################


########################################################################################
#
# image file utilities
#
########################################################################################

def _get_extension(filename, recognized_extensions):
    """ Return the file extension. Must be in the set of recognized extensions.
    :param filename:
    :param recognized_extensions:
    :return: None in case of unrecognized extension,
             else the recognized extension (begins with '.')
    """
    for e in recognized_extensions:
        if len(filename) < len(e):
            continue
        if filename[len(filename)-len(e):len(filename)] == e:
            return e
    return None


recognized_image_extensions = ['.zip', '.lux.h5', '.lux.h5.gz', '.h5', '.h5.gz', '.tif', '.tiff', '.TIF',
                               '.TIFF', '.inr', '.inr.gz', '.mha', '.mha.gz', '.nii', '.nii.gz']


def get_image_extension(filename):
    """ Return the file extension. Must be in the set of recognized extensions.
    :param filename:
    :return: None in case of unrecognized extension,
             else the recognized extension (begins with '.')
    """
    return _get_extension(filename, recognized_image_extensions)


def copy_image(source_filename, target_filename):
    proc = "copy_image"
    source_ext = get_image_extension(source_filename)
    target_ext = get_image_extension(target_filename)

    if source_ext is None:
        msg = proc + ": input filename '" + str(source_filename) + "' does not have a recognized extension"
        monitoring.to_log_and_console(msg)
    if target_ext is None:
        msg = proc + ": output filename '" + str(target_filename) + "' does not have a recognized extension"
        monitoring.to_log_and_console(msg)

    if source_ext is None and target_ext is None:
        msg = "copy '" + str(source_filename) + "' into '" + str(target_filename) + "' (without guarantee)"
        monitoring.to_log_and_console(proc + ": " + msg)
        shutil.copyfile(source_filename, target_filename)
        return

    if source_ext is not None and target_ext is not None:
        if source_ext == target_ext:
            shutil.copyfile(source_filename, target_filename)
        else:
            cpp_wrapping.copy(source_filename, target_filename)
        return

    monitoring.to_log_and_console(proc + ": will not copy file")
    sys.exit(1)


recognized_lineage_extensions = ['.pkl', '.xml']


def get_lineage_extension(filename):
    """ Return the file extension. Must be in the set of recognized extensions.
    :param filename:
    :return: None in case of unrecognized extension,
             else the recognized extension (begins with '.')
    """
    return _get_extension(filename, recognized_lineage_extensions)
#
#
#
#
#


def add_suffix(filename, suffix, new_dirname=None, new_extension=None):
    """
    Add a suffix to a *image* filename (ie before the extension)
    :param filename:
    :param suffix: suffix to be added
    :param new_dirname: change the directory name of the file
    :param new_extension: change the extension of the file
    :return: the transformed file name
    """
    proc = 'add_suffix'
    if filename is None:
        monitoring.to_log_and_console(proc + ": was called with '" + str(filename) + "'")
        return
    b = os.path.basename(filename)
    d = os.path.dirname(filename)
    e = get_image_extension(b)
    if e is None:
        # print(proc + ": file extension of '"+str(filename)+"' was not recognized")
        # print("\t Exiting")
        # monitoring.to_log_and_console(proc + ": file extension of '"+str(filename)+"' was not recognized", 0)
        # monitoring.to_log_and_console("\t Exiting", 0)
        # sys.exit(1)
        new_basename = b
    else:
        new_basename = b[0:len(b)-len(e)]
    new_basename += suffix
    if new_extension is None:
        new_basename += e
    else:
        if new_extension[0] == '.':
            new_basename += new_extension
        else:
            new_basename += '.' + new_extension
    if new_dirname is None:
        res_name = os.path.join(d, new_basename)
    else:
        res_name = os.path.join(new_dirname, new_basename)
    return res_name


#
#
#
#
#

def find_file(data_path, passed_file_prefix, file_type=None, callfrom=None, local_monitoring=None, verbose=True):
    """
    find a file in a directory with a given prefix. The suffix is unknown

    :param data_path:
    :param passed_file_prefix:
    :param file_type:
    :param callfrom:
    :param local_monitoring:
    :param verbose:

    :return:
    """
    proc = "find_file"

    if data_path is None:
        return None

    if not os.path.isdir(data_path):
        if local_monitoring is not None:
            if verbose:
                local_monitoring.to_log_and_console(proc + ": '" + str(data_path) + "' is not a valid directory ?!")
                if callfrom is not None:
                    local_monitoring.to_log_and_console("\t call from '" + str(callfrom) + "'")
            else:
                local_monitoring.to_log(proc + ": '" + str(data_path) + "' is not a valid directory ?!")
                if callfrom is not None:
                    local_monitoring.to_log("\t call from '" + str(callfrom) + "'")
        else:
            print(proc + ": '" + str(data_path) + "' is not a valid directory ?!")
            if callfrom is not None:
                print("\t call from '" + str(callfrom) + "'")
        return None

    if passed_file_prefix is None:
        print(proc + ": file prefix was 'None'?!")
        if callfrom is not None:
            print("\t call from '" + str(callfrom) + "'")
        return None

    # in case the argument was a (relative) path, not a single name
    file_prefix = passed_file_prefix.split(os.path.sep)[-1]

    #
    # if there is any extension, remove if from the file_prefix length
    # recall that the '.' is part of the extension
    #
    extension = None
    if file_type is not None:
        if file_type.lower() == 'lineage':
            extension = get_lineage_extension(file_prefix)
        elif file_type.lower() == 'image':
            extension = get_image_extension(file_prefix)

    if extension is not None:
        length_file_prefix = len(file_prefix) - len(extension)
    else:
        length_file_prefix = len(file_prefix)

    #
    # get all file names beginning by the given prefix followed by '.'
    #
    prefixedfilenames = []
    for f in os.listdir(data_path):
        if len(f) <= length_file_prefix:
            pass
        if f[0:length_file_prefix] == file_prefix[0:length_file_prefix]:
            if extension is None:
                prefixedfilenames.append(f)
            elif f[length_file_prefix] == '.':
                prefixedfilenames.append(f)
    filenames = []
    if file_type is not None:
        if file_type.lower() == 'lineage':
            for f in prefixedfilenames:
                if f[length_file_prefix:] in recognized_lineage_extensions:
                    filenames.append(f)
        elif file_type.lower() == 'image':
            for f in prefixedfilenames:
                if f[length_file_prefix:] in recognized_image_extensions:
                    filenames.append(f)
    else:
        filenames = prefixedfilenames

    if len(filenames) == 0:
        if local_monitoring is not None:
            if verbose:
                local_monitoring.to_log_and_console(proc + ": no file with name '" + str(file_prefix)
                                                    + "' was found in '" + str(data_path) + "'", 4)
            else:
                local_monitoring.to_log(proc + ": no file with name '" + str(file_prefix) + "' was found in '"
                                        + str(data_path) + "'")
        elif verbose is True:
            print(proc + ": no file with name '" + str(file_prefix) + "' was found in '" + str(data_path) + "'")
        return None

    if len(filenames) > 1:
        if local_monitoring is not None:
            if verbose:
                local_monitoring.to_log_and_console("\t " + proc + ": warning")
                local_monitoring.to_log_and_console("\t several files with name '" + str(file_prefix)
                                                    + "' were found in")
                local_monitoring.to_log_and_console("\t    '" + str(data_path) + "'")
                local_monitoring.to_log_and_console("\t    -> "+str(filenames))
                local_monitoring.to_log_and_console("\t returned file is '" + str(filenames[0]) + "'")
            else:
                local_monitoring.to_log("\t " + proc + ": warning")
                local_monitoring.to_log("\t several files with name '" + str(file_prefix) + "' were found in")
                local_monitoring.to_log("\t    '" + str(data_path) + "'")
                local_monitoring.to_log("\t    -> " + str(filenames))
                local_monitoring.to_log("\t returned file is '" + str(filenames[0]) + "'")
        elif verbose is True:
            print(proc + ": several files with name '"
                  + str(file_prefix) + "' were found in '" + str(data_path) + "'")
            print("\t "+str(filenames))
            print("\t returned file is '" + str(filenames[0]) + "'")
        # return None

    return filenames[0]


#
#
#
#
#

def get_file_suffix(experiment, data_path, file_format, flag_time=None):
    """

    :param experiment:
    :param data_path:
    :param file_format:
    :param flag_time:
    :return:
    """

    proc = "get_file_suffix"

    if not os.path.isdir(data_path):
        monitoring.to_log_and_console(proc + ": weird, data path '" + str(data_path) + "' is not a valid directory", 0)
        return None

    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    suffixes = {}
    nimages = 0
    nfiles = 0
    image_suffixes = {}

    if flag_time is not None:
        flag = flag_time
    else:
        flag = "$TIME"

    #
    # get and count suffixes for images
    #
    for current_time in range(first_time_point + experiment.delay_time_point,
                              last_time_point + experiment.delay_time_point + 1, experiment.delta_time_point):

        time_point = experiment.get_time_index(current_time)
        file_prefix = file_format.replace(flag, time_point)
        image_suffixes[current_time] = []
        for f in os.listdir(data_path):
            if len(f) <= len(file_prefix):
                pass
            if f[0:len(file_prefix)] == file_prefix and f[len(file_prefix)] == '.':
                suffix = f[len(file_prefix) + 1:len(f)]
                suffixes[suffix] = suffixes.get(suffix, 0) + 1
                nfiles += 1
                image_suffixes[current_time] += [suffix]
        nimages += 1

    #
    # there is one suffix for all images
    #
    for s, n in suffixes.items():
        if n == nimages:
            return s

    if nfiles < nimages:
        monitoring.to_log_and_console(proc + ": weird, not enough images '" + str(file_format)
                                      + "' were found in '" + str(data_path) + "'", 0)
        monitoring.to_log_and_console("\t found "+str(nfiles)+" images instead of "+str(nimages))
        monitoring.to_log_and_console("\t Exiting.", 0)
        exit(1)

    for s in suffixes:
        if s + ".gz" not in suffixes:
            continue
        if suffixes[s] + suffixes[s + ".gz"] < nimages:
            continue
        #
        # check whether s and s+".gz" cover all images
        #
        found_suffix = True
        for i in image_suffixes:
            if s not in image_suffixes[i] and s + ".gz" not in image_suffixes[i]:
                found_suffix = False
        if found_suffix:
            monitoring.to_log_and_console(proc + ": warning, mixed suffixes '" + str(s) + "' and '" + str(s) + ".gz'"
                                          + " were found in '" + str(data_path) + "'", 2)
            return s

    monitoring.to_log_and_console(proc + ": no common suffix for '" + str(file_format)
                                  + "' was found in '" + str(data_path) + "'", 2)
    monitoring.to_log_and_console("\t time point range was ["+str(first_time_point)+", "+str(last_time_point)+"]")
    return None

