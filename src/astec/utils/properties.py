##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#
##############################################################
#
#
#
##############################################################

import os
import sys

import numpy as np
import math
import copy
from scipy import ndimage as nd
import shutil

from operator import itemgetter

import astec.utils.common as common
import astec.utils.ioproperties as ioproperties
import astec.utils.datadir as datadir

import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.components.spatial_image import SpatialImage
from astec.io.image import imread

#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
# classes
# - computation parameters
#
########################################################################################

class CellPropertiesParameters(common.PrefixedParameter):

    def __init__(self, prefix=None):

        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        self.max_chunks_properties = None
        self.sigma_segmentation_smoothing = None

    def print_parameters(self):
        print("")
        print('#')
        print('# CellPropertiesParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('max_chunks_properties', self.max_chunks_properties)
        self.varprint('sigma_segmentation_smoothing', self.sigma_segmentation_smoothing)

        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# CellPropertiesParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'max_chunks_properties', self.max_chunks_properties)
        self.varwrite(logfile, 'sigma_segmentation_smoothing', self.sigma_segmentation_smoothing)

        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    def update_from_parameters(self, parameters):
        self.max_chunks_properties = self.read_parameter(parameters, 'max_chunks_properties',
                                                         self.max_chunks_properties)
        self.sigma_segmentation_smoothing = self.read_parameter(parameters, 'sigma_segmentation_smoothing',
                                                                self.sigma_segmentation_smoothing)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


########################################################################################
#
# properties computation from a sequence
# calculation is done after sequence intra-registration
#
########################################################################################

def property_computation(experiment, parameters):
    """

    :param experiment:
    :param parameters:
    :return:
    """

    proc = 'property_computation'

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, CellPropertiesParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)
    #
    # get directory name where to find co-registered images of the sequence
    # as well as the common image suffix
    #

    intrareg_path = os.path.join(experiment.intrareg_dir.get_directory(), experiment.post_dir.get_sub_directory())
    #
    # is there a post-segmentation directory in the intra-registration directory ?
    #
    if not os.path.isdir(intrareg_path):
        monitoring.to_log(proc + ": '" + str(intrareg_path) + "' does not exist")
        intrareg_path = os.path.join(experiment.intrareg_dir.get_directory(), experiment.astec_dir.get_sub_directory())
        #
        # if no, is there a segmentation directory in the intra-registration directory ?
        #
        if not os.path.isdir(intrareg_path):
            monitoring.to_log(proc + ": '" + str(intrareg_path) + "' does not exist")
            intrareg_path = experiment.intrareg_dir.get_directory()
            monitoring.to_log_and_console(proc + ": neither POST/ or SEG/ sub-directories in '"
                                          + str(intrareg_path) + "'", 0)
            monitoring.to_log_and_console("Exiting.", 0)
            return None
        else:
            working_dir = experiment.astec_dir
    else:
        working_dir = experiment.post_dir

    monitoring.to_log_and_console("... will compute sequence properties from '" + str(intrareg_path) + "'", 0)

    #
    # build name format for (post-corrected) segmentation images
    #
    name_format = experiment.intrareg_dir.get_file_prefix() + experiment.intrareg_dir.get_file_suffix() + \
                  working_dir.get_file_suffix() + experiment.intrareg_dir.get_time_prefix() + \
                  experiment.get_time_format()

    suffix = common.get_file_suffix(experiment, intrareg_path, name_format, flag_time=experiment.get_time_format())
    if suffix is None:
        monitoring.to_log_and_console(proc + ": no consistent naming was found in '"
                                      + str(intrareg_path) + "'", 1)
        monitoring.to_log_and_console("Exiting.", 0)
        sys.exit(1)
    name_format += "." + str(suffix)
    template_format = os.path.join(intrareg_path, name_format)

    #
    # lineage to be computed
    #
    output_name = experiment.intrareg_dir.get_file_prefix() + experiment.intrareg_dir.get_file_suffix() + \
                  working_dir.get_file_suffix() + "_lineage"
    output_name = os.path.join(intrareg_path, output_name)

    #
    # copy previous properties file
    #
    if os.path.isfile(output_name + ".xml"):
        shutil.copyfile(output_name + ".xml", output_name + ".xml" + ".bak")
    if os.path.isfile(output_name + ".pkl"):
        shutil.copyfile(output_name + ".pkl", output_name + ".pkl" + ".bak")

    previous_lineage_tree = None
    read_output_name = None
    if os.path.isfile(output_name + ".xml"):
        previous_lineage_tree = ioproperties.read_dictionary(output_name + ".xml")
        read_output_name = output_name + ".xml"
    elif os.path.isfile(output_name + ".pkl"):
        previous_lineage_tree = ioproperties.read_dictionary(output_name + ".pkl")
        read_output_name = output_name + ".pkl"

    #
    # if both exists, do nothing
    #
    if os.path.isfile(output_name + ".xml") and os.path.isfile(output_name + ".pkl"):
        if not monitoring.forceResultsToBeBuilt:
            monitoring.to_log_and_console('    xml and pkl files already exist')
            return output_name + ".xml"
        else:
            monitoring.to_log_and_console('    xml and pkl files already exist, but computation is forced')

    #
    #
    #
    first_time_point = experiment.first_time_point + experiment.delay_time_point
    last_time_point = experiment.last_time_point + experiment.delay_time_point

    other_options = None
    if previous_lineage_tree is None or previous_lineage_tree == {}:
        other_options = "-property lineage"
        msg = "    no property file in '" + str(intrareg_path) + "'?! Lineage will be recomputed from images ..."
        monitoring.to_log_and_console(msg)
    elif 'cell_lineage' not in previous_lineage_tree:
        other_options = "-property lineage"
        msg = "    property file '" + str(read_output_name) + "' has no lineage?! Recompute it from images ..."
        monitoring.to_log_and_console(msg)

    cpp_wrapping.cell_properties(template_format, output_name + ".xml", first_time_point, last_time_point,
                                 diagnosis_file=output_name + ".txt", n_processors=parameters.max_chunks_properties,
                                 cell_based_sigma=parameters.sigma_segmentation_smoothing, other_options=other_options,
                                 monitoring=monitoring)

    if previous_lineage_tree is not None:
        lineage_tree = ioproperties.read_dictionary(output_name + ".xml")
        for k in previous_lineage_tree:
            if k not in lineage_tree:
                lineage_tree[k] = previous_lineage_tree[k]
        ioproperties.write_dictionary(output_name + ".xml", lineage_tree)

    monitoring.to_log_and_console("    computation done", 0)
    return output_name + ".xml"


########################################################################################
#
# properties computation from one image
#
########################################################################################

def cell_volume_computation(image):
    """

    :param image:
    :return:
    """
    proc = "cell_volume_computation"

    if type(image) is str:
        readim = imread(image)
    elif type(image) is SpatialImage:
        readim = image
    else:
        monitoring.to_log_and_console(str(proc) + ": unhandled type for image '" + str(type(image)) + "'")
        return

    labels = np.unique(readim)
    volume = nd.sum(np.ones_like(readim), readim, index=np.int16(labels))
    if type(image) is str:
        del readim
    return dict(list(zip(labels, volume)))


def update_cell_volume_properties(lineage, image, current_time=0, time_digits_for_cell_id=4):

    volumes = cell_volume_computation(image)
    volume_key = ioproperties.keydictionary['volume']['output_key']
    # uncomment next line to have a lineage file readable by old post-correction version
    # volume_key = 'volumes_information'
    if lineage is None:
        lineage = {}
    if volume_key not in lineage:
        lineage[volume_key] = {}

    dtmp = {}
    for key, value in volumes.items():
        if int(key) == 1 or int(key) == 0:
            continue
        newkey = current_time * 10 ** time_digits_for_cell_id + int(key)
        dtmp[newkey] = value
    lineage[volume_key].update(dtmp)
    return lineage


########################################################################################
#
# selection on time
#
########################################################################################

def extract_time_interval(properties, mintime=None, maxtime=None, time_digits_for_cell_id=4):
    """

    :param properties:
    :param mintime:
    :param maxtime:
    :param time_digits_for_cell_id:
    :return:
    """
    if mintime is None and maxtime is None:
        return properties

    msg = "extracting properties for interval [" + str(mintime) + ", " + str(maxtime) + "]"
    monitoring.to_log_and_console("... " + msg)

    div = int(10 ** time_digits_for_cell_id)
    output = copy.deepcopy(properties)

    for k in output:
        monitoring.to_log_and_console("     " + str(k))
        if k == 'all_cells':
            localmintime = mintime
            if mintime is not None:
                localmintime = int(mintime)
            localmaxtime = maxtime
            if maxtime is not None:
                localmaxtime = int(maxtime)
            ids = []
            for c in output[k]:
                t = c // div
                if localmintime is not None and t < localmintime:
                    continue
                if localmaxtime is not None and t > localmaxtime:
                    continue
                ids += [c]
            output[k] = ids
        else:
            if isinstance(output[k], dict) is False:
                continue
            ids = list(output[k].keys())
            localmintime = mintime
            if mintime is not None:
                localmintime = int(mintime)
            localmaxtime = maxtime
            if maxtime is not None:
                localmaxtime = int(maxtime)
                if k == 'cell_lineage':
                    localmaxtime -= 1
            for c in ids:
                t = c // div
                if localmintime is not None and t < localmintime:
                    del output[k][c]
                    continue
                if localmaxtime is not None and t > localmaxtime:
                    del output[k][c]
                    continue

    return output


########################################################################################
#
# comparison of two dictionaries
#
########################################################################################

def _intersection_lists(l1, l2, name1, name2, maxtoprint=10):
    """

    :param l1:
    :param l2:
    :param name1:
    :param name2:
    :return:
    """
    intersection = sorted(list(set(l1).intersection(set(l2))))
    difference1 = sorted(list(set(l1).difference(set(l2))))
    difference2 = sorted(list(set(l2).difference(set(l1))))

    monitoring.to_log_and_console("    ... " + str(len(list(l1))) + " key cells are in '" + str(name1) + "'")
    monitoring.to_log_and_console("    ... " + str(len(list(l2))) + " key cells are in '" + str(name2) + "'")
    if len(difference1) > 0:
        monitoring.to_log_and_console("    ... " + str(len(difference1)) + " key cells are in '" + str(name1)
                                      + "' and are not in '" + str(name2) + "'", 1)
        if len(difference1) <= maxtoprint:
            s = repr(difference1)
            monitoring.to_log_and_console("        " + s, 1)

    if len(difference2) > 0:
        monitoring.to_log_and_console("    ... " + str(len(difference2)) + " key cells are not in '" + str(name1)
                                      + "' but are in '" + str(name2) + "'", 1)
        if len(difference2) <= maxtoprint:
            s = repr(difference2)
            monitoring.to_log_and_console("        " + s, 1)

    return intersection


def intersection_cell_keys(e1, e2, name1, name2):
    return _intersection_lists(list(e1.keys()), list(e2.keys()), name1, name2)


def _compare_lineage(d1, d2, name1, name2, description):

    # e1 and e2 are respectively the lineages of name1 and name2
    e1 = d1[description]
    e2 = d2[description]
    msg = "  === " + str(description) + " comparison between " + str(name1) + " and " + str(name2) + " === "
    monitoring.to_log_and_console(msg, 1)

    #
    # mother cell comparison
    #

    mothers1 = set(e1.keys())
    mothers2 = set(e2.keys())
    all_mothers = mothers1.union(mothers2)

    mothers_only_in_1 = []
    mothers_only_in_2 = []
    mothers_different = []
    for m in all_mothers:
        if m in mothers1 and m not in mothers2:
            mothers_only_in_1 += [m]
            continue
        if m not in mothers1 and m in mothers2:
            mothers_only_in_2 += [m]
            continue
        # here the mother is in both lineage
        all_daughters = set(e1[m]).union(set(e2[m]))
        for d in all_daughters:
            if (d in e1[m] and d not in e2[m]) or (d not in e1[m] and d in e2[m]):
                mothers_different += [m]

    monitoring.to_log_and_console("", 1)
    msg = "    = " + str(description) + " comparison between mother cells in " + str(name1) + " and " + str(name2)
    monitoring.to_log_and_console(msg, 1)

    msg = "there are " + str(len(mothers1)) + " mother cells  in '" + str(name1) + "'"
    monitoring.to_log_and_console("    ... " + msg)
    msg = "there are " + str(len(mothers2)) + " mother cells  in '" + str(name2) + "'"
    monitoring.to_log_and_console("    ... " + msg)

    if len(mothers_only_in_1) > 0:
        mothers_only_in_1 = sorted(mothers_only_in_1)
        msg = str(len(mothers_only_in_1)) + " are in '" + str(name1) + "' and not  in '" + str(name2) + "'"
        monitoring.to_log_and_console("     .. " + msg)
        for m in mothers_only_in_1:
            monitoring.to_log_and_console("        " + str(m) + " -> " + str(e1[m]))

    if len(mothers_only_in_2) > 0:
        mothers_only_in_2 = sorted(mothers_only_in_2)
        msg = str(len(mothers_only_in_2)) + " are in '" + str(name2) + "' and not  in '" + str(name1) + "'"
        monitoring.to_log_and_console("     .. " + msg)
        for m in mothers_only_in_2:
            monitoring.to_log_and_console("        " + str(m) + " -> " + str(e2[m]))

    if len(mothers_different) > 0:
        mothers_different = sorted(list(set(mothers_different)))
        msg = str(len(mothers_different)) + " have different daughter cell(s)"
        monitoring.to_log_and_console("     .. " + msg)
        for m in mothers_different:
            msg = str(m) + " -> " + str(e1[m]) + " in '" + str(name1) + "'"
            msg += " and " + str(e2[m]) + " in '" + str(name2) + "'"
            monitoring.to_log_and_console("        " + msg)

    #
    # daughter cell comparison
    #
    reverse_e1 = {v: k for k, values in e1.items() for v in values}
    reverse_e2 = {v: k for k, values in e2.items() for v in values}

    daughters1 = set(reverse_e1.keys())
    daughters2 = set(reverse_e2.keys())
    all_daughters = daughters1.union(daughters2)

    daughters_only_in_1 = []
    daughters_only_in_2 = []
    daughters_different = []
    for d in all_daughters:
        if d in daughters1 and d not in daughters2:
            daughters_only_in_1 += [d]
            continue
        if d not in daughters1 and d in daughters2:
            daughters_only_in_2 += [d]
            continue
        # here the daughter is in both lineage
        if reverse_e1[d] != reverse_e2[d]:
            daughters_different += [d]

    monitoring.to_log_and_console("", 1)
    msg = "    = " + str(description) + " comparison between daughter cells in " + str(name1) + " and " + str(name2)
    monitoring.to_log_and_console(msg, 1)

    if len(daughters_only_in_1) > 0:
        daughters_only_in_1 = sorted(daughters_only_in_1)
        msg = str(len(daughters_only_in_1)) + " are in '" + str(name1) + "' and not  in '" + str(name2) + "'"
        monitoring.to_log_and_console("     .. " + msg)
        for d in daughters_only_in_1:
            monitoring.to_log_and_console("        " + str(reverse_e1[d]) + " -> " + str(d))

    if len(daughters_only_in_2) > 0:
        daughters_only_in_2 = sorted(daughters_only_in_2)
        msg = str(len(daughters_only_in_2)) + " are in '" + str(name2) + "' and not  in '" + str(name1) + "'"
        monitoring.to_log_and_console("     .. " + msg)
        for d in daughters_only_in_2:
            monitoring.to_log_and_console("        " + str(reverse_e2[d]) + " -> " + str(d))

    if len(daughters_different) > 0:
        daughters_different = sorted(list(set(daughters_different)))
        msg = str(len(daughters_different)) + " have different mother cell"
        monitoring.to_log_and_console("     .. " + msg)
        for d in daughters_different:
            msg = str(reverse_e1[d]) + " in '" + str(name1) + "'"
            msg += " and " + str(reverse_e2[d]) + " in '" + str(name2) + "'"
            msg += " -> " + str(d)
            monitoring.to_log_and_console("        " + msg)

    monitoring.to_log_and_console("", 1)
    msg = "    = " + str(description) + " comparison between all cells in " + str(name1) + " and " + str(name2)
    monitoring.to_log_and_console(msg, 1)
    l1 = list(set(e1.keys()).union(set([v for values in list(e1.values()) for v in values])))
    l2 = list(set(e2.keys()).union(set([v for values in list(e2.values()) for v in values])))
    _intersection_lists(l1, l2, name1, name2)

    return


# def _compare_h_min(d1, d2, name1, name2, description):
#     return


def _compare_volume(d1, d2, name1, name2, description):
    """

    :param d1:
    :param d2:
    :param name1:
    :param name2:
    :param description:
    :return:
    """
    #     dictionary de int
    #     cell_volume.590002 = <type 'int'>
    #     590002: 236936

    e1 = d1[description]
    e2 = d2[description]

    msg = "  === " + str(description) + " comparison between " + str(name1) + " and " + str(name2) + " === "
    monitoring.to_log_and_console(msg, 1)

    intersection = intersection_cell_keys(e1, e2, name1, name2)
    if len(intersection) > 0:
        n = 0
        for k in intersection:
            if e1[k] != e2[k]:
                n += 1
        if n > 0:
            monitoring.to_log_and_console("    ... " + str(n) + " cells have different volumes", 1)
            for k in intersection:
                if e1[k] != e2[k]:
                    s = "cell " + str(k) + " has different volumes: "
                    monitoring.to_log_and_console("        " + s, 1)
                    s = str(e1[k]) + " in " + str(name1)
                    monitoring.to_log_and_console("          " + s, 1)
                    s = str(e2[k]) + " in " + str(name2)
                    monitoring.to_log_and_console("          " + s, 1)
    return


# def _compare_sigma(d1, d2, name1, name2, description):
#     return


# def _compare_label_in_time(d1, d2, name1, name2, description):
#     return


def _compare_barycenter(d1, d2, name1, name2, description):
    """

    :param d1:
    :param d2:
    :param name1:
    :param name2:
    :param description:
    :return:
    """

    # 'barycenter': 'cell_barycenter'
    #     dictionary de numpy.ndarray de numpy.float64
    #     cell_barycenter.590002 = <type 'numpy.ndarray'>
    #     590002: array([ 258.41037242,  226.74975943,  303.67167927])

    e1 = d1[description]
    e2 = d2[description]

    monitoring.to_log_and_console("  === " + str(description) + " comparison === ", 1)

    intersection = intersection_cell_keys(e1, e2, name1, name2)

    residual = []

    if len(intersection) > 0:
        for k in intersection:
            residual.append([k, math.sqrt(sum((e1[k] - e2[k]) * (e1[k] - e2[k])))])

    residual = sorted(residual, key=itemgetter(1), reverse=True)
    monitoring.to_log_and_console("    ... largest residual at cell #" + str(residual[0][0]) + " is "
                                  + str(residual[0][1]), 1)
    s = str(e1[residual[0][0]]) + " <-> " + str(e2[residual[0][0]])
    monitoring.to_log_and_console("        " + s, 1)

    # for i in range(min(len(intersection),10)):
    #     print "#" + str(i) + ": " + str(residual[i])

    return


def _are_fates_equal(fate1, fate2):
    if isinstance(fate1, str):
        if isinstance(fate2, str):
            if fate1 != fate2:
                return False
        elif isinstance(fate2, list):
            if fate1 not in fate2 or len(set(fate2)) > 1:
                return False
        else:
            return False
    elif isinstance(fate1, list):
        if isinstance(fate2, str):
            if fate2 not in fate1 or len(set(fate1)) > 1:
                return False
        elif isinstance(fate2, list):
            if set(fate1) != set(fate2):
                return False
        else:
            return False
    else:
        return False
    return True


def _compare_fate(d1, d2, name1, name2, description):

    e1 = d1[description]
    e2 = d2[description]

    msg = "  === " + str(description) + " comparison between " + str(name1) + " and " + str(name2) + " === "
    monitoring.to_log_and_console(msg, 1)

    intersection = intersection_cell_keys(e1, e2, name1, name2)
    if len(intersection) == 0:
        return

    n = 0
    for k in intersection:
        if not _are_fates_equal(e1[k], e2[k]):
            n += 1

    if n == 0:
        return
    monitoring.to_log_and_console("    ... " + str(n) + " cells have different fates", 1)

    return


def _compare_all_cells(d1, d2, name1, name2, description):
    """

    :param d1:
    :param d2:
    :param name1:
    :param name2:
    :param description:
    :return:
    """

    # 'all-cells': 'all_cells'  # liste de toutes les cellules ?
    #     liste de numpy.int64
    #     all_cells = <type 'list'>

    e1 = d1[description]
    e2 = d2[description]

    monitoring.to_log_and_console("  === " + str(description) + " comparison === ", 1)

    difference1 = list(set(e1).difference(set(e2)))
    difference2 = list(set(e2).difference(set(e1)))

    if len(difference1) > 0:
        monitoring.to_log_and_console("    ... cells that are in '" + str(name1) + "' and not in '"
                                      + str(name2) + "'", 1)
        s = repr(difference1)
        monitoring.to_log_and_console("        " + s, 1)

    if len(difference2) > 0:
        monitoring.to_log_and_console("    ... cells that are not in '" + str(name1) + "' but in '"
                                      + str(name2) + "'", 1)
        s = repr(difference2)
        monitoring.to_log_and_console("        " + s, 1)

    return


def _compare_principal_value(d1, d2, name1, name2, description):
    """

    :param d1:
    :param d2:
    :param name1:
    :param name2:
    :param description:
    :return:
    """

    # 'principal-value': 'cell_principal_values'
    #     dictionary de liste de numpy.float64
    #     cell_principal_values.590002 = <type 'list'>
    #     590002: [1526.0489371146978, 230.60881177650205, 91.063513300019849]

    e1 = d1[description]
    e2 = d2[description]

    monitoring.to_log_and_console("  === " + str(description) + " comparison === ", 1)

    intersection = intersection_cell_keys(e1, e2, name1, name2)

    residual = []

    if len(intersection) > 0:
        for k in intersection:
            residual.append([k, max(abs(np.array(e1[k]) - np.array(e2[k])))])

    residual = sorted(residual, key=itemgetter(1), reverse=True)
    monitoring.to_log_and_console("    ... largest residual at cell #" + str(residual[0][0]) + " is "
                                  + str(residual[0][1]), 1)
    s = str(e1[residual[0][0]]) + " <-> " + str(e2[residual[0][0]])
    monitoring.to_log_and_console("        " + s, 1)

    # for i in range(min(len(intersection),10)):
    #     print "#" + str(i) + ": " + str(residual[i])

    return


def _compare_contact(d1, d2, name1, name2, description):
    """

    :param d1:
    :param d2:
    :param name1:
    :param name2:
    :param description:
    :return:
    """
    # 'contact': 'cell_contact_surface',
    #     dictionary de dictionary de int
    #     cell_contact_surface.590002.590019 = <type 'int'>

    e1 = d1[description]
    e2 = d2[description]

    msg = "  === " + str(description) + " comparison between " + str(name1) + " and " + str(name2) + " === "
    monitoring.to_log_and_console(msg, 1)

    intersection = intersection_cell_keys(e1, e2, name1, name2)
    if len(intersection) > 0:
        n = 0
        for k in intersection:
            d = list(set(e1[k].keys()).symmetric_difference(set(e2[k].keys())))
            if len(d) > 0:
                n += 1
        if n > 0:
            monitoring.to_log_and_console("    ... " + str(n) + " cells have different contact surfaces", 1)
            for k in intersection:
                d = list(set(e1[k].keys()).symmetric_difference(set(e2[k].keys())))
                if len(d) > 0:
                    difference1 = list(set(e1[k].keys()).difference(set(e2[k].keys())))
                    difference2 = list(set(e2[k].keys()).difference(set(e1[k].keys())))
                    s = "cell " + str(k) + " has different contact surfaces: "
                    monitoring.to_log_and_console("        " + s, 1)
                    if len(difference1) > 0:
                        s = str(difference1) + " in '" + str(name1) + "' and not in '" + str(name2) + "'"
                        monitoring.to_log_and_console("          " + s, 1)
                    if len(difference2) > 0:
                        s = str(difference2) + " in '" + str(name2) + "' and not in '" + str(name1) + "'"
                        monitoring.to_log_and_console("          " + s, 1)
    return


# def _compare_history(d1, d2, name1, name2, description):
#    return


def _compare_principal_vector(d1, d2, name1, name2, description):
    """

    :param d1:
    :param d2:
    :param name1:
    :param name2:
    :param description:
    :return:
    """
    # 'principal-vector': 'cell_principal_vectors'    # liste de numpy.ndarray
    #     dictionary de liste de numpy.ndarray de numpy.float64
    #     cell_principal_vectors.590002 = <type 'list'>
    #     590002: [array([ 0.17420991, -0.74923203,  0.63898534]),
    #         array([-0.24877611,  0.59437038,  0.7647446 ]),
    #         array([ 0.95276511,  0.29219037,  0.08284582])]

    e1 = d1[description]
    e2 = d2[description]

    monitoring.to_log_and_console("  === " + str(description) + " comparison === ", 1)

    intersection = intersection_cell_keys(e1, e2, name1, name2)

    residual = []

    if len(intersection) > 0:
        for k in intersection:
            residual.append([k, max(math.sqrt(sum((e1[k][0] - e2[k][0]) * (e1[k][0] - e2[k][0]))),
                                    math.sqrt(sum((e1[k][1] - e2[k][1]) * (e1[k][1] - e2[k][1]))),
                                    math.sqrt(sum((e1[k][2] - e2[k][2]) * (e1[k][2] - e2[k][2]))))])

    residual = sorted(residual, key=itemgetter(1), reverse=True)
    monitoring.to_log_and_console("    ... largest residual at cell #" + str(residual[0][0]) + " is "
                                  + str(residual[0][1]), 1)
    s = str(e1[residual[0][0]]) + "\n" + "        " + " <-> " + str(e2[residual[0][0]])
    monitoring.to_log_and_console("        " + s, 1)

    return


def compare_keys(d1, d2, features, name1, name2, print_summary=True):

    unpairedkeys1 = []
    unpairedkeys2 = []
    pairedkeys = []
    unrecognizedkeys1 = []
    unrecognizedkeys2 = []

    for k1 in d1:
        #
        # loop on known dictionary
        #
        recognizedkey = False
        for k in ioproperties.keydictionary:

            if k1 in ioproperties.keydictionary[k]['input_keys']:
                recognizedkey = True
                pairedkey = False
                #
                # got it, try to pair it
                #
                for k2 in d2:
                    if k2 in ioproperties.keydictionary[k]['input_keys']:
                        pairedkey = True
                        pairedkeys.append([k1, k2, k])
                        break
                if pairedkey is False:
                    unpairedkeys1.append(k1)
                break

        if recognizedkey is False:
            unrecognizedkeys1.append(k1)

    for k2 in d2:
        #
        # loop on known dictionary
        #
        recognizedkey = False
        for k in ioproperties.keydictionary:
            if k2 in ioproperties.keydictionary[k]['input_keys']:
                recognizedkey = True
                pairedkey = False
                #
                # got it, try to pair it
                #
                for k1 in d1:
                    if k1 in ioproperties.keydictionary[k]['input_keys']:
                        pairedkey = True
                        # pairedkeys.append([k1,k2])
                        break
                if pairedkey is False:
                    unpairedkeys2.append(k2)
                break

        if recognizedkey is False:
            unrecognizedkeys2.append(k2)

    #
    # first output, compare the dictionaries keys
    #

    local_print_summary = False

    if print_summary is True and (features is None or len(features) == 0):
        local_print_summary = True

    #
    #
    #

    if local_print_summary is True:
        monitoring.to_log_and_console("    found keys", 1)
        if len(pairedkeys) > 0:
            monitoring.to_log_and_console("    ... common keys to '" + str(name1) + "' and '" + str(name2) + "'", 1)
            for p in pairedkeys:
                monitoring.to_log_and_console("        " + str(p[0]) + " <-> " + str(p[1]), 1)
        if len(unpairedkeys1) > 0:
            monitoring.to_log_and_console("    ... keys in '" + str(name1) + "' and not in '" + str(name2) + "'", 1)
            for k in unpairedkeys1:
                monitoring.to_log_and_console("        " + str(k), 1)
        if len(unpairedkeys2) > 0:
            monitoring.to_log_and_console("    ... keys not in '" + str(name1) + "' and in '" + str(name2) + "'", 1)
            for k in unpairedkeys2:
                monitoring.to_log_and_console("        " + str(k), 1)
        if len(unrecognizedkeys1) > 0:
            monitoring.to_log_and_console("    ... keys in '" + str(name1) + "' not recognized", 1)
            for k in unrecognizedkeys1:
                monitoring.to_log_and_console("        " + str(k), 1)
        if len(unrecognizedkeys2) > 0:
            monitoring.to_log_and_console("    ... keys in '" + str(name2) + "' not recognized", 1)
            for k in unrecognizedkeys2:
                monitoring.to_log_and_console("        " + str(k), 1)

    return pairedkeys


def comparison(d1, d2, features, name1, name2):
    """

    :param d1:
    :param d2:
    :param features:
    :param name1:
    :param name2:
    :return:
    """

    monitoring.to_log_and_console("\n", 1)
    monitoring.to_log_and_console("... comparison between '" + str(name1) + "' and '" + str(name2) + "'", 1)

    #
    # 1. find common keys
    #
    pairedkeys = compare_keys(d1, d2, features, name1, name2, print_summary=True)

    #
    # 2. perform a comparison key by key
    #

    if len(pairedkeys) == 0:
        monitoring.to_log_and_console("... no common keys between '" + str(name1) + "' and '" + str(name2) + "'", 1)
        monitoring.to_log_and_console("    comparison is not possible", 1)
        return

    #
    # recall that the dictionary keys are the 'output_key' of the ioproperties.keydictionary
    #

    if features is None or len(features) == 0:
        comparison_keys = [k[2] for k in pairedkeys]
    else:
        comparison_keys = features

    monitoring.to_log_and_console("", 1)

    for f in comparison_keys:
        if f not in ioproperties.keydictionary:
            monitoring.to_log_and_console("    unknown property '" + str(f) + "' for comparison", 1)
            continue

        outk = ioproperties.keydictionary[f]['output_key']

        for i in range(len(pairedkeys)):
            if pairedkeys[i][0] == outk:
                if outk == ioproperties.keydictionary['lineage']['output_key']:
                    _compare_lineage(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['h_min']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['volume']['output_key']:
                    _compare_volume(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['sigma']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['label_in_time']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['barycenter']['output_key']:
                    _compare_barycenter(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['fate']['output_key']:
                    _compare_fate(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['all-cells']['output_key']:
                    _compare_all_cells(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['principal-value']['output_key']:
                    _compare_principal_value(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['name']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['contact']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['history']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['principal-vector']['output_key']:
                    _compare_principal_vector(d1, d2, name1, name2, outk)
                else:
                    monitoring.to_log_and_console("    unknown key '" + str(outk) + "' for comparison", 1)
                break

    return


########################################################################################
#
# utilities for debugging, etc.
#
########################################################################################

def print_keys(d, desc=None):

    monitoring.to_log_and_console("\n", 1)
    if desc is None:
        monitoring.to_log_and_console("... contents", 1)
    else:
        monitoring.to_log_and_console("... contents of '" + str(desc) + "'", 1)

    if type(d) is dict:
        if d == {}:
            monitoring.to_log_and_console("    " + "empty dictionary", 1)
        else:
            monitoring.to_log_and_console("    " + "keys are:", 1)
            for k in d:
                monitoring.to_log_and_console("    " + "- " + str(k), 1)
    else:
        monitoring.to_log_and_console("    " + "input is not a dictionary", 1)

    return


def print_type(d, t=None, desc=None):

    if desc is None:
        desc = ""
    if t is None:
        t = ""

    if type(d) is dict:

        print("type of " + desc + " is " + str(t) + str(type(d)))
        for k in d:
            print_type(d[k], t + str(type(d)) + ".", desc + "." + str(k))

    elif type(d) in (list, np.array, np.ndarray):
        print("type of " + desc + " is " + str(t) + str(type(d)))
        print_type(d[0], t + str(type(d)) + ".", desc + "[0]")

    else:
        print("type of " + desc + " is " + str(t) + str(type(d)))

    return
