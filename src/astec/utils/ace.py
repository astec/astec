
import os
import sys
import operator
import multiprocessing

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.wrapping.cpp_wrapping as cpp_wrapping

from astec.io.image import imread


#
#
#
#
#


monitoring = common.Monitoring()

_instrumented_ = False

########################################################################################
#
# classes
# - computation parameters
#
# ### Membrane reconstruction parameters
# sigma_membrane=0.9: parameter for membranes enhancement filter (before the membranes binarization step)
#     (default, in real coordinates, adapted to images like Patrick/Ralph/Aquila)
#
# hard_thresholding=False (default): this option enables the user to set a "hard threshold"
#     (avoiding the automatic computation of anisotropic thresholds) if set to True.
#     In that case, the hard_threshold parameter is applied on the whole enhanced image
# hard_threshold=1.0 (default): if 'hard_thresholding' is set to True, this is the threshold for the
#     binarization of enhanced membranes image (1.0 : adapted for the time-point t001 of Aquila for example)
#
# manual=False (default): if set to True, this parameter activates the "manual" mode,
#     ie the user fixes the manual parameters for the thresholds computation for membranes binarization
# manual_sigma=7 (default): manual parameter for the initialization of Rayleigh function sigma parameter
#     for the directional histograms fitting for the thresholds computation
# sensitivity=0.99 (default) : sensitivity parameter for axial thresholds computation for membranes binarization,
#     with respect to the following criterion
#     (true positive rate) : threshold = #(membrane class>=threshold)/#(membrane class)
#
# sigma_TV=3.6: parameter of membranes propagation by the tensor voting algorithm
#     (default, real coordinates, adapted to images with a spatial resolution of 0.3um^3)
# sigma_LF=0.9  : parameter for the gaussian blurring of the reconstructed image by the tensor voting method
#     (default, real coordinates)
# sample=0.2 (default) : multiplicative parameter between 0 and 1 that fixes the sampling of voting token
#     from the initial image (1.0 means no resampling) (has an influence on the processing time)
#
# ### Intermediary images keep-or-leave parameters
# keep_membrane=False : if set to True, keeps all the images from membrane enhancement step (extrema and angles images)
#     (is automatically set to True if path_membrane_prefix is provided)
#
########################################################################################


class AceParameters(common.PrefixedParameter):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):
        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        doc = "\n"
        doc += "G(l)ace parameters overview:\n"
        doc += "============================\n"
        doc += "G(l)ace is a membrane-enhancement procedure. It works as follows\n"
        doc += "- an extrema image, corresponding to thethe membrane centers, is\n"
        doc += "  computed, thanks to a hessian based filtering\n"
        doc += "- these extrema are thresholded to get a binary image\n"
        doc += "  Thresholds are automatically computed from histograms of the\n"
        doc += "  extrema image"
        doc += "  - Gace: the threshold is computed globally\n"
        doc += "  - Glace: thresholds are computed on a cell-based manner\n"
        doc += "    One threshold is computed per cell, and the binary sub-images\n"
        doc += "    are fused afterwards. The cells are the cells at t-1 deformed on\n"
        doc += "    t. Works only in the propagation segmentation stage.\n"
        doc += "- the binary image is extended through a tensor voting procedure\n"
        doc += "- this last image is smoothed\n"
        doc += "\n"
        self.doc['ace_overview'] = doc

        #
        # parameters for filtering and membrane extrema extraction
        #
        doc = "\t Sigma of the gaussian used to compute the derivatives and thus\n"
        doc += "\t the extrema image. In real units.\n"
        self.doc['sigma_membrane'] = doc
        self.sigma_membrane = 0.9

        #
        # parameters for binarization
        #
        doc = "\t Possible values are True or False.\n"
        doc += "\t Should not be set to True.\n"
        doc += "\t If True, used the threshold 'hard_thresholding' to\n"
        doc += "\t binarize the extrema image\n"
        self.doc['hard_thresholding'] = doc
        self.hard_thresholding = False

        doc = "\t Threshold value when 'hard_thresholding' is set to True\n"
        self.doc['hard_threshold'] = doc
        self.hard_threshold = 1.0

        doc = "\t Possible values are True or False.\n"
        doc += "\t Should not be set to True.\n"
        doc += "\t If True, allows to tune the parameter 'manual_sigma' to compute \n"
        doc += "\t threshold from the histogram.\n"
        self.doc['manual'] = doc
        self.manual = False

        doc = "\t Axial histograms fitting initialization parameter\n"
        doc += "\t for the computation of membrane image binarization\n"
        doc += "\t Values have to be chosen in [5, 25]\n"
        self.doc['manual_sigma'] = doc
        self.manual_sigma = 15

        doc = "\t  membrane binarization parameter\n"
        self.doc['sensitivity'] = doc
        self.sensitivity = 0.99

        #
        # parameters for tensor voting
        #
        doc = "\t Sigma that defines the voting scale for membrane\n"
        doc += "\t In real units\n"
        self.doc['sigma_TV'] = doc
        self.sigma_TV = 1.08

        doc = "\t Sigma to smooth the image after tensor voting.\n"
        doc += "\t In real units\n"
        self.doc['sigma_LF'] = doc
        self.sigma_LF = 0.9

        doc = "\t fraction of the points used for tensor voting.\n"
        doc += "\t 1.0: all the points are used.\n"
        doc += "\t the more the points, the longest the computation.\n"
        self.doc['sample'] = doc
        self.sample = 0.2

        doc = "\t Random seed to be used when drawing points for the\n"
        doc += "\t tensor voting. Allows to reproduce an experiment when\n"
        doc += "\t 'sample' is less than 1.0. The used random seed can be \n"
        doc += "\t found in the log file.\n"
        self.doc['sample_random_seed'] = doc
        self.sample_random_seed = None

        #
        # parameter for image ouput
        #
        doc = "\t Maximal value for tensor voting output. \n"
        doc += "\t After smoothing by 'sigma_LF', the values are casted into 1 or 2 bytes\n"
        doc += "\t depending on this maximal value. \n"
        self.doc['ace_max_value'] = doc
        self.ace_max_value = 255

        #
        # for cell-based enhancement
        # dilation (in real unit)
        #
        doc = "\t Dilation radius for the cell bounding boxes\n"
        doc += "\t Used to compute local histograms\n"
        self.doc['bounding_box_dilation'] = doc
        self.bounding_box_dilation = 3.6

        #
        #
        #
        doc = "\t Number of processors for parallelization\n"
        self.doc['processors'] = doc
        self.processors = 10

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# AceParameters')
        print('#')
        print("")
        
        common.PrefixedParameter.print_parameters(self)

        for line in self.doc['ace_overview'].splitlines():
            print('# ' + line)

        self.varprint('sigma_membrane', self.sigma_membrane, self.doc['sigma_membrane'])

        self.varprint('hard_thresholding', self.hard_thresholding, self.doc['hard_thresholding'])
        self.varprint('hard_threshold', self.hard_threshold, self.doc['hard_threshold'])

        self.varprint('manual', self.manual, self.doc['manual'])
        self.varprint('manual_sigma', self.manual_sigma, self.doc['manual_sigma'])
        self.varprint('sensitivity', self.sensitivity, self.doc['sensitivity'])

        self.varprint('sigma_TV', self.sigma_TV, self.doc['sigma_TV'])
        self.varprint('sigma_LF', self.sigma_LF, self.doc['sigma_LF'])

        self.varprint('sample', self.sample, self.doc['sample'])
        self.varprint('sample_random_seed', self.sample_random_seed, self.doc['sample_random_seed'])

        self.varprint('ace_max_value', self.ace_max_value, self.doc['ace_max_value'])

        self.varprint('bounding_box_dilation', self.bounding_box_dilation, self.doc['bounding_box_dilation'])
        self.varprint('processors', self.processors, self.doc['processors'])
        print("")
        return

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# AceParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        for line in self.doc['ace_overview'].splitlines():
            logfile.write('# ' + line + '\n')

        self.varwrite(logfile, 'sigma_membrane', self.sigma_membrane, self.doc['sigma_membrane'])

        self.varwrite(logfile, 'hard_thresholding', self.hard_thresholding, self.doc['hard_thresholding'])
        self.varwrite(logfile, 'hard_threshold', self.hard_threshold, self.doc['hard_threshold'])

        self.varwrite(logfile, 'manual', self.manual, self.doc['manual'])
        self.varwrite(logfile, 'manual_sigma', self.manual_sigma, self.doc['manual_sigma'])
        self.varwrite(logfile, 'sensitivity', self.sensitivity, self.doc['sensitivity'])

        self.varwrite(logfile, 'sigma_TV', self.sigma_TV, self.doc['sigma_TV'])
        self.varwrite(logfile, 'sigma_LF', self.sigma_LF, self.doc['sigma_LF'])

        self.varwrite(logfile, 'sample', self.sample, self.doc['sample'])
        self.varwrite(logfile, 'sample_random_seed', self.sample_random_seed, self.doc['sample_random_seed'])

        self.varwrite(logfile, 'ace_max_value', self.ace_max_value, self.doc['ace_max_value'])

        self.varwrite(logfile, 'bounding_box_dilation', self.bounding_box_dilation, self.doc['bounding_box_dilation'])
        self.varwrite(logfile, 'processors', self.processors, self.doc['processors'])
        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):
        self.sigma_membrane = self.read_parameter(parameters, 'sigma_membrane', self.sigma_membrane)

        self.hard_thresholding = self.read_parameter(parameters, 'hard_thresholding', self.hard_thresholding)
        self.hard_threshold = self.read_parameter(parameters, 'hard_threshold', self.hard_threshold)

        self.manual = self.read_parameter(parameters, 'manual', self.manual)
        self.manual_sigma = self.read_parameter(parameters, 'manual_sigma', self.manual_sigma)
        self.sensitivity = self.read_parameter(parameters, 'sensitivity', self.sensitivity)

        self.sigma_TV = self.read_parameter(parameters, 'sigma_TV', self.sigma_TV)
        self.sigma_LF = self.read_parameter(parameters, 'sigma_LF', self.sigma_LF)

        self.sample = self.read_parameter(parameters, 'sample', self.sample)
        self.sample_random_seed = self.read_parameter(parameters, 'sample_random_seed', self.sample_random_seed)

        self.ace_max_value = self.read_parameter(parameters, 'ace_max_value', self.ace_max_value)
        self.ace_max_value = self.read_parameter(parameters, 'enhancement_normalization_max_value', self.ace_max_value)

        self.bounding_box_dilation = self.read_parameter(parameters, 'bounding_box_dilation',
                                                         self.bounding_box_dilation)

        self.processors = self.read_parameter(parameters, 'processors', self.processors)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

    ############################################################
    #
    #
    #
    ############################################################

    def is_equal(self, p):
        if self.sigma_membrane != p.sigma_membrane:
            return False

        if self.hard_thresholding != p.hard_thresholding:
            return False
        if self.hard_threshold != p.hard_threshold:
            return False

        if self.manual != p.manual:
            return False
        if self.manual_sigma != p.manual_sigma:
            return False
        if self.sensitivity != p.sensitivity:
            return False

        if self.sigma_TV != p.sigma_TV:
            return False
        if self.sigma_LF != p.sigma_LF:
            return False
        if self.sample != p.sample:
            return False
        if self.sample_random_seed != p.sample_random_seed:
            return False

        if self.bounding_box_dilation != p.bounding_box_dilation:
            return False

        return True

########################################################################################
#
#
#
########################################################################################
#
# sigma_membrane=0.9, manual=False, manual_sigma=7, sensitivity=0.99, hard_thresholding=False,
# hard_threshold=1.0,
# sigma_TV=3.6, sigma_LF=0.9, sample=0.2,
# keep_membrane=False, keep_hist=False, keep_all=False, verbose=False):
# binary_input = False, path_bin = None,
# path_membrane_prefix = None,
#


def global_membrane_enhancement(path_input, path_output, experiment, binary_input=False,
                                temporary_path=None, parameters=None):
    """
    GACE for Global Automated Cell Extractor
    :param path_input:
    :param path_output:
    :param experiment:
    :param binary_input:
    :param temporary_path: 
    :param parameters: 
    :return: 
    """

    proc = 'global_membrane_enhancement'

    #
    # parameter checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, AceParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    if not os.path.isfile(path_input):
        monitoring.to_log_and_console(proc + ": input image does not exist", 0)
        monitoring.to_log_and_console("\t input image was '" + str(path_input) + "'", 0)
        monitoring.to_log_and_console("Exiting.", 0)
        sys.exit(1)

    #
    # set names
    # path_input is the full input image name
    # - e is the extension, including the '.' (e.g. '.inr')
    # - prefix_name is the basename without extension

    e = common.get_image_extension(path_input)
    b = os.path.basename(path_input)
    prefix_name = b[0:len(b) - len(e)]

    # tmp_prefix_name = temporary_path + os.path.sep + prefix_name

    #
    # if the output is already binary, there is no membrane extraction
    #

    path_tv_input = path_input

    if binary_input is False:

        #
        # get a binary image of membranes
        #
        # 1. surface extrema extraction
        #    it will generate (in the 'temporary_path' directory)
        #    - 'tmp_prefix_name'.ext
        #      the directional extrema image
        #    - 'tmp_prefix_name'.theta
        #    - 'tmp_prefix_name'.phi
        #      the two angle images that give the direction of the vector orthogonal to the membrane
        #
        ext_image = common.find_file(temporary_path, prefix_name + ".ext", file_type='image', callfrom=proc,
                                     local_monitoring=None, verbose=False)
        phi_image = common.find_file(temporary_path, prefix_name + ".phi", file_type='image', callfrom=proc,
                                     local_monitoring=None, verbose=False)
        theta_image = common.find_file(temporary_path, prefix_name + ".theta", file_type='image', callfrom=proc,
                                       local_monitoring=None, verbose=False)
        if ext_image is None or phi_image is None or theta_image is None or monitoring.forceResultsToBeBuilt is True:
            monitoring.to_log_and_console("       membrane extraction of '"
                                          + str(path_input).split(os.path.sep)[-1] + "'", 2)
            #
            # Local enhancement of membranes from 'path_input' image and extraction of the directional maxima
            # (generates the tmp_prefix_name+'.[ext|theta|phi].inr') images
            #
            tmp_prefix_name = os.path.join(temporary_path, prefix_name)
            cpp_wrapping.membrane_extraction(path_input, tmp_prefix_name, scale=parameters.sigma_membrane,
                                             other_options="-extension " + str(experiment.default_image_suffix),
                                             monitoring=monitoring)

        #
        # Membranes binarization
        #
        # 2. threshold
        #    it will generate (in the 'temporary_path' directory)
        #    - 'tmp_prefix_name'.bin
        #      the thresholded directional extrema image
        #    - 'tmp_prefix_name'.hist.txt
        #      a text file describing the image histogram
        #
        ext_image = common.find_file(temporary_path, prefix_name + ".ext", file_type='image', callfrom=proc,
                                     local_monitoring=monitoring)
        if ext_image is None:
            monitoring.to_log_and_console("       image '" + str(prefix_name + ".ext") + "' not found in '"
                                          + str(temporary_path) + "'", 2)
            monitoring.to_log_and_console("\t Exiting.")
            sys.exit(1)
        ext_image = os.path.join(temporary_path, ext_image)
        hist_file = os.path.join(temporary_path, prefix_name + ".histogram.txt")

        bin_image = common.find_file(temporary_path, prefix_name + ".bin", file_type='image', callfrom=proc,
                                     local_monitoring=None,
                                     verbose=False)
        if bin_image is None or monitoring.forceResultsToBeBuilt is True:
            if bin_image is None:
                bin_image = os.path.join(temporary_path, prefix_name + ".bin." + experiment.default_image_suffix)
            if parameters.hard_thresholding is True:
                #
                # hard threshold
                #
                monitoring.to_log_and_console("       membrane hard thresholding of '" +
                                              str(ext_image).split(os.path.sep)[-1] + "'", 2)
                cpp_wrapping.seuillage(path_input=ext_image, path_output=bin_image,
                                       low_threshold=parameters.hard_threshold, monitoring=monitoring)
            else:
                #
                # Anisotropic threshold of membranes (the choice of the sensitivity parameter may be critical)
                # Pay attention, the input name is the prefix name (without '.ext.inr')
                #
                monitoring.to_log_and_console("       membrane anisotropic thresholding of '" +
                                              str(ext_image).split(os.path.sep)[-1] + "'", 2)
                cpp_wrapping.anisotropic_histogram(path_input_extrema=ext_image, path_output_histogram=hist_file,
                                                   path_output=bin_image, manual=parameters.manual,
                                                   manual_sigma=parameters.manual_sigma,
                                                   sensitivity=parameters.sensitivity, monitoring=monitoring)

                if monitoring.keepTemporaryFiles is False:
                    os.remove(hist_file)
        else:
            bin_image = os.path.join(temporary_path, bin_image)
        #
        #
        #
        path_tv_input = bin_image

    #
    # Tensor voting on the image of binarized membranes
    #
    # it will generate (in the 'temporary_path' directory)
    #  - 'tmp_prefix_name'.bin.imvp1
    #  - 'tmp_prefix_name'.bin.imvp2
    #  - 'tmp_prefix_name'.bin.imvp3
    #    the three eigenvalue images generated by the tensor voting
    #  - 'tmp_prefix_name'.bin.tv
    #    the tensor voting scalar response (enhancing the membranes)
    #  - 'tmp_prefix_name'.bin.lf
    #    the previous image after smoothing (if smoothing is required)

    monitoring.to_log_and_console("       tensor voting from '"
                                  + str(path_tv_input).split(os.path.sep)[-1] + "'", 2)

    e = common.get_image_extension(path_tv_input)
    b = os.path.basename(path_tv_input)
    bin_name = b[0:len(b) - len(e)]

    bin_prefix_name = os.path.join(temporary_path, bin_name)

    if not os.path.isfile(path_output) or monitoring.forceResultsToBeBuilt is True:
        cpp_wrapping.tensor_voting_membrane(path_tv_input, bin_prefix_name, path_output,
                                            scale_tensor_voting=parameters.sigma_TV,
                                            sigma_smoothing=parameters.sigma_LF, sample=parameters.sample,
                                            random_seed=parameters.sample_random_seed,
                                            max_value=parameters.ace_max_value, monitoring=monitoring)

    #
    # remove images
    #
    if binary_input is False:
        for suffix in ['.ext', '.phi', '.theta', '.bin']:
            tmp_image = common.find_file(temporary_path, prefix_name + suffix, file_type='image', callfrom=proc,
                                         local_monitoring=None, verbose=False)
            if tmp_image is not None and monitoring.keepTemporaryFiles is False:
                os.remove(os.path.join(temporary_path, tmp_image))
        tmp_image = common.find_file(temporary_path, prefix_name + ".histogram.txt", file_type=None, callfrom=proc,
                                     local_monitoring=None, verbose=False)
        if tmp_image is not None and monitoring.keepTemporaryFiles is False:
            os.remove(os.path.join(temporary_path, tmp_image))

    for suffix in ['.imvp1', '.imvp2', '.imvp3', '.tv', '.lf']:
        tmp_image = common.find_file(temporary_path, bin_name + suffix, file_type='image', callfrom=proc,
                                     local_monitoring=None, verbose=False)
        if tmp_image is not None and monitoring.keepTemporaryFiles is False:
            os.remove(os.path.join(temporary_path, tmp_image))

    return


########################################################################################
#
#
#
########################################################################################


def cell_binarization(parameters_for_parallelism):
    """

    :param parameters_for_parallelism:
    :return:
    """

    label_of_interest, bbox, previous_deformed_segmentation, tmp_prefix_name, parameters, default_image_suffix = \
        parameters_for_parallelism
    label_width = 5

    #
    # the GACE output images are cropped accordingly to the cell (ie label) bounding box
    #

    cellid = str('{:0{width}d}'.format(label_of_interest, width=label_width))
    cell_prefix_name = tmp_prefix_name + "_cell" + cellid

    cell_mask = cell_prefix_name + ".mask" + "." + default_image_suffix

    #
    # defines the dilated cell mask
    #

    cpp_wrapping.crop_image(previous_deformed_segmentation, cell_mask, bbox, monitoring=monitoring)
    cpp_wrapping.seuillage(cell_mask, cell_mask, low_threshold=label_of_interest, high_threshold=label_of_interest,
                           monitoring=monitoring)

    immask = imread(cell_mask)
    voxelsize = immask.voxelsize
    del immask

    #
    # bounding_box_dilation was already used for the bounding box dilation,
    # thus it remains consistent
    # default value for bounding_box_dilation is 3.6 um, thus 12 voxels
    # for an image of resolution 0.3 um
    #

    rx = int(parameters.bounding_box_dilation / voxelsize[0] + 0.5)
    ry = int(parameters.bounding_box_dilation / voxelsize[1] + 0.5)
    rz = int(parameters.bounding_box_dilation / voxelsize[2] + 0.5)
    dilation_radius = max(rx, ry, rz)
    cpp_wrapping.mathematical_morphology(cell_mask, cell_mask, other_options=" -dil -R " + str(dilation_radius),
                                         monitoring=monitoring)

    #
    # threshold extrema
    #

    full_ext = tmp_prefix_name + ".ext" "." + default_image_suffix
    cell_ext = cell_prefix_name + ".ext" + "." + default_image_suffix
    cell_hist = cell_prefix_name + ".hist" + ".txt"
    cell_bin = cell_prefix_name + ".bin" + "." + default_image_suffix

    cpp_wrapping.crop_image(full_ext, cell_ext, bbox, monitoring=monitoring)

    if parameters.hard_thresholding is True:
        #
        # hard threshold
        #
        cpp_wrapping.seuillage(path_input=cell_ext, path_output=cell_bin,
                               low_threshold=parameters.hard_threshold, monitoring=monitoring)
    else:
        full_theta = tmp_prefix_name + ".theta" + "." + default_image_suffix
        full_phi = tmp_prefix_name + ".phi" + "." + default_image_suffix
        cell_theta = cell_prefix_name + ".theta" + "." + default_image_suffix
        cell_phi = cell_prefix_name + ".phi" + "." + default_image_suffix

        cpp_wrapping.crop_image(full_theta, cell_theta, bbox, monitoring=monitoring)
        cpp_wrapping.crop_image(full_phi, cell_phi, bbox, monitoring=monitoring)

        cpp_wrapping.anisotropic_histogram(path_input_extrema=cell_ext, path_output_histogram=cell_hist,
                                           path_output=cell_bin, path_input_mask=cell_mask, manual=parameters.manual,
                                           manual_sigma=parameters.manual_sigma, sensitivity=parameters.sensitivity,
                                           monitoring=monitoring)
        if not _instrumented_:
            os.remove(cell_theta)
            os.remove(cell_phi)
            os.remove(cell_hist)

    if not _instrumented_:
        os.remove(cell_ext)

    cpp_wrapping.logical_operation(cell_mask, cell_bin, cell_bin, other_options="-mask", monitoring=monitoring)

    if not _instrumented_:
        os.remove(cell_mask)

    return label_of_interest, cell_bin


#
#
#


def cell_membrane_enhancement(path_input, previous_deformed_segmentation, path_output, experiment,
                              temporary_path=None, parameters=None):
    """

    :param path_input:
    :param previous_deformed_segmentation:
    :param path_output:
    :param experiment:
    :param temporary_path:
    :param parameters:
    :return:
    """

    proc = "cell_membrane_enhancement"

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, AceParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    #
    # set names
    #

    e = common.get_image_extension(path_input)
    b = os.path.basename(path_input)
    prefix_name = b[0:len(b) - len(e)]

    tmp_prefix_name = os.path.join(temporary_path, prefix_name)

    #
    # first step
    # membrane extrema computation
    #
    #    it will generate (in the 'temporary_path' directory)
    #    - 'tmp_prefix_name'.ext
    #      the directional extrema image
    #    - 'tmp_prefix_name'.theta
    #    - 'tmp_prefix_name'.phi
    #      the two angle images that give the direction of the vector orthogonal to the membrane

    ext_image = common.find_file(temporary_path, prefix_name + '.ext', file_type='image', callfrom=proc,
                                 local_monitoring=None, verbose=False)
    phi_image = common.find_file(temporary_path, prefix_name + '.phi', file_type='image', callfrom=proc,
                                 local_monitoring=None, verbose=False)
    theta_image = common.find_file(temporary_path, prefix_name + '.theta', file_type='image', callfrom=proc,
                                   local_monitoring=None, verbose=False)

    if ext_image is None or phi_image is None or theta_image is None or monitoring.forceResultsToBeBuilt is True:
        monitoring.to_log_and_console("       membrane extraction of '"
                                      + str(path_input).split(os.path.sep)[-1] + "'", 2)
        #
        # Local enhancement of membranes from 'path_input' image and extraction of the directional maxima
        # (generates the tmp_prefix_name+'.[ext|theta|phi].inr') images
        #
        cpp_wrapping.membrane_extraction(path_input, tmp_prefix_name,
                                         scale=parameters.sigma_membrane, monitoring=monitoring)

    #
    # second step
    # cell-based thresholding
    #

    #
    # bounding boxes of all labels
    # bboxes is a list of tuples [volume, xmin, ymin, zmin, xmax, ymax, zmax]
    #
    path_bboxes = common.add_suffix(previous_deformed_segmentation, '_bounding_boxes',
                                    new_dirname=temporary_path, new_extension='txt')
    bboxes = cpp_wrapping.bounding_boxes(previous_deformed_segmentation, path_bboxes=path_bboxes)

    #
    # dilation of bounding boxes
    # default value for bounding_box_dilation is 3.6 um
    # which yield 12 voxels for a image of resolution 0.3 um
    #
    if parameters.bounding_box_dilation > 0:

        imseg = imread(previous_deformed_segmentation)
        voxelsize = imseg.voxelsize
        xdim = imseg.shape[0]
        ydim = imseg.shape[1]
        zdim = imseg.shape[2]
        del imseg

        dx = int(parameters.bounding_box_dilation/voxelsize[0] + 0.5)
        if len(voxelsize) >= 2:
            dy = int(parameters.bounding_box_dilation / voxelsize[1] + 0.5)
        else:
            dy = int(parameters.bounding_box_dilation / voxelsize[0] + 0.5)
        if len(voxelsize) >= 3:
            dz = int(parameters.bounding_box_dilation / voxelsize[2] + 0.5)
        else:
            dz = int(parameters.bounding_box_dilation / voxelsize[0] + 0.5)

        dilation_operator = (0, -dx, -dy, -dz, dx, dy, dz)

        for x, b in bboxes.items():
            b = list(map(operator.add, b, dilation_operator))
            #
            # Note of Gael:
            # the coordinates of the "final" point of the bounding box can "go" out of the original image dimensions
            # without affecting the following of the program
            #
            if b[1] < 0:
                b[1] = 0
            if b[2] < 0:
                b[2] = 0
            if b[3] < 0:
                b[3] = 0
            if b[4] >= xdim:
                b[4] = xdim-1
            if b[5] >= ydim:
                b[5] = ydim-1
            if b[6] >= zdim:
                b[6] = zdim-1
            bboxes[x] = tuple(b)

    #
    # selection of cells to be enhanced
    # Gael choice was to define two parameters:
    # - labels_of_interest that can either be a list of labels, ie [3, 4, 7] or the string 'all'
    # - background that is nothing but [0,1], ie the two possible labels for the backgroung
    #
    # if type(labels_of_interest)==int:
    #     labels_of_interest=[labels_of_interest]
    # if type(labels_of_interest)==str and labels_of_interest=='all':
    #     labels_of_interest=[x for x in bboxes.keys() if not background.count(x)]
    #

    labels_of_interest = [x for x in bboxes if x != 0 and x != 1]

    #
    # parallel cell-based binarization
    #

    monitoring.to_log_and_console(" ", 3)
    monitoring.to_log_and_console("       ----- start: cell-based parallel binarization -----", 3)

    pool = multiprocessing.Pool(processes=parameters.processors)
    mapping = []

    for label_of_interest in labels_of_interest:
        monitoring.to_log_and_console("       membrane binarization of cell '" + str(label_of_interest) + "'", 3)

        parameters_for_parallelism = (label_of_interest, bboxes[label_of_interest],
                                      previous_deformed_segmentation, tmp_prefix_name, parameters,
                                      experiment.default_image_suffix)
        # print str(parameters_for_parallelism)
        mapping.append(parameters_for_parallelism)

    outputs = pool.map(cell_binarization, mapping)
    pool.close()
    pool.terminate()

    monitoring.to_log_and_console("       ----- end:   cell-based parallel binarization -----", 3)
    monitoring.to_log_and_console(" ", 3)

    #
    # gather all cell binary images
    #

    bin_image = tmp_prefix_name + ".bin." + experiment.default_image_suffix
    cpp_wrapping.create_image(bin_image, tmp_prefix_name + ".ext." + experiment.default_image_suffix, "-o 1",
                              monitoring=monitoring)

    for binary_output_cell in outputs:
        cell_label = binary_output_cell[0]
        cell_bin = binary_output_cell[1]
        bbox = bboxes[cell_label]
        cpp_wrapping.patch_logical_operation(cell_bin, bin_image, bin_image, bbox, "-or", monitoring=monitoring)
        if not _instrumented_:
            os.remove(cell_bin)

    #
    # tensor voting
    #

    global_membrane_enhancement(bin_image, path_output, experiment, binary_input=True,
                                temporary_path=temporary_path, parameters=parameters)

    #
    # remove images
    #
    for suffix in ['.ext', '.phi', '.theta', '.bin']:
        tmp_image = common.find_file(temporary_path, prefix_name + suffix, file_type='image', callfrom=proc,
                                     local_monitoring=None, verbose=False)
        if tmp_image is not None and monitoring.keepTemporaryFiles is False:
            os.remove(os.path.join(temporary_path, tmp_image))

    return
